package com.suning.park.dto.dao;

import com.suning.database.mybatis.SuperMapper;
import com.suning.park.dto.model.ParkOrder;
import com.suning.park.dto.model.ParkOrderExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ParkOrderMapper extends SuperMapper {
    int countByExample(ParkOrderExample example);

    int deleteByExample(ParkOrderExample example);

    int deleteByPrimaryKey(String id);

    int insert(ParkOrder record);

    int insertSelective(ParkOrder record);

    List<ParkOrder> selectByExample(ParkOrderExample example);

    ParkOrder selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") ParkOrder record, @Param("example") ParkOrderExample example);

    int updateByExample(@Param("record") ParkOrder record, @Param("example") ParkOrderExample example);

    int updateByPrimaryKeySelective(ParkOrder record);

    int updateByPrimaryKey(ParkOrder record);
}