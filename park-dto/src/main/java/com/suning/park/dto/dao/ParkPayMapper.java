package com.suning.park.dto.dao;

import com.suning.database.mybatis.SuperMapper;
import com.suning.park.dto.model.ParkPay;
import com.suning.park.dto.model.ParkPayExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ParkPayMapper extends SuperMapper {
    int countByExample(ParkPayExample example);

    int deleteByExample(ParkPayExample example);

    int deleteByPrimaryKey(String id);

    int insert(ParkPay record);

    int insertSelective(ParkPay record);

    List<ParkPay> selectByExample(ParkPayExample example);

    ParkPay selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") ParkPay record, @Param("example") ParkPayExample example);

    int updateByExample(@Param("record") ParkPay record, @Param("example") ParkPayExample example);

    int updateByPrimaryKeySelective(ParkPay record);

    int updateByPrimaryKey(ParkPay record);
}