package com.suning.park.dto.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ParkOrderExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Integer limitStart;

    protected Integer limitRows;

    public ParkOrderExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimitStart(Integer limitStart) {
        this.limitStart=limitStart;
    }

    public Integer getLimitStart() {
        return limitStart;
    }

    public void setLimitRows(Integer limitRows) {
        this.limitRows=limitRows;
    }

    public Integer getLimitRows() {
        return limitRows;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("ID like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("ID not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCarNumIsNull() {
            addCriterion("CAR_NUM is null");
            return (Criteria) this;
        }

        public Criteria andCarNumIsNotNull() {
            addCriterion("CAR_NUM is not null");
            return (Criteria) this;
        }

        public Criteria andCarNumEqualTo(String value) {
            addCriterion("CAR_NUM =", value, "carNum");
            return (Criteria) this;
        }

        public Criteria andCarNumNotEqualTo(String value) {
            addCriterion("CAR_NUM <>", value, "carNum");
            return (Criteria) this;
        }

        public Criteria andCarNumGreaterThan(String value) {
            addCriterion("CAR_NUM >", value, "carNum");
            return (Criteria) this;
        }

        public Criteria andCarNumGreaterThanOrEqualTo(String value) {
            addCriterion("CAR_NUM >=", value, "carNum");
            return (Criteria) this;
        }

        public Criteria andCarNumLessThan(String value) {
            addCriterion("CAR_NUM <", value, "carNum");
            return (Criteria) this;
        }

        public Criteria andCarNumLessThanOrEqualTo(String value) {
            addCriterion("CAR_NUM <=", value, "carNum");
            return (Criteria) this;
        }

        public Criteria andCarNumLike(String value) {
            addCriterion("CAR_NUM like", value, "carNum");
            return (Criteria) this;
        }

        public Criteria andCarNumNotLike(String value) {
            addCriterion("CAR_NUM not like", value, "carNum");
            return (Criteria) this;
        }

        public Criteria andCarNumIn(List<String> values) {
            addCriterion("CAR_NUM in", values, "carNum");
            return (Criteria) this;
        }

        public Criteria andCarNumNotIn(List<String> values) {
            addCriterion("CAR_NUM not in", values, "carNum");
            return (Criteria) this;
        }

        public Criteria andCarNumBetween(String value1, String value2) {
            addCriterion("CAR_NUM between", value1, value2, "carNum");
            return (Criteria) this;
        }

        public Criteria andCarNumNotBetween(String value1, String value2) {
            addCriterion("CAR_NUM not between", value1, value2, "carNum");
            return (Criteria) this;
        }

        public Criteria andInTimeIsNull() {
            addCriterion("IN_TIME is null");
            return (Criteria) this;
        }

        public Criteria andInTimeIsNotNull() {
            addCriterion("IN_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andInTimeEqualTo(Date value) {
            addCriterion("IN_TIME =", value, "inTime");
            return (Criteria) this;
        }

        public Criteria andInTimeNotEqualTo(Date value) {
            addCriterion("IN_TIME <>", value, "inTime");
            return (Criteria) this;
        }

        public Criteria andInTimeGreaterThan(Date value) {
            addCriterion("IN_TIME >", value, "inTime");
            return (Criteria) this;
        }

        public Criteria andInTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("IN_TIME >=", value, "inTime");
            return (Criteria) this;
        }

        public Criteria andInTimeLessThan(Date value) {
            addCriterion("IN_TIME <", value, "inTime");
            return (Criteria) this;
        }

        public Criteria andInTimeLessThanOrEqualTo(Date value) {
            addCriterion("IN_TIME <=", value, "inTime");
            return (Criteria) this;
        }

        public Criteria andInTimeIn(List<Date> values) {
            addCriterion("IN_TIME in", values, "inTime");
            return (Criteria) this;
        }

        public Criteria andInTimeNotIn(List<Date> values) {
            addCriterion("IN_TIME not in", values, "inTime");
            return (Criteria) this;
        }

        public Criteria andInTimeBetween(Date value1, Date value2) {
            addCriterion("IN_TIME between", value1, value2, "inTime");
            return (Criteria) this;
        }

        public Criteria andInTimeNotBetween(Date value1, Date value2) {
            addCriterion("IN_TIME not between", value1, value2, "inTime");
            return (Criteria) this;
        }

        public Criteria andParkTimeIsNull() {
            addCriterion("PARK_TIME is null");
            return (Criteria) this;
        }

        public Criteria andParkTimeIsNotNull() {
            addCriterion("PARK_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andParkTimeEqualTo(String value) {
            addCriterion("PARK_TIME =", value, "parkTime");
            return (Criteria) this;
        }

        public Criteria andParkTimeNotEqualTo(String value) {
            addCriterion("PARK_TIME <>", value, "parkTime");
            return (Criteria) this;
        }

        public Criteria andParkTimeGreaterThan(String value) {
            addCriterion("PARK_TIME >", value, "parkTime");
            return (Criteria) this;
        }

        public Criteria andParkTimeGreaterThanOrEqualTo(String value) {
            addCriterion("PARK_TIME >=", value, "parkTime");
            return (Criteria) this;
        }

        public Criteria andParkTimeLessThan(String value) {
            addCriterion("PARK_TIME <", value, "parkTime");
            return (Criteria) this;
        }

        public Criteria andParkTimeLessThanOrEqualTo(String value) {
            addCriterion("PARK_TIME <=", value, "parkTime");
            return (Criteria) this;
        }

        public Criteria andParkTimeLike(String value) {
            addCriterion("PARK_TIME like", value, "parkTime");
            return (Criteria) this;
        }

        public Criteria andParkTimeNotLike(String value) {
            addCriterion("PARK_TIME not like", value, "parkTime");
            return (Criteria) this;
        }

        public Criteria andParkTimeIn(List<String> values) {
            addCriterion("PARK_TIME in", values, "parkTime");
            return (Criteria) this;
        }

        public Criteria andParkTimeNotIn(List<String> values) {
            addCriterion("PARK_TIME not in", values, "parkTime");
            return (Criteria) this;
        }

        public Criteria andParkTimeBetween(String value1, String value2) {
            addCriterion("PARK_TIME between", value1, value2, "parkTime");
            return (Criteria) this;
        }

        public Criteria andParkTimeNotBetween(String value1, String value2) {
            addCriterion("PARK_TIME not between", value1, value2, "parkTime");
            return (Criteria) this;
        }

        public Criteria andRecvChannelIsNull() {
            addCriterion("RECV_CHANNEL is null");
            return (Criteria) this;
        }

        public Criteria andRecvChannelIsNotNull() {
            addCriterion("RECV_CHANNEL is not null");
            return (Criteria) this;
        }

        public Criteria andRecvChannelEqualTo(String value) {
            addCriterion("RECV_CHANNEL =", value, "recvChannel");
            return (Criteria) this;
        }

        public Criteria andRecvChannelNotEqualTo(String value) {
            addCriterion("RECV_CHANNEL <>", value, "recvChannel");
            return (Criteria) this;
        }

        public Criteria andRecvChannelGreaterThan(String value) {
            addCriterion("RECV_CHANNEL >", value, "recvChannel");
            return (Criteria) this;
        }

        public Criteria andRecvChannelGreaterThanOrEqualTo(String value) {
            addCriterion("RECV_CHANNEL >=", value, "recvChannel");
            return (Criteria) this;
        }

        public Criteria andRecvChannelLessThan(String value) {
            addCriterion("RECV_CHANNEL <", value, "recvChannel");
            return (Criteria) this;
        }

        public Criteria andRecvChannelLessThanOrEqualTo(String value) {
            addCriterion("RECV_CHANNEL <=", value, "recvChannel");
            return (Criteria) this;
        }

        public Criteria andRecvChannelLike(String value) {
            addCriterion("RECV_CHANNEL like", value, "recvChannel");
            return (Criteria) this;
        }

        public Criteria andRecvChannelNotLike(String value) {
            addCriterion("RECV_CHANNEL not like", value, "recvChannel");
            return (Criteria) this;
        }

        public Criteria andRecvChannelIn(List<String> values) {
            addCriterion("RECV_CHANNEL in", values, "recvChannel");
            return (Criteria) this;
        }

        public Criteria andRecvChannelNotIn(List<String> values) {
            addCriterion("RECV_CHANNEL not in", values, "recvChannel");
            return (Criteria) this;
        }

        public Criteria andRecvChannelBetween(String value1, String value2) {
            addCriterion("RECV_CHANNEL between", value1, value2, "recvChannel");
            return (Criteria) this;
        }

        public Criteria andRecvChannelNotBetween(String value1, String value2) {
            addCriterion("RECV_CHANNEL not between", value1, value2, "recvChannel");
            return (Criteria) this;
        }

        public Criteria andParkAmountIsNull() {
            addCriterion("PARK_AMOUNT is null");
            return (Criteria) this;
        }

        public Criteria andParkAmountIsNotNull() {
            addCriterion("PARK_AMOUNT is not null");
            return (Criteria) this;
        }

        public Criteria andParkAmountEqualTo(BigDecimal value) {
            addCriterion("PARK_AMOUNT =", value, "parkAmount");
            return (Criteria) this;
        }

        public Criteria andParkAmountNotEqualTo(BigDecimal value) {
            addCriterion("PARK_AMOUNT <>", value, "parkAmount");
            return (Criteria) this;
        }

        public Criteria andParkAmountGreaterThan(BigDecimal value) {
            addCriterion("PARK_AMOUNT >", value, "parkAmount");
            return (Criteria) this;
        }

        public Criteria andParkAmountGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("PARK_AMOUNT >=", value, "parkAmount");
            return (Criteria) this;
        }

        public Criteria andParkAmountLessThan(BigDecimal value) {
            addCriterion("PARK_AMOUNT <", value, "parkAmount");
            return (Criteria) this;
        }

        public Criteria andParkAmountLessThanOrEqualTo(BigDecimal value) {
            addCriterion("PARK_AMOUNT <=", value, "parkAmount");
            return (Criteria) this;
        }

        public Criteria andParkAmountIn(List<BigDecimal> values) {
            addCriterion("PARK_AMOUNT in", values, "parkAmount");
            return (Criteria) this;
        }

        public Criteria andParkAmountNotIn(List<BigDecimal> values) {
            addCriterion("PARK_AMOUNT not in", values, "parkAmount");
            return (Criteria) this;
        }

        public Criteria andParkAmountBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("PARK_AMOUNT between", value1, value2, "parkAmount");
            return (Criteria) this;
        }

        public Criteria andParkAmountNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("PARK_AMOUNT not between", value1, value2, "parkAmount");
            return (Criteria) this;
        }

        public Criteria andUnpaidIsNull() {
            addCriterion("UNPAID is null");
            return (Criteria) this;
        }

        public Criteria andUnpaidIsNotNull() {
            addCriterion("UNPAID is not null");
            return (Criteria) this;
        }

        public Criteria andUnpaidEqualTo(BigDecimal value) {
            addCriterion("UNPAID =", value, "unpaid");
            return (Criteria) this;
        }

        public Criteria andUnpaidNotEqualTo(BigDecimal value) {
            addCriterion("UNPAID <>", value, "unpaid");
            return (Criteria) this;
        }

        public Criteria andUnpaidGreaterThan(BigDecimal value) {
            addCriterion("UNPAID >", value, "unpaid");
            return (Criteria) this;
        }

        public Criteria andUnpaidGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("UNPAID >=", value, "unpaid");
            return (Criteria) this;
        }

        public Criteria andUnpaidLessThan(BigDecimal value) {
            addCriterion("UNPAID <", value, "unpaid");
            return (Criteria) this;
        }

        public Criteria andUnpaidLessThanOrEqualTo(BigDecimal value) {
            addCriterion("UNPAID <=", value, "unpaid");
            return (Criteria) this;
        }

        public Criteria andUnpaidIn(List<BigDecimal> values) {
            addCriterion("UNPAID in", values, "unpaid");
            return (Criteria) this;
        }

        public Criteria andUnpaidNotIn(List<BigDecimal> values) {
            addCriterion("UNPAID not in", values, "unpaid");
            return (Criteria) this;
        }

        public Criteria andUnpaidBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("UNPAID between", value1, value2, "unpaid");
            return (Criteria) this;
        }

        public Criteria andUnpaidNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("UNPAID not between", value1, value2, "unpaid");
            return (Criteria) this;
        }

        public Criteria andPayAmountIsNull() {
            addCriterion("PAY_AMOUNT is null");
            return (Criteria) this;
        }

        public Criteria andPayAmountIsNotNull() {
            addCriterion("PAY_AMOUNT is not null");
            return (Criteria) this;
        }

        public Criteria andPayAmountEqualTo(BigDecimal value) {
            addCriterion("PAY_AMOUNT =", value, "payAmount");
            return (Criteria) this;
        }

        public Criteria andPayAmountNotEqualTo(BigDecimal value) {
            addCriterion("PAY_AMOUNT <>", value, "payAmount");
            return (Criteria) this;
        }

        public Criteria andPayAmountGreaterThan(BigDecimal value) {
            addCriterion("PAY_AMOUNT >", value, "payAmount");
            return (Criteria) this;
        }

        public Criteria andPayAmountGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("PAY_AMOUNT >=", value, "payAmount");
            return (Criteria) this;
        }

        public Criteria andPayAmountLessThan(BigDecimal value) {
            addCriterion("PAY_AMOUNT <", value, "payAmount");
            return (Criteria) this;
        }

        public Criteria andPayAmountLessThanOrEqualTo(BigDecimal value) {
            addCriterion("PAY_AMOUNT <=", value, "payAmount");
            return (Criteria) this;
        }

        public Criteria andPayAmountIn(List<BigDecimal> values) {
            addCriterion("PAY_AMOUNT in", values, "payAmount");
            return (Criteria) this;
        }

        public Criteria andPayAmountNotIn(List<BigDecimal> values) {
            addCriterion("PAY_AMOUNT not in", values, "payAmount");
            return (Criteria) this;
        }

        public Criteria andPayAmountBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("PAY_AMOUNT between", value1, value2, "payAmount");
            return (Criteria) this;
        }

        public Criteria andPayAmountNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("PAY_AMOUNT not between", value1, value2, "payAmount");
            return (Criteria) this;
        }

        public Criteria andOrderTypeIsNull() {
            addCriterion("ORDER_TYPE is null");
            return (Criteria) this;
        }

        public Criteria andOrderTypeIsNotNull() {
            addCriterion("ORDER_TYPE is not null");
            return (Criteria) this;
        }

        public Criteria andOrderTypeEqualTo(String value) {
            addCriterion("ORDER_TYPE =", value, "orderType");
            return (Criteria) this;
        }

        public Criteria andOrderTypeNotEqualTo(String value) {
            addCriterion("ORDER_TYPE <>", value, "orderType");
            return (Criteria) this;
        }

        public Criteria andOrderTypeGreaterThan(String value) {
            addCriterion("ORDER_TYPE >", value, "orderType");
            return (Criteria) this;
        }

        public Criteria andOrderTypeGreaterThanOrEqualTo(String value) {
            addCriterion("ORDER_TYPE >=", value, "orderType");
            return (Criteria) this;
        }

        public Criteria andOrderTypeLessThan(String value) {
            addCriterion("ORDER_TYPE <", value, "orderType");
            return (Criteria) this;
        }

        public Criteria andOrderTypeLessThanOrEqualTo(String value) {
            addCriterion("ORDER_TYPE <=", value, "orderType");
            return (Criteria) this;
        }

        public Criteria andOrderTypeLike(String value) {
            addCriterion("ORDER_TYPE like", value, "orderType");
            return (Criteria) this;
        }

        public Criteria andOrderTypeNotLike(String value) {
            addCriterion("ORDER_TYPE not like", value, "orderType");
            return (Criteria) this;
        }

        public Criteria andOrderTypeIn(List<String> values) {
            addCriterion("ORDER_TYPE in", values, "orderType");
            return (Criteria) this;
        }

        public Criteria andOrderTypeNotIn(List<String> values) {
            addCriterion("ORDER_TYPE not in", values, "orderType");
            return (Criteria) this;
        }

        public Criteria andOrderTypeBetween(String value1, String value2) {
            addCriterion("ORDER_TYPE between", value1, value2, "orderType");
            return (Criteria) this;
        }

        public Criteria andOrderTypeNotBetween(String value1, String value2) {
            addCriterion("ORDER_TYPE not between", value1, value2, "orderType");
            return (Criteria) this;
        }

        public Criteria andYbIdIsNull() {
            addCriterion("YB_ID is null");
            return (Criteria) this;
        }

        public Criteria andYbIdIsNotNull() {
            addCriterion("YB_ID is not null");
            return (Criteria) this;
        }

        public Criteria andYbIdEqualTo(String value) {
            addCriterion("YB_ID =", value, "ybId");
            return (Criteria) this;
        }

        public Criteria andYbIdNotEqualTo(String value) {
            addCriterion("YB_ID <>", value, "ybId");
            return (Criteria) this;
        }

        public Criteria andYbIdGreaterThan(String value) {
            addCriterion("YB_ID >", value, "ybId");
            return (Criteria) this;
        }

        public Criteria andYbIdGreaterThanOrEqualTo(String value) {
            addCriterion("YB_ID >=", value, "ybId");
            return (Criteria) this;
        }

        public Criteria andYbIdLessThan(String value) {
            addCriterion("YB_ID <", value, "ybId");
            return (Criteria) this;
        }

        public Criteria andYbIdLessThanOrEqualTo(String value) {
            addCriterion("YB_ID <=", value, "ybId");
            return (Criteria) this;
        }

        public Criteria andYbIdLike(String value) {
            addCriterion("YB_ID like", value, "ybId");
            return (Criteria) this;
        }

        public Criteria andYbIdNotLike(String value) {
            addCriterion("YB_ID not like", value, "ybId");
            return (Criteria) this;
        }

        public Criteria andYbIdIn(List<String> values) {
            addCriterion("YB_ID in", values, "ybId");
            return (Criteria) this;
        }

        public Criteria andYbIdNotIn(List<String> values) {
            addCriterion("YB_ID not in", values, "ybId");
            return (Criteria) this;
        }

        public Criteria andYbIdBetween(String value1, String value2) {
            addCriterion("YB_ID between", value1, value2, "ybId");
            return (Criteria) this;
        }

        public Criteria andYbIdNotBetween(String value1, String value2) {
            addCriterion("YB_ID not between", value1, value2, "ybId");
            return (Criteria) this;
        }

        public Criteria andSuccessTimeIsNull() {
            addCriterion("SUCCESS_TIME is null");
            return (Criteria) this;
        }

        public Criteria andSuccessTimeIsNotNull() {
            addCriterion("SUCCESS_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andSuccessTimeEqualTo(Date value) {
            addCriterion("SUCCESS_TIME =", value, "successTime");
            return (Criteria) this;
        }

        public Criteria andSuccessTimeNotEqualTo(Date value) {
            addCriterion("SUCCESS_TIME <>", value, "successTime");
            return (Criteria) this;
        }

        public Criteria andSuccessTimeGreaterThan(Date value) {
            addCriterion("SUCCESS_TIME >", value, "successTime");
            return (Criteria) this;
        }

        public Criteria andSuccessTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("SUCCESS_TIME >=", value, "successTime");
            return (Criteria) this;
        }

        public Criteria andSuccessTimeLessThan(Date value) {
            addCriterion("SUCCESS_TIME <", value, "successTime");
            return (Criteria) this;
        }

        public Criteria andSuccessTimeLessThanOrEqualTo(Date value) {
            addCriterion("SUCCESS_TIME <=", value, "successTime");
            return (Criteria) this;
        }

        public Criteria andSuccessTimeIn(List<Date> values) {
            addCriterion("SUCCESS_TIME in", values, "successTime");
            return (Criteria) this;
        }

        public Criteria andSuccessTimeNotIn(List<Date> values) {
            addCriterion("SUCCESS_TIME not in", values, "successTime");
            return (Criteria) this;
        }

        public Criteria andSuccessTimeBetween(Date value1, Date value2) {
            addCriterion("SUCCESS_TIME between", value1, value2, "successTime");
            return (Criteria) this;
        }

        public Criteria andSuccessTimeNotBetween(Date value1, Date value2) {
            addCriterion("SUCCESS_TIME not between", value1, value2, "successTime");
            return (Criteria) this;
        }

        public Criteria andNotifyStateIsNull() {
            addCriterion("NOTIFY_STATE is null");
            return (Criteria) this;
        }

        public Criteria andNotifyStateIsNotNull() {
            addCriterion("NOTIFY_STATE is not null");
            return (Criteria) this;
        }

        public Criteria andNotifyStateEqualTo(String value) {
            addCriterion("NOTIFY_STATE =", value, "notifyState");
            return (Criteria) this;
        }

        public Criteria andNotifyStateNotEqualTo(String value) {
            addCriterion("NOTIFY_STATE <>", value, "notifyState");
            return (Criteria) this;
        }

        public Criteria andNotifyStateGreaterThan(String value) {
            addCriterion("NOTIFY_STATE >", value, "notifyState");
            return (Criteria) this;
        }

        public Criteria andNotifyStateGreaterThanOrEqualTo(String value) {
            addCriterion("NOTIFY_STATE >=", value, "notifyState");
            return (Criteria) this;
        }

        public Criteria andNotifyStateLessThan(String value) {
            addCriterion("NOTIFY_STATE <", value, "notifyState");
            return (Criteria) this;
        }

        public Criteria andNotifyStateLessThanOrEqualTo(String value) {
            addCriterion("NOTIFY_STATE <=", value, "notifyState");
            return (Criteria) this;
        }

        public Criteria andNotifyStateLike(String value) {
            addCriterion("NOTIFY_STATE like", value, "notifyState");
            return (Criteria) this;
        }

        public Criteria andNotifyStateNotLike(String value) {
            addCriterion("NOTIFY_STATE not like", value, "notifyState");
            return (Criteria) this;
        }

        public Criteria andNotifyStateIn(List<String> values) {
            addCriterion("NOTIFY_STATE in", values, "notifyState");
            return (Criteria) this;
        }

        public Criteria andNotifyStateNotIn(List<String> values) {
            addCriterion("NOTIFY_STATE not in", values, "notifyState");
            return (Criteria) this;
        }

        public Criteria andNotifyStateBetween(String value1, String value2) {
            addCriterion("NOTIFY_STATE between", value1, value2, "notifyState");
            return (Criteria) this;
        }

        public Criteria andNotifyStateNotBetween(String value1, String value2) {
            addCriterion("NOTIFY_STATE not between", value1, value2, "notifyState");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("CREATE_TIME is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("CREATE_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("CREATE_TIME =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("CREATE_TIME <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("CREATE_TIME >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("CREATE_TIME >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("CREATE_TIME <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("CREATE_TIME <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("CREATE_TIME in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("CREATE_TIME not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("CREATE_TIME between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("CREATE_TIME not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateIdIsNull() {
            addCriterion("CREATE_ID is null");
            return (Criteria) this;
        }

        public Criteria andCreateIdIsNotNull() {
            addCriterion("CREATE_ID is not null");
            return (Criteria) this;
        }

        public Criteria andCreateIdEqualTo(String value) {
            addCriterion("CREATE_ID =", value, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateIdNotEqualTo(String value) {
            addCriterion("CREATE_ID <>", value, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateIdGreaterThan(String value) {
            addCriterion("CREATE_ID >", value, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateIdGreaterThanOrEqualTo(String value) {
            addCriterion("CREATE_ID >=", value, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateIdLessThan(String value) {
            addCriterion("CREATE_ID <", value, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateIdLessThanOrEqualTo(String value) {
            addCriterion("CREATE_ID <=", value, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateIdLike(String value) {
            addCriterion("CREATE_ID like", value, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateIdNotLike(String value) {
            addCriterion("CREATE_ID not like", value, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateIdIn(List<String> values) {
            addCriterion("CREATE_ID in", values, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateIdNotIn(List<String> values) {
            addCriterion("CREATE_ID not in", values, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateIdBetween(String value1, String value2) {
            addCriterion("CREATE_ID between", value1, value2, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateIdNotBetween(String value1, String value2) {
            addCriterion("CREATE_ID not between", value1, value2, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateNameIsNull() {
            addCriterion("CREATE_NAME is null");
            return (Criteria) this;
        }

        public Criteria andCreateNameIsNotNull() {
            addCriterion("CREATE_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andCreateNameEqualTo(String value) {
            addCriterion("CREATE_NAME =", value, "createName");
            return (Criteria) this;
        }

        public Criteria andCreateNameNotEqualTo(String value) {
            addCriterion("CREATE_NAME <>", value, "createName");
            return (Criteria) this;
        }

        public Criteria andCreateNameGreaterThan(String value) {
            addCriterion("CREATE_NAME >", value, "createName");
            return (Criteria) this;
        }

        public Criteria andCreateNameGreaterThanOrEqualTo(String value) {
            addCriterion("CREATE_NAME >=", value, "createName");
            return (Criteria) this;
        }

        public Criteria andCreateNameLessThan(String value) {
            addCriterion("CREATE_NAME <", value, "createName");
            return (Criteria) this;
        }

        public Criteria andCreateNameLessThanOrEqualTo(String value) {
            addCriterion("CREATE_NAME <=", value, "createName");
            return (Criteria) this;
        }

        public Criteria andCreateNameLike(String value) {
            addCriterion("CREATE_NAME like", value, "createName");
            return (Criteria) this;
        }

        public Criteria andCreateNameNotLike(String value) {
            addCriterion("CREATE_NAME not like", value, "createName");
            return (Criteria) this;
        }

        public Criteria andCreateNameIn(List<String> values) {
            addCriterion("CREATE_NAME in", values, "createName");
            return (Criteria) this;
        }

        public Criteria andCreateNameNotIn(List<String> values) {
            addCriterion("CREATE_NAME not in", values, "createName");
            return (Criteria) this;
        }

        public Criteria andCreateNameBetween(String value1, String value2) {
            addCriterion("CREATE_NAME between", value1, value2, "createName");
            return (Criteria) this;
        }

        public Criteria andCreateNameNotBetween(String value1, String value2) {
            addCriterion("CREATE_NAME not between", value1, value2, "createName");
            return (Criteria) this;
        }

        public Criteria andOperatorIdIsNull() {
            addCriterion("OPERATOR_ID is null");
            return (Criteria) this;
        }

        public Criteria andOperatorIdIsNotNull() {
            addCriterion("OPERATOR_ID is not null");
            return (Criteria) this;
        }

        public Criteria andOperatorIdEqualTo(String value) {
            addCriterion("OPERATOR_ID =", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdNotEqualTo(String value) {
            addCriterion("OPERATOR_ID <>", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdGreaterThan(String value) {
            addCriterion("OPERATOR_ID >", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdGreaterThanOrEqualTo(String value) {
            addCriterion("OPERATOR_ID >=", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdLessThan(String value) {
            addCriterion("OPERATOR_ID <", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdLessThanOrEqualTo(String value) {
            addCriterion("OPERATOR_ID <=", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdLike(String value) {
            addCriterion("OPERATOR_ID like", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdNotLike(String value) {
            addCriterion("OPERATOR_ID not like", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdIn(List<String> values) {
            addCriterion("OPERATOR_ID in", values, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdNotIn(List<String> values) {
            addCriterion("OPERATOR_ID not in", values, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdBetween(String value1, String value2) {
            addCriterion("OPERATOR_ID between", value1, value2, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdNotBetween(String value1, String value2) {
            addCriterion("OPERATOR_ID not between", value1, value2, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorNameIsNull() {
            addCriterion("OPERATOR_NAME is null");
            return (Criteria) this;
        }

        public Criteria andOperatorNameIsNotNull() {
            addCriterion("OPERATOR_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andOperatorNameEqualTo(String value) {
            addCriterion("OPERATOR_NAME =", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameNotEqualTo(String value) {
            addCriterion("OPERATOR_NAME <>", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameGreaterThan(String value) {
            addCriterion("OPERATOR_NAME >", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameGreaterThanOrEqualTo(String value) {
            addCriterion("OPERATOR_NAME >=", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameLessThan(String value) {
            addCriterion("OPERATOR_NAME <", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameLessThanOrEqualTo(String value) {
            addCriterion("OPERATOR_NAME <=", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameLike(String value) {
            addCriterion("OPERATOR_NAME like", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameNotLike(String value) {
            addCriterion("OPERATOR_NAME not like", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameIn(List<String> values) {
            addCriterion("OPERATOR_NAME in", values, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameNotIn(List<String> values) {
            addCriterion("OPERATOR_NAME not in", values, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameBetween(String value1, String value2) {
            addCriterion("OPERATOR_NAME between", value1, value2, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameNotBetween(String value1, String value2) {
            addCriterion("OPERATOR_NAME not between", value1, value2, "operatorName");
            return (Criteria) this;
        }

        public Criteria andLastTimeIsNull() {
            addCriterion("LAST_TIME is null");
            return (Criteria) this;
        }

        public Criteria andLastTimeIsNotNull() {
            addCriterion("LAST_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andLastTimeEqualTo(Date value) {
            addCriterion("LAST_TIME =", value, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeNotEqualTo(Date value) {
            addCriterion("LAST_TIME <>", value, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeGreaterThan(Date value) {
            addCriterion("LAST_TIME >", value, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("LAST_TIME >=", value, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeLessThan(Date value) {
            addCriterion("LAST_TIME <", value, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeLessThanOrEqualTo(Date value) {
            addCriterion("LAST_TIME <=", value, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeIn(List<Date> values) {
            addCriterion("LAST_TIME in", values, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeNotIn(List<Date> values) {
            addCriterion("LAST_TIME not in", values, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeBetween(Date value1, Date value2) {
            addCriterion("LAST_TIME between", value1, value2, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeNotBetween(Date value1, Date value2) {
            addCriterion("LAST_TIME not between", value1, value2, "lastTime");
            return (Criteria) this;
        }

        public Criteria andParkIdIsNull() {
            addCriterion("PARK_ID is null");
            return (Criteria) this;
        }

        public Criteria andParkIdIsNotNull() {
            addCriterion("PARK_ID is not null");
            return (Criteria) this;
        }

        public Criteria andParkIdEqualTo(String value) {
            addCriterion("PARK_ID =", value, "parkId");
            return (Criteria) this;
        }

        public Criteria andParkIdNotEqualTo(String value) {
            addCriterion("PARK_ID <>", value, "parkId");
            return (Criteria) this;
        }

        public Criteria andParkIdGreaterThan(String value) {
            addCriterion("PARK_ID >", value, "parkId");
            return (Criteria) this;
        }

        public Criteria andParkIdGreaterThanOrEqualTo(String value) {
            addCriterion("PARK_ID >=", value, "parkId");
            return (Criteria) this;
        }

        public Criteria andParkIdLessThan(String value) {
            addCriterion("PARK_ID <", value, "parkId");
            return (Criteria) this;
        }

        public Criteria andParkIdLessThanOrEqualTo(String value) {
            addCriterion("PARK_ID <=", value, "parkId");
            return (Criteria) this;
        }

        public Criteria andParkIdLike(String value) {
            addCriterion("PARK_ID like", value, "parkId");
            return (Criteria) this;
        }

        public Criteria andParkIdNotLike(String value) {
            addCriterion("PARK_ID not like", value, "parkId");
            return (Criteria) this;
        }

        public Criteria andParkIdIn(List<String> values) {
            addCriterion("PARK_ID in", values, "parkId");
            return (Criteria) this;
        }

        public Criteria andParkIdNotIn(List<String> values) {
            addCriterion("PARK_ID not in", values, "parkId");
            return (Criteria) this;
        }

        public Criteria andParkIdBetween(String value1, String value2) {
            addCriterion("PARK_ID between", value1, value2, "parkId");
            return (Criteria) this;
        }

        public Criteria andParkIdNotBetween(String value1, String value2) {
            addCriterion("PARK_ID not between", value1, value2, "parkId");
            return (Criteria) this;
        }

        public Criteria andParkOsIdIsNull() {
            addCriterion("PARK_OS_ID is null");
            return (Criteria) this;
        }

        public Criteria andParkOsIdIsNotNull() {
            addCriterion("PARK_OS_ID is not null");
            return (Criteria) this;
        }

        public Criteria andParkOsIdEqualTo(String value) {
            addCriterion("PARK_OS_ID =", value, "parkOsId");
            return (Criteria) this;
        }

        public Criteria andParkOsIdNotEqualTo(String value) {
            addCriterion("PARK_OS_ID <>", value, "parkOsId");
            return (Criteria) this;
        }

        public Criteria andParkOsIdGreaterThan(String value) {
            addCriterion("PARK_OS_ID >", value, "parkOsId");
            return (Criteria) this;
        }

        public Criteria andParkOsIdGreaterThanOrEqualTo(String value) {
            addCriterion("PARK_OS_ID >=", value, "parkOsId");
            return (Criteria) this;
        }

        public Criteria andParkOsIdLessThan(String value) {
            addCriterion("PARK_OS_ID <", value, "parkOsId");
            return (Criteria) this;
        }

        public Criteria andParkOsIdLessThanOrEqualTo(String value) {
            addCriterion("PARK_OS_ID <=", value, "parkOsId");
            return (Criteria) this;
        }

        public Criteria andParkOsIdLike(String value) {
            addCriterion("PARK_OS_ID like", value, "parkOsId");
            return (Criteria) this;
        }

        public Criteria andParkOsIdNotLike(String value) {
            addCriterion("PARK_OS_ID not like", value, "parkOsId");
            return (Criteria) this;
        }

        public Criteria andParkOsIdIn(List<String> values) {
            addCriterion("PARK_OS_ID in", values, "parkOsId");
            return (Criteria) this;
        }

        public Criteria andParkOsIdNotIn(List<String> values) {
            addCriterion("PARK_OS_ID not in", values, "parkOsId");
            return (Criteria) this;
        }

        public Criteria andParkOsIdBetween(String value1, String value2) {
            addCriterion("PARK_OS_ID between", value1, value2, "parkOsId");
            return (Criteria) this;
        }

        public Criteria andParkOsIdNotBetween(String value1, String value2) {
            addCriterion("PARK_OS_ID not between", value1, value2, "parkOsId");
            return (Criteria) this;
        }

        public Criteria andIdLikeInsensitive(String value) {
            addCriterion("upper(ID) like", value.toUpperCase(), "id");
            return (Criteria) this;
        }

        public Criteria andCarNumLikeInsensitive(String value) {
            addCriterion("upper(CAR_NUM) like", value.toUpperCase(), "carNum");
            return (Criteria) this;
        }

        public Criteria andParkTimeLikeInsensitive(String value) {
            addCriterion("upper(PARK_TIME) like", value.toUpperCase(), "parkTime");
            return (Criteria) this;
        }

        public Criteria andRecvChannelLikeInsensitive(String value) {
            addCriterion("upper(RECV_CHANNEL) like", value.toUpperCase(), "recvChannel");
            return (Criteria) this;
        }

        public Criteria andOrderTypeLikeInsensitive(String value) {
            addCriterion("upper(ORDER_TYPE) like", value.toUpperCase(), "orderType");
            return (Criteria) this;
        }

        public Criteria andYbIdLikeInsensitive(String value) {
            addCriterion("upper(YB_ID) like", value.toUpperCase(), "ybId");
            return (Criteria) this;
        }

        public Criteria andNotifyStateLikeInsensitive(String value) {
            addCriterion("upper(NOTIFY_STATE) like", value.toUpperCase(), "notifyState");
            return (Criteria) this;
        }

        public Criteria andCreateIdLikeInsensitive(String value) {
            addCriterion("upper(CREATE_ID) like", value.toUpperCase(), "createId");
            return (Criteria) this;
        }

        public Criteria andCreateNameLikeInsensitive(String value) {
            addCriterion("upper(CREATE_NAME) like", value.toUpperCase(), "createName");
            return (Criteria) this;
        }

        public Criteria andOperatorIdLikeInsensitive(String value) {
            addCriterion("upper(OPERATOR_ID) like", value.toUpperCase(), "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorNameLikeInsensitive(String value) {
            addCriterion("upper(OPERATOR_NAME) like", value.toUpperCase(), "operatorName");
            return (Criteria) this;
        }

        public Criteria andParkIdLikeInsensitive(String value) {
            addCriterion("upper(PARK_ID) like", value.toUpperCase(), "parkId");
            return (Criteria) this;
        }

        public Criteria andParkOsIdLikeInsensitive(String value) {
            addCriterion("upper(PARK_OS_ID) like", value.toUpperCase(), "parkOsId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}