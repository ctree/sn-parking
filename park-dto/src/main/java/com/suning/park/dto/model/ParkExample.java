package com.suning.park.dto.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ParkExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected Integer limitStart;

    protected Integer limitRows;

    public ParkExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimitStart(Integer limitStart) {
        this.limitStart=limitStart;
    }

    public Integer getLimitStart() {
        return limitStart;
    }

    public void setLimitRows(Integer limitRows) {
        this.limitRows=limitRows;
    }

    public Integer getLimitRows() {
        return limitRows;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("ID is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("ID is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("ID =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("ID <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("ID >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("ID >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("ID <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("ID <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("ID like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("ID not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("ID in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("ID not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("ID between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("ID not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andParkNameIsNull() {
            addCriterion("PARK_NAME is null");
            return (Criteria) this;
        }

        public Criteria andParkNameIsNotNull() {
            addCriterion("PARK_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andParkNameEqualTo(String value) {
            addCriterion("PARK_NAME =", value, "parkName");
            return (Criteria) this;
        }

        public Criteria andParkNameNotEqualTo(String value) {
            addCriterion("PARK_NAME <>", value, "parkName");
            return (Criteria) this;
        }

        public Criteria andParkNameGreaterThan(String value) {
            addCriterion("PARK_NAME >", value, "parkName");
            return (Criteria) this;
        }

        public Criteria andParkNameGreaterThanOrEqualTo(String value) {
            addCriterion("PARK_NAME >=", value, "parkName");
            return (Criteria) this;
        }

        public Criteria andParkNameLessThan(String value) {
            addCriterion("PARK_NAME <", value, "parkName");
            return (Criteria) this;
        }

        public Criteria andParkNameLessThanOrEqualTo(String value) {
            addCriterion("PARK_NAME <=", value, "parkName");
            return (Criteria) this;
        }

        public Criteria andParkNameLike(String value) {
            addCriterion("PARK_NAME like", value, "parkName");
            return (Criteria) this;
        }

        public Criteria andParkNameNotLike(String value) {
            addCriterion("PARK_NAME not like", value, "parkName");
            return (Criteria) this;
        }

        public Criteria andParkNameIn(List<String> values) {
            addCriterion("PARK_NAME in", values, "parkName");
            return (Criteria) this;
        }

        public Criteria andParkNameNotIn(List<String> values) {
            addCriterion("PARK_NAME not in", values, "parkName");
            return (Criteria) this;
        }

        public Criteria andParkNameBetween(String value1, String value2) {
            addCriterion("PARK_NAME between", value1, value2, "parkName");
            return (Criteria) this;
        }

        public Criteria andParkNameNotBetween(String value1, String value2) {
            addCriterion("PARK_NAME not between", value1, value2, "parkName");
            return (Criteria) this;
        }

        public Criteria andParkConverPathIsNull() {
            addCriterion("PARK_CONVER_PATH is null");
            return (Criteria) this;
        }

        public Criteria andParkConverPathIsNotNull() {
            addCriterion("PARK_CONVER_PATH is not null");
            return (Criteria) this;
        }

        public Criteria andParkConverPathEqualTo(String value) {
            addCriterion("PARK_CONVER_PATH =", value, "parkConverPath");
            return (Criteria) this;
        }

        public Criteria andParkConverPathNotEqualTo(String value) {
            addCriterion("PARK_CONVER_PATH <>", value, "parkConverPath");
            return (Criteria) this;
        }

        public Criteria andParkConverPathGreaterThan(String value) {
            addCriterion("PARK_CONVER_PATH >", value, "parkConverPath");
            return (Criteria) this;
        }

        public Criteria andParkConverPathGreaterThanOrEqualTo(String value) {
            addCriterion("PARK_CONVER_PATH >=", value, "parkConverPath");
            return (Criteria) this;
        }

        public Criteria andParkConverPathLessThan(String value) {
            addCriterion("PARK_CONVER_PATH <", value, "parkConverPath");
            return (Criteria) this;
        }

        public Criteria andParkConverPathLessThanOrEqualTo(String value) {
            addCriterion("PARK_CONVER_PATH <=", value, "parkConverPath");
            return (Criteria) this;
        }

        public Criteria andParkConverPathLike(String value) {
            addCriterion("PARK_CONVER_PATH like", value, "parkConverPath");
            return (Criteria) this;
        }

        public Criteria andParkConverPathNotLike(String value) {
            addCriterion("PARK_CONVER_PATH not like", value, "parkConverPath");
            return (Criteria) this;
        }

        public Criteria andParkConverPathIn(List<String> values) {
            addCriterion("PARK_CONVER_PATH in", values, "parkConverPath");
            return (Criteria) this;
        }

        public Criteria andParkConverPathNotIn(List<String> values) {
            addCriterion("PARK_CONVER_PATH not in", values, "parkConverPath");
            return (Criteria) this;
        }

        public Criteria andParkConverPathBetween(String value1, String value2) {
            addCriterion("PARK_CONVER_PATH between", value1, value2, "parkConverPath");
            return (Criteria) this;
        }

        public Criteria andParkConverPathNotBetween(String value1, String value2) {
            addCriterion("PARK_CONVER_PATH not between", value1, value2, "parkConverPath");
            return (Criteria) this;
        }

        public Criteria andProvinceNameIsNull() {
            addCriterion("PROVINCE_NAME is null");
            return (Criteria) this;
        }

        public Criteria andProvinceNameIsNotNull() {
            addCriterion("PROVINCE_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andProvinceNameEqualTo(String value) {
            addCriterion("PROVINCE_NAME =", value, "provinceName");
            return (Criteria) this;
        }

        public Criteria andProvinceNameNotEqualTo(String value) {
            addCriterion("PROVINCE_NAME <>", value, "provinceName");
            return (Criteria) this;
        }

        public Criteria andProvinceNameGreaterThan(String value) {
            addCriterion("PROVINCE_NAME >", value, "provinceName");
            return (Criteria) this;
        }

        public Criteria andProvinceNameGreaterThanOrEqualTo(String value) {
            addCriterion("PROVINCE_NAME >=", value, "provinceName");
            return (Criteria) this;
        }

        public Criteria andProvinceNameLessThan(String value) {
            addCriterion("PROVINCE_NAME <", value, "provinceName");
            return (Criteria) this;
        }

        public Criteria andProvinceNameLessThanOrEqualTo(String value) {
            addCriterion("PROVINCE_NAME <=", value, "provinceName");
            return (Criteria) this;
        }

        public Criteria andProvinceNameLike(String value) {
            addCriterion("PROVINCE_NAME like", value, "provinceName");
            return (Criteria) this;
        }

        public Criteria andProvinceNameNotLike(String value) {
            addCriterion("PROVINCE_NAME not like", value, "provinceName");
            return (Criteria) this;
        }

        public Criteria andProvinceNameIn(List<String> values) {
            addCriterion("PROVINCE_NAME in", values, "provinceName");
            return (Criteria) this;
        }

        public Criteria andProvinceNameNotIn(List<String> values) {
            addCriterion("PROVINCE_NAME not in", values, "provinceName");
            return (Criteria) this;
        }

        public Criteria andProvinceNameBetween(String value1, String value2) {
            addCriterion("PROVINCE_NAME between", value1, value2, "provinceName");
            return (Criteria) this;
        }

        public Criteria andProvinceNameNotBetween(String value1, String value2) {
            addCriterion("PROVINCE_NAME not between", value1, value2, "provinceName");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeIsNull() {
            addCriterion("PROVINCE_CODE is null");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeIsNotNull() {
            addCriterion("PROVINCE_CODE is not null");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeEqualTo(String value) {
            addCriterion("PROVINCE_CODE =", value, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeNotEqualTo(String value) {
            addCriterion("PROVINCE_CODE <>", value, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeGreaterThan(String value) {
            addCriterion("PROVINCE_CODE >", value, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeGreaterThanOrEqualTo(String value) {
            addCriterion("PROVINCE_CODE >=", value, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeLessThan(String value) {
            addCriterion("PROVINCE_CODE <", value, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeLessThanOrEqualTo(String value) {
            addCriterion("PROVINCE_CODE <=", value, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeLike(String value) {
            addCriterion("PROVINCE_CODE like", value, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeNotLike(String value) {
            addCriterion("PROVINCE_CODE not like", value, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeIn(List<String> values) {
            addCriterion("PROVINCE_CODE in", values, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeNotIn(List<String> values) {
            addCriterion("PROVINCE_CODE not in", values, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeBetween(String value1, String value2) {
            addCriterion("PROVINCE_CODE between", value1, value2, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeNotBetween(String value1, String value2) {
            addCriterion("PROVINCE_CODE not between", value1, value2, "provinceCode");
            return (Criteria) this;
        }

        public Criteria andCityNameIsNull() {
            addCriterion("CITY_NAME is null");
            return (Criteria) this;
        }

        public Criteria andCityNameIsNotNull() {
            addCriterion("CITY_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andCityNameEqualTo(String value) {
            addCriterion("CITY_NAME =", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameNotEqualTo(String value) {
            addCriterion("CITY_NAME <>", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameGreaterThan(String value) {
            addCriterion("CITY_NAME >", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameGreaterThanOrEqualTo(String value) {
            addCriterion("CITY_NAME >=", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameLessThan(String value) {
            addCriterion("CITY_NAME <", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameLessThanOrEqualTo(String value) {
            addCriterion("CITY_NAME <=", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameLike(String value) {
            addCriterion("CITY_NAME like", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameNotLike(String value) {
            addCriterion("CITY_NAME not like", value, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameIn(List<String> values) {
            addCriterion("CITY_NAME in", values, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameNotIn(List<String> values) {
            addCriterion("CITY_NAME not in", values, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameBetween(String value1, String value2) {
            addCriterion("CITY_NAME between", value1, value2, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityNameNotBetween(String value1, String value2) {
            addCriterion("CITY_NAME not between", value1, value2, "cityName");
            return (Criteria) this;
        }

        public Criteria andCityCodeIsNull() {
            addCriterion("CITY_CODE is null");
            return (Criteria) this;
        }

        public Criteria andCityCodeIsNotNull() {
            addCriterion("CITY_CODE is not null");
            return (Criteria) this;
        }

        public Criteria andCityCodeEqualTo(String value) {
            addCriterion("CITY_CODE =", value, "cityCode");
            return (Criteria) this;
        }

        public Criteria andCityCodeNotEqualTo(String value) {
            addCriterion("CITY_CODE <>", value, "cityCode");
            return (Criteria) this;
        }

        public Criteria andCityCodeGreaterThan(String value) {
            addCriterion("CITY_CODE >", value, "cityCode");
            return (Criteria) this;
        }

        public Criteria andCityCodeGreaterThanOrEqualTo(String value) {
            addCriterion("CITY_CODE >=", value, "cityCode");
            return (Criteria) this;
        }

        public Criteria andCityCodeLessThan(String value) {
            addCriterion("CITY_CODE <", value, "cityCode");
            return (Criteria) this;
        }

        public Criteria andCityCodeLessThanOrEqualTo(String value) {
            addCriterion("CITY_CODE <=", value, "cityCode");
            return (Criteria) this;
        }

        public Criteria andCityCodeLike(String value) {
            addCriterion("CITY_CODE like", value, "cityCode");
            return (Criteria) this;
        }

        public Criteria andCityCodeNotLike(String value) {
            addCriterion("CITY_CODE not like", value, "cityCode");
            return (Criteria) this;
        }

        public Criteria andCityCodeIn(List<String> values) {
            addCriterion("CITY_CODE in", values, "cityCode");
            return (Criteria) this;
        }

        public Criteria andCityCodeNotIn(List<String> values) {
            addCriterion("CITY_CODE not in", values, "cityCode");
            return (Criteria) this;
        }

        public Criteria andCityCodeBetween(String value1, String value2) {
            addCriterion("CITY_CODE between", value1, value2, "cityCode");
            return (Criteria) this;
        }

        public Criteria andCityCodeNotBetween(String value1, String value2) {
            addCriterion("CITY_CODE not between", value1, value2, "cityCode");
            return (Criteria) this;
        }

        public Criteria andIntegralBodyTypeIsNull() {
            addCriterion("INTEGRAL_BODY_TYPE is null");
            return (Criteria) this;
        }

        public Criteria andIntegralBodyTypeIsNotNull() {
            addCriterion("INTEGRAL_BODY_TYPE is not null");
            return (Criteria) this;
        }

        public Criteria andIntegralBodyTypeEqualTo(String value) {
            addCriterion("INTEGRAL_BODY_TYPE =", value, "integralBodyType");
            return (Criteria) this;
        }

        public Criteria andIntegralBodyTypeNotEqualTo(String value) {
            addCriterion("INTEGRAL_BODY_TYPE <>", value, "integralBodyType");
            return (Criteria) this;
        }

        public Criteria andIntegralBodyTypeGreaterThan(String value) {
            addCriterion("INTEGRAL_BODY_TYPE >", value, "integralBodyType");
            return (Criteria) this;
        }

        public Criteria andIntegralBodyTypeGreaterThanOrEqualTo(String value) {
            addCriterion("INTEGRAL_BODY_TYPE >=", value, "integralBodyType");
            return (Criteria) this;
        }

        public Criteria andIntegralBodyTypeLessThan(String value) {
            addCriterion("INTEGRAL_BODY_TYPE <", value, "integralBodyType");
            return (Criteria) this;
        }

        public Criteria andIntegralBodyTypeLessThanOrEqualTo(String value) {
            addCriterion("INTEGRAL_BODY_TYPE <=", value, "integralBodyType");
            return (Criteria) this;
        }

        public Criteria andIntegralBodyTypeLike(String value) {
            addCriterion("INTEGRAL_BODY_TYPE like", value, "integralBodyType");
            return (Criteria) this;
        }

        public Criteria andIntegralBodyTypeNotLike(String value) {
            addCriterion("INTEGRAL_BODY_TYPE not like", value, "integralBodyType");
            return (Criteria) this;
        }

        public Criteria andIntegralBodyTypeIn(List<String> values) {
            addCriterion("INTEGRAL_BODY_TYPE in", values, "integralBodyType");
            return (Criteria) this;
        }

        public Criteria andIntegralBodyTypeNotIn(List<String> values) {
            addCriterion("INTEGRAL_BODY_TYPE not in", values, "integralBodyType");
            return (Criteria) this;
        }

        public Criteria andIntegralBodyTypeBetween(String value1, String value2) {
            addCriterion("INTEGRAL_BODY_TYPE between", value1, value2, "integralBodyType");
            return (Criteria) this;
        }

        public Criteria andIntegralBodyTypeNotBetween(String value1, String value2) {
            addCriterion("INTEGRAL_BODY_TYPE not between", value1, value2, "integralBodyType");
            return (Criteria) this;
        }

        public Criteria andIntegralBodyCodeIsNull() {
            addCriterion("INTEGRAL_BODY_CODE is null");
            return (Criteria) this;
        }

        public Criteria andIntegralBodyCodeIsNotNull() {
            addCriterion("INTEGRAL_BODY_CODE is not null");
            return (Criteria) this;
        }

        public Criteria andIntegralBodyCodeEqualTo(String value) {
            addCriterion("INTEGRAL_BODY_CODE =", value, "integralBodyCode");
            return (Criteria) this;
        }

        public Criteria andIntegralBodyCodeNotEqualTo(String value) {
            addCriterion("INTEGRAL_BODY_CODE <>", value, "integralBodyCode");
            return (Criteria) this;
        }

        public Criteria andIntegralBodyCodeGreaterThan(String value) {
            addCriterion("INTEGRAL_BODY_CODE >", value, "integralBodyCode");
            return (Criteria) this;
        }

        public Criteria andIntegralBodyCodeGreaterThanOrEqualTo(String value) {
            addCriterion("INTEGRAL_BODY_CODE >=", value, "integralBodyCode");
            return (Criteria) this;
        }

        public Criteria andIntegralBodyCodeLessThan(String value) {
            addCriterion("INTEGRAL_BODY_CODE <", value, "integralBodyCode");
            return (Criteria) this;
        }

        public Criteria andIntegralBodyCodeLessThanOrEqualTo(String value) {
            addCriterion("INTEGRAL_BODY_CODE <=", value, "integralBodyCode");
            return (Criteria) this;
        }

        public Criteria andIntegralBodyCodeLike(String value) {
            addCriterion("INTEGRAL_BODY_CODE like", value, "integralBodyCode");
            return (Criteria) this;
        }

        public Criteria andIntegralBodyCodeNotLike(String value) {
            addCriterion("INTEGRAL_BODY_CODE not like", value, "integralBodyCode");
            return (Criteria) this;
        }

        public Criteria andIntegralBodyCodeIn(List<String> values) {
            addCriterion("INTEGRAL_BODY_CODE in", values, "integralBodyCode");
            return (Criteria) this;
        }

        public Criteria andIntegralBodyCodeNotIn(List<String> values) {
            addCriterion("INTEGRAL_BODY_CODE not in", values, "integralBodyCode");
            return (Criteria) this;
        }

        public Criteria andIntegralBodyCodeBetween(String value1, String value2) {
            addCriterion("INTEGRAL_BODY_CODE between", value1, value2, "integralBodyCode");
            return (Criteria) this;
        }

        public Criteria andIntegralBodyCodeNotBetween(String value1, String value2) {
            addCriterion("INTEGRAL_BODY_CODE not between", value1, value2, "integralBodyCode");
            return (Criteria) this;
        }

        public Criteria andPropertyIsNull() {
            addCriterion("PROPERTY is null");
            return (Criteria) this;
        }

        public Criteria andPropertyIsNotNull() {
            addCriterion("PROPERTY is not null");
            return (Criteria) this;
        }

        public Criteria andPropertyEqualTo(String value) {
            addCriterion("PROPERTY =", value, "property");
            return (Criteria) this;
        }

        public Criteria andPropertyNotEqualTo(String value) {
            addCriterion("PROPERTY <>", value, "property");
            return (Criteria) this;
        }

        public Criteria andPropertyGreaterThan(String value) {
            addCriterion("PROPERTY >", value, "property");
            return (Criteria) this;
        }

        public Criteria andPropertyGreaterThanOrEqualTo(String value) {
            addCriterion("PROPERTY >=", value, "property");
            return (Criteria) this;
        }

        public Criteria andPropertyLessThan(String value) {
            addCriterion("PROPERTY <", value, "property");
            return (Criteria) this;
        }

        public Criteria andPropertyLessThanOrEqualTo(String value) {
            addCriterion("PROPERTY <=", value, "property");
            return (Criteria) this;
        }

        public Criteria andPropertyLike(String value) {
            addCriterion("PROPERTY like", value, "property");
            return (Criteria) this;
        }

        public Criteria andPropertyNotLike(String value) {
            addCriterion("PROPERTY not like", value, "property");
            return (Criteria) this;
        }

        public Criteria andPropertyIn(List<String> values) {
            addCriterion("PROPERTY in", values, "property");
            return (Criteria) this;
        }

        public Criteria andPropertyNotIn(List<String> values) {
            addCriterion("PROPERTY not in", values, "property");
            return (Criteria) this;
        }

        public Criteria andPropertyBetween(String value1, String value2) {
            addCriterion("PROPERTY between", value1, value2, "property");
            return (Criteria) this;
        }

        public Criteria andPropertyNotBetween(String value1, String value2) {
            addCriterion("PROPERTY not between", value1, value2, "property");
            return (Criteria) this;
        }

        public Criteria andParkTypeIsNull() {
            addCriterion("PARK_TYPE is null");
            return (Criteria) this;
        }

        public Criteria andParkTypeIsNotNull() {
            addCriterion("PARK_TYPE is not null");
            return (Criteria) this;
        }

        public Criteria andParkTypeEqualTo(String value) {
            addCriterion("PARK_TYPE =", value, "parkType");
            return (Criteria) this;
        }

        public Criteria andParkTypeNotEqualTo(String value) {
            addCriterion("PARK_TYPE <>", value, "parkType");
            return (Criteria) this;
        }

        public Criteria andParkTypeGreaterThan(String value) {
            addCriterion("PARK_TYPE >", value, "parkType");
            return (Criteria) this;
        }

        public Criteria andParkTypeGreaterThanOrEqualTo(String value) {
            addCriterion("PARK_TYPE >=", value, "parkType");
            return (Criteria) this;
        }

        public Criteria andParkTypeLessThan(String value) {
            addCriterion("PARK_TYPE <", value, "parkType");
            return (Criteria) this;
        }

        public Criteria andParkTypeLessThanOrEqualTo(String value) {
            addCriterion("PARK_TYPE <=", value, "parkType");
            return (Criteria) this;
        }

        public Criteria andParkTypeLike(String value) {
            addCriterion("PARK_TYPE like", value, "parkType");
            return (Criteria) this;
        }

        public Criteria andParkTypeNotLike(String value) {
            addCriterion("PARK_TYPE not like", value, "parkType");
            return (Criteria) this;
        }

        public Criteria andParkTypeIn(List<String> values) {
            addCriterion("PARK_TYPE in", values, "parkType");
            return (Criteria) this;
        }

        public Criteria andParkTypeNotIn(List<String> values) {
            addCriterion("PARK_TYPE not in", values, "parkType");
            return (Criteria) this;
        }

        public Criteria andParkTypeBetween(String value1, String value2) {
            addCriterion("PARK_TYPE between", value1, value2, "parkType");
            return (Criteria) this;
        }

        public Criteria andParkTypeNotBetween(String value1, String value2) {
            addCriterion("PARK_TYPE not between", value1, value2, "parkType");
            return (Criteria) this;
        }

        public Criteria andBusinessEntityIsNull() {
            addCriterion("BUSINESS_ENTITY is null");
            return (Criteria) this;
        }

        public Criteria andBusinessEntityIsNotNull() {
            addCriterion("BUSINESS_ENTITY is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessEntityEqualTo(String value) {
            addCriterion("BUSINESS_ENTITY =", value, "businessEntity");
            return (Criteria) this;
        }

        public Criteria andBusinessEntityNotEqualTo(String value) {
            addCriterion("BUSINESS_ENTITY <>", value, "businessEntity");
            return (Criteria) this;
        }

        public Criteria andBusinessEntityGreaterThan(String value) {
            addCriterion("BUSINESS_ENTITY >", value, "businessEntity");
            return (Criteria) this;
        }

        public Criteria andBusinessEntityGreaterThanOrEqualTo(String value) {
            addCriterion("BUSINESS_ENTITY >=", value, "businessEntity");
            return (Criteria) this;
        }

        public Criteria andBusinessEntityLessThan(String value) {
            addCriterion("BUSINESS_ENTITY <", value, "businessEntity");
            return (Criteria) this;
        }

        public Criteria andBusinessEntityLessThanOrEqualTo(String value) {
            addCriterion("BUSINESS_ENTITY <=", value, "businessEntity");
            return (Criteria) this;
        }

        public Criteria andBusinessEntityLike(String value) {
            addCriterion("BUSINESS_ENTITY like", value, "businessEntity");
            return (Criteria) this;
        }

        public Criteria andBusinessEntityNotLike(String value) {
            addCriterion("BUSINESS_ENTITY not like", value, "businessEntity");
            return (Criteria) this;
        }

        public Criteria andBusinessEntityIn(List<String> values) {
            addCriterion("BUSINESS_ENTITY in", values, "businessEntity");
            return (Criteria) this;
        }

        public Criteria andBusinessEntityNotIn(List<String> values) {
            addCriterion("BUSINESS_ENTITY not in", values, "businessEntity");
            return (Criteria) this;
        }

        public Criteria andBusinessEntityBetween(String value1, String value2) {
            addCriterion("BUSINESS_ENTITY between", value1, value2, "businessEntity");
            return (Criteria) this;
        }

        public Criteria andBusinessEntityNotBetween(String value1, String value2) {
            addCriterion("BUSINESS_ENTITY not between", value1, value2, "businessEntity");
            return (Criteria) this;
        }

        public Criteria andCompanyNoIsNull() {
            addCriterion("COMPANY_NO is null");
            return (Criteria) this;
        }

        public Criteria andCompanyNoIsNotNull() {
            addCriterion("COMPANY_NO is not null");
            return (Criteria) this;
        }

        public Criteria andCompanyNoEqualTo(String value) {
            addCriterion("COMPANY_NO =", value, "companyNo");
            return (Criteria) this;
        }

        public Criteria andCompanyNoNotEqualTo(String value) {
            addCriterion("COMPANY_NO <>", value, "companyNo");
            return (Criteria) this;
        }

        public Criteria andCompanyNoGreaterThan(String value) {
            addCriterion("COMPANY_NO >", value, "companyNo");
            return (Criteria) this;
        }

        public Criteria andCompanyNoGreaterThanOrEqualTo(String value) {
            addCriterion("COMPANY_NO >=", value, "companyNo");
            return (Criteria) this;
        }

        public Criteria andCompanyNoLessThan(String value) {
            addCriterion("COMPANY_NO <", value, "companyNo");
            return (Criteria) this;
        }

        public Criteria andCompanyNoLessThanOrEqualTo(String value) {
            addCriterion("COMPANY_NO <=", value, "companyNo");
            return (Criteria) this;
        }

        public Criteria andCompanyNoLike(String value) {
            addCriterion("COMPANY_NO like", value, "companyNo");
            return (Criteria) this;
        }

        public Criteria andCompanyNoNotLike(String value) {
            addCriterion("COMPANY_NO not like", value, "companyNo");
            return (Criteria) this;
        }

        public Criteria andCompanyNoIn(List<String> values) {
            addCriterion("COMPANY_NO in", values, "companyNo");
            return (Criteria) this;
        }

        public Criteria andCompanyNoNotIn(List<String> values) {
            addCriterion("COMPANY_NO not in", values, "companyNo");
            return (Criteria) this;
        }

        public Criteria andCompanyNoBetween(String value1, String value2) {
            addCriterion("COMPANY_NO between", value1, value2, "companyNo");
            return (Criteria) this;
        }

        public Criteria andCompanyNoNotBetween(String value1, String value2) {
            addCriterion("COMPANY_NO not between", value1, value2, "companyNo");
            return (Criteria) this;
        }

        public Criteria andCompanyNameIsNull() {
            addCriterion("COMPANY_NAME is null");
            return (Criteria) this;
        }

        public Criteria andCompanyNameIsNotNull() {
            addCriterion("COMPANY_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andCompanyNameEqualTo(String value) {
            addCriterion("COMPANY_NAME =", value, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameNotEqualTo(String value) {
            addCriterion("COMPANY_NAME <>", value, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameGreaterThan(String value) {
            addCriterion("COMPANY_NAME >", value, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameGreaterThanOrEqualTo(String value) {
            addCriterion("COMPANY_NAME >=", value, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameLessThan(String value) {
            addCriterion("COMPANY_NAME <", value, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameLessThanOrEqualTo(String value) {
            addCriterion("COMPANY_NAME <=", value, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameLike(String value) {
            addCriterion("COMPANY_NAME like", value, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameNotLike(String value) {
            addCriterion("COMPANY_NAME not like", value, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameIn(List<String> values) {
            addCriterion("COMPANY_NAME in", values, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameNotIn(List<String> values) {
            addCriterion("COMPANY_NAME not in", values, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameBetween(String value1, String value2) {
            addCriterion("COMPANY_NAME between", value1, value2, "companyName");
            return (Criteria) this;
        }

        public Criteria andCompanyNameNotBetween(String value1, String value2) {
            addCriterion("COMPANY_NAME not between", value1, value2, "companyName");
            return (Criteria) this;
        }

        public Criteria andProjectNoIsNull() {
            addCriterion("PROJECT_NO is null");
            return (Criteria) this;
        }

        public Criteria andProjectNoIsNotNull() {
            addCriterion("PROJECT_NO is not null");
            return (Criteria) this;
        }

        public Criteria andProjectNoEqualTo(String value) {
            addCriterion("PROJECT_NO =", value, "projectNo");
            return (Criteria) this;
        }

        public Criteria andProjectNoNotEqualTo(String value) {
            addCriterion("PROJECT_NO <>", value, "projectNo");
            return (Criteria) this;
        }

        public Criteria andProjectNoGreaterThan(String value) {
            addCriterion("PROJECT_NO >", value, "projectNo");
            return (Criteria) this;
        }

        public Criteria andProjectNoGreaterThanOrEqualTo(String value) {
            addCriterion("PROJECT_NO >=", value, "projectNo");
            return (Criteria) this;
        }

        public Criteria andProjectNoLessThan(String value) {
            addCriterion("PROJECT_NO <", value, "projectNo");
            return (Criteria) this;
        }

        public Criteria andProjectNoLessThanOrEqualTo(String value) {
            addCriterion("PROJECT_NO <=", value, "projectNo");
            return (Criteria) this;
        }

        public Criteria andProjectNoLike(String value) {
            addCriterion("PROJECT_NO like", value, "projectNo");
            return (Criteria) this;
        }

        public Criteria andProjectNoNotLike(String value) {
            addCriterion("PROJECT_NO not like", value, "projectNo");
            return (Criteria) this;
        }

        public Criteria andProjectNoIn(List<String> values) {
            addCriterion("PROJECT_NO in", values, "projectNo");
            return (Criteria) this;
        }

        public Criteria andProjectNoNotIn(List<String> values) {
            addCriterion("PROJECT_NO not in", values, "projectNo");
            return (Criteria) this;
        }

        public Criteria andProjectNoBetween(String value1, String value2) {
            addCriterion("PROJECT_NO between", value1, value2, "projectNo");
            return (Criteria) this;
        }

        public Criteria andProjectNoNotBetween(String value1, String value2) {
            addCriterion("PROJECT_NO not between", value1, value2, "projectNo");
            return (Criteria) this;
        }

        public Criteria andProjectNameIsNull() {
            addCriterion("PROJECT_NAME is null");
            return (Criteria) this;
        }

        public Criteria andProjectNameIsNotNull() {
            addCriterion("PROJECT_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andProjectNameEqualTo(String value) {
            addCriterion("PROJECT_NAME =", value, "projectName");
            return (Criteria) this;
        }

        public Criteria andProjectNameNotEqualTo(String value) {
            addCriterion("PROJECT_NAME <>", value, "projectName");
            return (Criteria) this;
        }

        public Criteria andProjectNameGreaterThan(String value) {
            addCriterion("PROJECT_NAME >", value, "projectName");
            return (Criteria) this;
        }

        public Criteria andProjectNameGreaterThanOrEqualTo(String value) {
            addCriterion("PROJECT_NAME >=", value, "projectName");
            return (Criteria) this;
        }

        public Criteria andProjectNameLessThan(String value) {
            addCriterion("PROJECT_NAME <", value, "projectName");
            return (Criteria) this;
        }

        public Criteria andProjectNameLessThanOrEqualTo(String value) {
            addCriterion("PROJECT_NAME <=", value, "projectName");
            return (Criteria) this;
        }

        public Criteria andProjectNameLike(String value) {
            addCriterion("PROJECT_NAME like", value, "projectName");
            return (Criteria) this;
        }

        public Criteria andProjectNameNotLike(String value) {
            addCriterion("PROJECT_NAME not like", value, "projectName");
            return (Criteria) this;
        }

        public Criteria andProjectNameIn(List<String> values) {
            addCriterion("PROJECT_NAME in", values, "projectName");
            return (Criteria) this;
        }

        public Criteria andProjectNameNotIn(List<String> values) {
            addCriterion("PROJECT_NAME not in", values, "projectName");
            return (Criteria) this;
        }

        public Criteria andProjectNameBetween(String value1, String value2) {
            addCriterion("PROJECT_NAME between", value1, value2, "projectName");
            return (Criteria) this;
        }

        public Criteria andProjectNameNotBetween(String value1, String value2) {
            addCriterion("PROJECT_NAME not between", value1, value2, "projectName");
            return (Criteria) this;
        }

        public Criteria andIsFeeIsNull() {
            addCriterion("IS_FEE is null");
            return (Criteria) this;
        }

        public Criteria andIsFeeIsNotNull() {
            addCriterion("IS_FEE is not null");
            return (Criteria) this;
        }

        public Criteria andIsFeeEqualTo(String value) {
            addCriterion("IS_FEE =", value, "isFee");
            return (Criteria) this;
        }

        public Criteria andIsFeeNotEqualTo(String value) {
            addCriterion("IS_FEE <>", value, "isFee");
            return (Criteria) this;
        }

        public Criteria andIsFeeGreaterThan(String value) {
            addCriterion("IS_FEE >", value, "isFee");
            return (Criteria) this;
        }

        public Criteria andIsFeeGreaterThanOrEqualTo(String value) {
            addCriterion("IS_FEE >=", value, "isFee");
            return (Criteria) this;
        }

        public Criteria andIsFeeLessThan(String value) {
            addCriterion("IS_FEE <", value, "isFee");
            return (Criteria) this;
        }

        public Criteria andIsFeeLessThanOrEqualTo(String value) {
            addCriterion("IS_FEE <=", value, "isFee");
            return (Criteria) this;
        }

        public Criteria andIsFeeLike(String value) {
            addCriterion("IS_FEE like", value, "isFee");
            return (Criteria) this;
        }

        public Criteria andIsFeeNotLike(String value) {
            addCriterion("IS_FEE not like", value, "isFee");
            return (Criteria) this;
        }

        public Criteria andIsFeeIn(List<String> values) {
            addCriterion("IS_FEE in", values, "isFee");
            return (Criteria) this;
        }

        public Criteria andIsFeeNotIn(List<String> values) {
            addCriterion("IS_FEE not in", values, "isFee");
            return (Criteria) this;
        }

        public Criteria andIsFeeBetween(String value1, String value2) {
            addCriterion("IS_FEE between", value1, value2, "isFee");
            return (Criteria) this;
        }

        public Criteria andIsFeeNotBetween(String value1, String value2) {
            addCriterion("IS_FEE not between", value1, value2, "isFee");
            return (Criteria) this;
        }

        public Criteria andStateIsNull() {
            addCriterion("STATE is null");
            return (Criteria) this;
        }

        public Criteria andStateIsNotNull() {
            addCriterion("STATE is not null");
            return (Criteria) this;
        }

        public Criteria andStateEqualTo(String value) {
            addCriterion("STATE =", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotEqualTo(String value) {
            addCriterion("STATE <>", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThan(String value) {
            addCriterion("STATE >", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateGreaterThanOrEqualTo(String value) {
            addCriterion("STATE >=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThan(String value) {
            addCriterion("STATE <", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLessThanOrEqualTo(String value) {
            addCriterion("STATE <=", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateLike(String value) {
            addCriterion("STATE like", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotLike(String value) {
            addCriterion("STATE not like", value, "state");
            return (Criteria) this;
        }

        public Criteria andStateIn(List<String> values) {
            addCriterion("STATE in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotIn(List<String> values) {
            addCriterion("STATE not in", values, "state");
            return (Criteria) this;
        }

        public Criteria andStateBetween(String value1, String value2) {
            addCriterion("STATE between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andStateNotBetween(String value1, String value2) {
            addCriterion("STATE not between", value1, value2, "state");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("REMARK is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("REMARK is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("REMARK =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("REMARK <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("REMARK >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("REMARK >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("REMARK <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("REMARK <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("REMARK like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("REMARK not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("REMARK in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("REMARK not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("REMARK between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("REMARK not between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andFreeParkingTimeIsNull() {
            addCriterion("FREE_PARKING_TIME is null");
            return (Criteria) this;
        }

        public Criteria andFreeParkingTimeIsNotNull() {
            addCriterion("FREE_PARKING_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andFreeParkingTimeEqualTo(Integer value) {
            addCriterion("FREE_PARKING_TIME =", value, "freeParkingTime");
            return (Criteria) this;
        }

        public Criteria andFreeParkingTimeNotEqualTo(Integer value) {
            addCriterion("FREE_PARKING_TIME <>", value, "freeParkingTime");
            return (Criteria) this;
        }

        public Criteria andFreeParkingTimeGreaterThan(Integer value) {
            addCriterion("FREE_PARKING_TIME >", value, "freeParkingTime");
            return (Criteria) this;
        }

        public Criteria andFreeParkingTimeGreaterThanOrEqualTo(Integer value) {
            addCriterion("FREE_PARKING_TIME >=", value, "freeParkingTime");
            return (Criteria) this;
        }

        public Criteria andFreeParkingTimeLessThan(Integer value) {
            addCriterion("FREE_PARKING_TIME <", value, "freeParkingTime");
            return (Criteria) this;
        }

        public Criteria andFreeParkingTimeLessThanOrEqualTo(Integer value) {
            addCriterion("FREE_PARKING_TIME <=", value, "freeParkingTime");
            return (Criteria) this;
        }

        public Criteria andFreeParkingTimeIn(List<Integer> values) {
            addCriterion("FREE_PARKING_TIME in", values, "freeParkingTime");
            return (Criteria) this;
        }

        public Criteria andFreeParkingTimeNotIn(List<Integer> values) {
            addCriterion("FREE_PARKING_TIME not in", values, "freeParkingTime");
            return (Criteria) this;
        }

        public Criteria andFreeParkingTimeBetween(Integer value1, Integer value2) {
            addCriterion("FREE_PARKING_TIME between", value1, value2, "freeParkingTime");
            return (Criteria) this;
        }

        public Criteria andFreeParkingTimeNotBetween(Integer value1, Integer value2) {
            addCriterion("FREE_PARKING_TIME not between", value1, value2, "freeParkingTime");
            return (Criteria) this;
        }

        public Criteria andDayQuotaAmountIsNull() {
            addCriterion("DAY_QUOTA_AMOUNT is null");
            return (Criteria) this;
        }

        public Criteria andDayQuotaAmountIsNotNull() {
            addCriterion("DAY_QUOTA_AMOUNT is not null");
            return (Criteria) this;
        }

        public Criteria andDayQuotaAmountEqualTo(BigDecimal value) {
            addCriterion("DAY_QUOTA_AMOUNT =", value, "dayQuotaAmount");
            return (Criteria) this;
        }

        public Criteria andDayQuotaAmountNotEqualTo(BigDecimal value) {
            addCriterion("DAY_QUOTA_AMOUNT <>", value, "dayQuotaAmount");
            return (Criteria) this;
        }

        public Criteria andDayQuotaAmountGreaterThan(BigDecimal value) {
            addCriterion("DAY_QUOTA_AMOUNT >", value, "dayQuotaAmount");
            return (Criteria) this;
        }

        public Criteria andDayQuotaAmountGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("DAY_QUOTA_AMOUNT >=", value, "dayQuotaAmount");
            return (Criteria) this;
        }

        public Criteria andDayQuotaAmountLessThan(BigDecimal value) {
            addCriterion("DAY_QUOTA_AMOUNT <", value, "dayQuotaAmount");
            return (Criteria) this;
        }

        public Criteria andDayQuotaAmountLessThanOrEqualTo(BigDecimal value) {
            addCriterion("DAY_QUOTA_AMOUNT <=", value, "dayQuotaAmount");
            return (Criteria) this;
        }

        public Criteria andDayQuotaAmountIn(List<BigDecimal> values) {
            addCriterion("DAY_QUOTA_AMOUNT in", values, "dayQuotaAmount");
            return (Criteria) this;
        }

        public Criteria andDayQuotaAmountNotIn(List<BigDecimal> values) {
            addCriterion("DAY_QUOTA_AMOUNT not in", values, "dayQuotaAmount");
            return (Criteria) this;
        }

        public Criteria andDayQuotaAmountBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("DAY_QUOTA_AMOUNT between", value1, value2, "dayQuotaAmount");
            return (Criteria) this;
        }

        public Criteria andDayQuotaAmountNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("DAY_QUOTA_AMOUNT not between", value1, value2, "dayQuotaAmount");
            return (Criteria) this;
        }

        public Criteria andFeeTypeIsNull() {
            addCriterion("FEE_TYPE is null");
            return (Criteria) this;
        }

        public Criteria andFeeTypeIsNotNull() {
            addCriterion("FEE_TYPE is not null");
            return (Criteria) this;
        }

        public Criteria andFeeTypeEqualTo(String value) {
            addCriterion("FEE_TYPE =", value, "feeType");
            return (Criteria) this;
        }

        public Criteria andFeeTypeNotEqualTo(String value) {
            addCriterion("FEE_TYPE <>", value, "feeType");
            return (Criteria) this;
        }

        public Criteria andFeeTypeGreaterThan(String value) {
            addCriterion("FEE_TYPE >", value, "feeType");
            return (Criteria) this;
        }

        public Criteria andFeeTypeGreaterThanOrEqualTo(String value) {
            addCriterion("FEE_TYPE >=", value, "feeType");
            return (Criteria) this;
        }

        public Criteria andFeeTypeLessThan(String value) {
            addCriterion("FEE_TYPE <", value, "feeType");
            return (Criteria) this;
        }

        public Criteria andFeeTypeLessThanOrEqualTo(String value) {
            addCriterion("FEE_TYPE <=", value, "feeType");
            return (Criteria) this;
        }

        public Criteria andFeeTypeLike(String value) {
            addCriterion("FEE_TYPE like", value, "feeType");
            return (Criteria) this;
        }

        public Criteria andFeeTypeNotLike(String value) {
            addCriterion("FEE_TYPE not like", value, "feeType");
            return (Criteria) this;
        }

        public Criteria andFeeTypeIn(List<String> values) {
            addCriterion("FEE_TYPE in", values, "feeType");
            return (Criteria) this;
        }

        public Criteria andFeeTypeNotIn(List<String> values) {
            addCriterion("FEE_TYPE not in", values, "feeType");
            return (Criteria) this;
        }

        public Criteria andFeeTypeBetween(String value1, String value2) {
            addCriterion("FEE_TYPE between", value1, value2, "feeType");
            return (Criteria) this;
        }

        public Criteria andFeeTypeNotBetween(String value1, String value2) {
            addCriterion("FEE_TYPE not between", value1, value2, "feeType");
            return (Criteria) this;
        }

        public Criteria andFeeAmountIsNull() {
            addCriterion("FEE_AMOUNT is null");
            return (Criteria) this;
        }

        public Criteria andFeeAmountIsNotNull() {
            addCriterion("FEE_AMOUNT is not null");
            return (Criteria) this;
        }

        public Criteria andFeeAmountEqualTo(BigDecimal value) {
            addCriterion("FEE_AMOUNT =", value, "feeAmount");
            return (Criteria) this;
        }

        public Criteria andFeeAmountNotEqualTo(BigDecimal value) {
            addCriterion("FEE_AMOUNT <>", value, "feeAmount");
            return (Criteria) this;
        }

        public Criteria andFeeAmountGreaterThan(BigDecimal value) {
            addCriterion("FEE_AMOUNT >", value, "feeAmount");
            return (Criteria) this;
        }

        public Criteria andFeeAmountGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("FEE_AMOUNT >=", value, "feeAmount");
            return (Criteria) this;
        }

        public Criteria andFeeAmountLessThan(BigDecimal value) {
            addCriterion("FEE_AMOUNT <", value, "feeAmount");
            return (Criteria) this;
        }

        public Criteria andFeeAmountLessThanOrEqualTo(BigDecimal value) {
            addCriterion("FEE_AMOUNT <=", value, "feeAmount");
            return (Criteria) this;
        }

        public Criteria andFeeAmountIn(List<BigDecimal> values) {
            addCriterion("FEE_AMOUNT in", values, "feeAmount");
            return (Criteria) this;
        }

        public Criteria andFeeAmountNotIn(List<BigDecimal> values) {
            addCriterion("FEE_AMOUNT not in", values, "feeAmount");
            return (Criteria) this;
        }

        public Criteria andFeeAmountBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("FEE_AMOUNT between", value1, value2, "feeAmount");
            return (Criteria) this;
        }

        public Criteria andFeeAmountNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("FEE_AMOUNT not between", value1, value2, "feeAmount");
            return (Criteria) this;
        }

        public Criteria andFeeExplainIsNull() {
            addCriterion("FEE_EXPLAIN is null");
            return (Criteria) this;
        }

        public Criteria andFeeExplainIsNotNull() {
            addCriterion("FEE_EXPLAIN is not null");
            return (Criteria) this;
        }

        public Criteria andFeeExplainEqualTo(String value) {
            addCriterion("FEE_EXPLAIN =", value, "feeExplain");
            return (Criteria) this;
        }

        public Criteria andFeeExplainNotEqualTo(String value) {
            addCriterion("FEE_EXPLAIN <>", value, "feeExplain");
            return (Criteria) this;
        }

        public Criteria andFeeExplainGreaterThan(String value) {
            addCriterion("FEE_EXPLAIN >", value, "feeExplain");
            return (Criteria) this;
        }

        public Criteria andFeeExplainGreaterThanOrEqualTo(String value) {
            addCriterion("FEE_EXPLAIN >=", value, "feeExplain");
            return (Criteria) this;
        }

        public Criteria andFeeExplainLessThan(String value) {
            addCriterion("FEE_EXPLAIN <", value, "feeExplain");
            return (Criteria) this;
        }

        public Criteria andFeeExplainLessThanOrEqualTo(String value) {
            addCriterion("FEE_EXPLAIN <=", value, "feeExplain");
            return (Criteria) this;
        }

        public Criteria andFeeExplainLike(String value) {
            addCriterion("FEE_EXPLAIN like", value, "feeExplain");
            return (Criteria) this;
        }

        public Criteria andFeeExplainNotLike(String value) {
            addCriterion("FEE_EXPLAIN not like", value, "feeExplain");
            return (Criteria) this;
        }

        public Criteria andFeeExplainIn(List<String> values) {
            addCriterion("FEE_EXPLAIN in", values, "feeExplain");
            return (Criteria) this;
        }

        public Criteria andFeeExplainNotIn(List<String> values) {
            addCriterion("FEE_EXPLAIN not in", values, "feeExplain");
            return (Criteria) this;
        }

        public Criteria andFeeExplainBetween(String value1, String value2) {
            addCriterion("FEE_EXPLAIN between", value1, value2, "feeExplain");
            return (Criteria) this;
        }

        public Criteria andFeeExplainNotBetween(String value1, String value2) {
            addCriterion("FEE_EXPLAIN not between", value1, value2, "feeExplain");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("CREATE_TIME is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("CREATE_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("CREATE_TIME =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("CREATE_TIME <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("CREATE_TIME >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("CREATE_TIME >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("CREATE_TIME <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("CREATE_TIME <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("CREATE_TIME in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("CREATE_TIME not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("CREATE_TIME between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("CREATE_TIME not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateIdIsNull() {
            addCriterion("CREATE_ID is null");
            return (Criteria) this;
        }

        public Criteria andCreateIdIsNotNull() {
            addCriterion("CREATE_ID is not null");
            return (Criteria) this;
        }

        public Criteria andCreateIdEqualTo(String value) {
            addCriterion("CREATE_ID =", value, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateIdNotEqualTo(String value) {
            addCriterion("CREATE_ID <>", value, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateIdGreaterThan(String value) {
            addCriterion("CREATE_ID >", value, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateIdGreaterThanOrEqualTo(String value) {
            addCriterion("CREATE_ID >=", value, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateIdLessThan(String value) {
            addCriterion("CREATE_ID <", value, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateIdLessThanOrEqualTo(String value) {
            addCriterion("CREATE_ID <=", value, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateIdLike(String value) {
            addCriterion("CREATE_ID like", value, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateIdNotLike(String value) {
            addCriterion("CREATE_ID not like", value, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateIdIn(List<String> values) {
            addCriterion("CREATE_ID in", values, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateIdNotIn(List<String> values) {
            addCriterion("CREATE_ID not in", values, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateIdBetween(String value1, String value2) {
            addCriterion("CREATE_ID between", value1, value2, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateIdNotBetween(String value1, String value2) {
            addCriterion("CREATE_ID not between", value1, value2, "createId");
            return (Criteria) this;
        }

        public Criteria andCreateNameIsNull() {
            addCriterion("CREATE_NAME is null");
            return (Criteria) this;
        }

        public Criteria andCreateNameIsNotNull() {
            addCriterion("CREATE_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andCreateNameEqualTo(String value) {
            addCriterion("CREATE_NAME =", value, "createName");
            return (Criteria) this;
        }

        public Criteria andCreateNameNotEqualTo(String value) {
            addCriterion("CREATE_NAME <>", value, "createName");
            return (Criteria) this;
        }

        public Criteria andCreateNameGreaterThan(String value) {
            addCriterion("CREATE_NAME >", value, "createName");
            return (Criteria) this;
        }

        public Criteria andCreateNameGreaterThanOrEqualTo(String value) {
            addCriterion("CREATE_NAME >=", value, "createName");
            return (Criteria) this;
        }

        public Criteria andCreateNameLessThan(String value) {
            addCriterion("CREATE_NAME <", value, "createName");
            return (Criteria) this;
        }

        public Criteria andCreateNameLessThanOrEqualTo(String value) {
            addCriterion("CREATE_NAME <=", value, "createName");
            return (Criteria) this;
        }

        public Criteria andCreateNameLike(String value) {
            addCriterion("CREATE_NAME like", value, "createName");
            return (Criteria) this;
        }

        public Criteria andCreateNameNotLike(String value) {
            addCriterion("CREATE_NAME not like", value, "createName");
            return (Criteria) this;
        }

        public Criteria andCreateNameIn(List<String> values) {
            addCriterion("CREATE_NAME in", values, "createName");
            return (Criteria) this;
        }

        public Criteria andCreateNameNotIn(List<String> values) {
            addCriterion("CREATE_NAME not in", values, "createName");
            return (Criteria) this;
        }

        public Criteria andCreateNameBetween(String value1, String value2) {
            addCriterion("CREATE_NAME between", value1, value2, "createName");
            return (Criteria) this;
        }

        public Criteria andCreateNameNotBetween(String value1, String value2) {
            addCriterion("CREATE_NAME not between", value1, value2, "createName");
            return (Criteria) this;
        }

        public Criteria andOperatorIdIsNull() {
            addCriterion("OPERATOR_ID is null");
            return (Criteria) this;
        }

        public Criteria andOperatorIdIsNotNull() {
            addCriterion("OPERATOR_ID is not null");
            return (Criteria) this;
        }

        public Criteria andOperatorIdEqualTo(String value) {
            addCriterion("OPERATOR_ID =", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdNotEqualTo(String value) {
            addCriterion("OPERATOR_ID <>", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdGreaterThan(String value) {
            addCriterion("OPERATOR_ID >", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdGreaterThanOrEqualTo(String value) {
            addCriterion("OPERATOR_ID >=", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdLessThan(String value) {
            addCriterion("OPERATOR_ID <", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdLessThanOrEqualTo(String value) {
            addCriterion("OPERATOR_ID <=", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdLike(String value) {
            addCriterion("OPERATOR_ID like", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdNotLike(String value) {
            addCriterion("OPERATOR_ID not like", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdIn(List<String> values) {
            addCriterion("OPERATOR_ID in", values, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdNotIn(List<String> values) {
            addCriterion("OPERATOR_ID not in", values, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdBetween(String value1, String value2) {
            addCriterion("OPERATOR_ID between", value1, value2, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdNotBetween(String value1, String value2) {
            addCriterion("OPERATOR_ID not between", value1, value2, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorNameIsNull() {
            addCriterion("OPERATOR_NAME is null");
            return (Criteria) this;
        }

        public Criteria andOperatorNameIsNotNull() {
            addCriterion("OPERATOR_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andOperatorNameEqualTo(String value) {
            addCriterion("OPERATOR_NAME =", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameNotEqualTo(String value) {
            addCriterion("OPERATOR_NAME <>", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameGreaterThan(String value) {
            addCriterion("OPERATOR_NAME >", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameGreaterThanOrEqualTo(String value) {
            addCriterion("OPERATOR_NAME >=", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameLessThan(String value) {
            addCriterion("OPERATOR_NAME <", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameLessThanOrEqualTo(String value) {
            addCriterion("OPERATOR_NAME <=", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameLike(String value) {
            addCriterion("OPERATOR_NAME like", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameNotLike(String value) {
            addCriterion("OPERATOR_NAME not like", value, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameIn(List<String> values) {
            addCriterion("OPERATOR_NAME in", values, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameNotIn(List<String> values) {
            addCriterion("OPERATOR_NAME not in", values, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameBetween(String value1, String value2) {
            addCriterion("OPERATOR_NAME between", value1, value2, "operatorName");
            return (Criteria) this;
        }

        public Criteria andOperatorNameNotBetween(String value1, String value2) {
            addCriterion("OPERATOR_NAME not between", value1, value2, "operatorName");
            return (Criteria) this;
        }

        public Criteria andLastTimeIsNull() {
            addCriterion("LAST_TIME is null");
            return (Criteria) this;
        }

        public Criteria andLastTimeIsNotNull() {
            addCriterion("LAST_TIME is not null");
            return (Criteria) this;
        }

        public Criteria andLastTimeEqualTo(Date value) {
            addCriterion("LAST_TIME =", value, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeNotEqualTo(Date value) {
            addCriterion("LAST_TIME <>", value, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeGreaterThan(Date value) {
            addCriterion("LAST_TIME >", value, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("LAST_TIME >=", value, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeLessThan(Date value) {
            addCriterion("LAST_TIME <", value, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeLessThanOrEqualTo(Date value) {
            addCriterion("LAST_TIME <=", value, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeIn(List<Date> values) {
            addCriterion("LAST_TIME in", values, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeNotIn(List<Date> values) {
            addCriterion("LAST_TIME not in", values, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeBetween(Date value1, Date value2) {
            addCriterion("LAST_TIME between", value1, value2, "lastTime");
            return (Criteria) this;
        }

        public Criteria andLastTimeNotBetween(Date value1, Date value2) {
            addCriterion("LAST_TIME not between", value1, value2, "lastTime");
            return (Criteria) this;
        }

        public Criteria andIdLikeInsensitive(String value) {
            addCriterion("upper(ID) like", value.toUpperCase(), "id");
            return (Criteria) this;
        }

        public Criteria andParkNameLikeInsensitive(String value) {
            addCriterion("upper(PARK_NAME) like", value.toUpperCase(), "parkName");
            return (Criteria) this;
        }

        public Criteria andParkConverPathLikeInsensitive(String value) {
            addCriterion("upper(PARK_CONVER_PATH) like", value.toUpperCase(), "parkConverPath");
            return (Criteria) this;
        }

        public Criteria andProvinceNameLikeInsensitive(String value) {
            addCriterion("upper(PROVINCE_NAME) like", value.toUpperCase(), "provinceName");
            return (Criteria) this;
        }

        public Criteria andProvinceCodeLikeInsensitive(String value) {
            addCriterion("upper(PROVINCE_CODE) like", value.toUpperCase(), "provinceCode");
            return (Criteria) this;
        }

        public Criteria andCityNameLikeInsensitive(String value) {
            addCriterion("upper(CITY_NAME) like", value.toUpperCase(), "cityName");
            return (Criteria) this;
        }

        public Criteria andCityCodeLikeInsensitive(String value) {
            addCriterion("upper(CITY_CODE) like", value.toUpperCase(), "cityCode");
            return (Criteria) this;
        }

        public Criteria andIntegralBodyTypeLikeInsensitive(String value) {
            addCriterion("upper(INTEGRAL_BODY_TYPE) like", value.toUpperCase(), "integralBodyType");
            return (Criteria) this;
        }

        public Criteria andIntegralBodyCodeLikeInsensitive(String value) {
            addCriterion("upper(INTEGRAL_BODY_CODE) like", value.toUpperCase(), "integralBodyCode");
            return (Criteria) this;
        }

        public Criteria andPropertyLikeInsensitive(String value) {
            addCriterion("upper(PROPERTY) like", value.toUpperCase(), "property");
            return (Criteria) this;
        }

        public Criteria andParkTypeLikeInsensitive(String value) {
            addCriterion("upper(PARK_TYPE) like", value.toUpperCase(), "parkType");
            return (Criteria) this;
        }

        public Criteria andBusinessEntityLikeInsensitive(String value) {
            addCriterion("upper(BUSINESS_ENTITY) like", value.toUpperCase(), "businessEntity");
            return (Criteria) this;
        }

        public Criteria andCompanyNoLikeInsensitive(String value) {
            addCriterion("upper(COMPANY_NO) like", value.toUpperCase(), "companyNo");
            return (Criteria) this;
        }

        public Criteria andCompanyNameLikeInsensitive(String value) {
            addCriterion("upper(COMPANY_NAME) like", value.toUpperCase(), "companyName");
            return (Criteria) this;
        }

        public Criteria andProjectNoLikeInsensitive(String value) {
            addCriterion("upper(PROJECT_NO) like", value.toUpperCase(), "projectNo");
            return (Criteria) this;
        }

        public Criteria andProjectNameLikeInsensitive(String value) {
            addCriterion("upper(PROJECT_NAME) like", value.toUpperCase(), "projectName");
            return (Criteria) this;
        }

        public Criteria andIsFeeLikeInsensitive(String value) {
            addCriterion("upper(IS_FEE) like", value.toUpperCase(), "isFee");
            return (Criteria) this;
        }

        public Criteria andStateLikeInsensitive(String value) {
            addCriterion("upper(STATE) like", value.toUpperCase(), "state");
            return (Criteria) this;
        }

        public Criteria andRemarkLikeInsensitive(String value) {
            addCriterion("upper(REMARK) like", value.toUpperCase(), "remark");
            return (Criteria) this;
        }

        public Criteria andFeeTypeLikeInsensitive(String value) {
            addCriterion("upper(FEE_TYPE) like", value.toUpperCase(), "feeType");
            return (Criteria) this;
        }

        public Criteria andFeeExplainLikeInsensitive(String value) {
            addCriterion("upper(FEE_EXPLAIN) like", value.toUpperCase(), "feeExplain");
            return (Criteria) this;
        }

        public Criteria andCreateIdLikeInsensitive(String value) {
            addCriterion("upper(CREATE_ID) like", value.toUpperCase(), "createId");
            return (Criteria) this;
        }

        public Criteria andCreateNameLikeInsensitive(String value) {
            addCriterion("upper(CREATE_NAME) like", value.toUpperCase(), "createName");
            return (Criteria) this;
        }

        public Criteria andOperatorIdLikeInsensitive(String value) {
            addCriterion("upper(OPERATOR_ID) like", value.toUpperCase(), "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorNameLikeInsensitive(String value) {
            addCriterion("upper(OPERATOR_NAME) like", value.toUpperCase(), "operatorName");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}