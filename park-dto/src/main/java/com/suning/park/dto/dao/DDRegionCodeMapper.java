package com.suning.park.dto.dao;

import com.suning.database.mybatis.SuperMapper;
import com.suning.park.dto.model.DDRegionCode;
import com.suning.park.dto.model.DDRegionCodeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DDRegionCodeMapper extends SuperMapper {
    int countByExample(DDRegionCodeExample example);

    int deleteByExample(DDRegionCodeExample example);

    int deleteByPrimaryKey(String ddRegionCodeId);

    int insert(DDRegionCode record);

    int insertSelective(DDRegionCode record);

    List<DDRegionCode> selectByExample(DDRegionCodeExample example);

    DDRegionCode selectByPrimaryKey(String ddRegionCodeId);

    int updateByExampleSelective(@Param("record") DDRegionCode record, @Param("example") DDRegionCodeExample example);

    int updateByExample(@Param("record") DDRegionCode record, @Param("example") DDRegionCodeExample example);

    int updateByPrimaryKeySelective(DDRegionCode record);

    int updateByPrimaryKey(DDRegionCode record);
}