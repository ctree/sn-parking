package com.suning.park.dto.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Park implements Serializable {
    private String id;

    private String parkName;

    private String parkConverPath;

    private String provinceName;

    private String provinceCode;

    private String cityName;

    private String cityCode;

    private String integralBodyType;

    private String integralBodyCode;

    private String property;

    private String parkType;

    private String businessEntity;

    private String companyNo;

    private String companyName;

    private String projectNo;

    private String projectName;

    private String isFee;

    private String state;

    private String remark;

    private Integer freeParkingTime;

    private BigDecimal dayQuotaAmount;

    private String feeType;

    private BigDecimal feeAmount;

    private String feeExplain;

    private Date createTime;

    private String createId;

    private String createName;

    private String operatorId;

    private String operatorName;

    private Date lastTime;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getParkName() {
        return parkName;
    }

    public void setParkName(String parkName) {
        this.parkName = parkName == null ? null : parkName.trim();
    }

    public String getParkConverPath() {
        return parkConverPath;
    }

    public void setParkConverPath(String parkConverPath) {
        this.parkConverPath = parkConverPath == null ? null : parkConverPath.trim();
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName == null ? null : provinceName.trim();
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode == null ? null : provinceCode.trim();
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName == null ? null : cityName.trim();
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode == null ? null : cityCode.trim();
    }

    public String getIntegralBodyType() {
        return integralBodyType;
    }

    public void setIntegralBodyType(String integralBodyType) {
        this.integralBodyType = integralBodyType == null ? null : integralBodyType.trim();
    }

    public String getIntegralBodyCode() {
        return integralBodyCode;
    }

    public void setIntegralBodyCode(String integralBodyCode) {
        this.integralBodyCode = integralBodyCode == null ? null : integralBodyCode.trim();
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property == null ? null : property.trim();
    }

    public String getParkType() {
        return parkType;
    }

    public void setParkType(String parkType) {
        this.parkType = parkType == null ? null : parkType.trim();
    }

    public String getBusinessEntity() {
        return businessEntity;
    }

    public void setBusinessEntity(String businessEntity) {
        this.businessEntity = businessEntity == null ? null : businessEntity.trim();
    }

    public String getCompanyNo() {
        return companyNo;
    }

    public void setCompanyNo(String companyNo) {
        this.companyNo = companyNo == null ? null : companyNo.trim();
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName == null ? null : companyName.trim();
    }

    public String getProjectNo() {
        return projectNo;
    }

    public void setProjectNo(String projectNo) {
        this.projectNo = projectNo == null ? null : projectNo.trim();
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName == null ? null : projectName.trim();
    }

    public String getIsFee() {
        return isFee;
    }

    public void setIsFee(String isFee) {
        this.isFee = isFee == null ? null : isFee.trim();
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state == null ? null : state.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Integer getFreeParkingTime() {
        return freeParkingTime;
    }

    public void setFreeParkingTime(Integer freeParkingTime) {
        this.freeParkingTime = freeParkingTime;
    }

    public BigDecimal getDayQuotaAmount() {
        return dayQuotaAmount;
    }

    public void setDayQuotaAmount(BigDecimal dayQuotaAmount) {
        this.dayQuotaAmount = dayQuotaAmount;
    }

    public String getFeeType() {
        return feeType;
    }

    public void setFeeType(String feeType) {
        this.feeType = feeType == null ? null : feeType.trim();
    }

    public BigDecimal getFeeAmount() {
        return feeAmount;
    }

    public void setFeeAmount(BigDecimal feeAmount) {
        this.feeAmount = feeAmount;
    }

    public String getFeeExplain() {
        return feeExplain;
    }

    public void setFeeExplain(String feeExplain) {
        this.feeExplain = feeExplain == null ? null : feeExplain.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateId() {
        return createId;
    }

    public void setCreateId(String createId) {
        this.createId = createId == null ? null : createId.trim();
    }

    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName == null ? null : createName.trim();
    }

    public String getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId == null ? null : operatorId.trim();
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName == null ? null : operatorName.trim();
    }

    public Date getLastTime() {
        return lastTime;
    }

    public void setLastTime(Date lastTime) {
        this.lastTime = lastTime;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that == null) {
            return false;
        }
        if (getClass() != that.getClass()) {
            return false;
        }
        Park other = (Park) that;
        return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
            && (this.getParkName() == null ? other.getParkName() == null : this.getParkName().equals(other.getParkName()))
            && (this.getParkConverPath() == null ? other.getParkConverPath() == null : this.getParkConverPath().equals(other.getParkConverPath()))
            && (this.getProvinceName() == null ? other.getProvinceName() == null : this.getProvinceName().equals(other.getProvinceName()))
            && (this.getProvinceCode() == null ? other.getProvinceCode() == null : this.getProvinceCode().equals(other.getProvinceCode()))
            && (this.getCityName() == null ? other.getCityName() == null : this.getCityName().equals(other.getCityName()))
            && (this.getCityCode() == null ? other.getCityCode() == null : this.getCityCode().equals(other.getCityCode()))
            && (this.getIntegralBodyType() == null ? other.getIntegralBodyType() == null : this.getIntegralBodyType().equals(other.getIntegralBodyType()))
            && (this.getIntegralBodyCode() == null ? other.getIntegralBodyCode() == null : this.getIntegralBodyCode().equals(other.getIntegralBodyCode()))
            && (this.getProperty() == null ? other.getProperty() == null : this.getProperty().equals(other.getProperty()))
            && (this.getParkType() == null ? other.getParkType() == null : this.getParkType().equals(other.getParkType()))
            && (this.getBusinessEntity() == null ? other.getBusinessEntity() == null : this.getBusinessEntity().equals(other.getBusinessEntity()))
            && (this.getCompanyNo() == null ? other.getCompanyNo() == null : this.getCompanyNo().equals(other.getCompanyNo()))
            && (this.getCompanyName() == null ? other.getCompanyName() == null : this.getCompanyName().equals(other.getCompanyName()))
            && (this.getProjectNo() == null ? other.getProjectNo() == null : this.getProjectNo().equals(other.getProjectNo()))
            && (this.getProjectName() == null ? other.getProjectName() == null : this.getProjectName().equals(other.getProjectName()))
            && (this.getIsFee() == null ? other.getIsFee() == null : this.getIsFee().equals(other.getIsFee()))
            && (this.getState() == null ? other.getState() == null : this.getState().equals(other.getState()))
            && (this.getRemark() == null ? other.getRemark() == null : this.getRemark().equals(other.getRemark()))
            && (this.getFreeParkingTime() == null ? other.getFreeParkingTime() == null : this.getFreeParkingTime().equals(other.getFreeParkingTime()))
            && (this.getDayQuotaAmount() == null ? other.getDayQuotaAmount() == null : this.getDayQuotaAmount().equals(other.getDayQuotaAmount()))
            && (this.getFeeType() == null ? other.getFeeType() == null : this.getFeeType().equals(other.getFeeType()))
            && (this.getFeeAmount() == null ? other.getFeeAmount() == null : this.getFeeAmount().equals(other.getFeeAmount()))
            && (this.getFeeExplain() == null ? other.getFeeExplain() == null : this.getFeeExplain().equals(other.getFeeExplain()))
            && (this.getCreateTime() == null ? other.getCreateTime() == null : this.getCreateTime().equals(other.getCreateTime()))
            && (this.getCreateId() == null ? other.getCreateId() == null : this.getCreateId().equals(other.getCreateId()))
            && (this.getCreateName() == null ? other.getCreateName() == null : this.getCreateName().equals(other.getCreateName()))
            && (this.getOperatorId() == null ? other.getOperatorId() == null : this.getOperatorId().equals(other.getOperatorId()))
            && (this.getOperatorName() == null ? other.getOperatorName() == null : this.getOperatorName().equals(other.getOperatorName()))
            && (this.getLastTime() == null ? other.getLastTime() == null : this.getLastTime().equals(other.getLastTime()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        result = prime * result + ((getParkName() == null) ? 0 : getParkName().hashCode());
        result = prime * result + ((getParkConverPath() == null) ? 0 : getParkConverPath().hashCode());
        result = prime * result + ((getProvinceName() == null) ? 0 : getProvinceName().hashCode());
        result = prime * result + ((getProvinceCode() == null) ? 0 : getProvinceCode().hashCode());
        result = prime * result + ((getCityName() == null) ? 0 : getCityName().hashCode());
        result = prime * result + ((getCityCode() == null) ? 0 : getCityCode().hashCode());
        result = prime * result + ((getIntegralBodyType() == null) ? 0 : getIntegralBodyType().hashCode());
        result = prime * result + ((getIntegralBodyCode() == null) ? 0 : getIntegralBodyCode().hashCode());
        result = prime * result + ((getProperty() == null) ? 0 : getProperty().hashCode());
        result = prime * result + ((getParkType() == null) ? 0 : getParkType().hashCode());
        result = prime * result + ((getBusinessEntity() == null) ? 0 : getBusinessEntity().hashCode());
        result = prime * result + ((getCompanyNo() == null) ? 0 : getCompanyNo().hashCode());
        result = prime * result + ((getCompanyName() == null) ? 0 : getCompanyName().hashCode());
        result = prime * result + ((getProjectNo() == null) ? 0 : getProjectNo().hashCode());
        result = prime * result + ((getProjectName() == null) ? 0 : getProjectName().hashCode());
        result = prime * result + ((getIsFee() == null) ? 0 : getIsFee().hashCode());
        result = prime * result + ((getState() == null) ? 0 : getState().hashCode());
        result = prime * result + ((getRemark() == null) ? 0 : getRemark().hashCode());
        result = prime * result + ((getFreeParkingTime() == null) ? 0 : getFreeParkingTime().hashCode());
        result = prime * result + ((getDayQuotaAmount() == null) ? 0 : getDayQuotaAmount().hashCode());
        result = prime * result + ((getFeeType() == null) ? 0 : getFeeType().hashCode());
        result = prime * result + ((getFeeAmount() == null) ? 0 : getFeeAmount().hashCode());
        result = prime * result + ((getFeeExplain() == null) ? 0 : getFeeExplain().hashCode());
        result = prime * result + ((getCreateTime() == null) ? 0 : getCreateTime().hashCode());
        result = prime * result + ((getCreateId() == null) ? 0 : getCreateId().hashCode());
        result = prime * result + ((getCreateName() == null) ? 0 : getCreateName().hashCode());
        result = prime * result + ((getOperatorId() == null) ? 0 : getOperatorId().hashCode());
        result = prime * result + ((getOperatorName() == null) ? 0 : getOperatorName().hashCode());
        result = prime * result + ((getLastTime() == null) ? 0 : getLastTime().hashCode());
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", parkName=").append(parkName);
        sb.append(", parkConverPath=").append(parkConverPath);
        sb.append(", provinceName=").append(provinceName);
        sb.append(", provinceCode=").append(provinceCode);
        sb.append(", cityName=").append(cityName);
        sb.append(", cityCode=").append(cityCode);
        sb.append(", integralBodyType=").append(integralBodyType);
        sb.append(", integralBodyCode=").append(integralBodyCode);
        sb.append(", property=").append(property);
        sb.append(", parkType=").append(parkType);
        sb.append(", businessEntity=").append(businessEntity);
        sb.append(", companyNo=").append(companyNo);
        sb.append(", companyName=").append(companyName);
        sb.append(", projectNo=").append(projectNo);
        sb.append(", projectName=").append(projectName);
        sb.append(", isFee=").append(isFee);
        sb.append(", state=").append(state);
        sb.append(", remark=").append(remark);
        sb.append(", freeParkingTime=").append(freeParkingTime);
        sb.append(", dayQuotaAmount=").append(dayQuotaAmount);
        sb.append(", feeType=").append(feeType);
        sb.append(", feeAmount=").append(feeAmount);
        sb.append(", feeExplain=").append(feeExplain);
        sb.append(", createTime=").append(createTime);
        sb.append(", createId=").append(createId);
        sb.append(", createName=").append(createName);
        sb.append(", operatorId=").append(operatorId);
        sb.append(", operatorName=").append(operatorName);
        sb.append(", lastTime=").append(lastTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}