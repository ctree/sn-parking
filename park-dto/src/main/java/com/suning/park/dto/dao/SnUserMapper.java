package com.suning.park.dto.dao;

import com.suning.database.mybatis.SuperMapper;
import com.suning.park.dto.model.SnUser;
import com.suning.park.dto.model.SnUserExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SnUserMapper extends SuperMapper {
    int countByExample(SnUserExample example);

    int deleteByExample(SnUserExample example);

    int deleteByPrimaryKey(String id);

    int insert(SnUser record);

    int insertSelective(SnUser record);

    List<SnUser> selectByExample(SnUserExample example);

    SnUser selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") SnUser record, @Param("example") SnUserExample example);

    int updateByExample(@Param("record") SnUser record, @Param("example") SnUserExample example);

    int updateByPrimaryKeySelective(SnUser record);

    int updateByPrimaryKey(SnUser record);
}