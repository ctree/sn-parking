package com.suning.park.dto.dao;

import com.suning.database.mybatis.SuperMapper;
import com.suning.park.dto.model.MerchantUser;
import com.suning.park.dto.model.MerchantUserExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface MerchantUserMapper extends SuperMapper {
    int countByExample(MerchantUserExample example);

    int deleteByExample(MerchantUserExample example);

    int deleteByPrimaryKey(String id);

    int insert(MerchantUser record);

    int insertSelective(MerchantUser record);

    List<MerchantUser> selectByExample(MerchantUserExample example);

    MerchantUser selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") MerchantUser record, @Param("example") MerchantUserExample example);

    int updateByExample(@Param("record") MerchantUser record, @Param("example") MerchantUserExample example);

    int updateByPrimaryKeySelective(MerchantUser record);

    int updateByPrimaryKey(MerchantUser record);
}