package com.suning.park.dto.dao;

import com.suning.database.mybatis.SuperMapper;
import com.suning.park.dto.model.FunctionSource;
import com.suning.park.dto.model.FunctionSourceExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface FunctionSourceMapper extends SuperMapper {
    int countByExample(FunctionSourceExample example);

    int deleteByExample(FunctionSourceExample example);

    int deleteByPrimaryKey(String functionSourceId);

    int insert(FunctionSource record);

    int insertSelective(FunctionSource record);

    List<FunctionSource> selectByExample(FunctionSourceExample example);

    FunctionSource selectByPrimaryKey(String functionSourceId);

    int updateByExampleSelective(@Param("record") FunctionSource record, @Param("example") FunctionSourceExample example);

    int updateByExample(@Param("record") FunctionSource record, @Param("example") FunctionSourceExample example);

    int updateByPrimaryKeySelective(FunctionSource record);

    int updateByPrimaryKey(FunctionSource record);
}