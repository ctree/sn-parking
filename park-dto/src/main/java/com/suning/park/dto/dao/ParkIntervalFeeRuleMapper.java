package com.suning.park.dto.dao;

import com.suning.database.mybatis.SuperMapper;
import com.suning.park.dto.model.ParkIntervalFeeRule;
import com.suning.park.dto.model.ParkIntervalFeeRuleExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ParkIntervalFeeRuleMapper extends SuperMapper {
    int countByExample(ParkIntervalFeeRuleExample example);

    int deleteByExample(ParkIntervalFeeRuleExample example);

    int deleteByPrimaryKey(String id);

    int insert(ParkIntervalFeeRule record);

    int insertSelective(ParkIntervalFeeRule record);

    List<ParkIntervalFeeRule> selectByExample(ParkIntervalFeeRuleExample example);

    ParkIntervalFeeRule selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") ParkIntervalFeeRule record, @Param("example") ParkIntervalFeeRuleExample example);

    int updateByExample(@Param("record") ParkIntervalFeeRule record, @Param("example") ParkIntervalFeeRuleExample example);

    int updateByPrimaryKeySelective(ParkIntervalFeeRule record);

    int updateByPrimaryKey(ParkIntervalFeeRule record);
}