package com.suning.park.dto.dao;

import com.suning.database.mybatis.SuperMapper;
import com.suning.park.dto.model.BaseData;
import com.suning.park.dto.model.BaseDataExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface BaseDataMapper extends SuperMapper {
    int countByExample(BaseDataExample example);

    int deleteByExample(BaseDataExample example);

    int deleteByPrimaryKey(String baseId);

    int insert(BaseData record);

    int insertSelective(BaseData record);

    List<BaseData> selectByExample(BaseDataExample example);

    BaseData selectByPrimaryKey(String baseId);

    int updateByExampleSelective(@Param("record") BaseData record, @Param("example") BaseDataExample example);

    int updateByExample(@Param("record") BaseData record, @Param("example") BaseDataExample example);

    int updateByPrimaryKeySelective(BaseData record);

    int updateByPrimaryKey(BaseData record);
}