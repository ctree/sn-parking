package com.suning.park.dto.dao;

import com.suning.database.mybatis.SuperMapper;
import com.suning.park.dto.model.Vali;
import com.suning.park.dto.model.ValiExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ValiMapper extends SuperMapper {
    int countByExample(ValiExample example);

    int deleteByExample(ValiExample example);

    int deleteByPrimaryKey(String valiId);

    int insert(Vali record);

    int insertSelective(Vali record);

    List<Vali> selectByExample(ValiExample example);

    Vali selectByPrimaryKey(String valiId);

    int updateByExampleSelective(@Param("record") Vali record, @Param("example") ValiExample example);

    int updateByExample(@Param("record") Vali record, @Param("example") ValiExample example);

    int updateByPrimaryKeySelective(Vali record);

    int updateByPrimaryKey(Vali record);
}