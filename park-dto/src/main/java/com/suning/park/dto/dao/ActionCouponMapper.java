package com.suning.park.dto.dao;

import com.suning.database.mybatis.SuperMapper;
import com.suning.park.dto.model.ActionCoupon;
import com.suning.park.dto.model.ActionCouponExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ActionCouponMapper extends SuperMapper {
    int countByExample(ActionCouponExample example);

    int deleteByExample(ActionCouponExample example);

    int deleteByPrimaryKey(String id);

    int insert(ActionCoupon record);

    int insertSelective(ActionCoupon record);

    List<ActionCoupon> selectByExample(ActionCouponExample example);

    ActionCoupon selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") ActionCoupon record, @Param("example") ActionCouponExample example);

    int updateByExample(@Param("record") ActionCoupon record, @Param("example") ActionCouponExample example);

    int updateByPrimaryKeySelective(ActionCoupon record);

    int updateByPrimaryKey(ActionCoupon record);
}