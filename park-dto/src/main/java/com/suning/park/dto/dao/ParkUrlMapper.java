package com.suning.park.dto.dao;

import com.suning.database.mybatis.SuperMapper;
import com.suning.park.dto.model.ParkUrl;
import com.suning.park.dto.model.ParkUrlExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ParkUrlMapper extends SuperMapper {
    int countByExample(ParkUrlExample example);

    int deleteByExample(ParkUrlExample example);

    int deleteByPrimaryKey(String id);

    int insert(ParkUrl record);

    int insertSelective(ParkUrl record);

    List<ParkUrl> selectByExample(ParkUrlExample example);

    ParkUrl selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") ParkUrl record, @Param("example") ParkUrlExample example);

    int updateByExample(@Param("record") ParkUrl record, @Param("example") ParkUrlExample example);

    int updateByPrimaryKeySelective(ParkUrl record);

    int updateByPrimaryKey(ParkUrl record);
}