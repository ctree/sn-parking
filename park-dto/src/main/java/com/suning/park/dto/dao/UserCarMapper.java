package com.suning.park.dto.dao;

import com.suning.database.mybatis.SuperMapper;
import com.suning.park.dto.model.UserCar;
import com.suning.park.dto.model.UserCarExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface UserCarMapper extends SuperMapper {
    int countByExample(UserCarExample example);

    int deleteByExample(UserCarExample example);

    int deleteByPrimaryKey(String id);

    int insert(UserCar record);

    int insertSelective(UserCar record);

    List<UserCar> selectByExample(UserCarExample example);

    UserCar selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") UserCar record, @Param("example") UserCarExample example);

    int updateByExample(@Param("record") UserCar record, @Param("example") UserCarExample example);

    int updateByPrimaryKeySelective(UserCar record);

    int updateByPrimaryKey(UserCar record);
}