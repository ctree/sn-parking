package com.suning.park.web.controller;

import com.suning.park.web.bean.Response;
import com.suning.park.web.service.CouponService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 优惠券服务
 * Created by Administrator on 2017/2/16.
 */
@Controller
@RequestMapping(value = "/coupon", produces = {"application/json;charset=UTF-8"})
public class CouponController {

    private static final Logger logger = LoggerFactory.getLogger(CouponController.class);

    @Autowired
    CouponService couponService;

    /**
     * 根据停车场ID和苏宁用户ID获得优惠券列表
     * @param parkId 停车场ID
     * @param snUserId 苏宁用户ID
     * @param getType 查询类型(0=可领取的，1=已领取的)
     * @return
     */
    @RequestMapping(value = "/getCouponList")
    public @ResponseBody String getCouponList(@RequestParam  String parkId,
                                              @RequestParam String snUserId,
                                              @RequestParam String getType) {
        logger.info("CouponController.getCouponList, parkId:{}, snUserId:{}, getType:{}",
                new Object[]{parkId,snUserId,getType});
        return Response.SUCCESS().put("couponList", couponService.getCouponList(parkId, snUserId, getType)).toJson();
    }

    /**
     * 用户关联优惠券
     * @param snUserId 苏宁用户ID
     * @param couponId 优惠券ID
     * @return
     */
    @RequestMapping(value = "/provideCoupon", method = RequestMethod.POST)
    public @ResponseBody String provideCoupon(@RequestParam String snUserId, @RequestParam String couponId) throws Exception {
        logger.info("CouponController.provideCoupon, snUserId:{}, couponId:{}", snUserId, couponId);
        couponService.provideCoupon(snUserId, couponId);
        return Response.SUCCESS().toJson();
    }
}
