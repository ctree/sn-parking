package com.suning.park.web.service;

import com.suning.park.admin.provider.IUserCarProvider;
import com.suning.park.dto.model.UserCar;
import com.suning.rsf.spring.Reference;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2017/2/15.
 */
@Service
public class UserCarService {

    @Reference(contract = IUserCarProvider.class)
    IUserCarProvider userCarProvider;

    public List<UserCar> selectBySnUserId(String snUserId) {
        return userCarProvider.selectBySnUserId(snUserId);
    }

    public void insertUserCar(UserCar userCar) {
        userCarProvider.insertUserCar(userCar);
    }

    public void delete(String id) {
        userCarProvider.delete(id);
    }
}
