package com.suning.park.web.enums;

public enum ResponseCode {


	SUCCESS("0","成功"),

	BUSINESS_FAIL("1","业务异常"),

	SYSTEM_FAIL("2","系统异常");



	public String value;

	public String memo;

	ResponseCode(String value, String memo) {
		
		this.value = value;
		this.memo = memo;
	}

}
