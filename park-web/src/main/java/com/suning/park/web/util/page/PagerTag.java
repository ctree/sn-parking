package com.suning.park.web.util.page;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * 自定义分页标签
 * 
 * @author zhsj
 *
 */
public class PagerTag extends TagSupport {

	private static final long serialVersionUID = 540981812287320468L;

	private String url;// 请求url

	private int pageNo = 1;// 当前页码

	private int totalPage;

	private static String CLASS_PREV = "prev";
	private static String CLASS_NEXT = "next";
	private static String CLASS_DISABLED = "disabled";
	private static String CLASS_ACTIVE = "active";

	public int doStartTag() throws JspException {

		// 拼写要输出到页面的HTML文本
		StringBuilder sb = new StringBuilder();
		sb.append("<div class=\"dataTables_paginate paging_bootstrap pagination\">");
		sb.append("<ul class=\"pagination\">");

		// 上一页
		if (pageNo == 1) {
			sb.append(this.getLiStr(CLASS_PREV + " " + CLASS_DISABLED,
					this.getHrefStr(pageNo, "上一页",false)));
		} else {
			sb.append(this.getLiStr(CLASS_PREV,
					this.getHrefStr(pageNo-1, "上一页",true)));
		}

		// 页码部分
		List<Integer> pageNoList = this.getPageNoList(pageNo, totalPage);
		for (Integer pgNo : pageNoList) {
			if (pgNo.intValue() == pageNo) {
				sb.append(this.getLiStr(CLASS_ACTIVE,
						this.getHrefStr(pgNo, null,true)));
			} else {
				sb.append(this.getLiStr(null, this.getHrefStr(pgNo, null,true)));
			}
		}

		// 下一页
		if (pageNo == totalPage || totalPage==0) {
			sb.append(this.getLiStr(CLASS_NEXT + " " + CLASS_DISABLED,
					this.getHrefStr(pageNo, "下一页",false)));
		} else {
			sb.append(this.getLiStr(CLASS_NEXT, this.getHrefStr(pageNo+1, "下一页",true)));
		}

		try {
			pageContext.getOut().println(sb.toString());
		} catch (IOException e) {
			throw new JspException(e);
		}
		return SKIP_BODY; // 本标签主体为空,所以直接跳过主体
	}

	/**
	 * 获取跳转url
	 * 
	 * @param currentPage
	 * @param isShowHref 是否显示超链接内的具体链接地址 
	 * @return
	 */
	private String getHrefStr(int currentPage, String showStr,boolean isShowHref) {
		if(isShowHref==false){
			return "<a href=\"javascript:void(0)\">"+showStr+"</a>";
		}else{
			StringBuilder sb = new StringBuilder("<a href=\"");
			sb.append(this.url).append("?currentPage=").append(currentPage);
			sb.append("\">");
			sb.append(null == showStr ? currentPage : showStr);
			sb.append("</a>");
			return sb.toString();
		}
		
	}

	/**
	 * 获取li
	 * 
	 * @param classStr
	 * @param content
	 * @return
	 */
	private String getLiStr(String classStr, String content) {
		StringBuilder sb = new StringBuilder("<li");
		if (null != classStr) {
			sb.append(" class=\"").append(classStr).append("\"");
		}
		sb.append(">");
		sb.append(content);
		sb.append("</li>");
		return sb.toString();
	}

	/**
	 * 获取所有显示的页码
	 * 
	 * @param pageNo
	 * @param totalPage
	 * @return
	 */
	private List<Integer> getPageNoList(int pageNo, int totalPage) {
		List<Integer> pageNoList = new ArrayList<Integer>();
		switch (totalPage) {
		case 0:
			break;
		case 1:
			pageNoList.add(1);
			break;
		case 2:
			pageNoList.add(1);
			pageNoList.add(2);
			break;
		case 3:
			pageNoList.add(1);
			pageNoList.add(2);
			pageNoList.add(3);
			break;
		case 4:
			pageNoList.add(1);
			pageNoList.add(2);
			pageNoList.add(3);
			pageNoList.add(4);
			break;
		case 5:
			pageNoList.add(1);
			pageNoList.add(2);
			pageNoList.add(3);
			pageNoList.add(4);
			pageNoList.add(5);
			break;
		default:
			// 大于5
			if(pageNo<4){
				pageNoList.add(1);
				pageNoList.add(2);
				pageNoList.add(3);
				pageNoList.add(4);
				pageNoList.add(5);
			}else {
				int subPage = totalPage - pageNo;
				switch (subPage) {
				case 0:
					pageNoList.add(pageNo - 4);
					pageNoList.add(pageNo - 3);
					pageNoList.add(pageNo - 2);
					pageNoList.add(pageNo - 1);
					pageNoList.add(pageNo);
					break;
				case 1:
					pageNoList.add(pageNo - 3);
					pageNoList.add(pageNo - 2);
					pageNoList.add(pageNo - 1);
					pageNoList.add(pageNo);
					pageNoList.add(pageNo + 1);
					break;
				default:
					pageNoList.add(pageNo - 2);
					pageNoList.add(pageNo - 1);
					pageNoList.add(pageNo);
					pageNoList.add(pageNo + 1);
					pageNoList.add(pageNo + 2);
					break;
				}
			}
			break;
		}
		return pageNoList;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

}
