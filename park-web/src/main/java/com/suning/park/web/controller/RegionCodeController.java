package com.suning.park.web.controller;

import com.suning.park.web.bean.Response;
import com.suning.park.web.service.RegionCodeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 地区编码信息服务
 * Created by Administrator on 2017/2/16.
 */
@Controller
@RequestMapping(value = "/regionCode", produces = {"application/json;charset=UTF-8"})
public class RegionCodeController {

    private static final Logger logger = LoggerFactory.getLogger(RegionCodeController.class);

    @Autowired
    RegionCodeService regionCodeService;

    /**
     * 获得地区省份列表
     *
     * @return
     */
    @RequestMapping(value = "/getProvinceList")
    public @ResponseBody String getProvinceList() {
        logger.info("RegionCodeController.getProvinceList");
        return Response.SUCCESS().put("regionCodeList", regionCodeService.getProvinceList()).toJson();
    }

    /**
     * 根据地区级别编码获得子节点
     *
     * @param levelCode 地区级别编码
     * @return
     */
    @RequestMapping(value = "/getCityListByLevelCode")
    public @ResponseBody String getCityListByLevelCode(@RequestParam String levelCode) {
        logger.info("RegionCodeController.getCityListByLevelCode, levelCode:{}", levelCode);
        return Response.SUCCESS().put("regionCodeList", regionCodeService.getCityListByLevelCode(levelCode)).toJson();
    }
}
