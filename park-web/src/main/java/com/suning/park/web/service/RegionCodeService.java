package com.suning.park.web.service;

import com.suning.park.admin.provider.IRegionCodeProvider;
import com.suning.park.dto.model.DDRegionCode;
import com.suning.rsf.spring.Reference;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2017/2/15.
 */
@Service
public class RegionCodeService {

    @Reference(contract = IRegionCodeProvider.class)
    IRegionCodeProvider regionCodeProvider;

    public List<DDRegionCode> getProvinceList() {
        return regionCodeProvider.getProvinceList();
    }

    public List<DDRegionCode> getCityListByLevelCode(String levelCode) {
        return regionCodeProvider.getCityListByLevelCode(levelCode);
    }
}
