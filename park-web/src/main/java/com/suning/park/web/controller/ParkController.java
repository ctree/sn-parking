package com.suning.park.web.controller;

import com.suning.park.web.bean.Response;
import com.suning.park.web.service.ParkService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 停车场业务服务
 * Created by Administrator on 2017/2/16.
 */
@Controller
@RequestMapping(value = "/park", produces = {"application/json;charset=UTF-8"})
public class ParkController {

    private static final Logger logger = LoggerFactory.getLogger(ParkController.class);

    @Autowired
    ParkService parkService;

    /**
     * 根据省市编码获得停车场列表
     *
     * @param provinceCode 省编码
     * @param cityCode     市编码
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/listByRegion")
    public @ResponseBody String listByRegion(
            @RequestParam String provinceCode,
            @RequestParam String cityCode) throws Exception {
        logger.info("ParkController.listByRegion, provinceCode:{}, cityCode:{}", provinceCode, cityCode);
        return Response.SUCCESS().put("parkList", parkService.selectByRegion(provinceCode, cityCode)).toJson();
    }

    /**
     * 根据停车场ID获得收费标准信息
     *
     * @param parkId 停车场ID
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/getFeeScale")
    public @ResponseBody String getFeeScale(@RequestParam String parkId) throws Exception {
        logger.info("ParkController.getFeeScale, parkId:{}", parkId);
        return Response.SUCCESS().putAll(parkService.getFeeRuleByParkId(parkId)).toJson();
    }
}
