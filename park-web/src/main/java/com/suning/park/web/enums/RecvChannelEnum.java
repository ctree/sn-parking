package com.suning.park.web.enums;


import com.suning.park.web.util.StringUtil;

/**
 * 收费渠道
 */
public enum RecvChannelEnum {

	WX("0", "微信"),
	APP("1", "APP");

	private String value;

	private String text;

	/**
	 *
	 * @param value
	 * @param text
	 */
	RecvChannelEnum(final String value, final String text) {
		this.value = value;
		this.text = text;
	}

	public String getValue() {
		return value;
	}

	public String getText() {
		return text;
	}

	/**
	 * 
	 * @param oridal
	 * @return
	 */
	public static RecvChannelEnum get(int oridal) {
		for (RecvChannelEnum dot : RecvChannelEnum.values()) {
			if (oridal == dot.ordinal()) {
				return dot;
			}
		}
		throw new IllegalArgumentException("Can't get enum with this oridal.");
	}

	/**
	 * 根据value获取text
	 * 
	 * @param value
	 * @return
	 */
	public static String getText(String value) {
		for (RecvChannelEnum dot : RecvChannelEnum.values()) {
			if (value.equals(dot.getValue())) {
				return dot.getText();
			}
		}
		return "";
	}

	/**
	 * 检测value是否是当前枚举的类型之一,区分大小写
	 * 
	 * @param value
	 * @return
	 */
	public static boolean isValue(String value) {
		if (StringUtil.hasEmpty(value))
			return false;
		for (RecvChannelEnum dot : RecvChannelEnum.values()) {
			if (value.equals(dot.getValue())) {
				return true;
			}
		}
		return false;
	}

}
