package com.suning.park.web.controller;

import com.alibaba.druid.support.json.JSONUtils;
import com.suning.park.dto.model.UserCar;
import com.suning.park.web.bean.Response;
import com.suning.park.web.service.UserCarService;
import com.suning.park.web.util.JsonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 用户车辆关系服务
 * Created by Administrator on 2017/2/16.
 */
@Controller
@RequestMapping(value = "/userCar", produces = {"application/json;charset=UTF-8"})
public class UserCarController {

    private static final Logger logger = LoggerFactory.getLogger(UserCarController.class);

    @Autowired
    UserCarService userCarService;

    /**
     * 根据苏宁用户ID获得用户车辆关系信息列表
     * @param snUserId 苏宁用户ID
     * @return
     */
    @RequestMapping(value = "/getListByUserId")
    public @ResponseBody String getListByUserId(@RequestParam  String snUserId) {
        logger.info("UserCarController.getListByUserId, snUserId:{}", snUserId);
        return Response.SUCCESS().put("userCarList", userCarService.selectBySnUserId(snUserId)).toJson();
    }

    /**
     * 绑定车牌
     * @param userCar 用户车辆关系
     * @return
     */
    @RequestMapping(value = "/bindCarNum", method = RequestMethod.POST)
    public @ResponseBody String bindCarNum(UserCar userCar) {
        logger.info("UserCarController.bindCarNum, userCar:{}", JsonUtil.objectToJson(userCar));
        Assert.notNull(userCar);
        Assert.hasText(userCar.getCarNum());
        Assert.hasText(userCar.getSnUserId());
        Assert.hasText(userCar.getMemberNum());
        userCarService.insertUserCar(userCar);
        return Response.SUCCESS().toJson();
    }

    /**
     * 解绑车牌
     * @param id 用户车辆关系ID
     * @return
     */
    @RequestMapping(value = "/unbindCarNum", method = RequestMethod.POST)
    public @ResponseBody String unbindCarNum(@RequestParam String id) {
        logger.info("UserCarController.unbindCarNum, id:{}", id);
        userCarService.delete(id);
        return Response.SUCCESS().toJson();
    }
}
