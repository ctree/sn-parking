package com.suning.park.web.service;

import com.suning.park.admin.provider.IParkOrderProvider;
import com.suning.park.dto.model.ParkOrder;
import com.suning.rsf.spring.Reference;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2017/2/15.
 */
@Service
public class ParkOrderService {

    @Reference(contract = IParkOrderProvider.class)
    IParkOrderProvider parkOrderProvider;

    /**
     * 根据苏宁用户ID获得停车支付订单列表
     *
     * @param snUserId  苏宁用户ID
     * @param limitType 限制类型(0=7天，1=一个月，2=三个月)
     * @param pageNum   当前页码
     * @param pageSize  记录数量
     * @return 停车支付订单列表
     */
    public List<ParkOrder> selectByUserId(String snUserId, String limitType, Integer pageNum, Integer pageSize) {
        return parkOrderProvider.selectByUserId(snUserId, limitType, pageNum, pageSize);
    }

    /**
     * 根据苏宁用户ID和车牌号获得未支付订单
     *
     * @param snUserId 苏宁用户ID
     * @param carNum   车牌号
     * @return
     */
    public ParkOrder selectByNoPayAndCarNum(String snUserId, String carNum) {
        return parkOrderProvider.selectByNoPayAndCarNum(snUserId, carNum);
    }
}
