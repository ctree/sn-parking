package com.suning.park.web.service;

import com.suning.park.dto.model.Park;
import com.suning.park.admin.provider.IParkProvider;
import com.suning.rsf.spring.Reference;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/2/15.
 */
@Service
public class ParkService {
   // private IParkProvider parkProvider = ServiceLocator.getService(IParkProvider.class, "", false);

    @Reference(contract = IParkProvider.class)
    IParkProvider parkProvider;

    public List<Park> selectByRegion(String provinceCode, String cityCode) {
        return parkProvider.selectByRegion(provinceCode, cityCode);
    }

    public Map<String, Object> getFeeRuleByParkId(String parkId) {
        return parkProvider.getFeeRuleByParkId(parkId);
    }
}
