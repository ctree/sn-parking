package com.suning.park.web.service;

import com.suning.park.admin.provider.ICouponProvider;
import com.suning.park.admin.provider.IUserCarProvider;
import com.suning.park.dto.model.Coupon;
import com.suning.park.dto.model.UserCar;
import com.suning.rsf.spring.Reference;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2017/2/15.
 */
@Service
public class CouponService {

    @Reference(contract = ICouponProvider.class)
    ICouponProvider couponProvider;

    public List<Coupon> getCouponList(String parkId, String snUserId, String getType) {
        return couponProvider.getCouponList(parkId, snUserId, getType);
    }

    public void provideCoupon(String snUserId,String couponId) throws Exception {
        couponProvider.provideCoupon(snUserId, couponId);
    }
}
