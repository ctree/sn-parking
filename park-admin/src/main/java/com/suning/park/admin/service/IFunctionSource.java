package com.suning.park.admin.service;

import com.suning.park.dto.model.FunctionSource;
import com.suning.park.dto.model.FunctionSourceExample;
import com.suning.park.admin.databasespec.model.SpecFunctionSource;

import java.util.List;
import java.util.Map;

/**
 *  功能资源DAO服务接口
 *  @author niecheng
 */
public interface IFunctionSource {
    /**
     * 获取权限资源
     * @param functionSourceExample
     * @return
     */
    public List<FunctionSource> selectByExample(final FunctionSourceExample functionSourceExample);

    /**
     * 获取权限资源
     * @param params
     * @return
     */
    public List<com.suning.park.dto.model.FunctionSource> selectByMap(final Map<String, Object> params);

    /**
     * 查询所有资源信息(角色拥有的资源)
     * @param params
     * @return
     */
    List<SpecFunctionSource> selectSpecByMap(Map<String, Object> params);

    /**
     * 插入资源信息
     * @param functionSource
     */
    public void insertSelective(com.suning.park.dto.model.FunctionSource functionSource);

    /**
     * 动态更新
     * @param functionSource 更新内容
     * @param functionSourceExample 更新查询条件
     */
    public void updateByExampleSelective(com.suning.park.dto.model.FunctionSource functionSource, FunctionSourceExample functionSourceExample);
}
