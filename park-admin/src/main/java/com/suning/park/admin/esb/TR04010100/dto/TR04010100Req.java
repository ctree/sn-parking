package com.suning.park.admin.esb.TR04010100.dto;

import com.suning.park.admin.esb.MbfBodyReq;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by Administrator on 2017/1/17.
 */
@XStreamAlias("MbfBody")
public class TR04010100Req extends MbfBodyReq<TR04010100BodyReq> {

    private TR04010100BodyReq body = new TR04010100BodyReq();

    public TR04010100Req(String transCode) {
        super(transCode);
    }

    @Override
    public TR04010100BodyReq getBody() {
        return body;
    }

    @Override
    public void setBody(TR04010100BodyReq body) {
        this.body = body;
    }
}
