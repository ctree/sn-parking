package com.suning.park.admin.service.impl;

import com.suning.park.dto.model.*;
import com.suning.park.admin.mgr.permissions.service.FuncTreeService;
import com.suning.park.admin.base.BasicData;
import com.suning.park.admin.databasespec.model.SpecFunctionSource;
import com.suning.park.admin.service.ISysUserService;
import com.suning.park.admin.service.PermissionsAbstractService;
import com.suning.park.admin.util.CheckValue;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 用户DAO服务接口实现
 * @author niecheng
 */
@Service(value = "iUserServiceImpl")
@Transactional(propagation= Propagation.REQUIRED,rollbackFor=Exception.class)
public class ISysUserServiceImpl extends PermissionsAbstractService implements ISysUserService, BasicData {

    @Resource(name = "funcTreeServiceImpl")
    protected FuncTreeService funcTreeService;

    @Override
    public List<SysUser> selectByExample(SysUserExample sysUserExample) {
        return sysUserMapper.selectByExample(sysUserExample);
    }

    @Override
    public List<SysUser> queryUserInfo(Map<String, Object> params) {
        //TODO 用户信息查询
        return specSysUserMapper.selectByMap(params);
    }

    @Override
    public void updateUserInfo(SysUser sysUser) {
        //TODO 动态更新系统用户
        sysUserMapper.updateByPrimaryKeySelective(sysUser);

    }

    @Override
    public void deleteUser(String userId) {
        //TODO 删除系统用户
        sysUserMapper.deleteByPrimaryKey(userId);
    }

    @Override
    public void insertSelective(SysUser sysUser) {
        //TODO 插入用户信息
        sysUserMapper.insertSelective(sysUser);
    }

    @Override
    public List<SpecFunctionSource> getSpecFunctionSourceByUserId(String userId) {
        //TODO 获取当前用户权限拥有的资源
        List<SpecFunctionSource> saftList = new ArrayList<SpecFunctionSource>();
        List<Role> roles = null;
        List<UserRole> assUserRoles = getUserRole(userId);
        List<String> roleIds = getRoleIds(assUserRoles);
        if(CheckValue.isEmpty(roleIds)){
            saftList = new ArrayList<SpecFunctionSource>();
        }else {
            roles = getAssRoles(roleIds);
            SpecFunctionSource specAssFunctionSource = new SpecFunctionSource();
            specAssFunctionSource.setCode(AU_FUNC_TREE_SECOND_ID);
            saftList = getSpecFunctionSource(specAssFunctionSource, roles);
        }
        return saftList;
    }

    /**
     * 获取用户角色
     * @param userId 用户ID
     * @return
     */
    public List<UserRole> getUserRole(String userId){
        UserRoleExample userRoleExample = new UserRoleExample();
        UserRoleExample.Criteria criteria = userRoleExample.createCriteria();
        criteria.andUserIdEqualTo(userId);
        return iUserRoleService.selectByExample(userRoleExample);
    }

    /**
     * 获取用户关联角色ID集
     * @param userRoles 用户关联角色
     * @return
     */
    private List<String> getRoleIds(List<UserRole> userRoles){
        List<String> roleList = new ArrayList<String>();
        for(UserRole userRole : userRoles){
            roleList.add(userRole.getRoleId());
        }
        return roleList;
    }

    /**
     * 获取角色信息
     * @param roleIds 用户关联角色ID集
     * @return
     */
    public List<Role> getAssRoles(List<String> roleIds){
        RoleExample roleExample = new RoleExample();
        RoleExample.Criteria criteria = roleExample.createCriteria();
        criteria.andRoleIdIn(roleIds);
        return iRoleService.selectByExample(roleExample);
    }

    @Override
    public List<SpecFunctionSource> getSpecFunctionSource(SpecFunctionSource specFunctionSource, List<Role> roles){
        //TODO 获取角色资源
        return funcTreeService.selectSpecByParentCodeForRoleAuth(specFunctionSource, roles);
    }
}
