package com.suning.park.admin.mgr.reportform.service.impl;

import com.google.common.collect.Lists;
import com.suning.park.admin.enums.PayTypeEnum;
import com.suning.park.admin.util.page.PageInfo;
import com.suning.park.admin.databasespec.dao.SpecOrderPayMapper;
import com.suning.park.admin.databasespec.model.SpecOrderPay;
import com.suning.park.admin.mgr.reportform.model.SettlementListExcel;
import com.suning.park.admin.mgr.reportform.service.SettleMentService;
import com.suning.park.admin.util.BigDecimalUtil;
import com.suning.park.admin.util.excelTools.ExcelUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * Author:   niecheng
 * Date:     2017/2/13 22:20
 * Project:  park-pom
 * Description: //停车券结算报表模块业务实现
 * History: //修改记录
 */
@Service(value = "settleMentServiceImpl")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class SettleMentServiceImpl implements SettleMentService {

    private static final Logger logger = LoggerFactory.getLogger(SettleMentServiceImpl.class);

    @Autowired
    SpecOrderPayMapper specOrderPayMapper;

    @Override
    public PageInfo ajaxSettlementList(PageInfo pageInfo, String parkId, String startTimeStr, String endTimeStr, String payType) {
        //TODO 分页获取停车券结算信息
        final Map<String, Object> params = new HashMap<>();
        params.put("page", pageInfo);
        params.put("startTime", startTimeStr);
        params.put("endTime", endTimeStr);
        params.put("parkId", parkId);
        params.put("payType", payType);
        final List<SpecOrderPay> specOrderPays = specOrderPayMapper.selectByMap(params);
        final String total = specOrderPayMapper.queryCount(params);
        logger.info("明细长度：{} 总金额：{}", specOrderPays.size(), total);
        pageInfo.setResultObject(specOrderPays);
        return pageInfo;
    }

    @Override
    public byte[] exportSettlementList(String parkId, String startTimeStr, String endTimeStr, String payType, String parkName) {
        //TODO 优惠券结算Excel
        final Map<String, Object> params = new HashMap<>();
        params.put("startTime", startTimeStr);
        params.put("endTime", endTimeStr);
        params.put("parkId", parkId);
        params.put("payType", payType);
        final List<SpecOrderPay> specOrderPays = specOrderPayMapper.selectByMap(params);
        final List<Object> settlementListExcelData = getSettlementListExcelData(specOrderPays , parkName);
        String sheetName = "停车券结算报表";
        String[] headers = {"广场","日期","付款方式","金额"};
        return getByteArrayOutputStream(settlementListExcelData, headers, sheetName).toByteArray();
    }

    /**
     * 获取要下载的停车券明细
     * @return
     */
    private List<Object> getSettlementListExcelData(List<SpecOrderPay> specOrderPays, String parkName){
        List<Object> results = Lists.newArrayList();
        SettlementListExcel settlementListExcel = null;
        for (SpecOrderPay specOrderPay : specOrderPays){
            settlementListExcel = new SettlementListExcel();
            settlementListExcel.setParkName( parkName );
            settlementListExcel.setTime( specOrderPay.getSuccessTime() );
            settlementListExcel.setPayType( PayTypeEnum.getText( specOrderPay.getPayType()) );
            settlementListExcel.setAmount(BigDecimalUtil.toString( specOrderPay.getAmount()) );
            results.add(settlementListExcel);
    }
        return results;
    }

    /**
     * 导出数据字节缓存
     * @param dataList
     * @return
     */
    public ByteArrayOutputStream getByteArrayOutputStream(List<Object> dataList, String[] headers, String sheetName){
        ByteArrayOutputStream out = null;
        try {
            HSSFWorkbook workbook = ExcelUtil.exportExcelByListModel(sheetName, headers, dataList,null);
            out = new ByteArrayOutputStream();
            HSSFWorkbook hssWb = (HSSFWorkbook) workbook;
            hssWb.write(out);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return out;
    }
}
