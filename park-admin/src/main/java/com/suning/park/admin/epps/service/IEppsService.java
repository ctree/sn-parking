package com.suning.park.admin.epps.service;

import com.suning.park.admin.exception.BusinessFailException;

/**
 * Created by Administrator on 2017/1/13.
 */
public interface IEppsService {

    /**
     * 支付请求
     *
     * @param salerMerchantNo  每个车场一个易付宝会员编号
     * @param orderId 订单ID
     * @param goodsName 商品名称
     * @param orderAmount 订单金额
     * @param orderTime 订单创建时间
     */
    String testpayment(String salerMerchantNo, String orderId, String goodsName, String orderAmount, String orderTime)throws BusinessFailException;


    /**
     * 易付宝支付参数构建
     * @param parkId
     * @param orderId
     * @param goodsName
     * @param orderAmount
     * @param orderTime
     * @return
     * @throws BusinessFailException
     */
    String pay(String parkId, String orderId, String goodsName, String orderAmount, String orderTime) throws BusinessFailException;

    /**
     * 支付成功回调
     * @param orders  支付成功信息  json
     */
    void paySuccess(String orders);

    /**
     * 查询支付订单
     * @param salerMerchantNo
     * @param orderId
     * @param orderTime
     * @return 订单状态：支付成功"S"，订单关闭"C"，新建"N"
     */
    String selectOrder(String salerMerchantNo,String orderId,String orderTime)throws BusinessFailException;


    /**
     * 退款
     * @param merchantNo 商户编号
     * @param refundOrderId 退订订单ID
     * @param refundOrderTime 退款时间
     * @param refundAmount 退款金额
     * @param orderId 支付订单ID
     * @param orderTime 订单时间
     * @return
     */
    boolean refundOrder(String merchantNo,String refundOrderId,String refundOrderTime,String refundAmount,String orderId,String orderTime);


    /**
     * 退款通知
     * @param backOrderId 退款订单ID
     * @param backAmount 退款金额
     */
    void refundNotify(String backOrderId,String backAmount);

    /**
     * 退款订单查询
     * @param merchantNo 商户号
     * @param refundOrderNo 退款订单ID
     * @param refundOrderTime 退款订单时间
     * @return 00 退款处理中 01 退款成功 02：退款失败
     */
    String queryRefundState(String merchantNo, String refundOrderNo, String refundOrderTime)throws BusinessFailException;


}
