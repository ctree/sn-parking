package com.suning.park.admin.mgr.action.service;

import com.suning.park.dto.model.Action;
import com.suning.park.dto.model.ActionCoupon;
import com.suning.park.dto.model.SysUser;
import com.suning.park.admin.bean.ActionVo;
import com.suning.park.admin.util.page.PageInfo;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/1/18.
 */
public interface ActionService {

    void addAction(ActionVo actionVo, SysUser sysUser) throws Exception;

    void queryByPage(Map map, PageInfo pageInfo);

    Action getActionById(String id) throws Exception;

    void pause(String id) throws Exception;

    void stop(String id) throws Exception;

    void start(String id) throws Exception;

    Action queryActionById(String id) throws Exception;

    void handle(String ids, String type) throws Exception;

    List<ActionCoupon> queryActionCouponByActionId(String actionId);

    void editAction(ActionVo actionVo, SysUser currentUser) throws Exception;
}
