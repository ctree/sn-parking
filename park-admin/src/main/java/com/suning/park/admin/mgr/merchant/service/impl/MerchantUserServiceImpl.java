package com.suning.park.admin.mgr.merchant.service.impl;

import com.suning.park.admin.mgr.merchant.service.IMerchantUserService;
import com.suning.park.admin.util.Md5ConverterUtil;
import com.suning.park.dto.dao.MerchantUserMapper;
import com.suning.park.dto.model.MerchantUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 商户用户操作
 * Created by YL-Zhang on 2017/2/16.
 */
@Service
public class MerchantUserServiceImpl implements IMerchantUserService {

    private final static Logger logger = LoggerFactory.getLogger(MerchantUserServiceImpl.class);

    @Autowired
    MerchantUserMapper merchantUserMapper;


    /**
     * 添加商户用户账户，此方法会对用户密码加密
     * @param merchantUser 用户对象
     * @return
     */
    @Override
    @Transactional
    public int add(MerchantUser merchantUser){
        merchantUser.setPassword(Md5ConverterUtil.Md5(merchantUser.getPassword()));
        return  merchantUserMapper.insert(merchantUser);
    }

    @Override
    @Transactional
    public int update(MerchantUser merchantUser){
        return  merchantUserMapper.updateByPrimaryKeySelective(merchantUser);
    }

    /**
     * 修改用户密码
     * @param userId 用户ID
     * @param oldPwd 旧密码
     * @param newPwd 新密码
     * @return 成功返回“success” ，失败返回错误提示
     */
    @Override
    @Transactional
    public String  modifyPassword(String userId, String oldPwd, String newPwd){
        MerchantUser merchantUser=merchantUserMapper.selectByPrimaryKey(userId);
        if(merchantUser == null){
            return "用户不存在";
        }
        if(!merchantUser.getPassword().equals(Md5ConverterUtil.Md5(oldPwd))){
           return "旧密码不正确";
        }else{
            merchantUser.setPassword(Md5ConverterUtil.Md5(newPwd));
            merchantUserMapper.updateByPrimaryKeySelective(merchantUser);
            return "success";
        }
    }

}
