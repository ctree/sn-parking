package com.suning.park.admin.mgr.coupon.service;

import com.suning.park.dto.model.Coupon;
import com.suning.park.dto.model.SysUser;
import com.suning.park.admin.util.page.PageInfo;
import com.suning.park.admin.bean.CouponExp;
import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/1/16.
 */
public interface ICouponService {

    /***
     * 新增优惠券
     * @param couponExp
     * @throws Exception
     */
    void add(CouponExp couponExp,SysUser sysUser) throws Exception;

    /***
     * 修改优惠券
     * @param couponExp
     * @throws Exception
     */
    void edit(CouponExp couponExp,SysUser sysUser) throws Exception;

    /**
     * 优惠券详情
     * @param id
     * @return
     * @throws Exception
     */
    Coupon selectCouponById(String id) throws Exception;
    /**
     * 根据停车场iD查询优惠券列表
     * @param parkId
     * @return
     * @throws Exception
     */
    List<Coupon> selectCouponByParkId(String parkId) throws Exception;

    /**
     * 优惠券列表
     * @param map
     * @param page
     * @return
     */
    PageInfo selectByPage(Map map, PageInfo page);

    /**
     * 关联优惠券
     * @param userIds
     * @param couponId
     * @throws Exception
     */
    void provideCoupon(String userIds,String couponId) throws Exception;

    /**
     * 优惠券使用统计
     * @param map
     * @return
     */
    Map statisCoupon(Map map);

    void handle(String id,String type) throws Exception;

    /***
     * 根据id查询优惠券
     * @param id
     * @return
     * @throws Exception
     */
    Coupon queryCouponById(String id) throws Exception;

    /**
     * 根据停车场ID和苏宁用户ID获得优惠券信息列表
     * @param parkId 停车场ID
     * @param snUserId 苏宁用户ID
     * @param getType 查询类型(0=可以领取的，1=已经领取的)
     * @return
     */
    List<Coupon> selectByParkIdAndUserId(String parkId, String snUserId, String getType);


    int add(Coupon coupon);

    int modify(Coupon coupon);
}
