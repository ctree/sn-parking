package com.suning.park.admin.enums;

import com.suning.park.admin.util.StringUtil;

/**
 * 优惠券使用规则限制枚举
 * Created by YL-Zhang on 2017/2/16.
 */
public enum RuleTypeEnum {

    NOT("0", "不限制"),
    AMOUNT("1", "金额限制");
    private String value;

    private String text;

    /**
     *
     * @param value
     * @param text
     */
    RuleTypeEnum(final String value, final String text) {
        this.value = value;
        this.text = text;
    }

    public String getValue() {
        return value;
    }

    public String getText() {
        return text;
    }

    /**
     *
     * @param oridal
     * @return
     */
    public static RuleTypeEnum get(int oridal) {
        for (RuleTypeEnum dot : RuleTypeEnum.values()) {
            if (oridal == dot.ordinal()) {
                return dot;
            }
        }
        throw new IllegalArgumentException("Can't get enum with this oridal.");
    }

    /**
     * 根据value获取text
     *
     * @param value
     * @return
     */
    public static String getText(String value) {
        for (RuleTypeEnum dot : RuleTypeEnum.values()) {
            if (value.equals(dot.getValue())) {
                return dot.getText();
            }
        }
        return "";
    }

    /**
     * 检测value是否是当前枚举的类型之一,区分大小写
     *
     * @param value
     * @return
     */
    public static boolean isValue(String value) {
        if (StringUtil.hasEmpty(value))
            return false;
        for (RuleTypeEnum dot : RuleTypeEnum.values()) {
            if (value.equals(dot.getValue())) {
                return true;
            }
        }
        return false;
    }
}
