package com.suning.park.admin.esb.TR03010952.dto;

import com.suning.park.admin.esb.MbfBodyReq;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by Administrator on 2017/2/16.
 */
@XStreamAlias("MbfBody")
public class TR03010952Req extends MbfBodyReq<TR03010952BodyReq> {

    private TR03010952BodyReq body = new TR03010952BodyReq();

    public TR03010952Req(String transCode) {
        super(transCode);
    }

    @Override
    public TR03010952BodyReq getBody() {
        return body;
    }

    @Override
    public void setBody(TR03010952BodyReq body) {
        this.body = body;
    }
}
