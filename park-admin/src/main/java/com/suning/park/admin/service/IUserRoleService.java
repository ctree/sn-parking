package com.suning.park.admin.service;

import com.suning.park.dto.model.UserRole;
import com.suning.park.dto.model.UserRoleExample;

import java.util.List;

/**
 * 用户角色关系DAO服务接口
 * @author niecheng
 */
public interface IUserRoleService {

    /**
     * 获取用户角色信息
     * @param userRoleExample
     * @return
     */
    public List<UserRole> selectByExample(UserRoleExample userRoleExample);

    /**
     * 删除用户角色信息
     * @param userRoleExample
     */
    public void deleteByExample(UserRoleExample userRoleExample);

    /**
     * 插入用户角色信息
     * @param userRole
     */
    public void insertSelective(UserRole userRole);
}
