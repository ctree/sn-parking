package com.suning.park.admin.databasespec.model;

import com.suning.park.dto.model.Park;

/**
 * 停车场扩展实体
 */
public class SpecPark extends Park {

   private String propertyText;

   private String parkTypeText;

   private String businessEntityText;

    public String getPropertyText() {
        return propertyText;
    }

    public void setPropertyText(String propertyText) {
        this.propertyText = propertyText;
    }

    public String getParkTypeText() {
        return parkTypeText;
    }

    public void setParkTypeText(String parkTypeText) {
        this.parkTypeText = parkTypeText;
    }

    public String getBusinessEntityText() {
        return businessEntityText;
    }

    public void setBusinessEntityText(String businessEntityText) {
        this.businessEntityText = businessEntityText;
    }
}
