package com.suning.park.admin.esb.TA04010350.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.ArrayList;
import java.util.List;


public class TA04010350BodyRes {

    @XStreamAlias("cashMarginStructList")
    private List<CashMarginStruct> cashMarginStructList = new ArrayList<CashMarginStruct>();

    @XStreamAlias("lackStructList")
    private List<LackStruct> lackStructList = new ArrayList<LackStruct>();


    public List<CashMarginStruct> getCashMarginStructList() {
        return cashMarginStructList;
    }

    public void setCashMarginStructList(List<CashMarginStruct> cashMarginStructList) {
        this.cashMarginStructList = cashMarginStructList;
    }

    public List<LackStruct> getLackStructList() {
        return lackStructList;
    }

    public void setLackStructList(List<LackStruct> lackStructList) {
        this.lackStructList = lackStructList;
    }

    @XStreamAlias("cashMarginStruct")
    public static class CashMarginStruct{

        private String accountType;

        private String accountVarAmt;


        public String getAccountType() {
            return accountType;
        }

        public void setAccountType(String accountType) {
            this.accountType = accountType;
        }

        public String getAccountVarAmt() {
            return accountVarAmt;
        }

        public void setAccountVarAmt(String accountVarAmt) {
            this.accountVarAmt = accountVarAmt;
        }
    }

    @XStreamAlias("lackStruct")
    public static class LackStruct{

        private String accountType;

        private String accountVarAmt;

        public String getAccountType() {
            return accountType;
        }

        public void setAccountType(String accountType) {
            this.accountType = accountType;
        }

        public String getAccountVarAmt() {
            return accountVarAmt;
        }

        public void setAccountVarAmt(String accountVarAmt) {
            this.accountVarAmt = accountVarAmt;
        }
    }



    /**
     *
     * toString
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
