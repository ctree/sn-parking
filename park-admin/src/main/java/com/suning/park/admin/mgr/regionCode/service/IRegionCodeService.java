package com.suning.park.admin.mgr.regionCode.service;

import com.suning.park.dto.model.DDRegionCode;
import com.suning.park.dto.model.DDRegionCodeExample;
import com.suning.park.admin.util.page.PageInfo;

import java.util.List;
import java.util.Map;

/**
 * 地区编码相关
 * Created by Administrator on 2017/2/13.
 */
public interface IRegionCodeService {

    PageInfo queryByPage(final Map<String, Object> params, final PageInfo page);

    DDRegionCode selectByPrimaryKey(String id);

    List<DDRegionCode> selectByExample(DDRegionCodeExample example);

    List<DDRegionCode> listByProvince();

    List<DDRegionCode> listByLevelCode(String levelCode);
}
