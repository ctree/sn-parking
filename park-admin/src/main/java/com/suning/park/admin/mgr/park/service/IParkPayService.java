package com.suning.park.admin.mgr.park.service;

import com.suning.park.dto.model.ParkPay;
import com.suning.park.dto.model.ParkPayExample;
import com.suning.park.admin.util.page.PageInfo;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/1/13.
 */
public interface IParkPayService {

    PageInfo queryByPage(final Map<String, Object> params, final PageInfo page);

    void insert(ParkPay parkPay, String operatorId, String  operatorName);

    void delete(String id);

    void update(ParkPay parkPay, String operatorId, String  operatorName);

    ParkPay selectByPrimaryKey(String id);

    List<ParkPay> selectByExample(ParkPayExample example);
}
