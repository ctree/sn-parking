package com.suning.park.admin.esb.TR01030020.dto;

import com.suning.park.admin.esb.MbfBodyReq;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by Administrator on 2017/1/18.
 */
@XStreamAlias("MbfBody")
public class TR01030020Req extends MbfBodyReq<TR01030020BodyReq> {

    private TR01030020BodyReq body = new TR01030020BodyReq();

    public TR01030020Req(String transCode) {
        super(transCode);
    }

    @Override
    public TR01030020BodyReq getBody() {
        return body;
    }

    @Override
    public void setBody(TR01030020BodyReq body) {
        this.body = body;
    }
}
