package com.suning.park.admin.mgr.userCar.service.impl;

import com.suning.park.admin.enums.YesOrNoEnum;
import com.suning.park.admin.mgr.userCar.service.IUserCarService;
import com.suning.park.admin.util.IdBuilder;
import com.suning.park.dto.dao.UserCarMapper;
import com.suning.park.dto.model.UserCar;
import com.suning.park.dto.model.UserCarExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2017/2/16.
 */
@Service
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class UserCarServiceImpl implements IUserCarService {

    @Autowired
    UserCarMapper userCarMapper;

    @Override
    public void insert(UserCar userCar) {
        userCar.setId(IdBuilder.getID());
        userCar.setIsDelete(YesOrNoEnum.NO.getValue());
        userCar.setCreateDate(new Date());
        userCarMapper.insert(userCar);
    }

    @Override
    public void delete(String id) {
        UserCar userCar = new UserCar();
        userCar.setId(id);
        userCar.setIsDelete(YesOrNoEnum.YES.getValue());
        userCarMapper.updateByPrimaryKeySelective(userCar);
    }

    @Override
    public void update(UserCar userCar) {
        userCarMapper.updateByPrimaryKeySelective(userCar);
    }

    @Override
    public UserCar selectByPrimaryKey(String id) {
        return userCarMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<UserCar> selectByExample(UserCarExample example) {
        return userCarMapper.selectByExample(example);
    }
}
