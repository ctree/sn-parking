package com.suning.park.admin.databasespec.dao;

import com.suning.database.mybatis.SuperMapper;
import com.suning.park.admin.databasespec.model.SpecParkPay;
import com.suning.park.dto.model.Coupon;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 易付宝商户号配置扩展mapper
 */
public interface SpecCouponMapper extends SuperMapper {

    List<Coupon> selectByParkIdAndUserId(@Param("parkId") String parkId, @Param("snUserId") String snUserId, @Param("getType") String getType);
}
