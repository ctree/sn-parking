package com.suning.park.admin.bean;

import com.suning.park.dto.model.Coupon;

/**
 * Created by Administrator on 2017/1/20.
 */
public class CouponExp extends Coupon {

    private String startTimeStr;

    private String endTimeStr;

    public String getStartTimeStr() {
        return startTimeStr;
    }

    public void setStartTimeStr(String startTimeStr) {
        this.startTimeStr = startTimeStr;
    }

    public String getEndTimeStr() {
        return endTimeStr;
    }

    public void setEndTimeStr(String endTimeStr) {
        this.endTimeStr = endTimeStr;
    }
}
