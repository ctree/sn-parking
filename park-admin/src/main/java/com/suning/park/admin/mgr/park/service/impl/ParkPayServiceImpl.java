package com.suning.park.admin.mgr.park.service.impl;

import com.suning.park.dto.dao.ParkPayMapper;
import com.suning.park.dto.model.ParkPay;
import com.suning.park.dto.model.ParkPayExample;
import com.suning.park.admin.databasespec.dao.SpecParkPayMapper;
import com.suning.park.admin.databasespec.model.SpecParkPay;
import com.suning.park.admin.mgr.park.service.IParkPayService;
import com.suning.park.admin.util.IdBuilder;
import com.suning.park.admin.util.page.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/1/13.
 */
@Service
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class ParkPayServiceImpl implements IParkPayService {

    @Autowired
    ParkPayMapper parkPayMapper;
    @Autowired
    SpecParkPayMapper specParkPayMapper;

    @Override
    public PageInfo queryByPage(Map<String, Object> params, PageInfo page) {
        Assert.notNull(params, "params is must not null");
        Assert.notNull(page, "page is must not null");
        params.put("page", page);
        List<SpecParkPay> parkPays = specParkPayMapper.selectByMap(params);
        page.setResultObject(parkPays);
        return page;
    }

    @Override
    public void insert(ParkPay parkPay, String operatorId, String operatorName) {
        parkPay.setId(IdBuilder.getID());
        parkPay.setCreateId(operatorId);
        parkPay.setCreateName(operatorName);
        parkPay.setCreateTime(new Date());
        parkPay.setOperatorId(operatorId);
        parkPay.setOperatorName(operatorName);
        parkPay.setLastTime(new Date());
        parkPayMapper.insert(parkPay);
    }

    @Override
    public void delete(String id) {
        parkPayMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void update(ParkPay parkPay, String operatorId, String operatorName) {
        parkPay.setOperatorId(operatorId);
        parkPay.setOperatorName(operatorName);
        parkPay.setLastTime(new Date());
        parkPayMapper.updateByPrimaryKeySelective(parkPay);
    }

    @Override
    public ParkPay selectByPrimaryKey(String id) {
        return parkPayMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<ParkPay> selectByExample(ParkPayExample example) {
        return parkPayMapper.selectByExample(example);
    }
}
