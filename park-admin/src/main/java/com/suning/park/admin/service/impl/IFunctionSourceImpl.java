package com.suning.park.admin.service.impl;

import com.suning.park.dto.model.FunctionSource;
import com.suning.park.dto.model.FunctionSourceExample;
import com.suning.park.admin.databasespec.model.SpecFunctionSource;
import com.suning.park.admin.service.IFunctionSource;
import com.suning.park.admin.service.PermissionsAbstractService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 功能资源DAO服务接口实现
 * @author niecheng
 */
@Service(value = "iFunctionSourceImpl")
public class IFunctionSourceImpl extends PermissionsAbstractService implements IFunctionSource {

    @Override
    public List<FunctionSource> selectByExample(FunctionSourceExample functionSourceExample) {
        //TODO 获取权限资源
        return  functionSourceMapper.selectByExample(functionSourceExample);
    }

    @Override
    public List<FunctionSource> selectByMap(Map<String, Object> params) {
        //TODO 获取功能资源
        return specFunctionSourceMapper.selectByMap(params);
    }


    public List<SpecFunctionSource> selectSpecByMap(Map<String, Object> params) {
        //TODO 获取功能资源
        return specFunctionSourceMapper.selectSpecByMap(params);
    }

    @Override
    public void insertSelective(FunctionSource functionSource) {
        //TODO 插入功能资源
        functionSourceMapper.insertSelective(functionSource);
    }

    @Override
    public void updateByExampleSelective(com.suning.park.dto.model.FunctionSource functionSource, FunctionSourceExample functionSourceExample) {
        //TODO 更新功能资源
        functionSourceMapper.updateByExampleSelective(functionSource, functionSourceExample);
    }
}
