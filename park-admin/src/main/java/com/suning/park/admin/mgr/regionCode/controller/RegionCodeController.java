package com.suning.park.admin.mgr.regionCode.controller;

import com.suning.park.admin.mgr.regionCode.service.IRegionCodeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * 地理编码
 * Created by Administrator on 2017/1/17.
 */
@Controller
@RequestMapping(value = "/mgr/regionCode", produces = {"application/json;charset=UTF-8"})
public class RegionCodeController {

    private static final Logger logger = LoggerFactory.getLogger(RegionCodeController.class);

    @Resource
    IRegionCodeService regionCodeService;

    /**
     * 获得省份列表
     * @return
     */
    @RequestMapping(value = "/getProvinces", method = RequestMethod.GET)
    public @ResponseBody Object getProvinces() {
        try {
            return regionCodeService.listByProvince();
        } catch (Exception e) {
            logger.error("获得省份列表 程序异常, 失败信息：{}", new Object[]{e.getMessage()});
           return null;
        }
    }

    /**
     * 根据省级别编码获得市列表
     * @return
     */
    @RequestMapping(value = "/listByParentCode/{levelCode}", method = RequestMethod.GET)
    public @ResponseBody Object listByParentCode(@PathVariable("levelCode") String levelCode) {
        try {
            return regionCodeService.listByLevelCode(levelCode);
        } catch (Exception e) {
            logger.error("根据省级别编码获得市列表 程序异常, 失败信息：{}", new Object[]{e.getMessage()});
            return null;
        }
    }
}
