package com.suning.park.admin.databasespec.dao;

import com.suning.database.mybatis.SuperMapper;
import com.suning.park.admin.databasespec.model.SpecParkPay;

import java.util.List;
import java.util.Map;

/**
 * 易付宝商户号配置扩展mapper
 */
public interface SpecParkPayMapper extends SuperMapper {

    List<SpecParkPay> selectByMap(Map<String, Object> params);
}
