package com.suning.park.admin.provider.impl;

import com.suning.park.admin.mgr.regionCode.service.IRegionCodeService;
import com.suning.park.admin.provider.IParkProvider;
import com.suning.park.admin.provider.IRegionCodeProvider;
import com.suning.park.dto.model.DDRegionCode;
import com.suning.rsf.provider.annotation.Implement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户车辆关系服务实现
 * Created by Administrator on 2017/2/16.
 */
@Service
@Implement(contract = IRegionCodeProvider.class)
public class RegionCodeProvider implements IRegionCodeProvider {

    private static final Logger logger = LoggerFactory.getLogger(RegionCodeProvider.class);

    @Autowired
    IRegionCodeService regionCodeService;

    @Override
    public List<DDRegionCode> getProvinceList() {
        logger.info("ParkProvider.getProvinceList");
        return regionCodeService.listByProvince();
    }

    @Override
    public List<DDRegionCode> getCityListByLevelCode(String levelCode) {
        logger.info("ParkProvider.getCityListByLevelCode, levelCode:{}", levelCode);
        return regionCodeService.listByLevelCode(levelCode);
    }
}
