package com.suning.park.admin.mgr.reportform.model;

/*
 * Author:   niecheng
 * Date:     2017/2/10 14:37
 * Project:  park-pom
 * Description: //优惠券结算Excel数据
 * History: //修改记录
 */
public class SettlementListExcel {

    String parkName; //广场/停车场名称

    String time; //结算时间

    String payType; //支付方式

    String amount; //实付金额

    public String getParkName() {
        return parkName;
    }

    public void setParkName(String parkName) {
        this.parkName = parkName;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
