package com.suning.park.admin.esb.TR01030020;

import com.suning.park.admin.bean.Content;
import com.suning.park.admin.esb.TR01030020.dto.TR01030020Req;
import com.suning.park.admin.esb.TR01030020.dto.TR01030020Res;
import com.suning.rsc.RscException;
import com.suning.rsc.dto.responsedto.MbfResponse;
import com.suning.rsc.httpservice.annotation.EsbEIHttpMethod;
import com.suning.rsc.httpservice.annotation.EsbEIHttpService;

/**
 * 会员账户类型查询
 * Created by Administrator on 2017/1/17.
 */
@EsbEIHttpService(appCode = Content.APP_CODE, serviceCode = Content.CMF_MEMBERINFO_MGMT)
public interface CMFMemberInfoMgmtService {


    @EsbEIHttpMethod(operation = Content.QUERY_MEMBER_TYPE, serviceResource = "/", reqMbfBodyNode = false,
            requestBodyClass = TR01030020Req.class, responseBodyClass = TR01030020Res.class, timeOut = 1000, soTimeOut = 5000)
    MbfResponse queryMemberType(TR01030020Req dto) throws RscException;

}
