package com.suning.park.admin.databasespec.dao;

import com.suning.database.mybatis.SuperMapper;
import com.suning.park.dto.model.BaseData;

import java.util.List;
import java.util.Map;

/**
 * 优惠券查询拓展
 */
public interface SpecCouponUserMapper extends SuperMapper {

    /***
     * 优惠券使用统计
     * @param params
     * @return
     */
    Map statisCoupon(Map<String, Object> params);
}
