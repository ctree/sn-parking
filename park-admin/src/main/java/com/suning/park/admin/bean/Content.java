package com.suning.park.admin.bean;

/**
 * Created by Administrator on 2017/1/17.
 */
public class Content {

    public final static String APP_CODE = "PARK";

    //查询会员类型 服务码
    public final static String CMF_MEMBERINFO_MGMT = "CMFMemberInfoMgmt";

    //查询会员类型 操作码
    public final static String QUERY_MEMBER_TYPE = "queryMemberType";

    //查询积分账户余额 服务码
    public final static String POINT_ACCOUNT_POINTS_MGMT = "PointAccountPointsMgmt";

    //查询会员类型 操作码
    public final static String QUERY_POINTS_BALANCE = "queryPointsBalance";

    //退货账户交易 服务码
    public final static String MEMBER_ACCOUNT_MGMT = "MemberAccountMgmt";

    //账户回滚 操作码
    public final static String ROLLBACK_ACCOUNT = "rollbackAccount";

    //退货账户交易 操作码
    public final static String UPDATE_ACCOUNT_RESOURCE_WHENRETURNGOODS = "updateAccountResourceWhenReturnGoods";

    //创建线下账号（基于会员编号） 服务码
    public final static String MEMBER_CARD_ACCOUNT_MGMT = "MemberCardAccountMgmt";

    //创建线下账号（基于会员编号） 操作码
    public final static String CRT_OFFLINE_ACNT_WITH_CUSTNUMACARDNO = "crtOfflineAcntWithCustNumACardNo";

    //购物使用 操作码
    public final static String PM_MGMT = "PEMgmt";

    //购物使用 操作码
    public final static String ORDER_POINT_USE = "orderPointUse";

    //CMF系统编码
    public final static String DESTSYSTEMNO = "139000000080";
    //会员中心PARK系统编码
    public final static String SOURCE_SYSTEMNO = "139000002270";

    //别名类型
    public final static String ALIASTYPE = "138000000010";

    //业态类型
    public final static String ECO_TYPE = "140000000040";
    //账户类型
    public final static String ACCOUNT_TYPE = "8012";
    //订单类型
    public final static String ORDER_TYPE = "PARK001";
    //个人会员CODE
    public final static String SIMPLE_PEOPLE = "145000000010";
    //团体会员CODE
    public final static String TEAM = "145000000020";
    //苏宁积分发放主体编码
    public final static String SN_SUPPLIER_TYPE = "209000000010";
    //c店积分发放主体编码
    public final static String CLIENT_SUPPLIER_TYPE = "209000000020";
    //购物场景编码
    public final static String SCENECODE = "PE1120000560";

    public final static String TR01030020REQ_CODE = "TR01030020";//会员类型查询CODE
    public final static String TR04010100REQ_CODE = "TR04010100";//会员积分查询CODE
    public final static String TA0301020501REQ_CODE = "TA0301020501";//创建线下会员交易CODE
    public final static String TA04010350REQ_CODE = "TA04010350";//会员云钻退货CODE
    public final static String TA04010360REQ_CODE = "TA04010360";//账户操作回滚CODE
    public final static String TR03010952REQ_CODE = "TR03010952";//购物使用CODE


}
