package com.suning.park.admin.esb.TA04010360.dto;

import com.suning.park.admin.esb.MbfBodyRes;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by Administrator on 2017/2/15.
 */
@XStreamAlias("MbfBody")
public class TA04010360Res extends MbfBodyRes<TA04010360BodyRes> {

    private TA04010360BodyRes body;

    @Override
    public TA04010360BodyRes getBody() {
        return body;
    }

    @Override
    public void setBody(TA04010360BodyRes body) {
        this.body = body;
    }

}
