package com.suning.park.admin.databasespec.model;

import com.suning.park.dto.model.OrderPay;

/*
 * Author:   niecheng
 * Date:     2017/2/13 22:30
 * Project:  park-pom
 * Description: //支付明细自定义Model
 * History: //修改记录
 */
public class SpecOrderPay extends OrderPay {

    String parkId; //停车场Is

    String successTime; //支付成功时间

    String couponAmount; //优惠券面值

    public String getCouponAmount() {
        return couponAmount;
    }

    public void setCouponAmount(String couponAmount) {
        this.couponAmount = couponAmount;
    }

    public String getSuccessTime() {
        return successTime;
    }

    public void setSuccessTime(String successTime) {
        this.successTime = successTime;
    }

    public String getParkId() {
        return parkId;
    }

    public void setParkId(String parkId) {
        this.parkId = parkId;
    }
}
