package com.suning.park.admin.esb.TR03010952.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/2/16.
 */
public class TR03010952BodyRes {

    private String orderId;

    @XStreamAlias("orderItem")
    private List<Order> orderItem = new ArrayList<Order>();


    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public List<Order> getOrderItem() {
        return orderItem;
    }

    public void setOrderItem(List<Order> orderItem) {
        this.orderItem = orderItem;
    }

    @XStreamAlias("order")
    public static class Order{
        private String orderItemId;

        private String isSucc;

        private String errorCode;

        private String errorDesc;

        public String getOrderItemId() {
            return orderItemId;
        }

        public void setOrderItemId(String orderItemId) {
            this.orderItemId = orderItemId;
        }

        public String getIsSucc() {
            return isSucc;
        }

        public void setIsSucc(String isSucc) {
            this.isSucc = isSucc;
        }

        public String getErrorCode() {
            return errorCode;
        }

        public void setErrorCode(String errorCode) {
            this.errorCode = errorCode;
        }

        public String getErrorDesc() {
            return errorDesc;
        }

        public void setErrorDesc(String errorDesc) {
            this.errorDesc = errorDesc;
        }
    }
}
