package com.suning.park.admin.mgr.userManager.controller;

import com.suning.park.dto.model.SysUser;
import com.suning.park.admin.util.page.PageInfo;
import com.suning.park.admin.util.page.PageResult;
import com.suning.park.admin.base.BasicData;
import com.suning.park.admin.bean.Response;
import com.suning.park.admin.mgr.userManager.service.SysUserManagerService;
import com.suning.park.admin.util.CheckValue;
import com.suning.park.admin.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/*
 * Author:   niecheng
 * Date:     2017/1/16 18:48
 * Project:  park-pom
 * Description: 系统用户管理模块
 */
@Controller
@RequestMapping(value = "/mgr/sysUserManager")
public class SysUserManagerController implements BasicData {

    private static final Logger logger = LoggerFactory.getLogger(SysUserManagerController.class);

    @Resource(name = "sysUserManagerServiceImpl")
    SysUserManagerService sysUserManagerService;

    /**
     * 查询用户信息
     *
     * @param request
     * @param draw
     * @param pageInfo      分页信息
     * @param sysUser       用户信息
     * @return
     */
    @RequestMapping(value = "/ajaxList", method = RequestMethod.POST)
    public
    @ResponseBody
    PageResult uesrlist(HttpServletRequest request, @RequestParam(value = "draw") int draw, PageInfo pageInfo, SysUser sysUser) {
        logger.info("查询用户信息，当前用户ID:{}， 用户账号：{}", new Object[]{sysUser.getUserId(), sysUser.getLogin()});
        pageInfo = sysUserManagerService.queryByPage(sysUser, pageInfo);
        return new PageResult(pageInfo, draw);
    }

    /**
     * 添加系统用户
     * @return
     */
    @ResponseBody
    @RequestMapping(value="/add", method = RequestMethod.POST)
    public String add(SysUser sysUser){
        logger.info("添加用户");
        if(CheckValue.isEmpty(sysUser)){
            return Response.FAIL("1", "参数错误").toJson();
        }
        sysUserManagerService.add(sysUser, new Date());
        return Response.SUCCESS().toJson();
    }

    /**
     * 设置系统用户状态
     * @param userId 系统用户ID
     * @param state 设置状态
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/setUserState", method = RequestMethod.POST)
    public String setUserState( @RequestParam(value = "userId", required = true) String userId ,
                               @RequestParam(value = "state", required = true) String state){
        logger.info("用户状态设置");
        if(StringUtil.hasEmpty(userId, state)){
            return Response.FAIL("1", "参数错误").toJson();
        }
        sysUserManagerService.setUserState(userId, state, new Date());
        return Response.SUCCESS().toJson();
    }

    /**
     * 系统用户编辑信息查看
     * @param view
     * @return
     */
    @RequestMapping(value = "/userEditInfo", method = RequestMethod.GET)
    public ModelAndView userEditInfo(ModelAndView view, HttpServletRequest request ,
                              @RequestParam(value = "userId", required = true) String userId ) {
        logger.info("系统用户编辑信息查看");
        if( CheckValue.isEmpty(userId) ){
            logger.error("系统用户编辑信息查看参数ID:{} 为空, 调用错误", userId);
            view.setViewName("userManager/userList");
        }
        SysUser sysUser = sysUserManagerService.getSysUser(userId);
        view.addObject("sysUser", sysUser);
        view.setViewName("userManager/userEdit");
        return view;
    }

    /**
     * 更新系统用户
     * @return
     */
    @ResponseBody
    @RequestMapping(value="/updateUserInfo", method = RequestMethod.POST)
    public String updateUserInfo(SysUser sysUser){
        logger.info("更新系统用户");
        if(CheckValue.isEmpty(sysUser)){
            return Response.FAIL("1", "参数错误").toJson();
        }
        sysUserManagerService.updateSysUser(sysUser, new Date());
        return Response.SUCCESS().toJson();
    }


}
