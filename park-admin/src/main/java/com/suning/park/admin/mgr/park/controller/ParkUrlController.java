package com.suning.park.admin.mgr.park.controller;

import com.suning.park.dto.model.ParkUrl;
import com.suning.park.dto.model.SysUser;
import com.suning.park.admin.bean.Response;
import com.suning.park.admin.util.JsonUtil;
import com.suning.park.admin.util.page.PageInfo;
import com.suning.park.admin.util.page.PageResult;
import com.suning.park.admin.base.IAbstractController;
import com.suning.park.admin.mgr.park.service.IParkUrlService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * 停车场本地系统访问地址
 * Created by Administrator on 2017/2/10.
 */
@Controller
@RequestMapping(value = "/mgr/parkUrl", produces = {"application/json;charset=UTF-8"})
public class ParkUrlController extends IAbstractController {

    private static final Logger logger = LoggerFactory.getLogger(ParkUrlController.class);

    @Resource
    IParkUrlService parkUrlService;

    /**
     * 列表页面跳转
     * @param view
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(ModelAndView view) {
        logger.info("停车场本地系统访问地址列表页面跳转");
        view.setViewName("parkUrl/list");
        return view;
    }

    /**
     * 添加页面跳转
     * @param view
     * @return
     */
    @RequestMapping(value = "/addUI", method = RequestMethod.GET)
    public ModelAndView add(ModelAndView view) {
        logger.info("添加停车场本地系统访问地址页面跳转");
        view.setViewName("parkUrl/add");
        return view;
    }

    /**
     * 编辑页面跳转
     * @param view
     * @param id
     * @return
     */
    @RequestMapping(value = "/editUI/{id}", method = RequestMethod.GET)
    public ModelAndView editUI(ModelAndView view, @PathVariable("id") String id) throws Exception {
        logger.info("编辑停车场本地系统访问地址页面跳转，ID：{}", id);
        view.addObject("object", parkUrlService.selectByPrimaryKey(id));
        view.setViewName("parkUrl/edit");
        return view;
    }

    /**
     * 删除停车场本地系统访问地址
     * @param id
     * @return
     */
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public @ResponseBody String delete(String id) throws Exception {
        logger.info("删除停车场本地系统访问地址，ID{}",id);
        parkUrlService.delete(id);
        return Response.SUCCESS().toJson();
    }

    /**
     * 新增停车场本地系统访问地址
     * @param parkUrl
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public @ResponseBody String add(HttpServletRequest request, ParkUrl parkUrl) throws Exception {
        logger.info("新增停车场本地系统访问地址：{}", JsonUtil.objectToJson(parkUrl));
        SysUser sysUser = super.getCurrentUser(request);
        parkUrlService.insert(parkUrl, sysUser.getUserId(), sysUser.getName());
        return Response.SUCCESS().toJson();
    }

    /**
     * 编辑停车场本地系统访问地址
     * @param parkUrl
     * @return
     */
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public @ResponseBody String edit(HttpServletRequest request, ParkUrl parkUrl) throws Exception {
        logger.info("编辑停车场本地系统访问地址：{}", JsonUtil.objectToJson(parkUrl));
        SysUser sysUser = super.getCurrentUser(request);
        parkUrlService.update(parkUrl, sysUser.getUserId(), sysUser.getName());
        return Response.SUCCESS().toJson();
    }

    /**
     * 获得列表
     * @param draw
     * @param pageInfo
     * @param parkId
     * @return
     */
    @RequestMapping(value = "/ajaxList")
    public @ResponseBody
    PageResult ajaxList(
            @RequestParam(value = "draw") int draw, PageInfo pageInfo,
            @RequestParam(value = "parkId", required = false) String parkId) throws Exception {
        logger.info("停车场本地系统访问地址列表查询， 停车场Id：{}", parkId);
        Map<String, Object> params = new HashMap<>();
        if (StringUtils.isNotBlank(parkId)) {
            params.put("parkId", parkId.trim());
        }
        pageInfo = parkUrlService.queryByPage(params, pageInfo);
        return new PageResult(pageInfo, draw);
    }
}
