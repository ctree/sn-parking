package com.suning.park.admin.databasespec.dao;

import com.suning.database.mybatis.SuperMapper;
import com.suning.park.dto.model.SysUser;

import java.util.List;
import java.util.Map;

/**
 * 自定义系统用户DAO服务接口
 *
 * @author niecheng
 */
public interface SpecSysUserMapper extends SuperMapper {

    List<SysUser> selectByMap(Map<String, Object> params);
}
