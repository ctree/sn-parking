package com.suning.park.admin.esb.TR03010952.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/2/16.
 */
public class TR03010952BodyReq {

    private String transId;

    private String invokerCode;

    private String sceneType;

    private String sceneCode;

    private String custNum;

    private String ecoType;

    private String deviceId;

    private String orderId;

    @XStreamAlias("orderItem")
    private List<Order> orderItem = new ArrayList<Order>();

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public String getInvokerCode() {
        return invokerCode;
    }

    public void setInvokerCode(String invokerCode) {
        this.invokerCode = invokerCode;
    }

    public String getSceneType() {
        return sceneType;
    }

    public void setSceneType(String sceneType) {
        this.sceneType = sceneType;
    }

    public String getSceneCode() {
        return sceneCode;
    }

    public void setSceneCode(String sceneCode) {
        this.sceneCode = sceneCode;
    }

    public String getCustNum() {
        return custNum;
    }

    public void setCustNum(String custNum) {
        this.custNum = custNum;
    }

    public String getEcoType() {
        return ecoType;
    }

    public void setEcoType(String ecoType) {
        this.ecoType = ecoType;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public List<Order> getOrderItem() {
        return orderItem;
    }

    public void setOrderItem(List<Order> orderItem) {
        this.orderItem = orderItem;
    }

    @XStreamAlias("order")
    public static class Order{
        private String store;
        private String branch;
        private String orderItemId;
        private String orderAmt;
        private String cmdtyCode;
        private String cmdtyName;
        private String cmdtyCatalog;
        private String cmdtyBrand;
        private String cmdtyGroup;
        private String orderType;
        private String orderTypeDesc;
        private String supplierType;
        private String supplierCode;

        @XStreamAlias("subtractItem")
        private List<Subtract> subtractItem = new ArrayList<Subtract>();

        public String getStore() {
            return store;
        }

        public void setStore(String store) {
            this.store = store;
        }

        public String getBranch() {
            return branch;
        }

        public void setBranch(String branch) {
            this.branch = branch;
        }

        public String getOrderItemId() {
            return orderItemId;
        }

        public void setOrderItemId(String orderItemId) {
            this.orderItemId = orderItemId;
        }

        public String getOrderAmt() {
            return orderAmt;
        }

        public void setOrderAmt(String orderAmt) {
            this.orderAmt = orderAmt;
        }

        public String getCmdtyCode() {
            return cmdtyCode;
        }

        public void setCmdtyCode(String cmdtyCode) {
            this.cmdtyCode = cmdtyCode;
        }

        public String getCmdtyName() {
            return cmdtyName;
        }

        public void setCmdtyName(String cmdtyName) {
            this.cmdtyName = cmdtyName;
        }

        public String getCmdtyCatalog() {
            return cmdtyCatalog;
        }

        public void setCmdtyCatalog(String cmdtyCatalog) {
            this.cmdtyCatalog = cmdtyCatalog;
        }

        public String getCmdtyBrand() {
            return cmdtyBrand;
        }

        public void setCmdtyBrand(String cmdtyBrand) {
            this.cmdtyBrand = cmdtyBrand;
        }

        public String getCmdtyGroup() {
            return cmdtyGroup;
        }

        public void setCmdtyGroup(String cmdtyGroup) {
            this.cmdtyGroup = cmdtyGroup;
        }

        public String getOrderType() {
            return orderType;
        }

        public void setOrderType(String orderType) {
            this.orderType = orderType;
        }

        public String getOrderTypeDesc() {
            return orderTypeDesc;
        }

        public void setOrderTypeDesc(String orderTypeDesc) {
            this.orderTypeDesc = orderTypeDesc;
        }

        public String getSupplierType() {
            return supplierType;
        }

        public void setSupplierType(String supplierType) {
            this.supplierType = supplierType;
        }

        public String getSupplierCode() {
            return supplierCode;
        }

        public void setSupplierCode(String supplierCode) {
            this.supplierCode = supplierCode;
        }

        public List<Subtract> getSubtractItem() {
            return subtractItem;
        }

        public void setSubtractItem(List<Subtract> subtractItem) {
            this.subtractItem = subtractItem;
        }
    }

    @XStreamAlias("subtract")
    public static class Subtract{

        private String accountType;

        private BigDecimal subtractAmt;


        public String getAccountType() {
            return accountType;
        }

        public void setAccountType(String accountType) {
            this.accountType = accountType;
        }

        public BigDecimal getSubtractAmt() {
            return subtractAmt;
        }

        public void setSubtractAmt(BigDecimal subtractAmt) {
            this.subtractAmt = subtractAmt;
        }
    }
}
