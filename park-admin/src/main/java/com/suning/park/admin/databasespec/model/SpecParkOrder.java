package com.suning.park.admin.databasespec.model;

import com.suning.park.dto.model.OrderPay;
import com.suning.park.dto.model.ParkOrder;

import java.util.List;

/*
 * Author:   niecheng
 * Date:     2017/2/14 16:11
 * Project:  park-pom
 * Description: //支付订单自定义模型
 * History: //修改记录
 */
public class SpecParkOrder extends ParkOrder {

    String snUserId;

    public String getSnUserId() {
        return snUserId;
    }

    public void setSnUserId(String snUserId) {
        this.snUserId = snUserId;
    }

    List<OrderPay> orderPayList;

    public List<OrderPay> getOrderPayList() {
        return orderPayList;
    }

    public void setOrderPayList(List<OrderPay> orderPayList) {
        this.orderPayList = orderPayList;
    }
}
