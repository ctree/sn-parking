package com.suning.park.admin.enums;


import com.suning.park.admin.util.StringUtil;

/**
 * 支付方式
 * @author niecheng
 */
public enum PayTypeEnum {

	CASH("0", "现金"),
	COUPON("1", "优惠券");
	//待补充

	private String value;

	private String text;

	/**
	 *
	 * @param value
	 * @param text
	 */
	PayTypeEnum(final String value, final String text) {
		this.value = value;
		this.text = text;
	}

	public String getValue() {
		return value;
	}

	public String getText() {
		return text;
	}

	/**
	 * 
	 * @param oridal
	 * @return
	 */
	public static PayTypeEnum get(int oridal) {
		for (PayTypeEnum dot : PayTypeEnum.values()) {
			if (oridal == dot.ordinal()) {
				return dot;
			}
		}
		throw new IllegalArgumentException("Can't get enum with this oridal.");
	}

	/**
	 * 根据value获取text
	 * 
	 * @param value
	 * @return
	 */
	public static String getText(String value) {
		for (PayTypeEnum dot : PayTypeEnum.values()) {
			if (value.equals(dot.getValue())) {
				return dot.getText();
			}
		}
		return "";
	}

	/**
	 * 检测value是否是当前枚举的类型之一,区分大小写
	 * 
	 * @param value
	 * @return
	 */
	public static boolean isValue(String value) {
		if (StringUtil.hasEmpty(value))
			return false;
		for (PayTypeEnum dot : PayTypeEnum.values()) {
			if (value.equals(dot.getValue())) {
				return true;
			}
		}
		return false;
	}

}
