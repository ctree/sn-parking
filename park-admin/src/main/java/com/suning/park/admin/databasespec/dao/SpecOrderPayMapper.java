package com.suning.park.admin.databasespec.dao;

import com.suning.database.mybatis.SuperMapper;
import com.suning.park.admin.databasespec.model.SpecOrderPay;

import java.util.List;
import java.util.Map;

/*
* Author:   niecheng
* Date:     2017/2/13 22:26
* Project:  park-pom
* Description: //支付明细DAO接口
* History: //修改记录
*/
public interface SpecOrderPayMapper extends SuperMapper {

    List<SpecOrderPay> selectByMap(Map<String, Object> params);

    String queryCount(Map<String, Object> params);

}
