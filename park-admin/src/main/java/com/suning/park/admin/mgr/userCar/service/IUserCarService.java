package com.suning.park.admin.mgr.userCar.service;

import com.suning.park.dto.model.UserCar;
import com.suning.park.dto.model.UserCarExample;

import java.util.List;

/**
 * Created by Administrator on 2017/2/16.
 */
public interface IUserCarService {

    void insert(UserCar userCar);

    void delete(String id);

    void update(UserCar userCar);

    UserCar selectByPrimaryKey(String id);

    List<UserCar> selectByExample(UserCarExample example);

}
