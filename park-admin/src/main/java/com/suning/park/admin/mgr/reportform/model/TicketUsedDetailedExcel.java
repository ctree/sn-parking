package com.suning.park.admin.mgr.reportform.model;

/*
 * Author:   niecheng
 * Date:     2017/2/8 11:38
 * Project:  park-pom
 * Description: 停车券使用明细Excel数据
 * History:
 */
public class TicketUsedDetailedExcel {

    String snUserId; //会员ID

    String couponName; //券名称

    String actionName; //活动名称

    String getTime; //领券时间

    String useTime; //用券时间

    String state; //券状态

    public String getSnUserId() {
        return snUserId;
    }

    public void setSnUserId(String snUserId) {
        this.snUserId = snUserId;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getGetTime() {
        return getTime;
    }

    public void setGetTime(String getTime) {
        this.getTime = getTime;
    }

    public String getUseTime() {
        return useTime;
    }

    public void setUseTime(String useTime) {
        this.useTime = useTime;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
