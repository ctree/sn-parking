package com.suning.park.admin.mgr.basedata.service.impl;

import com.suning.park.dto.dao.BaseDataMapper;
import com.suning.park.dto.model.BaseData;
import com.suning.park.dto.model.BaseDataExample;
import com.suning.park.admin.databasespec.dao.SpecBaseDataMapper;
import com.suning.park.admin.mgr.basedata.service.IBaseDataService;
import com.suning.park.admin.util.IdBuilder;
import com.suning.park.admin.util.StringUtil;
import com.suning.park.admin.util.page.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/1/17.
 */
@Service
public class BaseDataServiceImpl implements IBaseDataService {

    @Autowired
    BaseDataMapper baseDataMapper;
    @Autowired
    SpecBaseDataMapper specBaseDataMapper;

    @Override
    public PageInfo queryByPage(Map<String, Object> params, PageInfo page) {
        Assert.notNull(params, "params is must not null");
        Assert.notNull(page, "page is must not null");
        params.put("page", page);
        List<BaseData> parks = specBaseDataMapper.selectByMap(params);
        page.setResultObject(parks);
        return page;
    }

    @Override
    public void insert(BaseData baseData) {
        baseData.setBaseId(IdBuilder.getID());
        baseData.setCreateTime(new Date());
        baseData.setUpdateTime(new Date());
        baseDataMapper.insert(baseData);
    }

    @Override
    public void delete(String baseId) {
        baseDataMapper.deleteByPrimaryKey(baseId);
    }

    @Override
    public void update(BaseData baseData) {
        baseData.setUpdateTime(new Date());
        baseDataMapper.updateByPrimaryKeySelective(baseData);
    }

    @Override
    public BaseData selectByPrimaryKey(String baseId) {
        return baseDataMapper.selectByPrimaryKey(baseId);
    }

    @Override
    public List<BaseData> selectByExample(BaseDataExample example) {
        return baseDataMapper.selectByExample(example);
    }

    @Override
    public List<BaseData> listByType(String type) {
        BaseDataExample example = new BaseDataExample();
        example.createCriteria().andTypeEqualTo(type);
        example.setOrderByClause("ORDER_CODE");
        return  baseDataMapper.selectByExample(example);
    }

    @Override
    public int countByIdAndTypeAndValue(String baseId, String type, String value) {
        BaseDataExample example = new BaseDataExample();
        BaseDataExample.Criteria criteria = example.createCriteria().andTypeEqualTo(type).andValueEqualTo(value);
        if(StringUtil.isNotBlank(baseId)){
            criteria.andBaseIdNotEqualTo(baseId);
        }
        return  baseDataMapper.countByExample(example);
    }
}
