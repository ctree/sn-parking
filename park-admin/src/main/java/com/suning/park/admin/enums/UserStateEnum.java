package com.suning.park.admin.enums;


import com.suning.park.admin.util.StringUtil;

/**
 * 用户状态
 * @author niecheng
 */
public enum UserStateEnum {

	NORMAL("0", "正常"),
	DELETE("1", "删除");
	private String value;

	private String text;

	/**
	 *
	 * @param value
	 * @param text
	 */
	UserStateEnum(final String value, final String text) {
		this.value = value;
		this.text = text;
	}

	public String getValue() {
		return value;
	}

	public String getText() {
		return text;
	}

	/**
	 * 
	 * @param oridal
	 * @return
	 */
	public static UserStateEnum get(int oridal) {
		for (UserStateEnum dot : UserStateEnum.values()) {
			if (oridal == dot.ordinal()) {
				return dot;
			}
		}
		throw new IllegalArgumentException("Can't get enum with this oridal.");
	}

	/**
	 * 根据value获取text
	 * 
	 * @param value
	 * @return
	 */
	public static String getText(String value) {
		for (UserStateEnum dot : UserStateEnum.values()) {
			if (value.equals(dot.getValue())) {
				return dot.getText();
			}
		}
		return "";
	}

	/**
	 * 检测value是否是当前枚举的类型之一,区分大小写
	 * 
	 * @param value
	 * @return
	 */
	public static boolean isValue(String value) {
		if (StringUtil.hasEmpty(value))
			return false;
		for (UserStateEnum dot : UserStateEnum.values()) {
			if (value.equals(dot.getValue())) {
				return true;
			}
		}
		return false;
	}

}
