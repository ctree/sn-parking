package com.suning.park.admin.util.epps;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Administrator on 2017/1/12.
 */
public class MD5 {
    private static ThreadLocal threadLocal = new ThreadLocal() {
        protected synchronized Object initialValue() {
            try {
                return MessageDigest.getInstance("MD5"); } catch (NoSuchAlgorithmException e) {
            }
            return null;
        }
    };

    public static MessageDigest getMessageDigest(){
        return ((MessageDigest)threadLocal.get());
    }

    public static String digest(String s, String charset) throws UnsupportedEncodingException {
        getMessageDigest().update(s.getBytes(charset));
        return HexUtil.bytes2Hexstr(getMessageDigest().digest());
    }
}
