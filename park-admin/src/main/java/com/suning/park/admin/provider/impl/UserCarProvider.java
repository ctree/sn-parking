package com.suning.park.admin.provider.impl;

import com.alibaba.druid.support.json.JSONUtils;
import com.suning.park.admin.enums.YesOrNoEnum;
import com.suning.park.admin.mgr.userCar.service.IUserCarService;
import com.suning.park.admin.provider.IRegionCodeProvider;
import com.suning.park.admin.provider.IUserCarProvider;
import com.suning.park.admin.util.JsonUtil;
import com.suning.park.dto.model.UserCar;
import com.suning.park.dto.model.UserCarExample;
import com.suning.rsf.provider.annotation.Implement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户车辆关系服务实现
 * Created by Administrator on 2017/2/16.
 */
@Service
@Implement(contract = IUserCarProvider.class)
public class UserCarProvider implements IUserCarProvider {

    private static final Logger logger = LoggerFactory.getLogger(UserCarProvider.class);

    @Autowired
    IUserCarService userCarService;

    @Override
    public List<UserCar> selectBySnUserId(String snUserId) {
        logger.info("UserCarProvider.selectBySnUserId, snUserId:{}", snUserId);
        UserCarExample example = new UserCarExample();
        example.createCriteria().andSnUserIdEqualTo(snUserId).andIsDeleteEqualTo(YesOrNoEnum.NO.getValue());
        example.setOrderByClause("CREATE_DATE DESC");
        return userCarService.selectByExample(example);
    }

    @Override
    public void insertUserCar(UserCar userCar) {
        logger.info("UserCarProvider.insertUserCar, userCar:{}", JsonUtil.objectToJson(userCar));
        userCarService.insert(userCar);
    }

    @Override
    public void delete(String id) {
        logger.info("UserCarProvider.deleteByCarNum, id:{}", id);
        userCarService.delete(id);
    }
}
