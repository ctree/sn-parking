package com.suning.park.admin.esb.TR01030020.dto;

/**
 * Created by Administrator on 2017/1/18.
 */
public class TR01030020BodyReq {

    private String custNum;

    private String aliasName;

    private String aliasType;

    private String cardNo;


    public String getCustNum() {
        return custNum;
    }

    public void setCustNum(String custNum) {
        this.custNum = custNum;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getAliasType() {
        return aliasType;
    }

    public void setAliasType(String aliasType) {
        this.aliasType = aliasType;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }
}
