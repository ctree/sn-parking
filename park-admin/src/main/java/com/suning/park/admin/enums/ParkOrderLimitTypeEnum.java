package com.suning.park.admin.enums;


import com.suning.park.admin.util.StringUtil;

/**
 * 停车支付订单记录限制类型
 */
public enum ParkOrderLimitTypeEnum {

	DAY_7("0", "7天"),
	MONTH_1("1", "1个月"),
	MONTH_3("2", "3个月");

	private String value;

	private String text;

	/**
	 *
	 * @param value
	 * @param text
	 */
	ParkOrderLimitTypeEnum(final String value, final String text) {
		this.value = value;
		this.text = text;
	}

	public String getValue() {
		return value;
	}

	public String getText() {
		return text;
	}

	/**
	 * 
	 * @param oridal
	 * @return
	 */
	public static ParkOrderLimitTypeEnum get(int oridal) {
		for (ParkOrderLimitTypeEnum dot : ParkOrderLimitTypeEnum.values()) {
			if (oridal == dot.ordinal()) {
				return dot;
			}
		}
		throw new IllegalArgumentException("Can't get enum with this oridal.");
	}

	/**
	 * 根据value获取text
	 * 
	 * @param value
	 * @return
	 */
	public static String getText(String value) {
		for (ParkOrderLimitTypeEnum dot : ParkOrderLimitTypeEnum.values()) {
			if (value.equals(dot.getValue())) {
				return dot.getText();
			}
		}
		return "";
	}

	/**
	 * 检测value是否是当前枚举的类型之一,区分大小写
	 * 
	 * @param value
	 * @return
	 */
	public static boolean isValue(String value) {
		if (StringUtil.hasEmpty(value))
			return false;
		for (ParkOrderLimitTypeEnum dot : ParkOrderLimitTypeEnum.values()) {
			if (value.equals(dot.getValue())) {
				return true;
			}
		}
		return false;
	}

}
