package com.suning.park.admin.provider.impl;

import com.suning.park.admin.mgr.merchant.service.IMerchantUserService;
import com.suning.park.admin.provider.IMerchantUserProvider;
import com.suning.park.admin.provider.IParkProvider;
import com.suning.park.admin.util.Md5ConverterUtil;
import com.suning.park.dto.dao.MerchantUserMapper;
import com.suning.park.dto.model.MerchantUser;
import com.suning.park.dto.model.MerchantUserExample;
import com.suning.rsf.provider.annotation.Implement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 商户用户远程调用
 * Created by YL-Zhang on 2017/2/16.
 */
@Service
@Implement(contract = IMerchantUserProvider.class)
public class MerchantUserProvider implements IMerchantUserProvider {

    private static final Logger logger = LoggerFactory.getLogger(MerchantUserProvider.class);

    @Autowired
    MerchantUserMapper merchantUserMapper;

    @Autowired
    IMerchantUserService merchantUserService;

    /**
     * 根据用户名和密码查找
     *
     * @param login    登录用户名
     * @param password 密码
     * @return
     */
    @Override
    public MerchantUser find(String login, String password) {
        logger.info("远程调用接收参数 login:{} password:{}", login, password);
        MerchantUserExample merchantUserExample = new MerchantUserExample();
        MerchantUserExample.Criteria criteria = merchantUserExample.createCriteria();
        criteria.andLoginEqualTo(login);
        criteria.andPasswordEqualTo(Md5ConverterUtil.Md5(password));
        List<MerchantUser> merchantUsers = merchantUserMapper.selectByExample(merchantUserExample);
        if (merchantUsers.size() < 1 || merchantUsers == null) {
            return null;
        } else {
            return merchantUsers.get(1);
        }
    }

    /**
     * 修改密码
     *
     * @param userId 用户ID
     * @param oldPwd 旧密码
     * @param newPwd 新密码
     * @return
     */
    @Override
    public String modifyPassword(String userId, String oldPwd, String newPwd) {
        logger.info("远程调用接收参数 userId:{} oldPwd:{} newPwd:{}", new Object[]{userId, oldPwd, newPwd});
        return merchantUserService.modifyPassword(userId, oldPwd, newPwd);
    }

}
