package com.suning.park.admin.mgr.park.controller;

import com.suning.park.dto.model.ParkPay;
import com.suning.park.dto.model.SysUser;
import com.suning.park.admin.base.IAbstractController;
import com.suning.park.admin.bean.Response;
import com.suning.park.admin.mgr.park.service.IParkPayService;
import com.suning.park.admin.util.JsonUtil;
import com.suning.park.admin.util.page.PageInfo;
import com.suning.park.admin.util.page.PageResult;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * 易付宝商户号配置
 * Created by Administrator on 2017/1/20.
 */
@Controller
@RequestMapping(value = "/mgr/parkPay", produces = {"application/json;charset=UTF-8"})
public class ParkPayController extends IAbstractController {

    private static final Logger logger = LoggerFactory.getLogger(ParkPayController.class);

    @Resource
    IParkPayService parkPayService;

    /**
     * 列表页面跳转
     * @param view
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(ModelAndView view) {
        logger.info("易付宝商户号配置列表页面跳转");
        view.setViewName("parkPay/list");
        return view;
    }

    /**
     * 添加页面跳转
     * @param view
     * @return
     */
    @RequestMapping(value = "/addUI", method = RequestMethod.GET)
    public ModelAndView add(ModelAndView view) {
        logger.info("添加易付宝商户号配置页面跳转");
        view.setViewName("parkPay/add");
        return view;
    }

    /**
     * 编辑页面跳转
     * @param view
     * @param id
     * @return
     */
    @RequestMapping(value = "/editUI/{id}", method = RequestMethod.GET)
    public ModelAndView editUI(ModelAndView view, @PathVariable("id") String id) throws Exception {
        logger.info("编辑易付宝商户号配置页面跳转，id：{}", id);
        view.addObject("object", parkPayService.selectByPrimaryKey(id));
        view.setViewName("parkPay/edit");
        return view;
    }

    /**
     * 删除易付宝商户号配置
     * @param id
     * @return
     */
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public @ResponseBody String delete(String id) throws Exception {
        logger.info("删除易付宝商户号配置，id：{}",id);
        parkPayService.delete(id);
        return Response.SUCCESS().toJson();
    }

    /**
     * 新增易付宝商户号配置
     * @param parkPay
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public @ResponseBody String add(HttpServletRequest request, ParkPay parkPay) throws Exception {
        logger.info("新增易付宝商户号配置：{}", JsonUtil.objectToJson(parkPay));
        SysUser sysUser = super.getCurrentUser(request);
        parkPayService.insert(parkPay, sysUser.getUserId(), sysUser.getName());
        return Response.SUCCESS().toJson();
    }

    /**
     * 编辑易付宝商户号配置
     * @param parkPay
     * @return
     */
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public  @ResponseBody String edit(HttpServletRequest request, ParkPay parkPay) throws Exception {
        logger.info("编辑易付宝商户号配置：{}", JsonUtil.objectToJson(parkPay));
        SysUser sysUser = super.getCurrentUser(request);
        parkPayService.update(parkPay, sysUser.getUserId(), sysUser.getName());
        return Response.SUCCESS().toJson();
    }

    /**
     * 获得列表
     * @param draw
     * @param pageInfo
     * @param parkId
     * @return
     */
    @RequestMapping(value = "/ajaxList")
    public @ResponseBody
    PageResult ajaxList(
            @RequestParam(value = "draw") int draw, PageInfo pageInfo,
            @RequestParam(value = "parkId", required = false) String parkId) throws Exception {
        logger.info("易付宝商户号配置列表查询， 停车场Id：{}", parkId);
        Map<String, Object> params = new HashMap<>();
        if (StringUtils.isNotBlank(parkId)) {
            params.put("parkId", parkId.trim());
        }
        pageInfo = parkPayService.queryByPage(params, pageInfo);
        return new PageResult(pageInfo, draw);
    }
}
