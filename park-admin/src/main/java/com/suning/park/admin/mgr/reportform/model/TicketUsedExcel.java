package com.suning.park.admin.mgr.reportform.model;

/*
 * Author:   niecheng
 * Date:     2017/2/8 11:38
 * Project:  park-pom
 * Description: 停车券使用Excel数据
 * History:
 */
public class TicketUsedExcel {

    String id; //券ID

    String ascription; //广场

    String couponName; //券名称

    String startTime; //券有效开始时间

    String endTime; //券有效结束时间

    String total; //券点数

    String obtained; //已领券

    String used; //已使用

    String unUsed; //未使用

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAscription() {
        return ascription;
    }

    public void setAscription(String ascription) {
        this.ascription = ascription;
    }

    public String getCouponName() {
        return couponName;
    }

    public void setCouponName(String couponName) {
        this.couponName = couponName;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getObtained() {
        return obtained;
    }

    public void setObtained(String obtained) {
        this.obtained = obtained;
    }

    public String getUsed() {
        return used;
    }

    public void setUsed(String used) {
        this.used = used;
    }

    public String getUnUsed() {
        return unUsed;
    }

    public void setUnUsed(String unUsed) {
        this.unUsed = unUsed;
    }
}
