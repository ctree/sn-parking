package com.suning.park.admin.mgr.ordermanager.controller;

import com.suning.park.dto.model.ParkOrder;
import com.suning.park.admin.databasespec.model.SpecParkUserOrder;
import com.suning.park.admin.base.BasicData;
import com.suning.park.admin.bean.Response;
import com.suning.park.admin.exception.BusinessFailException;
import com.suning.park.admin.mgr.ordermanager.service.OrderManagerService;
import com.suning.park.admin.util.CheckValue;
import com.suning.park.admin.util.DateUtil;
import com.suning.park.admin.util.StringUtil;
import com.suning.park.admin.util.page.PageInfo;
import com.suning.park.admin.util.page.PageResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;


/*
 * Author:   niecheng
 * Date:     2017/1/16 18:48
 * Project:  park-pom
 * Description: 订单管理模块
 */
@Controller
@RequestMapping(value = "/mgr/orderManager")
public class OrderManagerController implements BasicData {

    private static final Logger logger = LoggerFactory.getLogger(OrderManagerController.class);

    @Resource(name = "orderManagerServiceImpl")
    protected OrderManagerService orderManagerService;

    /**
     * 获取支付订单信息
     *
     * @param draw
     * @param pageInfo  分页信息
     * @param parkOrder 支付订单查询条件
     * @param queryTime 查询时间
     * @return
     */
    @RequestMapping(value = "/ajaxParkOrderList")
    @ResponseBody
    public PageResult ajaxParkOrderList(@RequestParam(value = "draw") int draw, PageInfo pageInfo, ParkOrder parkOrder, String queryTime) {
        if (!CheckValue.isEmpty(queryTime)) {
            try {
                final Date date = DateUtil.parseDateTime(queryTime, DateUtil.dateFormat);
                parkOrder.setSuccessTime(date);
            } catch (ParseException e) {
                logger.error("当前查询日期解析错误, 错误信息:{}", e.getMessage());
                throw new BusinessFailException("日期格式解析错误");
            }
        }
        pageInfo = orderManagerService.ajaxParkOrderList(pageInfo, parkOrder);
        return new PageResult(pageInfo, draw);
    }

    /**
     * 获取临停车辆收费日报表
     * @param draw
     * @param pageInfo
     * @param parkId 停车场ID
     * @param payType 收费方式
     * @param queryTime 查询时间
     * @return
     */
    @RequestMapping(value = "/ajaxPayOrderDailyList")
    @ResponseBody
    public PageResult ajaxPayOrderDailyList(@RequestParam(value = "draw") int draw, PageInfo pageInfo, String parkId, String payType,  String queryTime) {
        pageInfo = orderManagerService.ajaxParkOrderDailyList(pageInfo, parkId, payType, queryTime);
        return new PageResult(pageInfo, draw);
    }

    /**
     * 退款操作
     * @param id 订单编号
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/refund", method = RequestMethod.POST)
    public String refund(String id){
        return Response.SUCCESS().toJson();
    }

    /**
     * 支付明细
     * @param parkOrderId 订单编号
     * @param queryDate
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/orderPay", produces = {"text/javascript;charset=UTF-8"}, method = RequestMethod.POST)
    public String orderPay(String parkOrderId, String queryDate){
        final SpecParkUserOrder specParkUserOrder = orderManagerService.getOrderPay(parkOrderId, queryDate);
        return Response.SUCCESS().put(RESULT_DESC, specParkUserOrder).toJson();
    }

    /**
     * 导出临停车辆收费订单明细Excel
     * @param response
     * @param parkOrder
     * @param queryTime
     */
    @RequestMapping(value = "/exportPayOrderList", method = RequestMethod.GET)
    public void exportPayOrderList(HttpServletRequest request, HttpServletResponse response, ParkOrder parkOrder, String queryTime,
                                   @RequestParam(value = "carNum") String carNum){
        ServletOutputStream outputStream = null;
        try {
            if( !StringUtil.hasEmpty(queryTime) ) {
                final Date date = DateUtil.parseDateTime(queryTime, DateUtil.dateFormat);
                parkOrder.setSuccessTime(date);
            }
            byte[] bytes = orderManagerService.exportPayOrderList(parkOrder);
            response.setContentType("application/x-msdownload");
            response.setHeader("Content-Disposition", "attachment;filename=payOrder.xls");
            response.setContentLength(bytes.length);
            outputStream = response.getOutputStream();
            outputStream.write(bytes);
            outputStream.flush();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("下载临停车收费订单明细出错, 错误信息：{ }",e);
        }finally {
            if(outputStream != null){
                try {
                    outputStream.close();
                } catch (IOException e) {
                    logger.info("输出流关闭失败，错误信息：{}", e.getMessage());
                }
            }
        }
    }

}
