package com.suning.park.admin.mgr.userManager.service;

import com.suning.park.dto.model.SysUser;
import com.suning.park.admin.util.page.PageInfo;

import java.util.Date;

/*
 * Author:   niecheng
 * Date:     2017/1/16 21:51
 * Project:  park-pom
 * Description: 用户管理模块业务接口
 */
public interface SysUserManagerService {

    /**
     * 分页查询系统用户信息
     * @param sysUser 查询系统用户条件
     * @param pageInfo 分页条件
     * @return
     */
    PageInfo queryByPage(final SysUser sysUser, PageInfo pageInfo);

    /**
     * 添加系统用户
     * @param sysUser 系统用户
     * @param currentDate 当前时间
     */
    public void add(final SysUser sysUser, Date currentDate);

    /**
     * 系统用户状态设置
     * @param userId 系统用户ID
     * @param state 设置状态
     * @param currentDate 当前时间
     */
    public void setUserState(final String userId, final String state, final Date currentDate);

    /**
     * 系统用户信息查看
     * @param userId 用户ID
     * @return
     */
    public SysUser getSysUser(String userId);

    /**
     * 添加系统用户
     * @param sysUser 系统用户
     * @param currentDate 当前时间
     */
    public void updateSysUser(final SysUser sysUser, Date currentDate);

}
