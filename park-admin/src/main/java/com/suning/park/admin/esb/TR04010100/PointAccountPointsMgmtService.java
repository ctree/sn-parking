package com.suning.park.admin.esb.TR04010100;

import com.suning.park.admin.bean.Content;
import com.suning.park.admin.esb.TR04010100.dto.TR04010100Req;
import com.suning.park.admin.esb.TR04010100.dto.TR04010100Res;
import com.suning.rsc.RscException;
import com.suning.rsc.dto.responsedto.MbfResponse;
import com.suning.rsc.httpservice.annotation.EsbEIHttpMethod;
import com.suning.rsc.httpservice.annotation.EsbEIHttpService;

/**
 * 查询积分账户余额
 * <p/>
 * Created by Administrator on 2017/1/18.
 */
@EsbEIHttpService(appCode = Content.APP_CODE, serviceCode = Content.POINT_ACCOUNT_POINTS_MGMT)
public interface PointAccountPointsMgmtService {

    @EsbEIHttpMethod(operation = Content.QUERY_POINTS_BALANCE, serviceResource = "/", reqMbfBodyNode = false,
            requestBodyClass = TR04010100Req.class, responseBodyClass = TR04010100Res.class, timeOut = 1000, soTimeOut = 5000)
    MbfResponse queryPointsBalance(TR04010100Req dto) throws RscException;
}
