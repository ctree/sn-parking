package com.suning.park.admin.mgr.basedata.service;

import com.suning.park.dto.model.BaseData;
import com.suning.park.dto.model.BaseDataExample;
import com.suning.park.admin.util.page.PageInfo;

import java.util.List;
import java.util.Map;

/**
 * 基础数据相关
 * Created by Administrator on 2017/1/17.
 */
public interface IBaseDataService {

    PageInfo queryByPage(final Map<String, Object> params, final PageInfo page);

    void insert(BaseData baseData);

    void delete(String baseId);

    void update(BaseData baseData);

    BaseData selectByPrimaryKey(String baseId);

    List<BaseData> selectByExample(BaseDataExample example);

    List<BaseData> listByType(String type);

    int countByIdAndTypeAndValue(String baseId, String type, String value);
}
