package com.suning.park.admin.mgr.permissions.service;

import com.suning.park.dto.model.UserRole;
import com.suning.park.admin.util.page.PageInfo;
import org.springframework.ui.Model;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 授权业务接口
 * @author niecheng
 * @version 0.0.1
 */
public interface ValiService {

    /**
     * 查询用户授权列表信息
     * @param params 查询条件
     * @param page 分页信息
     * @return
     */
    public PageInfo queryByPage(final Map<String, Object> params, final PageInfo page);

    /**
     * 准备授权
     * @param model
     * @param userId 用户Id
     */
    public void roleUI(final Model model, final String userId);

    /**
     * 授权操作
     * @param userId 用户ID
     * @param roleIds 角色ID
     * @param currentDate 当前时间
     */
    public void saveRoleFunctree(String userId, String[] roleIds, Date currentDate);

    /**
     * 获取用户角色信息
     * @param useId
     * @return
     */
    public List<UserRole> getUserRole(String useId);
}
