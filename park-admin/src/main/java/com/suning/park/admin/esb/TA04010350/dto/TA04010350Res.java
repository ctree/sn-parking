package com.suning.park.admin.esb.TA04010350.dto;

import com.suning.park.admin.esb.MbfBodyRes;
import com.thoughtworks.xstream.annotations.XStreamAlias;


@XStreamAlias("MbfBody")
public class TA04010350Res extends MbfBodyRes<TA04010350BodyRes> {
    /**
     *
     * body
     */
    private TA04010350BodyRes body;

    /**
     *
     * 设置body
     */
    @Override
    public void setBody(TA04010350BodyRes body) {
        this.body = body;
    }

    /**
     *
     * 获取body
     */
    @Override
    public TA04010350BodyRes getBody() {
        return body;
    }

}
