package com.suning.park.admin.service;

import com.suning.park.dto.model.Role;
import com.suning.park.dto.model.RoleExample;

import java.util.List;
import java.util.Map;

/**
 * 角色DAO服务接口
 * @author niecheng
 */
public interface IRoleService {

    /**
     * 查询角色信息
     * @param assRoleExample
     * @return
     */
    public List<Role> selectByExample(RoleExample assRoleExample);

    /**
     * 获取角色信息
     * @param params
     * @return
     */
    public List<Role> selectByMap(final Map<String, Object> params);

    /**
     * 添加角色
     * @param role
     */
    public void insertSelective(Role role);

    /**
     * 更新角色
     * @param role 更新内容
     * @param roleExample 查询条件
     */
    public void updateByExampleSelective(Role role, RoleExample roleExample);

}
