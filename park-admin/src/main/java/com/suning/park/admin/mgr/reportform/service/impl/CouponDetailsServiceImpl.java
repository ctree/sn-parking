package com.suning.park.admin.mgr.reportform.service.impl;

import com.suning.park.admin.databasespec.dao.SpecParkOrderMapper;
import com.suning.park.admin.databasespec.model.SpecParkUserOrder;
import com.suning.park.admin.mgr.reportform.service.CouponDetailsService;
import com.suning.park.admin.util.JsonUtil;
import com.suning.park.admin.util.page.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
 * Author:   niecheng
 * Date:     2017/2/15 17:52
 * Project:  park-pom
 * Description: //模块目的、功能描述      
 * History: //修改记录
 */
@Service(value = "couponDetailsServiceImpl")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class CouponDetailsServiceImpl implements CouponDetailsService {

    private static final Logger logger = LoggerFactory.getLogger(CouponDetailsServiceImpl.class);

    @Autowired
    SpecParkOrderMapper specParkOrderMapper;

    @Override
    public PageInfo couponDetailsList(PageInfo pageInfo, String parkId, String startDate, String endDate, String recvChannel, String payType) {
        final Map<String, Object> param = new HashMap<>();
        param.put("page", pageInfo);
//        param.put("parkOrderId", parkOrderId);
//        param.put("queryDate", queryDate);
        final List<SpecParkUserOrder> specParkUserOrders = specParkOrderMapper.selectBaseDataByMap(param);
        logger.info("返回结果：{}条 {}", new Object[]{specParkUserOrders.size(), JsonUtil.objectToJson(specParkUserOrders)});
        pageInfo.setResultObject(specParkUserOrders);
        return pageInfo;
    }

}
