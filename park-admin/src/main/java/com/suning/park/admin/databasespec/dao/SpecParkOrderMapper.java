package com.suning.park.admin.databasespec.dao;

import com.suning.database.mybatis.SuperMapper;
import com.suning.park.admin.databasespec.model.SpecParkUserOrder;
import com.suning.park.admin.databasespec.model.SpecParkOrder;

import java.util.List;
import java.util.Map;

/*
* Author:   niecheng
* Date:     2017/2/13 22:26
* Project:  park-pom
* Description: //支付订单DAO接口
* History: //修改记录
*/
public interface SpecParkOrderMapper extends SuperMapper {

    List<SpecParkOrder> selectByMap(Map<String, Object> params);

    List<SpecParkUserOrder> selectBaseDataByMap(Map<String, Object> params);

}
