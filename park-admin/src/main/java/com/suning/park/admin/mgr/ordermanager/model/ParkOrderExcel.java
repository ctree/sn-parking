package com.suning.park.admin.mgr.ordermanager.model;

/*
 * Author:   niecheng
 * Date:     2017/2/8 11:38
 * Project:  park-pom
 * Description: 收费明细导出Excel实体
 * History:
 */
public class ParkOrderExcel {

    String parkOrderId;//收费订单ID

    String orderType;//订单状态

    String carNum;//车牌号

    String parkTime;//停放时长

    String recvChannel;//收费渠道

    String parkAmount;//收费金额

    String successTime;//收费日期

    public String getParkOrderId() {
        return parkOrderId;
    }

    public void setParkOrderId(String parkOrderId) {
        this.parkOrderId = parkOrderId;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getCarNum() {
        return carNum;
    }

    public void setCarNum(String carNum) {
        this.carNum = carNum;
    }

    public String getParkTime() {
        return parkTime;
    }

    public void setParkTime(String parkTime) {
        this.parkTime = parkTime;
    }

    public String getRecvChannel() {
        return recvChannel;
    }

    public void setRecvChannel(String recvChannel) {
        this.recvChannel = recvChannel;
    }

    public String getParkAmount() {
        return parkAmount;
    }

    public void setParkAmount(String parkAmount) {
        this.parkAmount = parkAmount;
    }

    public String getSuccessTime() {
        return successTime;
    }

    public void setSuccessTime(String successTime) {
        this.successTime = successTime;
    }
}
