package com.suning.park.admin.mgr.reportform.service;

import com.suning.park.admin.util.page.PageInfo;

/*
 * Author:   niecheng
 * Date:     2017/2/15 17:51
 * Project:  park-pom
 * Description: //模块目的、功能描述      
 * History: //修改记录
 */
public interface CouponDetailsService {

    /**
     * 分页获取优惠券结算明细
     * @param pageInfo 分页信息
     * @param parkId 停车场ID
     * @param startDate 查询起始日期
     * @param endDate 查询结束日期
     * @param recvChannel 缴费渠道
     * @param payType 支付方式
     * @return
     */
    PageInfo couponDetailsList(PageInfo pageInfo, String parkId, String startDate, String endDate, String recvChannel, String payType);

}
