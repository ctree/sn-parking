package com.suning.park.admin.enums;


import com.suning.park.admin.util.StringUtil;

/**
 * 功能启用/禁用
 * @author niecheng
 */
public enum EnableStatusEnum {

	ENABLE("0", "启用"), UNENABLE("1", "禁用");
	private String value;

	private String text;

	/**
	 * 
	 * @param value
	 * @param text
	 */
	EnableStatusEnum(final String value, final String text) {
		this.value = value;
		this.text = text;
	}

	public String getValue() {
		return value;
	}

	public String getText() {
		return text;
	}

	/**
	 * 
	 * @param oridal
	 * @return
	 */
	public static EnableStatusEnum get(int oridal) {
		for (EnableStatusEnum dot : EnableStatusEnum.values()) {
			if (oridal == dot.ordinal()) {
				return dot;
			}
		}
		throw new IllegalArgumentException("Can't get enum with this oridal.");
	}

	/**
	 * 根据value获取text
	 * 
	 * @param value
	 * @return
	 */
	public static String getText(String value) {
		for (EnableStatusEnum dot : EnableStatusEnum.values()) {
			if (value.equals(dot.getValue())) {
				return dot.getText();
			}
		}
		return "";
	}

	/**
	 * 检测value是否是当前枚举的类型之一,区分大小写
	 * 
	 * @param value
	 * @return
	 */
	public static boolean isValue(String value) {
		if (StringUtil.hasEmpty(value))
			return false;
		for (EnableStatusEnum dot : EnableStatusEnum.values()) {
			if (value.equals(dot.getValue())) {
				return true;
			}
		}
		return false;
	}

}
