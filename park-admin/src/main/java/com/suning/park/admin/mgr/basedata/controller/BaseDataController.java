package com.suning.park.admin.mgr.basedata.controller;

import com.suning.park.dto.model.BaseData;
import com.suning.park.admin.bean.Response;
import com.suning.park.admin.util.JsonUtil;
import com.suning.park.admin.util.page.PageInfo;
import com.suning.park.admin.util.page.PageResult;
import com.suning.park.admin.mgr.basedata.service.IBaseDataService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * 基础数据
 * Created by Administrator on 2017/1/17.
 */
@Controller
@RequestMapping(value = "/mgr/basedata", produces = {"application/json;charset=UTF-8"})
public class BaseDataController {

    private static final Logger logger = LoggerFactory.getLogger(BaseDataController.class);

    @Resource
    IBaseDataService baseDataService;

    /**
     * 列表页面跳转
     * @param view
     * @return
     */
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(ModelAndView view) {
        view.setViewName("basedata/list");
        return view;
    }

    /**
     * 添加页面跳转
     * @param view
     * @return
     */
    @RequestMapping(value = "/addUI", method = RequestMethod.GET)
    public ModelAndView addUI(ModelAndView view) {
        view.setViewName("basedata/add");
        return view;
    }

    /**
     * 编辑页面跳转
     * @param view
     * @param baseId
     * @return
     */
    @RequestMapping(value = "/editUI/{baseId}", method = RequestMethod.GET)
    public ModelAndView editUI(ModelAndView view, @PathVariable("baseId") String baseId) throws Exception {
        logger.info("编辑基础数据跳转，id：{}", baseId);
        view.addObject("object", baseDataService.selectByPrimaryKey(baseId));
        view.setViewName("basedata/edit");
        return view;
    }

    /**
     * 新增基础数据
     * @param baseData
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public @ResponseBody String add(BaseData baseData) throws Exception {
        logger.info("新增基础数据信息，baseData：{}", JsonUtil.objectToJson(baseData));
        baseDataService.insert(baseData);
        return Response.SUCCESS().toJson();
    }

    /**
     * 删除基础数据
     * @param baseId
     * @return
     */
    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public @ResponseBody String delete(String baseId) throws Exception {
        logger.info("删除基础数据信息，id：{}", baseId);
        baseDataService.delete(baseId);
        return Response.SUCCESS().toJson();
    }

    /**
     * 编辑基础数据
     * @param baseData
     * @return
     */
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public @ResponseBody String edit(BaseData baseData) throws Exception {
        logger.info("编辑基础数据信息，baseData：{}", JsonUtil.objectToJson(baseData));
        baseDataService.update(baseData);
        return Response.SUCCESS().toJson();
    }

    /**
     * 获得列表
     * @param draw
     * @param pageInfo
     * @param type
     * @param name
     * @param text
     * @param value
     * @return
     */
    @RequestMapping(value = "/ajaxList")
    public @ResponseBody
    PageResult ajaxList(
            @RequestParam(value = "draw") int draw, PageInfo pageInfo,
            @RequestParam(value = "type", required = false) String type,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "text", required = false) String text,
            @RequestParam(value = "value", required = false) String value) throws Exception {
        logger.info("基础数据列表查询， 类型：{}，名称：{}", new Object[]{type, name});
        Map<String, Object> params = getAjaxListParmas(type, name, text, value);
        pageInfo = baseDataService.queryByPage(params, pageInfo);
        return new PageResult(pageInfo, draw);
    }

    /**
     * 验证数据是否存在(type+value)
     * @return
     */
    @RequestMapping(value = "/isExistByTypeAndValue",method = RequestMethod.POST)
    public @ResponseBody boolean validateField(
            @RequestParam(value = "baseId", required = false) String baseId,
            @RequestParam("type") String type,
            @RequestParam("value") String value) {
        return !(baseDataService.countByIdAndTypeAndValue(baseId, type, value)>0);
    }

    /**
     * 查询条件
     * @param type
     * @param name
     * @param text
     * @param value
     * @return
     */
    private Map<String, Object> getAjaxListParmas(String type, String name, String text, String value) {
        Map<String, Object> params = new HashMap<String, Object>();
        if (StringUtils.isNotBlank(type)) {
            params.put("type", type.trim());
        }
        if (StringUtils.isNotBlank(name)) {
            params.put("name", name.trim());
        }
        if (StringUtils.isNotBlank(text)) {
            params.put("text", text.trim());
        }
        if (StringUtils.isNotBlank(value)) {
            params.put("value", value.trim());
        }
        return params;
    }

    /**
     * 根据类型获得列表
     * @param type
     * @return
     */
    @RequestMapping(value = "/listByType/{type}", method = RequestMethod.GET)
    public @ResponseBody Object listByType(@PathVariable("type") String type) throws Exception {
        logger.info("根据类型获得基础数据列表，类型：{}", type);
        return baseDataService.listByType(type);
    }
}
