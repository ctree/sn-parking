package com.suning.park.admin.databasespec.model;

import com.suning.park.dto.model.ParkUrl;

/**
 * 停车场本地系统访问地址扩展实体
 */
public class SpecParkUrl extends ParkUrl {

    private String parkName;

    public String getParkName() {
        return parkName;
    }

    public void setParkName(String parkName) {
        this.parkName = parkName;
    }
}
