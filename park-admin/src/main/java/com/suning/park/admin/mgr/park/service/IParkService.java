package com.suning.park.admin.mgr.park.service;

import com.suning.park.dto.model.Park;
import com.suning.park.dto.model.ParkExample;
import com.suning.park.dto.model.ParkIntervalFeeRule;
import com.suning.park.dto.model.ParkIntervalFeeRuleExample;
import com.suning.park.admin.util.page.PageInfo;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/1/13.
 */
public interface IParkService {

    PageInfo queryByPage(final Map<String, Object> params, final PageInfo page);

    void insert(Park park, List<ParkIntervalFeeRule> parkIntervalFeeRules, String operatorId, String  operatorName);

    void batchInsertParkIntervalFeeRule(String parkId, List<ParkIntervalFeeRule> parkIntervalFeeRules,
                                               String operatorId, String operatorName);

    List<ParkIntervalFeeRule> selectByFeeRuleExample(ParkIntervalFeeRuleExample example);

    String getParkIntervalFeeRules(String parkId);

    void delete(String id);

    void update(Park park, String operatorId, String  operatorName);

    Park selectByPrimaryKey(String id);

    List<Park> selectByExample(ParkExample example);

    List<Park> selectAllList();

    void updateStateByIds(List<String> ids, String state);
}
