package com.suning.park.admin.mgr.permissions.service.impl;

import com.suning.park.dto.model.Role;
import com.suning.park.dto.model.RoleExample;
import com.suning.park.dto.model.Vali;
import com.suning.park.dto.model.ValiExample;
import com.suning.park.admin.util.page.PageInfo;
import com.suning.park.admin.enums.EnableStatusEnum;
import com.suning.park.admin.mgr.permissions.service.RoleService;
import com.suning.park.admin.service.PermissionsAbstractService;
import com.suning.park.admin.util.CheckValue;
import com.suning.park.admin.util.IdBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 角色管理业务逻辑实现
 * @author niehceng
 */
@Service("roleServiceImpl")
@Transactional(propagation= Propagation.REQUIRED,rollbackFor=Exception.class)
public class RoleServiceImpl  extends PermissionsAbstractService implements RoleService {

    private static final Logger logger = LoggerFactory.getLogger(RoleServiceImpl.class);

    @Override
    public PageInfo queryByPage(Map<String, Object> params, PageInfo page) {
        //TODO 查询权限资源列表信息
        Assert.notNull(params, "params is must not null");
        Assert.notNull(page, "page is must not null");
        params.put("page", page);
        List<Role> Roles = iRoleService.selectByMap(params);
        page.setResultObject(Roles);
        return page;
    }

    @Override
    public List<Role> getRole() {
        RoleExample RoleExample = new RoleExample();
        RoleExample.Criteria criteria = RoleExample.createCriteria();
        return iRoleService.selectByExample(RoleExample);
    }

    @Override
    public List<Role> selectRole(String roleId) {
        RoleExample RoleExample = new RoleExample();
        RoleExample.Criteria criteria = RoleExample.createCriteria();
        criteria.andRoleIdEqualTo(roleId);
        return iRoleService.selectByExample(RoleExample);
    }

    @Override
    public boolean isExistByName(String sPositionName) {
        RoleExample RoleExample = new RoleExample();
        RoleExample.Criteria criteria = RoleExample.createCriteria();
        criteria.andPositionNameEqualTo(sPositionName.trim());
        List<Role> Roles = iRoleService.selectByExample(RoleExample);
        return CheckValue.isEmpty(Roles) ? false : true;
    }

    @Override
    public void addRole(Role Role, Date currentDate) {
        //TODO 添加角色
        Role.setRoleId(IdBuilder.getID());
        Role.setUpdateTime(currentDate);
        Role.setCreateTime(currentDate);
        Role.setEnableTime(currentDate);
        iRoleService.insertSelective(Role);
    }

    @Override
    public List<Vali> getVali(String roleId) {
        ValiExample ValiExample = new ValiExample();
        ValiExample.Criteria criteria = ValiExample.createCriteria();
        criteria.andRoleIdEqualTo(roleId);
        return iValiService.selectByExample(ValiExample);
    }

    @Override
    public void update(Role Role, Date currentDate) {
        Role.setUpdateTime(currentDate);
        RoleExample RoleExample = new RoleExample();
        RoleExample.Criteria criteria = RoleExample.createCriteria();
        criteria.andRoleIdEqualTo(Role.getRoleId());
        iRoleService.updateByExampleSelective(Role, RoleExample);
    }

    @Override
    public void saveRoleFunctree(String roleId,String[] functionSourceIds, Date currentDate) {
        //TODO 保存角色资源关系
        Assert.notNull(roleId, "roleId is must not null");
        Assert.notNull(functionSourceIds, "functionSourceIds is must not null");
        deleteIvali(roleId);
        // 逐条创建角色资源关系信息
        Vali Vali = null;
        for (String functionSourceId : functionSourceIds) {
            Vali = new Vali();
            Vali.setValiId(IdBuilder.getID());
            Vali.setRoleId(roleId);
            Vali.setFunctionSourceId(functionSourceId);
            Vali.setEnableStatus(EnableStatusEnum.ENABLE.getValue());
            Vali.setCreateTime(currentDate);
            insertVali(Vali);
        }
    }

    /**
     * 删除授权
     * @param roleId 角色ID
     */
    public void deleteIvali(final String roleId){
        ValiExample ValiExample = new ValiExample();
        ValiExample.Criteria criteria = ValiExample.createCriteria();
        criteria.andRoleIdEqualTo(roleId);
        iValiService.deleteByExample(ValiExample);
    }

    /**
     * 插入授权
     * @param Vali
     */
    public void insertVali(final Vali Vali){
        iValiService.insertSelective(Vali);
    }
}
