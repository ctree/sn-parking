package com.suning.park.admin.databasespec.dao;

import com.suning.database.mybatis.SuperMapper;
import com.suning.park.dto.model.FunctionSource;
import com.suning.park.admin.databasespec.model.SpecFunctionSource;

import java.util.List;
import java.util.Map;

/**
 * 自定义功能资源DAO服务接口
 *
 * @author niecheng
 */
public interface SpecFunctionSourceMapper extends SuperMapper {

    List<FunctionSource> selectByMap(Map<String, Object> params);

    List<SpecFunctionSource> selectSpecByMap(Map<String, Object> params);
}
