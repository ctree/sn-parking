package com.suning.park.admin.service;


import com.suning.park.dto.model.Role;
import com.suning.park.dto.model.SysUser;
import com.suning.park.dto.model.SysUserExample;
import com.suning.park.admin.databasespec.model.SpecFunctionSource;

import java.util.List;
import java.util.Map;

/**
 * 系统用户DAO服务接口
 * @author niecheng
 */
public interface ISysUserService {

    public List<SysUser> selectByExample(final SysUserExample sysUserExample);

    /**
     * 用户信息查询(分页)
     * @param params
     * @return
     */
    public List<SysUser> queryUserInfo(final Map<String, Object> params);

    /**
     * 更新用户信息
     * @param sysUser 用户实体
     */
    public void updateUserInfo(final SysUser sysUser);

    /**
     * 删除用户
     * @param userId 用户ID
     */
    public void deleteUser(final String userId);



    /**
     * 用户信息录入
     * @param assUser
     */
    public void insertSelective(final SysUser assUser);

    /**
     * 获取当前用户权限拥有的资源
     * @param userId
     * @return
     */
    public List<SpecFunctionSource> getSpecFunctionSourceByUserId(final String userId);

    /**
     * 获取角色资源
     * @param specFunctionSource
     * @param roles
     * @return
     */
    public List<SpecFunctionSource> getSpecFunctionSource(SpecFunctionSource specFunctionSource, List<Role> roles);


}
