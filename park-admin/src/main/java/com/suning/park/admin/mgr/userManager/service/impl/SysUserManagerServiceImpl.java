package com.suning.park.admin.mgr.userManager.service.impl;

import com.suning.park.dto.dao.SysUserMapper;
import com.suning.park.dto.model.SysUser;
import com.suning.park.dto.model.SysUserExample;
import com.suning.park.admin.util.IdBuilder;
import com.suning.park.admin.util.page.PageInfo;
import com.suning.park.admin.enums.UserStateEnum;
import com.suning.park.admin.mgr.userManager.service.SysUserManagerService;
import com.suning.park.admin.util.CheckValue;
import com.suning.park.admin.util.Md5ConverterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/*
 * Author:   niecheng
 * Date:     2017/1/16 21:54
 * Project:  park-pom
 * Description: 用户管理模块业务接口实现
 */
@Service(value = "sysUserManagerServiceImpl")
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class SysUserManagerServiceImpl implements SysUserManagerService {

    private static final Logger logger = LoggerFactory.getLogger(SysUserManagerServiceImpl.class);

    @Autowired
    private SysUserMapper sysUserMapper;

    @Override
    public PageInfo queryByPage(SysUser sysUser, PageInfo pageInfo) {
        //TODO 分页查询用户信息
        List<SysUser> resultSysUser = getSysUser(sysUser, pageInfo);
        int resultTotal = CheckValue.isEmpty(resultSysUser) ? 0 : querySysUserCount(sysUser);
        pageInfo.setTotalResult(resultTotal);
        pageInfo.setResultObject(resultSysUser);
        return pageInfo;
    }

    @Override
    public void add(SysUser sysUser, Date currentDate) {
        //TODO 添加系统用户
        sysUser.setUserId(IdBuilder.getID());
        sysUser.setPassword(Md5ConverterUtil.Md5(sysUser.getPassword()));
        sysUser.setState(UserStateEnum.NORMAL.getValue());
        sysUser.setUptime(currentDate);
        sysUser.setCreateDate(currentDate);
        insertSysUser(sysUser);
    }

    @Override
    public void setUserState(String userId, String state, Date currentDate) {
        //TODO 系统用户状态设置
        SysUser sysUser = new SysUser();
        sysUser.setUserId(userId);
        sysUser.setState(state);
        sysUser.setUptime(currentDate);
        sysUserMapper.updateByPrimaryKeySelective(sysUser);
    }

    @Override
    public SysUser getSysUser(String userId) {
        //TODO 系统用户信息查看
        return sysUserMapper.selectByPrimaryKey(userId);
    }

    @Override
    public void updateSysUser(SysUser sysUser, Date currentDate) {
        //TODO 更新系统用户信息
        sysUser.setUptime(currentDate);
        sysUserMapper.updateByPrimaryKeySelective(sysUser);
    }

    /**
     * 分页查询用户信息
     *
     * @param sysUser  用户条件
     * @param pageInfo 页数信息
     * @return
     */
    public List<SysUser> getSysUser(SysUser sysUser, PageInfo pageInfo) {
        SysUserExample sysUserExample = getSysUserExampleHandle(sysUser);
        sysUserExample.setLimitStart((pageInfo.getCurrentPage() - 1) * pageInfo.getShowCount());
        sysUserExample.setLimitRows(pageInfo.getShowCount());
        sysUserExample.setOrderByClause("UPTIME desc");
        return sysUserMapper.selectByExample(sysUserExample);
    }

    /**
     * 查询系统用户总量
     *
     * @param sysUser 查询用户条件
     * @return
     */
    public int querySysUserCount(SysUser sysUser) {
        List<SysUser> sysUsers = sysUserMapper.selectByExample(getSysUserExampleHandle(sysUser));
        return CheckValue.isEmpty(sysUsers) ? 0 : sysUsers.size();
    }

    /**
     * 系统用户查询条件处理
     *
     * @param sysUser
     * @return
     */
    private static SysUserExample getSysUserExampleHandle(SysUser sysUser) {
        SysUserExample sysUserExample = new SysUserExample();
        SysUserExample.Criteria criteria = sysUserExample.createCriteria();
        if (!CheckValue.isEmpty(sysUser.getUserId())) {
            criteria.andUserIdEqualTo(sysUser.getUserId());
        }
        if (!CheckValue.isEmpty(sysUser.getLogin())) {
            criteria.andLoginEqualTo(sysUser.getLogin());
        }
        if (!CheckValue.isEmpty(sysUser.getName())) {
            criteria.andNameEqualTo(sysUser.getName());
        }
        if (!CheckValue.isEmpty(sysUser.getPhone())) {
            criteria.andPhoneEqualTo(sysUser.getPhone());
        }
        if (!CheckValue.isEmpty(sysUser.getType())) {
            criteria.andTypeEqualTo(sysUser.getType());
        }
        if (!CheckValue.isEmpty(sysUser.getState())) {
            criteria.andStateEqualTo(sysUser.getState());
        }
        return sysUserExample;
    }

    /**
     * 添加系统用户信息
     *
     * @param sysUser 系统用户信息
     */
    public void insertSysUser(SysUser sysUser) {
        sysUserMapper.insert(sysUser);
    }
}
