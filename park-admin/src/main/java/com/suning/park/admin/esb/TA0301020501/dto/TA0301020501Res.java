package com.suning.park.admin.esb.TA0301020501.dto;

import com.suning.park.admin.esb.MbfBodyRes;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by Administrator on 2017/2/15.
 */
@XStreamAlias("MbfBody")
public class TA0301020501Res extends MbfBodyRes<TA0301020501BodyRes> {


    private TA0301020501BodyRes body;

    @Override
    public TA0301020501BodyRes getBody() {
        return body;
    }

    @Override
    public void setBody(TA0301020501BodyRes body) {
        this.body = body;
    }
}
