package com.suning.park.admin.mgr.permissions.service;


import com.suning.park.dto.model.Role;
import com.suning.park.dto.model.Vali;
import com.suning.park.admin.util.page.PageInfo;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 角色业务接口
 * @author niecheng
 * @version 0.0.1
 */
public interface RoleService {

    /**
     * 查询权限资源列表信息
     * @param params 查询条件
     * @param page 分页信息
     * @return
     */
    public PageInfo queryByPage(final Map<String, Object> params, final PageInfo page);

    List<Role> getRole();

    /**
     * 获取角色信息
     * @param roleId 角色ID
     * @return
     */
    List<Role> selectRole(final String roleId);

    /**
     * 验证角色名称是否存在
     * @param sPositionName 角色名称
     * @return
     */
    public boolean isExistByName(final String sPositionName);

    /**
     * 添加角色
     * @param Role
     * @param currentDate
     */
    public void addRole(final Role Role, final Date currentDate);

    /**
     * 根据角色ID获取角色授权关系信息
     * @param roleId
     * @return
     */
    public List<Vali> getVali(final String roleId);

    /**
     * 更新
     * @param Role
     * @param currentDate
     */
    public void update(final Role Role, final Date currentDate);

    /**
     * 保存角色资源关系
     * @param roleId
     * @param functionSourceIds
     * @param currentDate
     */
    void saveRoleFunctree(String roleId, String[] functionSourceIds, Date currentDate);
}
