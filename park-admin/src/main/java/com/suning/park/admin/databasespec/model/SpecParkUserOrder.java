package com.suning.park.admin.databasespec.model;

import java.util.Date;

/*
 * Author:   niecheng
 * Date:     2017/2/14 16:11
 * Project:  park-pom
 * Description: //支付订单自定义模型
 * History: //修改记录
 */
public class SpecParkUserOrder extends SpecParkOrder {

    Date outTime;

    public Date getOutTime() {
        return outTime;
    }

    public void setOutTime(Date outTime) {
        this.outTime = outTime;
    }

}
