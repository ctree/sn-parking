package com.suning.park.admin.esb.TR03010952;

import com.suning.park.admin.bean.Content;
import com.suning.park.admin.esb.TR03010952.dto.TR03010952Req;
import com.suning.park.admin.esb.TR03010952.dto.TR03010952Res;
import com.suning.rsc.RscException;
import com.suning.rsc.dto.responsedto.MbfResponse;
import com.suning.rsc.httpservice.annotation.EsbEIHttpMethod;
import com.suning.rsc.httpservice.annotation.EsbEIHttpService;

/**
 * 会员购物使用
 * Created by Administrator on 2017/2/16.
 */
@EsbEIHttpService(appCode = Content.APP_CODE, serviceCode = Content.PM_MGMT)
public interface OrderPointUseService {

    @EsbEIHttpMethod(operation = Content.ORDER_POINT_USE, serviceResource = "/", reqMbfBodyNode = false,
            requestBodyClass = TR03010952Req.class, responseBodyClass = TR03010952Res.class, timeOut = 1000, soTimeOut = 5000)
    MbfResponse pointUse(TR03010952Req dto) throws RscException;
}
