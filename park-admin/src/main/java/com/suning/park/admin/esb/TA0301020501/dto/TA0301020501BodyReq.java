package com.suning.park.admin.esb.TA0301020501.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/2/15.
 */
public class TA0301020501BodyReq {

    private String ecoType;

    private String custNum;

    private String cardNo;

    private String store;

    private String branch;

    private String accountCreatedChannel;

    private IndividualInfo individualInfo = new IndividualInfo();

    public String getEcoType() {
        return ecoType;
    }

    public void setEcoType(String ecoType) {
        this.ecoType = ecoType;
    }

    public String getCustNum() {
        return custNum;
    }

    public void setCustNum(String custNum) {
        this.custNum = custNum;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getAccountCreatedChannel() {
        return accountCreatedChannel;
    }

    public void setAccountCreatedChannel(String accountCreatedChannel) {
        this.accountCreatedChannel = accountCreatedChannel;
    }

    public IndividualInfo getIndividualInfo() {
        return individualInfo;
    }

    public void setIndividualInfo(IndividualInfo individualInfo) {
        this.individualInfo = individualInfo;
    }

    @XStreamAlias("individualInfo")
    public static class IndividualInfo{

        private Registration registration = new Registration();

        private IndividualBase individualBase = new IndividualBase();

        @XStreamAlias("contactPointList")
        private List<ContactPoint> contactPointList = new ArrayList<ContactPoint>();


        public Registration getRegistration() {
            return registration;
        }

        public void setRegistration(Registration registration) {
            this.registration = registration;
        }

        public IndividualBase getIndividualBase() {
            return individualBase;
        }

        public void setIndividualBase(IndividualBase individualBase) {
            this.individualBase = individualBase;
        }

        public List<ContactPoint> getContactPointList() {
            return contactPointList;
        }

        public void setContactPointList(List<ContactPoint> contactPointList) {
            this.contactPointList = contactPointList;
        }

    }


    @XStreamAlias("contactPoint")
    public static class ContactPoint{
        private String cntctPointType;

        private String preferFlag;

        private String state;

        private String city;

        private String town;

        private String street;

        private String address;

        private String mobileNumMain;

        public String getCntctPointType() {
            return cntctPointType;
        }

        public void setCntctPointType(String cntctPointType) {
            this.cntctPointType = cntctPointType;
        }

        public String getPreferFlag() {
            return preferFlag;
        }

        public void setPreferFlag(String preferFlag) {
            this.preferFlag = preferFlag;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getTown() {
            return town;
        }

        public void setTown(String town) {
            this.town = town;
        }

        public String getStreet() {
            return street;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getMobileNumMain() {
            return mobileNumMain;
        }

        public void setMobileNumMain(String mobileNumMain) {
            this.mobileNumMain = mobileNumMain;
        }
    }

    @XStreamAlias("individualBase")
    public static class IndividualBase{

    }
    @XStreamAlias("registration")
    public static class Registration{
        private String partyName;

        public String getPartyName() {
            return partyName;
        }

        public void setPartyName(String partyName) {
            this.partyName = partyName;
        }
    }


}
