package com.suning.park.admin.mgr.view;

import com.suning.park.admin.base.BasicData;
import com.suning.park.admin.mgr.permissions.service.FuncTreeService;
import com.suning.park.admin.base.IAbstractController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 后台管理页面跳转
 */
@Controller
@RequestMapping(value = "/view", produces = {"application/json;charset=UTF-8"})
public class ViewController extends IAbstractController implements BasicData {

    private static final Logger logger = LoggerFactory.getLogger(ViewController.class);

    @Resource(name = "funcTreeServiceImpl")
    protected FuncTreeService funcTreeService;

    /**
     * 后台首页
     * @param view
     * @return
     */
    @RequestMapping(value = "/main", method = RequestMethod.GET)
    public ModelAndView index(ModelAndView view, HttpServletRequest request) {
        view.setViewName("main");
        return view;
    }

    /**
     * 用户管理
     *          ---用户信息页面
     * @param view
     * @return
     */
    @RequestMapping(value = "/userManager", method = RequestMethod.GET)
    public ModelAndView userManager(ModelAndView view, HttpServletRequest request) {
        logger.info("主页面跳转");
        view.setViewName("userManager/userList");
        return view;
    }

    /**
     * 用户添加
     * @param view
     * @return
     */
    @RequestMapping(value = "/userAdd", method = RequestMethod.GET)
    public ModelAndView userAdd(ModelAndView view, HttpServletRequest request) {
        view.setViewName("userManager/userAdd");
        return view;
    }

    /**
     * 角色管理
     *
     * @param view
     * @return
     */
    @RequestMapping(value = "/mgr/roleList")
    public ModelAndView roleList(ModelAndView view) {
        view.setViewName("role/list");
        return view;
    }

    @RequestMapping(value="/mgr/addUI", method = RequestMethod.GET)
    public ModelAndView addUI(ModelAndView view){
        view.setViewName("role/add");
        return view;
    }

    /**
     * 资源管理
     *
     * @param view
     * @return
     */
    @RequestMapping(value = "/mgr/funcTreeList", method = RequestMethod.GET)
    public ModelAndView funcTreeList(ModelAndView view) {
        view.setViewName("funcTree/list");
        return view;
    }

    @RequestMapping(value="/mgr/addFuncTreeUI", method = RequestMethod.GET)
    public ModelAndView addFuncTreeUI(ModelAndView view){
        view.setViewName("funcTree/add");
        return view;
    }

    @RequestMapping(value="/editUI/{functionSourceId}", method = RequestMethod.GET)
    public ModelAndView editUI(ModelAndView view, @PathVariable("functionSourceId")String functionSourceId){
        view.addObject("object", funcTreeService.selectByPrimaryKey(functionSourceId).get(0));
        view.setViewName("funcTree/edit");
        return view;
    }

    @RequestMapping(value="/mgr/list")
    public ModelAndView list(ModelAndView view) {
        view.setViewName("funcTree/list");
        return view;
    }

    /**
     * 授权管理
     *
     * @param view
     * @return
     */
    @RequestMapping(value = "/mgr/userRoleList")
    public ModelAndView index(ModelAndView view) {
        view.setViewName("userRole/list");
        return view;
    }

    @RequestMapping(value="/mgr/vali/list")
    public String list(Model model) {
        return "userRole/list";
    }

    /**
     * 订单管理模块
     *      临停车辆收费订单明细
     * @param view
     * @return
     */
    @RequestMapping(value = "/mgr/payOrderList")
    public ModelAndView payOrderList(ModelAndView view) {
        view.setViewName("orderManager/payOrderList");
        return view;
    }

    /**
     * 订单管理模块
     *      临停车辆收费日报表
     * @param view
     * @return
     */
    @RequestMapping(value = "/mgr/payOrderDailyReportList")
    public ModelAndView payOrderDailyReport(ModelAndView view) {
        view.setViewName("orderManager/payOrderDailyReportList");
        return view;
    }

    /**
     * 报表模块
     *      停车券使用数据报表
     * @param view
     * @return
     */
    @RequestMapping(value = "/mgr/ticketUsed")
    public ModelAndView ticketUsed(ModelAndView view) {
        view.setViewName("reportForm/ticketUsed");
        return view;
    }

    /**
     * 报表模块
     *      停车券结算报表
     * @param view
     * @return
     */
    @RequestMapping(value = "/mgr/settlement")
    public ModelAndView settlement(ModelAndView view) {
        view.setViewName("reportForm/settlement");
        return view;
    }

    /**
     * 报表模块
     *      停车券结算明细报表
     * @param view
     * @return
     */
    @RequestMapping(value = "/mgr/couponDetails")
    public ModelAndView couponDetails(ModelAndView view) {
        view.setViewName("reportForm/couponDetails");
        return view;
    }
}
