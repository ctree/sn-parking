package com.suning.park.admin.esb.TR01030020.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 功能描述：TR01030020Body响应类
 * 
 * @author 16050576
 * 
 */
public class TR01030020BodyRes {
    /**
     * 
     * custNum
     */
    private String custNum;
    /**
     * 
     * custType
     */
    private String custType;

    /**
     * 
     * 获取custNum
     */
    public String getCustNum() {
        return custNum;
    }

    /**
     * 
     * 设置custNum
     */
    public void setCustNum(String custNum) {
        this.custNum = custNum;
    }

    /**
     * 
     * 获取custType
     */
    public String getCustType() {
        return custType;
    }

    /**
     * 
     * 设置custType
     */
    public void setCustType(String custType) {
        this.custType = custType;
    }

    /**
     * 
     * toString
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
