package com.suning.park.admin.base;

public interface BasicData {

	// 系统返回JSON统一字段名称
	public static final String IS_SUCCESS_DESC = "isSuccess"; //是否成功
	public static final String MESSAGE_DESC = "errorMSG"; //返回消息
	public static final String RESULT_DESC = "result"; //返回内容

	public final static String SUCCESS="0";//成功
	public final static String SYSTEM_FAIL="2";//系统级失败
	public final static String BUSINESS_FAIL="1";//失败
	public final static String BUSINESS_STATE="3";//业务状态 ,进场车牌已存在
	public final static String REQUEST_FAIL="4";//三方接口请求异常



	public final static String USER_IN_SESSION = "user_in_session";

	/**
	 * 资源根节点CODE编号
	 */
	public static final String AU_FUNC_TREE_ROOT_ID = "-1";//系统CODE
	public static final String AU_FUNC_TREE_SECOND_ID="0";//菜单跟CODE


	//系统管理员账号名称
	public static final String SYSTEM_LOGIN_NAME = "suning";//系统管理员账号
	
}
