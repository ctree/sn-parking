package com.suning.park.admin.service;

import com.suning.park.dto.model.Vali;
import com.suning.park.dto.model.ValiExample;

import java.util.List;

/**
 * 授权DAO服务接口
 * @author niecheng
 */
public interface IValiService {

    /**
     * 查询角色授权关联信息
     * @param valiExample
     * @return
     */
    public List<Vali> selectByExample(ValiExample valiExample);

    /**
     * 根据局角色ID删除授权
     * @param valiExample
     */
    public void deleteByExample(ValiExample valiExample);

    /**
     * 插入授权
     * @param vali
     */
    public void insertSelective(Vali vali);

}
