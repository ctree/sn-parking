package com.suning.park.admin.mgr.permissions.service.impl;

import com.suning.park.dto.model.*;
import com.suning.park.admin.util.IdBuilder;
import com.suning.park.admin.util.JsonUtil;
import com.suning.park.admin.util.page.PageInfo;
import com.suning.park.admin.enums.EnableStatusEnum;
import com.suning.park.admin.mgr.permissions.service.ValiService;
import com.suning.park.admin.service.PermissionsAbstractService;
import com.suning.park.admin.util.CheckValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.util.Assert;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 授权管理业务逻辑实现
 * @author niehceng
 */
@Service("valiServiceImpl")
@Transactional(propagation= Propagation.REQUIRED,rollbackFor=Exception.class)
public class ValiServiceImpl extends PermissionsAbstractService implements ValiService {

    private static final Logger logger = LoggerFactory.getLogger(ValiServiceImpl.class);

    @Override
    public PageInfo queryByPage(Map<String, Object> params, PageInfo page) {
        //TODO 分页条件查询系统用户信息
        Assert.notNull(params, "params is must not null");
        Assert.notNull(page, "page is must not null");
        params.put("page", page);
        List<SysUser> SysUsers = iUserService.queryUserInfo(params);
        page.setResultObject(SysUsers);
        return page;
    }

    @Override
    public void roleUI(Model model, String userId) {
        //TODO 授权
        List<SysUser> SysUser = getSysUser(userId);
        if(!CheckValue.isEmpty(SysUser)){
            model.addAttribute("sysUser", SysUser.get(0));
        }
        List<Role> role = getRole();
        if(!CheckValue.isEmpty(role)){
            model.addAttribute("arList", role);
        }
        List<UserRole> userRole = getUserRole(userId);
        if(!CheckValue.isEmpty(userRole)){
            model.addAttribute("aurList", JsonUtil.objectToJson(userRole));
        }
    }

    @Override
    public void saveRoleFunctree(String userId, String[] roleIds, Date currentDate) {
        //TODO 授权操作
        Assert.notNull(userId, "gmUserId is must not null");
        deleteUserRole(userId);
        if(null == roleIds || roleIds.length == 0){
            return ;
        }
        // 逐条创建用户角色信息
        UserRole UserRole = null;
        for (String roleId : roleIds) {
            UserRole = new UserRole();
            UserRole.setUserRoleId(IdBuilder.getID());
            UserRole.setUserId(userId);
            UserRole.setRoleId(roleId);
            UserRole.setEnableStatus(EnableStatusEnum.ENABLE.getValue());
            UserRole.setCreateTime(currentDate);
            UserRole.setUpdateTime(currentDate);
            iUserRoleService.insertSelective(UserRole);
        }
    }

    /**
     * 删除授权信息
     * @param userId 用户ID
     */
    public void deleteUserRole(String userId){
        UserRoleExample UserRoleExample = new UserRoleExample();
        UserRoleExample.Criteria criteria = UserRoleExample.createCriteria();
        criteria.andUserIdEqualTo(userId);
        iUserRoleService.deleteByExample(UserRoleExample);
    }


    /**
     * 获取用户信息
     * @param gmUserId 用户ID
     * @return
     */
    public List<SysUser> getSysUser(final String gmUserId){
        SysUserExample SysUserExample = new SysUserExample();
        SysUserExample.Criteria criteria = SysUserExample.createCriteria();
        criteria.andUserIdEqualTo(gmUserId);
        return iUserService.selectByExample(SysUserExample);
    }

    /**
     * 获取角色信息
     * @return
     */
    public  List<Role> getRole(){
        return iRoleService.selectByMap(null);
    }

    /**
     * 获取用户角色信息
     * @param useId
     * @return
     */
    public List<UserRole> getUserRole(String useId){
        UserRoleExample UserRoleExample = new UserRoleExample();
        UserRoleExample.Criteria criteria = UserRoleExample.createCriteria();
        criteria.andUserIdEqualTo(useId);
        return iUserRoleService.selectByExample(UserRoleExample);
    }
}
