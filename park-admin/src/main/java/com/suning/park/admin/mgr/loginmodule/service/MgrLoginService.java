package com.suning.park.admin.mgr.loginmodule.service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

/**
 * 登录业务接口
 * @author niecheng
 * @version 0.0.1
 */
public interface MgrLoginService {

    /**
     * 用户登录
     * @param request
     * @param login 账号
     * @param pwd 密码
     * @param currDate 当前时间
     * @return
     */
    public Map<String,Object> loginService(final HttpServletRequest request, final String login, final String pwd, final Date currDate);
}
