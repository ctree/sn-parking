package com.suning.park.admin.esb.TA04010350.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2017/1/18.
 */
public class TA04010350BodyReq {

    private String cardNo;

    private String custNum;

    private String ecoType;

    private String orderItemId;//销售订单行号

    private String backOrderItemId;//退货订单号

    private String store;//门店代码

    private String branch;//分公司代码

    private String operator;

    @XStreamAlias("addStructList")
    private List<AddStruct> addStructList = new ArrayList<AddStruct>();

    @XStreamAlias("subtractStructList")
    private List<SubtractStruct> subtractStructList = new ArrayList<SubtractStruct>();


    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getCustNum() {
        return custNum;
    }

    public void setCustNum(String custNum) {
        this.custNum = custNum;
    }

    public String getEcoType() {
        return ecoType;
    }

    public void setEcoType(String ecoType) {
        this.ecoType = ecoType;
    }

    public String getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(String orderItemId) {
        this.orderItemId = orderItemId;
    }

    public String getBackOrderItemId() {
        return backOrderItemId;
    }

    public void setBackOrderItemId(String backOrderItemId) {
        this.backOrderItemId = backOrderItemId;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public List<AddStruct> getAddStructList() {
        return addStructList;
    }

    public void setAddStructList(List<AddStruct> addStructList) {
        this.addStructList = addStructList;
    }

    public List<SubtractStruct> getSubtractStructList() {
        return subtractStructList;
    }

    public void setSubtractStructList(List<SubtractStruct> subtractStructList) {
        this.subtractStructList = subtractStructList;
    }


    @XStreamAlias("addStruct")
    public static class AddStruct{
        private String accountType;

        private BigDecimal accountAddAmt;


        public String getAccountType() {
            return accountType;
        }

        public void setAccountType(String accountType) {
            this.accountType = accountType;
        }

        public BigDecimal getAccountAddAmt() {
            return accountAddAmt;
        }

        public void setAccountAddAmt(BigDecimal accountAddAmt) {
            this.accountAddAmt = accountAddAmt;
        }
    }


    @XStreamAlias("subtractStruct")
    public static class SubtractStruct{
        private String accountType;

        private BigDecimal accountSubAmt;

        public String getAccountType() {
            return accountType;
        }

        public void setAccountType(String accountType) {
            this.accountType = accountType;
        }

        public BigDecimal getAccountSubAmt() {
            return accountSubAmt;
        }

        public void setAccountSubAmt(BigDecimal accountSubAmt) {
            this.accountSubAmt = accountSubAmt;
        }
    }
}
