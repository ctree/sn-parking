package com.suning.park.admin.mgr.reportform.controller;

import com.suning.park.admin.base.BasicData;
import com.suning.park.admin.enums.PayTypeEnum;
import com.suning.park.admin.mgr.reportform.service.SettleMentService;
import com.suning.park.admin.util.DateUtil;
import com.suning.park.admin.util.page.PageInfo;
import com.suning.park.admin.util.page.PageResult;
import com.suning.park.admin.util.CheckValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*
 * Author:   niecheng
 * Date:     2017/1/16 18:48
 * Project:  park-pom
 * Description: 停车券结算报表模块
 */
@Controller
@RequestMapping(value = "/mgr/settleMent")
public class SettleMentController implements BasicData {

    private static final Logger logger = LoggerFactory.getLogger(SettleMentController.class);

    @Resource(name = "settleMentServiceImpl")
    protected SettleMentService settleMentService;


    /**
     * 获取停车券结算信息
     * @param draw
     * @param pageInfo 分页信息
     * @param parkId 停车场ID
     * @param startTimeStr 开始时间
     * @param endTimeStr 结束时间
     * @return
     */
    @RequestMapping(value = "/ajaxSettlementList")
    @ResponseBody
    public PageResult ajaxSettlementList(@RequestParam(value = "draw") int draw, PageInfo pageInfo, String parkId, String startTimeStr, String endTimeStr) {
        if ( !CheckValue.isEmpty(startTimeStr)){
            startTimeStr = DateUtil.formatTime(startTimeStr, DateUtil.dateFormat, DateUtil.datetimeFormat);
        }
        if ( !CheckValue.isEmpty(endTimeStr)){
            endTimeStr = DateUtil.formatTime(endTimeStr + " 23:59:59", DateUtil.datetimeFormat, DateUtil.datetimeFormat);
        }
        pageInfo = settleMentService.ajaxSettlementList(pageInfo, parkId, startTimeStr, endTimeStr, PayTypeEnum.COUPON.getValue());
        return new PageResult(pageInfo, draw);
    }

    /**
     * 导出优惠券结算
     * @param request
     * @param response
     * @param parkId 停车场ID
     * @param startTimeStr 优惠券结算起始时间
     * @param endTimeStr 优惠券结算结束时间
     * @param parkName 停车场/广场名字
     */
    @RequestMapping(value = "/exportSettlementList", method = RequestMethod.GET)
    public void exportSettlementList(HttpServletRequest request, HttpServletResponse response, String parkId, String startTimeStr, String endTimeStr, String parkName){
        ServletOutputStream outputStream = null;
        try {
            if ( !CheckValue.isEmpty(startTimeStr)){
                startTimeStr = DateUtil.formatTime(startTimeStr, DateUtil.dateFormat, DateUtil.datetimeFormat);
            }
            if ( !CheckValue.isEmpty(endTimeStr)){
                endTimeStr = DateUtil.formatTime(endTimeStr + " 23:59:59", DateUtil.datetimeFormat, DateUtil.datetimeFormat);
            }
            byte[] bytes = settleMentService.exportSettlementList(parkId, startTimeStr, endTimeStr, PayTypeEnum.COUPON.getValue(), new String( parkName.getBytes("ISO8859-1"), "UTF-8" ));
            response.setContentType("application/x-msdownload");
            response.setHeader("Content-Disposition", "attachment;filename=setttlementList.xls");
            response.setContentLength(bytes.length);
            outputStream = response.getOutputStream();
            outputStream.write(bytes);
            outputStream.flush();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("下载优惠券结算数据出错, 错误信息：{ }",e);
        }finally {
            if(outputStream != null){
                try {
                    outputStream.close();
                } catch (IOException e) {
                    logger.info("输出流关闭失败，错误信息：{}", e.getMessage());
                }
            }
        }
    }

}
