package com.suning.park.admin.esb.TA0301020501;

import com.suning.park.admin.bean.Content;
import com.suning.park.admin.esb.TA0301020501.dto.TA0301020501Req;
import com.suning.park.admin.esb.TA0301020501.dto.TA0301020501Res;
import com.suning.rsc.RscException;
import com.suning.rsc.dto.responsedto.MbfResponse;
import com.suning.rsc.httpservice.annotation.EsbEIHttpMethod;
import com.suning.rsc.httpservice.annotation.EsbEIHttpService;

/**
 * 创建线下账号（基于会员编号）
 * Created by Administrator on 2017/2/15.
 */
@EsbEIHttpService(appCode = Content.APP_CODE, serviceCode = Content.MEMBER_CARD_ACCOUNT_MGMT)
public interface MemberCardAccountMgmtService {

    @EsbEIHttpMethod(operation = Content.CRT_OFFLINE_ACNT_WITH_CUSTNUMACARDNO, serviceResource = "/", reqMbfBodyNode = false,
            requestBodyClass = TA0301020501Req.class, responseBodyClass = TA0301020501Res.class, timeOut = 1000, soTimeOut = 5000)
    MbfResponse createMember(TA0301020501Req dto) throws RscException;
}
