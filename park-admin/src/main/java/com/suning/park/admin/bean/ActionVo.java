package com.suning.park.admin.bean;

import com.suning.park.dto.model.Action;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Administrator on 2017/2/8.
 */
public class ActionVo extends Action {

    private String startDate;

    private String endDate;

    private MultipartFile cover;

    private MultipartFile logo;

    private String[] couponId;

    private String[] couponNum;

    public String[] getCouponId() {
        return couponId;
    }

    public void setCouponId(String[] couponId) {
        this.couponId = couponId;
    }

    public String[] getCouponNum() {
        return couponNum;
    }

    public void setCouponNum(String[] couponNum) {
        this.couponNum = couponNum;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public MultipartFile getCover() {
        return cover;
    }

    public void setCover(MultipartFile cover) {
        this.cover = cover;
    }

    public MultipartFile getLogo() {
        return logo;
    }

    public void setLogo(MultipartFile logo) {
        this.logo = logo;
    }
}
