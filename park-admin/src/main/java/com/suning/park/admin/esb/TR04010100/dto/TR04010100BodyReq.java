package com.suning.park.admin.esb.TR04010100.dto;

/**
 * Created by Administrator on 2017/1/17.
 */


public class TR04010100BodyReq {

    private String ecoType;//当前业态类型

    private String normEcoType;//目标业态类型

    private String cardNo;//外卡号

    private String custNum;//会员编号

    private String store;//门店代码

    private String branch;//分公司代码


    public String getEcoType() {
        return ecoType;
    }

    public void setEcoType(String ecoType) {
        this.ecoType = ecoType;
    }

    public String getNormEcoType() {
        return normEcoType;
    }

    public void setNormEcoType(String normEcoType) {
        this.normEcoType = normEcoType;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getCustNum() {
        return custNum;
    }

    public void setCustNum(String custNum) {
        this.custNum = custNum;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }
}
