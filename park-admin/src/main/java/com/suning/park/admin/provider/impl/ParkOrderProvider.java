package com.suning.park.admin.provider.impl;

import com.suning.park.admin.enums.ParkOrderLimitTypeEnum;
import com.suning.park.admin.enums.ParkOrderTypeEnum;
import com.suning.park.admin.mgr.ordermanager.service.OrderManagerService;
import com.suning.park.admin.provider.IParkOrderProvider;
import com.suning.park.admin.provider.IParkProvider;
import com.suning.park.admin.util.StringUtil;
import com.suning.park.dto.model.ParkOrder;
import com.suning.park.dto.model.ParkOrderExample;
import com.suning.rsf.provider.annotation.Implement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

/**
 * 停车支付订单服务实现
 * Created by Administrator on 2017/2/16.
 */
@Service
@Implement(contract = IParkOrderProvider.class)
public class ParkOrderProvider implements IParkOrderProvider {

    private static final Logger logger = LoggerFactory.getLogger(ParkOrderProvider.class);

    @Autowired
    OrderManagerService orderManagerService;

    /**
     * 根据苏宁用户ID获得停车支付订单列表
     *
     * @param snUserId  苏宁用户ID
     * @param limitType 限制类型(0=7天，1=一个月，2=三个月)
     * @param pageNum   当前页码
     * @param pageSize  记录数量
     * @return 停车支付订单列表
     */
    @Override
    public List<ParkOrder> selectByUserId(String snUserId, String limitType, Integer pageNum, Integer pageSize) {
        logger.info("ParkOrderProvider.selectByUserId, snUserId:{}, limitType:{}, pageNum:{}, pageSize:{}",
                new Object[]{snUserId, limitType, pageNum, pageSize});
        ParkOrderExample example = new ParkOrderExample();
        ParkOrderExample.Criteria criteria = example.createCriteria().andCreateIdEqualTo(snUserId);
        // limit Date
        Calendar calendar = Calendar.getInstance();
        if (StringUtil.equals(ParkOrderLimitTypeEnum.DAY_7.getValue(), limitType)) {
            calendar.roll(Calendar.DATE, -7);
            criteria.andCreateTimeLessThanOrEqualTo(calendar.getTime());
        } else if (StringUtil.equals(ParkOrderLimitTypeEnum.MONTH_1.getValue(), limitType)) {
            calendar.roll(Calendar.MONTH, -1);
            criteria.andCreateTimeLessThanOrEqualTo(calendar.getTime());
        } else if (StringUtil.equals(ParkOrderLimitTypeEnum.MONTH_3.getValue(), limitType)) {
            calendar.roll(Calendar.MONTH, -3);
            criteria.andCreateTimeLessThanOrEqualTo(calendar.getTime());
        }
        // limit Page
        if (pageNum != null && pageSize != null) {
            example.setLimitStart((pageNum - 1) * pageSize);
            example.setLimitRows(pageSize);
        }
        return orderManagerService.selectByExample(example);
    }

    /**
     * 根据苏宁用户ID和车牌号获得未支付订单
     *
     * @param snUserId 苏宁用户ID
     * @param carNum   车牌号
     * @return
     */
    @Override
    public ParkOrder selectByNoPayAndCarNum(String snUserId, String carNum) {
        logger.info("ParkOrderProvider.selectByNoPayAndCarNum, snUserId:{}, carNum:{}", snUserId, carNum);
        ParkOrderExample example = new ParkOrderExample();
        example.createCriteria()
                .andCreateIdEqualTo(snUserId)
                .andCarNumEqualTo(carNum)
                .andOrderTypeIn(Arrays.asList(new String[]{
                        ParkOrderTypeEnum.UNPAY.getValue(),
                        ParkOrderTypeEnum.PAYMENTS_FAILURES.getValue()
                }));
        List<ParkOrder> parkOrders = orderManagerService.selectByExample(example);
        return parkOrders.size() == 0 ? null : parkOrders.get(0);
    }
}
