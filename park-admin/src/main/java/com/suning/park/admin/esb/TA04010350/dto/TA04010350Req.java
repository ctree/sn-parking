package com.suning.park.admin.esb.TA04010350.dto;

import com.suning.park.admin.esb.MbfBodyReq;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by Administrator on 2017/1/18.
 */
@XStreamAlias("MbfBody")
public class TA04010350Req extends MbfBodyReq<TA04010350BodyReq> {

    private TA04010350BodyReq body = new TA04010350BodyReq();

    public TA04010350Req(String transCode) {
        super(transCode);
    }

    @Override
    public TA04010350BodyReq getBody() {
        return body;
    }

    @Override
    public void setBody(TA04010350BodyReq body) {
        this.body = body;
    }
}
