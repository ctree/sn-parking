package com.suning.park.admin.mgr.reportform.service;

import com.suning.park.dto.model.Coupon;
import com.suning.park.admin.util.page.PageInfo;

/*
 * Author:   niecheng
 * Date:     2017/1/19 11:19
 * Project:  park-pom
 * Description: //停车券用券数据服务接口
 */
public interface TicketUsedService {

    /**
     * 获取停车券使用数据
     * @param pageInfo 分页信息
     * @param coupon 停车券使用查询条件
     * @return
     */
    PageInfo ajaxTicketUsedList(PageInfo pageInfo, Coupon coupon);

    /**
     * 导出停车券使用数据Excel
     * @param coupon 优惠券查询条件
     * @return
     */
    byte[] exportTicketUsedList(Coupon coupon);


    /**
     * 获取明细
     * @param pageInfo 分页信息
     * @param state 券使用状态
     * @param couponId 券ID
     * @return
     */
    PageInfo ajaxTicketUsedDetailedList(PageInfo pageInfo, String state, String couponId);

    /**
     * 导出停车券使用明细数据Excel
     * @param state 优惠券状态
     * @param couponId 优惠券ID
     * @return
     */
    byte[] exportTicketUsedDetailed( String state, String couponId );


}
