package com.suning.park.admin.mgr.reportform.service;

import com.suning.park.admin.util.page.PageInfo;

/*
 * Author:   niecheng
 * Date:     2017/2/13 22:18
 * Project:  park-pom
 * Description: //停车券结算报表模块
 * History: //修改记录
 */
public interface SettleMentService {

    /**
     * 分页获取停车券结算信息
     * @param pageInfo 分页信息
     * @param parkId 停车场ID
     * @param startTimeStr 起始时间
     * @param endTimeStr 结束时间
     * @param payType 支付方式
     * @return
     */
    PageInfo ajaxSettlementList(PageInfo pageInfo, String parkId, String startTimeStr, String endTimeStr, String payType);

    /**
     * 优惠券结算Excel
     * @param parkId 停车场ID
     * @param startTimeStr 优惠券结算起始时间
     * @param endTimeStr 优惠券结算结束时间
     * @param payType 支付方式
     * @param parkName 广场/停车场名字
     * @return
     */
    byte[] exportSettlementList(String parkId, String startTimeStr, String endTimeStr, String payType, String parkName);
}
