package com.suning.park.admin.base;


import com.suning.park.dto.model.SysUser;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by niecheng on 2015/12/20.
 */
public abstract class IAbstractController implements BasicData {


    /**
     * 获取当前登陆用户信息
     * @param request
     * @return
     */
    protected SysUser getCurrentUser(HttpServletRequest request){
        return (SysUser) request.getSession().getAttribute(USER_IN_SESSION);
    }

}

