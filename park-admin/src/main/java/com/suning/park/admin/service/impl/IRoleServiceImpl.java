package com.suning.park.admin.service.impl;

import com.suning.park.dto.model.Role;
import com.suning.park.dto.model.RoleExample;
import com.suning.park.admin.service.PermissionsAbstractService;
import com.suning.park.admin.service.IRoleService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 角色DAO服务接口实现
 * @author niecheng
 */
@Service(value = "iRoleServiceImpl")
public class IRoleServiceImpl extends PermissionsAbstractService implements IRoleService {
    @Override
    public List<Role> selectByExample(RoleExample roleExample) {
        //TODO 查询角色信息
        return roleMapper.selectByExample(roleExample);
    }

    @Override
    public List<Role> selectByMap(Map<String, Object> params) {
        //TODO 查询角色信息
        return specRoleMapper.selectByMap(params);
    }

    @Override
    public void insertSelective(Role role) {
        //TODO 添加角色
        roleMapper.insertSelective(role);
    }

    @Override
    public void updateByExampleSelective(Role role, RoleExample roleExample) {
        //TODO 更新角色
        roleMapper.updateByExampleSelective(role, roleExample);
    }
}
