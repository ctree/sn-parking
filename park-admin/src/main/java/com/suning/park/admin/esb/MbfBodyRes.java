package com.suning.park.admin.esb;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 功能描述：MbfBody响应类
 * 
 * @author 16050576
 * 
 */
public abstract class MbfBodyRes<T> {
    /**
     * 
     * appHeader
     */
    private AppHeaderRes appHeader;

    /**
     * 
     * 获取appHeader
     */
    public AppHeaderRes getAppHeader() {
        return appHeader;
    }

    /**
     * 
     * 设置AppHeader
     */
    public void setAppHeader(AppHeaderRes appHeader) {
        this.appHeader = appHeader;
    }

    /**
     * 功能描述：AppHeader请求类
     * 
     * @author 16050576
     * 
     */
    public class AppHeaderRes {
        /**
         * 
         * getRecNum
         */
        private Integer getRecNum = 0;
        /**
         * 
         * beginRecNum
         */
        private Integer beginRecNum = 0;
        /**
         * 
         * isEnd
         */
        private Character isEnd;
        /**
         * 
         * totalNum
         */
        private Integer totalNum = 0;

        /**
         * 
         * 获取getRecNum
         */
        public Integer getGetRecNum() {
            return getRecNum;
        }

        /**
         * 
         * 设置getRecNum
         */
        public void setGetRecNum(Integer getRecNum) {
            this.getRecNum = getRecNum;
        }

        /**
         * 
         * 获取beginRecNum
         */
        public Integer getBeginRecNum() {
            return beginRecNum;
        }

        /**
         * 
         * 设置beginRecNum
         */
        public void setBeginRecNum(Integer beginRecNum) {
            this.beginRecNum = beginRecNum;
        }

        /**
         * 
         * 获取isEnd
         */
        public Character getIsEnd() {
            return isEnd;
        }

        /**
         * 
         * 设置isEnd
         */
        public void setIsEnd(Character isEnd) {
            this.isEnd = isEnd;
        }

        /**
         * 
         * 获取totalNum
         */
        public Integer getTotalNum() {
            return totalNum;
        }

        /**
         * 
         * 设置totalNum
         */
        public void setTotalNum(Integer totalNum) {
            this.totalNum = totalNum;
        }

        /**
         * 
         * toString
         */
        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }

    }

    /**
     * 
     * 获取Body
     */
    public abstract T getBody();

    /**
     * 
     * 设置Body
     */
    public abstract void setBody(T body);

    /**
     * 
     * toString
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
