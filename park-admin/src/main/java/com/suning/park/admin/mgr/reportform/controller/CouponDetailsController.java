package com.suning.park.admin.mgr.reportform.controller;

import com.suning.park.admin.base.BasicData;
import com.suning.park.admin.mgr.reportform.service.CouponDetailsService;
import com.suning.park.admin.util.page.PageInfo;
import com.suning.park.admin.util.page.PageResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/*
 * Author:   niecheng
 * Date:     2017/1/16 18:48
 * Project:  park-pom
 * Description: 停车券结算明细报表
 */
@Controller
@RequestMapping(value = "/mgr/couponDetails")
public class CouponDetailsController implements BasicData {

    private static final Logger logger = LoggerFactory.getLogger(CouponDetailsController.class);

    @Resource(name = "couponDetailsServiceImpl")
    protected CouponDetailsService couponDetailsService;

    /**
     * 停车券结算明细报表列表
     * @param parkId 停车场Id
     * @param startDate 起始时间
     * @param endDate 结束时间
     * @param recvChannel 收费渠道
     * @param payType 支付类型
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/couponDetailsList", method = RequestMethod.POST)
    public PageResult couponDetailsList(@RequestParam(value = "draw") int draw, PageInfo pageInfo, String parkId, String startDate, String endDate, String recvChannel, String payType){
        pageInfo = couponDetailsService.couponDetailsList(pageInfo, parkId, startDate, endDate, recvChannel, payType);
        return new PageResult(pageInfo, draw);
    }


}
