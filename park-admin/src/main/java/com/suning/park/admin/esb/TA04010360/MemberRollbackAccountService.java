package com.suning.park.admin.esb.TA04010360;

import com.suning.park.admin.bean.Content;
import com.suning.park.admin.esb.TA04010360.dto.TA04010360Req;
import com.suning.park.admin.esb.TA04010360.dto.TA04010360Res;
import com.suning.rsc.RscException;
import com.suning.rsc.dto.responsedto.MbfResponse;
import com.suning.rsc.httpservice.annotation.EsbEIHttpMethod;
import com.suning.rsc.httpservice.annotation.EsbEIHttpService;

/**
 * 账户回滚操作
 * Created by Administrator on 2017/2/15.
 */
@EsbEIHttpService(appCode = Content.APP_CODE, serviceCode = Content.MEMBER_ACCOUNT_MGMT)
public interface MemberRollbackAccountService {

    @EsbEIHttpMethod(operation = Content.ROLLBACK_ACCOUNT, serviceResource = "/", reqMbfBodyNode = false,
            requestBodyClass = TA04010360Req.class, responseBodyClass = TA04010360Res.class, timeOut = 1000, soTimeOut = 5000)
    MbfResponse rollbackAccount(TA04010360Req dto) throws RscException;

}
