package com.suning.park.admin.service.impl;

import com.suning.park.dto.model.UserRole;
import com.suning.park.dto.model.UserRoleExample;
import com.suning.park.admin.service.IUserRoleService;
import com.suning.park.admin.service.PermissionsAbstractService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 用户角色关系DAO服务接口实现
 * @author niecheng
 */
@Service(value = "iUserRoleServiceImpl")
public class IUserRoleServiceImpl extends PermissionsAbstractService implements IUserRoleService {
    @Override
    public List<UserRole> selectByExample(UserRoleExample userRoleExample) {
        //TODO 获取用户角色关系
        return userRoleMapper.selectByExample(userRoleExample);
    }

    @Override
    public void deleteByExample(UserRoleExample userRoleExample) {
        //TODO 删除用户角色关系
        userRoleMapper.deleteByExample(userRoleExample);
    }

    @Override
    public void insertSelective(UserRole userRole) {
        //TODO 插入用户角色关系
        userRoleMapper.insertSelective(userRole);
    }
}
