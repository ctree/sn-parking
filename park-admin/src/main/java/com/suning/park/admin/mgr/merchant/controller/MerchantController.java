package com.suning.park.admin.mgr.merchant.controller;

import com.suning.park.admin.base.IAbstractController;
import com.suning.park.admin.mgr.merchant.service.MerchantService;
import com.suning.park.admin.util.page.PageInfo;
import com.suning.park.admin.util.page.PageResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2017/1/16.
 */
@Controller
@RequestMapping(value = "/mgr/merchant", produces = {"application/json;charset=UTF-8"})
public class MerchantController extends IAbstractController {

    private static final Logger logger = LoggerFactory.getLogger(MerchantController.class);

    @Autowired
    MerchantService merchantService;
    /***
     *商家账户管理
     * @param draw
     * @param pageInfo
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/ajaxList", method = RequestMethod.POST)
    public PageResult ajaxList(@RequestParam(value = "draw") int draw,
                               PageInfo pageInfo,
                               @RequestParam(value = "merchantId") String merchantId,
                               @RequestParam(value = "parkId") String parkId){

        Map map = new HashMap();
        map.put("merchantId",merchantId);
        map.put("parkId",parkId);
        merchantService.queryByPage(map,pageInfo);
        return new PageResult(pageInfo, draw);
    }

    @RequestMapping(value="/accountList")
    public String accountList(Model model) {
        return "merchant/accountList";
    }
    @RequestMapping(value="/accountIn/{userId}", method = RequestMethod.GET)
    public ModelAndView accountIn(ModelAndView view, @PathVariable("userId")String userId){
//        view.addObject("object", funcTreeService.selectByPrimaryKey(functionSourceId).get(0));
        view.setViewName("merchant/accountIn");
        return view;
    }

    @RequestMapping(value="/accountOut/{userId}", method = RequestMethod.GET)
    public ModelAndView accountOut(ModelAndView view, @PathVariable("userId")String userId){
//        view.addObject("object", funcTreeService.selectByPrimaryKey(functionSourceId).get(0));
        view.setViewName("merchant/accountIn");
        return view;
    }

}
