package com.suning.park.admin.mgr.merchant.service;

import com.suning.park.dto.model.MerchantUser;
import org.springframework.transaction.annotation.Transactional;

/**
 *  商户用户操作
 * Created by YL-Zhang on 2017/2/16.
 */
public interface IMerchantUserService {
    @Transactional
    int add(MerchantUser merchantUser);

    @Transactional
    int update(MerchantUser merchantUser);

    @Transactional
    String  modifyPassword(String userId, String oldPwd, String newPwd);
}
