package com.suning.park.admin.esb.TR03010952.dto;

import com.suning.park.admin.esb.MbfBodyRes;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by Administrator on 2017/2/16.
 */
@XStreamAlias("MbfBody")
public class TR03010952Res extends MbfBodyRes<TR03010952BodyRes> {

    private TR03010952BodyRes body;

    @Override
    public TR03010952BodyRes getBody() {
        return body;
    }

    @Override
    public void setBody(TR03010952BodyRes body) {
        this.body = body;
    }
}
