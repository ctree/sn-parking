package com.suning.park.admin.databasespec.model;

import com.suning.park.dto.model.ParkPay;

/**
 * 易付宝商户号配置扩展实体
 */
public class SpecParkPay extends ParkPay {

    private String parkName;

    public String getParkName() {
        return parkName;
    }

    public void setParkName(String parkName) {
        this.parkName = parkName;
    }
}
