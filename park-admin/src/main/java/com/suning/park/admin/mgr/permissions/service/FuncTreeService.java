package com.suning.park.admin.mgr.permissions.service;

import com.suning.park.dto.model.Role;
import com.suning.park.admin.util.page.PageInfo;
import com.suning.park.admin.databasespec.model.SpecFunctionSource;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 功能资源业务接口
 * @author niecheng
 * @version 0.0.1
 */
public interface FuncTreeService {

    /**
     * 查询权限资源列表信息
     * @param params 查询条件
     * @param page 分页信息
     * @return
     */
    public PageInfo queryByPage(final Map<String, Object> params, final PageInfo page);

    /**
     * 根据父节点查询所有子节点
     * @param specFunctionSource
     * @param roleList 当前所有角色
     * @return
     */
    List<SpecFunctionSource> selectSpecByParentCodeForRoleAuth(SpecFunctionSource specFunctionSource, List<Role> roleList);

    /**
     * 根据资源名称查询名称是否存在
     * @param name
     * @param functionSourceId
     * @return
     */
    boolean isExistByName(String name,String functionSourceId);

    /**
     * 查询资源信息
     * @param name 资源名称
     * @return
     */
    public com.suning.park.dto.model.FunctionSource selectByName(String name);

    /**
     * 动态条件查询资源信息
     * @param params
     * @return
     */
    public List<com.suning.park.dto.model.FunctionSource> selectByMap(Map<String, Object> params);

    /**
     * 查询资源信息
     * @param functionSourceId 主键
     * @return
     */
    public List<com.suning.park.dto.model.FunctionSource> selectByPrimaryKey(String functionSourceId);

    /**
     * 查询该资源路径名称是否存在
     * @param sSourceUrl 查询路径
     * @param functionSourceId 主键
     * @return
     */
    public boolean isExistByUrl(String sSourceUrl, String functionSourceId);

    /**
     * 根据资源路径查询功能资源信息
     * @param sSourceUrl 资源路径
     * @return
     */
    public com.suning.park.dto.model.FunctionSource selectByUrl(String sSourceUrl);

    /**
     * 添加资源功能
     * @param assFunctionSource 功能资源实体
     * @param currentDate 添加时间
     */
    public void add(com.suning.park.dto.model.FunctionSource assFunctionSource, Date currentDate);

    /**
     * 修改资源使用状态
     * @param functionSourceId
     * @param sEnableStatus
     */
    public void modifyEnableStatus(String functionSourceId, String sEnableStatus);

    /**
     * 更新资源路径
     * @param assFunctionSource
     * @param currentDate
     */
    public void update(com.suning.park.dto.model.FunctionSource assFunctionSource, Date currentDate);

}
