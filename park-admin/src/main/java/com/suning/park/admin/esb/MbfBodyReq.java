package com.suning.park.admin.esb;

import com.suning.park.admin.bean.Content;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * 功能描述：MbfBody请求类
 * 
 *
 */
public abstract class MbfBodyReq<T> {
    /**
     * 
     * sysHeader
     */
    private SysHeader sysHeader;
    /**
     * 
     * appHeader
     */
    private AppHeaderReq appHeader;

    public MbfBodyReq(String transCode) {
        sysHeader = new SysHeader(transCode);
        appHeader = new AppHeaderReq();
    }

    /**
     * 
     * 获取sysHeader
     */
    public SysHeader getSysHeader() {
        return sysHeader;
    }

    /**
     * 
     * 设置sysHeader
     */
    public void setSysHeader(SysHeader sysHeader) {
        this.sysHeader = sysHeader;
    }

    /**
     * 
     * 获取appHeader
     */
    public AppHeaderReq getAppHeader() {
        return appHeader;
    }

    /**
     * 
     * 设置AppHeader
     */
    public void setAppHeader(AppHeaderReq appHeader) {
        this.appHeader = appHeader;
    }

    /**
     * 功能描述：SysHeader类
     * 
     *
     */
    public static class SysHeader {

        public SysHeader() {
            // do nothing
        }

        public SysHeader(String transCode) {
            this.setTransCode(transCode);
            this.setTranType("1000"); // 同步交易
            SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat sdfTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            Date date = new Date();
            String timestamp = sdfTime.format(date);
            this.setTranDate(sdfDate.format(date));
            this.setTranTimestamp(timestamp);
            this.setCreatedTime(timestamp);
            this.setUpdatedTime(timestamp);
            this.setUserName(null);
            this.setPassword(null);
            this.setSourceChannel(null);
            this.setSourceByEmployee(null);
            this.setSourceSystemNo(Content.SOURCE_SYSTEMNO);
            this.setDestSystemNo(Content.DESTSYSTEMNO);
//            try {
//                this.setSourceChannel(InetAddress.getLocalHost().toString()); // 本机IP地址
//            } catch (UnknownHostException e) {// NOSONAR
//                this.setSourceChannel("");
//            }

        }

        /**
         * 
         * transCode
         */
        private String transCode;
        /**
         * 
         * tranType
         */
        private String tranType;
        /**
         * 
         * userName
         */
        private String userName;
        /**
         * 
         * password
         */
        private String password;
        /**
         * 
         * tranDate
         */
        private String tranDate;
        /**
         * 
         * tranTimestamp
         */
        private String tranTimestamp;
        /**
         * 
         * sourceSystemNo
         */
        private String sourceSystemNo;
        /**
         * 
         * destSystemNo
         */
        private String destSystemNo;
        /**
         * 
         * createdTime
         */
        private String createdTime;
        /**
         * 
         * updatedTime
         */
        private String updatedTime;
        /**
         * 
         * sourceByEmployee
         */
        private String sourceByEmployee;
        /**
         * 
         * sourceChannel
         */
        private String sourceChannel;

        /**
         * 
         * 获取transCode
         */
        public String getTransCode() {
            return transCode;
        }

        /**
         * 
         * 设置transCode
         */
        public void setTransCode(String transCode) {
            this.transCode = transCode;
        }

        /**
         * 
         * 获取tranType
         */
        public String getTranType() {
            return tranType;
        }

        /**
         * 
         * 设置tranType
         */
        public void setTranType(String tranType) {
            this.tranType = tranType;
        }

        /**
         * 
         * 获取userName
         */
        public String getUserName() {
            return userName;
        }

        /**
         * 
         * 设置userName
         */
        public void setUserName(String userName) {
            this.userName = userName;
        }

        /**
         * 
         * 获取password
         */
        public String getPassword() {
            return password;
        }

        /**
         * 
         * 设置password
         */
        public void setPassword(String password) {
            this.password = password;
        }

        /**
         * 
         * 获取tranDate
         */
        public String getTranDate() {
            return tranDate;
        }

        /**
         * 
         * 设置tranDate
         */
        public void setTranDate(String tranDate) {
            this.tranDate = tranDate;
        }

        /**
         * 
         * 获取tranTimestamp
         */
        public String getTranTimestamp() {
            return tranTimestamp;
        }

        /**
         * 
         * 设置tranTimestamp
         */
        public void setTranTimestamp(String tranTimestamp) {
            this.tranTimestamp = tranTimestamp;
        }

        /**
         * 
         * 获取sourceSystemNo
         */
        public String getSourceSystemNo() {
            return sourceSystemNo;
        }

        /**
         * 
         * 设置sourceSystemNo
         */
        public void setSourceSystemNo(String sourceSystemNo) {
            this.sourceSystemNo = sourceSystemNo;
        }

        /**
         * 
         * 获取destSystemNo
         */
        public String getDestSystemNo() {
            return destSystemNo;
        }

        /**
         * 
         * 设置destSystemNo
         */
        public void setDestSystemNo(String destSystemNo) {
            this.destSystemNo = destSystemNo;
        }

        /**
         * 
         * 获取createdTime
         */
        public String getCreatedTime() {
            return createdTime;
        }

        /**
         * 
         * 设置createdTime
         */
        public void setCreatedTime(String createdTime) {
            this.createdTime = createdTime;
        }

        /**
         * 
         * 获取updatedTime
         */
        public String getUpdatedTime() {
            return updatedTime;
        }

        /**
         * 
         * 设置updatedTime
         */
        public void setUpdatedTime(String updatedTime) {
            this.updatedTime = updatedTime;
        }

        /**
         * 
         * 获取sourceByEmployee
         */
        public String getSourceByEmployee() {
            return sourceByEmployee;
        }

        /**
         * 
         * 设置sourceByEmployee
         */
        public void setSourceByEmployee(String sourceByEmployee) {
            this.sourceByEmployee = sourceByEmployee;
        }

        /**
         * 
         * 获取sourceChannel
         */
        public String getSourceChannel() {
            return sourceChannel;
        }

        /**
         * 
         * 设置sourceChannel
         */
        public void setSourceChannel(String sourceChannel) {
            this.sourceChannel = sourceChannel;
        }

        /**
         * 
         * toString
         */
        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }

    }

    /**
     * 功能描述：AppHeader请求类
     * 
     * @author 16050576
     * 
     */
    public static class AppHeaderReq {
        /**
         * 
         * getRecNum
         */
        private Integer getRecNum = 0;
        /**
         * 
         * beginRecNum
         */
        private Integer beginRecNum = 0;

        public AppHeaderReq(Integer getRecNum, Integer beginRecNum) {
            this.getRecNum = getRecNum;
            this.beginRecNum = beginRecNum;
        }

        public AppHeaderReq() {
            this(1, 0);
        }

        /**
         * 
         * 获取getRecNum
         */
        public Integer getGetRecNum() {
            return getRecNum;
        }

        /**
         * 
         * 设置getRecNum
         */
        public void setGetRecNum(Integer getRecNum) {
            this.getRecNum = getRecNum;
        }

        /**
         * 
         * 获取beginRecNum
         */
        public Integer getBeginRecNum() {
            return beginRecNum;
        }

        /**
         * 
         * 设置beginRecNum
         */
        public void setBeginRecNum(Integer beginRecNum) {
            this.beginRecNum = beginRecNum;
        }

        /**
         * 
         * toString
         */
        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
        }

    }

    public abstract T getBody();

    /**
     * 
     * 设置body
     */
    public abstract void setBody(T body);

    /**
     * 
     * toString
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
