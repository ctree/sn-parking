package com.suning.park.admin.mgr.loginmodule.service.impl;

import com.suning.park.dto.model.SysUser;
import com.suning.park.admin.base.BasicData;
import com.suning.park.admin.databasespec.model.SpecFunctionSource;
import com.suning.park.admin.enums.UserStateEnum;
import com.suning.park.admin.exception.BusinessFailException;
import com.suning.park.admin.mgr.loginmodule.service.MgrLoginService;
import com.suning.park.admin.mgr.permissions.service.FuncTreeService;
import com.suning.park.admin.service.ISysUserService;
import com.suning.park.admin.util.CheckValue;
import com.suning.park.admin.util.JsonUtil;
import com.suning.park.admin.util.Md5ConverterUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 登录业务接口实现
 * @author niecheng
 * @version 0.0.1
 */
@Service(value = "mgrLoginServiceImpl")
@Transactional(propagation= Propagation.REQUIRED,rollbackFor=Exception.class)
public class MgrLoginServiceImpl implements MgrLoginService, BasicData {

    private static final Logger logger = LoggerFactory.getLogger(MgrLoginServiceImpl.class);

    @Resource(name = "iUserServiceImpl")
    ISysUserService iSysUserService;

    @Resource(name = "funcTreeServiceImpl")
    protected FuncTreeService funcTreeService;

    @Override
    public Map<String, Object> loginService(HttpServletRequest request, String login, String pwd, Date currDate) {
        //TODO 用户登录
        Map<String, Object> params = new HashMap<>();
        params.put("login",login);
        params.put("password", Md5ConverterUtil.Md5(pwd));
        final List<SysUser> sysUsers = iSysUserService.queryUserInfo(params);
        if(CheckValue.isEmpty(sysUsers)){
            throw new BusinessFailException("账号/密码错误");
        }else if(!sysUsers.get(0).getState().equals(UserStateEnum.NORMAL.getValue())){
            throw new BusinessFailException("帐号状态异常,联系管理员");
        }else{
            request.getSession().setAttribute(USER_IN_SESSION, sysUsers.get(0));
            // 获取权限
            List<SpecFunctionSource> specFunctionSources = getFunctionSource(sysUsers.get(0));
            logger.info("当前用户权限：{}", JsonUtil.objectToJson(specFunctionSources));
            request.getSession().setAttribute("aftList", specFunctionSources);
            params.clear();
            params.put(IS_SUCCESS_DESC, SUCCESS);
            params.put(MESSAGE_DESC, "登录成功");
            params.put("userId", sysUsers.get(0).getUserId());

        }
        return params;
    }

    /**
     * 获取用户权限菜单
     * @param sysUser 用户信息
     */
    public List<SpecFunctionSource> getFunctionSource(SysUser sysUser){
        List<SpecFunctionSource> specFunctionSources = null;
        if(SYSTEM_LOGIN_NAME.equals(sysUser.getLogin())){//系统管理员账号
            SpecFunctionSource specFunctionSource = new SpecFunctionSource();
            specFunctionSource.setCode(AU_FUNC_TREE_SECOND_ID);
//            specFunctionSources = funcTreeService.selectSpecByParentCodeForRoleAuth(specFunctionSource,null);

//            specAssFunctionSource.setCode(AU_FUNC_TREE_SECOND_ID);
            specFunctionSources = iSysUserService.getSpecFunctionSource(specFunctionSource, null);
        }else{//非系统管理员账号
            specFunctionSources = iSysUserService.getSpecFunctionSourceByUserId(sysUser.getUserId());
        }
        return specFunctionSources;
    }

}
