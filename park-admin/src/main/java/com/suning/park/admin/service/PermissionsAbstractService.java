package com.suning.park.admin.service;

import com.suning.park.dto.dao.*;
import com.suning.park.admin.databasespec.dao.SpecFunctionSourceMapper;
import com.suning.park.admin.databasespec.dao.SpecRoleMapper;
import com.suning.park.admin.databasespec.dao.SpecSysUserMapper;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Resource;

/**
 * 权限模块DAO层服务接口集
 * @author niecheng
 */
public abstract class PermissionsAbstractService {

    //用户角色关系服务接口
    @Resource(name = "iUserRoleServiceImpl")
    public IUserRoleService iUserRoleService;

    //用户服务接口
    @Resource(name = "iUserServiceImpl")
    public ISysUserService iUserService;

    //角色服务接口
    @Resource(name = "iRoleServiceImpl")
    public IRoleService iRoleService;

    //授权服务接口
    @Resource(name = "iValiServiceImpl")
    public IValiService iValiService;

    //功能资源服务接口
    @Resource(name = "iFunctionSourceImpl")
    public IFunctionSource iFunctionSource;

    @Autowired
    public SysUserMapper sysUserMapper;

    @Autowired
    public SpecSysUserMapper specSysUserMapper;

    @Autowired
    public RoleMapper roleMapper;

    @Autowired
    public SpecRoleMapper specRoleMapper;

    @Autowired
    public UserRoleMapper userRoleMapper;

    @Autowired
    public FunctionSourceMapper functionSourceMapper;

    @Autowired
    public SpecFunctionSourceMapper specFunctionSourceMapper;

    @Autowired
    public ValiMapper valiMapper;

}
