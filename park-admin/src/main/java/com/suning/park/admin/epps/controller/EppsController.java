package com.suning.park.admin.epps.controller;

import com.suning.park.admin.bean.Properties;
import com.suning.park.admin.util.DateUtil;
import com.suning.park.admin.epps.service.IEppsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * Created by Administrator on 2017/1/13.
 */
@Controller
@RequestMapping(value="/epps", produces = { "application/json;charset=UTF-8" })
public class EppsController {

    private final static Logger logger = LoggerFactory.getLogger(EppsController.class);

    @Autowired
    IEppsService eppsService;
    @Autowired
    Properties properties;

    private final static String SUCCESS = "0000";

    private final static String NOTIFY_SUCCESS = "true";

    private final static String NOTIFY_FAIL = "false";



    /**
     * 获取支付请求信息
     * @return
     */
    @RequestMapping(value = "payment", method = RequestMethod.POST)
    @ResponseBody
    public String payment() {
        String orderId = String.valueOf(System.currentTimeMillis());
        String goodsName = "停车费支付";
        long amount = (long) (0.1 * 100);
        String orderAmount = String.valueOf(amount);
        String orderTime = DateUtil.formatDatetimeForYmdhms(new Date());
        return eppsService.testpayment(properties.getEppsMerchantNo(),orderId,goodsName,orderAmount,orderTime);
    }

    @RequestMapping(value = "selectOrder", method = RequestMethod.POST)
    @ResponseBody
    public String selectOrder(HttpServletRequest request){
        return "";
    }



    /**
     * 支付成功回调
     * @param request
     * @return
     */
    @RequestMapping(value = "paymentNotify", method = RequestMethod.POST)
    @ResponseBody
    public String paymentNotify(HttpServletRequest request){
        String code = request.getParameter("responseCode");
        if(SUCCESS.equals(code)){
            eppsService.paySuccess(request.getParameter("orders"));
            return NOTIFY_SUCCESS;
        }
        return NOTIFY_FAIL;
    }

    /**
     * 退款通知结果
     * @param request
     * @return
     */
    @RequestMapping(value = "refundNotify", method = RequestMethod.POST)
    @ResponseBody
    public String refundNotify(HttpServletRequest request){
        String code = request.getParameter("responseCode");
        if(SUCCESS.equals(code)){
            eppsService.refundNotify(request.getParameter("refundOrderNo"),request.getParameter("refundAmount"));
//            logger.info("退款单号:{}",request.getParameter("refundOrderNo"));
//            logger.info("退款金额:{}",request.getParameter("refundAmount"));
//            logger.info("退款订单创建时间:{}",request.getParameter("refundOrderTime"));
//            logger.info("退款完成时间:{}",request.getParameter("refundFinishTime"));
            return NOTIFY_SUCCESS;
        }
        return NOTIFY_FAIL;
    }

}
