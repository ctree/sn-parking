package com.suning.park.admin.mgr.regionCode.service.impl;

import com.suning.park.dto.dao.DDRegionCodeMapper;
import com.suning.park.dto.model.DDRegionCode;
import com.suning.park.dto.model.DDRegionCodeExample;
import com.suning.park.admin.mgr.regionCode.service.IRegionCodeService;
import com.suning.park.admin.util.page.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/2/13.
 */
@Service
public class RegionCodeServiceImpl implements IRegionCodeService {

    @Autowired
    DDRegionCodeMapper regionCodeMapper;

    @Override
    public PageInfo queryByPage(Map<String, Object> params, PageInfo page) {
        return page;
    }

    @Override
    public DDRegionCode selectByPrimaryKey(String id) {
        return regionCodeMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<DDRegionCode> selectByExample(DDRegionCodeExample example) {
        return regionCodeMapper.selectByExample(example);
    }

    @Override
    public List<DDRegionCode> listByProvince() {
        DDRegionCodeExample example = new DDRegionCodeExample();
        example.createCriteria().andLevelEqualTo("2");
        example.setOrderByClause("LEVEL_CODE");
        return  regionCodeMapper.selectByExample(example);
    }

    @Override
    public List<DDRegionCode> listByLevelCode(String levelCode) {
        DDRegionCodeExample example = new DDRegionCodeExample();
        example.createCriteria().andParentCodeEqualTo(levelCode);
        example.setOrderByClause("LEVEL_CODE");
        return  regionCodeMapper.selectByExample(example);
    }
}
