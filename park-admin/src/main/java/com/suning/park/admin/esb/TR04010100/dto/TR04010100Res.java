package com.suning.park.admin.esb.TR04010100.dto;

import com.suning.park.admin.esb.MbfBodyRes;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 功能描述：TR01030020响应类
 *
 */
@XStreamAlias("MbfBody")
public class TR04010100Res extends MbfBodyRes<TR04010100BodyRes> {

    private TR04010100BodyRes body;

    @Override
    public TR04010100BodyRes getBody() {
        return body;
    }

    @Override
    public void setBody(TR04010100BodyRes body) {
        this.body = body;
    }
}
