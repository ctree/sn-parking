package com.suning.park.admin.mgr.action.controller;

import com.suning.park.admin.bean.Response;
import com.suning.park.admin.util.page.PageInfo;
import com.suning.park.admin.util.page.PageResult;
import com.suning.park.admin.base.IAbstractController;
import com.suning.park.admin.bean.ActionVo;
import com.suning.park.admin.mgr.action.service.ActionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * 活动管理
 * Created by Administrator on 2017/1/18.
 */
@Controller
@RequestMapping(value = "/mgr/action", produces = {"application/json;charset=UTF-8"})
public class ActionController extends IAbstractController {

    private static final Logger logger = LoggerFactory.getLogger(ActionController.class);

    @Autowired
    ActionService actionService;

    /***
     *
     * 新增活动视图
     * @param model
     * @return
     */
    @RequestMapping(value="/addActionView")
    public String addActionView(Model model) {
        return "action/addAction";
    }

    /***
     *
     * 新增活动视图
     * @param model
     * @return
     */
    @RequestMapping(value="/actionListView")
    public String actionListView(Model model) {
        return "action/actionList";
    }

    /***
     * 活动修改
     * @param view
     * @param id
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/actionEditView/{id}", method = RequestMethod.GET)
    public ModelAndView actionEditView(ModelAndView view, @PathVariable("id") String id) throws Exception {
        view.addObject("action",actionService.getActionById(id));
        view.setViewName("action/actionEdit");
        return view;
    }
    /***
     * 新增活动
     * @param actionVo
     * @param request
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/addAction", method = RequestMethod.POST)
    public String addAction(ActionVo actionVo,HttpServletRequest request) throws Exception{
        logger.info("接收参数actionVo----{}",actionVo);
        actionService.addAction(actionVo,getCurrentUser(request));
        return Response.SUCCESS().toJson();
    }

    /***
     * 编辑活动
     * @param actionVo
     * @param request
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value = "/editAction", method = RequestMethod.POST)
    public String editAction(ActionVo actionVo,HttpServletRequest request) throws Exception{
        logger.info("接收参数actionVo----{}",actionVo);
        actionService.editAction(actionVo,getCurrentUser(request));
        return Response.SUCCESS().toJson();
    }

    @ResponseBody
    @RequestMapping(value = "/actionList", method = RequestMethod.POST)
    public PageResult actionList(@RequestParam(value = "draw") int draw,
                                 PageInfo pageInfo,
                                 @RequestParam(value = "startTime") String startTime,
                                 @RequestParam(value = "endTime") String endTime,
                                 @RequestParam(value = "parkId") String parkId,
                                 @RequestParam(value = "state") String state,
                                 @RequestParam(value = "type") String type,
                                 @RequestParam(value = "actionName") String actionName) throws Exception{
        Map map = new HashMap();
        map.put("startTime",startTime);
        map.put("endTime",endTime);
        map.put("parkId",parkId);
        map.put("state",state);
        map.put("type",type);
        map.put("actionName",actionName);
        actionService.queryByPage(map,pageInfo);
        return new PageResult(pageInfo, draw);
    }

    @RequestMapping(value = "queryActionById", method = RequestMethod.GET,produces = {"text/json;charset=UTF-8"})
    @ResponseBody
    public String queryActionById(@RequestParam(value = "id", required = false) String id) throws Exception {
        return Response.SUCCESS().put("action",actionService.queryActionById(id)).toJson();
    }

    /***
     * 活动状态操作
     * @param ids
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/actionHandle", method = RequestMethod.GET)
    public String actionHandle( @RequestParam(value = "ids",required = true) String ids,
                                @RequestParam(value = "type",required = true) String type) throws Exception {
        actionService.handle(ids,type);
        return Response.SUCCESS().toJson();
    }
    @ResponseBody
    @RequestMapping(value="/queryActionCouponByActionId", method = RequestMethod.GET)
    public String queryActionCouponByActionId( @RequestParam(value = "actionId",required = true) String actionId) throws Exception {
        return Response.SUCCESS().put("actionCouponList", actionService.queryActionCouponByActionId(actionId)).toJson();
    }

}
