package com.suning.park.admin.esb.service;


import com.suning.park.admin.exception.BusinessFailException;

import java.math.BigDecimal;

/**
 * 会员相关接口对外服务
 * Created by Administrator on 2017/1/24.
 */
public interface IMemberService {

    /**
     * 根据会员编号查询会员类型
     * @param custNum 会员编号
     * @return 0 个人会员   1团体会员
     * @throws BusinessFailException
     */
    String queryMemberType(String custNum) throws BusinessFailException;

    /**
     * 根据手机号查询会员编号
     * @param phone
     * @return
     * @throws BusinessFailException
     */
    String queryCustNumByPhone(String phone)throws BusinessFailException;

    /**
     * 会员积分余额查询
     * @param custNum 会员编号
     * @param branchCode 公司代码
     * @param storeCode 门店代码
     * @return
     * @throws BusinessFailException
     */
    double queryBalance(String custNum, String branchCode, String storeCode)throws BusinessFailException;


    /**
     * 创建线下会员
     * @param custNum 易付宝会员编号(线上)
     * @param branchCode 分公司代码
     * @param storeCode 门店代码
     * @throws BusinessFailException
     */
    void createMember(String custNum, String branchCode, String storeCode)throws BusinessFailException;


    /**
     * 会员云钻退货接口
     * @param custNum 会员编号
     * @param branchCode 分公司代码
     * @param storeCode 门店代码
     * @param orderId 购物使用订单ID 纯数字
     * @param backOrderId 退货订单ID 纯数字组合
     * @param amt 云钻数量
     */
    void accountReturnGoods(String custNum, String branchCode, String storeCode, String orderId, String backOrderId, String amt);


    /**
     * 账户回滚操作
     * @param custNum 会员编号
     * @param branchCode 分公司代码
     * @param storeCode 门店代码
     * @param orderId 购物使用订单ID/退货订单ID
     */
    void memberAccountRollback(String custNum, String branchCode, String storeCode, String orderId);


    /**
     * 会员积分购物使用
     * @param custNum 会员编号
     * @param branchCode 分公司代码
     * @param storeCode 门店代码
     * @param transId 交易ID UUID
     * @param orderId 订单ID需要时纯数字组成
     * @param amount 云钻数量
     * @param supplierCode C店商户积分发放主体编码
     * @return
     */
    void pointUse(String custNum, String branchCode, String storeCode, String transId, String orderId, BigDecimal amount, String supplierCode);




}
