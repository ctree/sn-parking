package com.suning.park.admin.esb.TA04010350;

import com.suning.park.admin.bean.Content;
import com.suning.park.admin.esb.TA04010350.dto.TA04010350Req;
import com.suning.park.admin.esb.TA04010350.dto.TA04010350Res;
import com.suning.rsc.RscException;
import com.suning.rsc.dto.responsedto.MbfResponse;
import com.suning.rsc.httpservice.annotation.EsbEIHttpMethod;
import com.suning.rsc.httpservice.annotation.EsbEIHttpService;

/**
 * 退货账户交易
 * Created by Administrator on 2017/1/17.
 */
@EsbEIHttpService(appCode = Content.APP_CODE, serviceCode = Content.MEMBER_ACCOUNT_MGMT)
public interface MemberAccountMgmtService {


    @EsbEIHttpMethod(operation = Content.UPDATE_ACCOUNT_RESOURCE_WHENRETURNGOODS, serviceResource = "/", reqMbfBodyNode = false,
            requestBodyClass = TA04010350Req.class, responseBodyClass = TA04010350Res.class, timeOut = 1000, soTimeOut = 5000)
    MbfResponse updateAccountResourceWhenReturnGoods(TA04010350Req dto) throws RscException;

}
