package com.suning.park.admin.enums;

import com.suning.park.admin.util.StringUtil;

public enum CouponStateEnum {

	USE("0", "可使用"),
	DELETE("1", "已删除");//已使用
	private String value;

	private String text;

	/**
	 *
	 * @param value
	 * @param text
	 */
	CouponStateEnum(final String value, final String text) {
		this.value = value;
		this.text = text;
	}

	public String getValue() {
		return value;
	}

	public String getText() {
		return text;
	}

	/**
	 * 
	 * @param oridal
	 * @return
	 */
	public static CouponStateEnum get(int oridal) {
		for (CouponStateEnum dot : CouponStateEnum.values()) {
			if (oridal == dot.ordinal()) {
				return dot;
			}
		}
		throw new IllegalArgumentException("Can't get enum with this oridal.");
	}

	/**
	 * 根据value获取text
	 * 
	 * @param value
	 * @return
	 */
	public static String getText(String value) {
		for (CouponStateEnum dot : CouponStateEnum.values()) {
			if (value.equals(dot.getValue())) {
				return dot.getText();
			}
		}
		return "";
	}

	/**
	 * 检测value是否是当前枚举的类型之一,区分大小写
	 * 
	 * @param value
	 * @return
	 */
	public static boolean isValue(String value) {
		if (StringUtil.isBlank(value)) {
			return false;
		}
		for (CouponStateEnum dot : CouponStateEnum.values()) {
			if (value.equals(dot.getValue())) {
				return true;
			}
		}
		return false;
	}

}
