package com.suning.park.admin.databasespec.dao;

import com.suning.database.mybatis.SuperMapper;
import com.suning.park.admin.databasespec.model.SpecParkUrl;

import java.util.List;
import java.util.Map;

/**
 * 停车场本地系统访问地址扩展mapper
 */
public interface SpecParkUrlMapper extends SuperMapper {

    List<SpecParkUrl> selectByMap(Map<String, Object> params);
}
