package com.suning.park.admin.bean;

import com.suning.park.admin.util.JsonUtil;
import com.suning.park.admin.enums.ResponseCode;
import com.suning.park.admin.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


public class Response implements Serializable {

    private static final Logger logger = LoggerFactory.getLogger(Response.class);

    private static final long serialVersionUID = 1L;

    private Map<String, Object> result;

    public static Response SUCCESS() {
        return new Response(ResponseCode.SUCCESS.value, "");
    }

    public static Response FAIL(String memo) {
        return new Response(ResponseCode.BUSINESS_FAIL.value, memo);
    }

    public static Response SYSFALL(){
        return new Response(ResponseCode.SYSTEM_FAIL.value, ResponseCode.SYSTEM_FAIL.memo);
    }

    public static Response FAIL(String value, String memo) {
        return new Response(value, memo);
    }

    /**
     * 向result中添加额外的参数
     *
     * @param key
     * @param value
     * @return
     */
    public Response put(String key, String value) {
        if(StringUtil.isBlank(value)){
            value = "";
        }
        result.put(key, value);
        return this;
    }

    public Response put(String key, Object value) {
        if(value == null){
            value = "";
        }
        result.put(key, value);
        return this;
    }

    public Response putAll(Map<String, Object> map) {
        result.putAll(map);
        return this;
    }

    public String toJson() {
        return JsonUtil.objectToJson(result);
    }


    public Response() {
    }

    public Response(String code, String errorMsg) {
        result = new HashMap<String, Object>();
        result.put("isSuccess", code);
        result.put("requestId", MDC.get("requestId"));
        result.put("errorMSG", (errorMsg == null || "null".equals(errorMsg)) ? "" : errorMsg);
    }


    public Map<String, Object> getResult() {
        return result;
    }

    public void setResult(Map<String, Object> result) {
        this.result = result;
    }


}
