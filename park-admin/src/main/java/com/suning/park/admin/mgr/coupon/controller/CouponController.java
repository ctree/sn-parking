package com.suning.park.admin.mgr.coupon.controller;

import com.suning.park.dto.model.SysUser;
import com.suning.park.admin.util.page.PageInfo;
import com.suning.park.admin.base.IAbstractController;
import com.suning.park.admin.bean.CouponExp;
import com.suning.park.admin.bean.Response;
import com.suning.park.admin.mgr.coupon.service.ICouponService;
import com.suning.park.admin.util.page.PageResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * 优惠券管理
 * Created by Administrator on 2017/1/16.
 */
@Controller
@RequestMapping(value = "/mgr/coupon")
public class CouponController extends IAbstractController {

    private final static Logger logger = LoggerFactory.getLogger(CouponController.class);

    @Autowired
    ICouponService couponService;

    @ResponseBody
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String add(HttpServletRequest request,CouponExp couponExp) throws Exception {
        logger.info("====couponExp===={}",couponExp);
        SysUser sysUser = getCurrentUser(request);
        couponService.add(couponExp,sysUser);
        return Response.SUCCESS().toJson();
    }
    @ResponseBody
    @RequestMapping(value = "/edit", method = RequestMethod.POST,produces = {"text/json;charset=UTF-8"})
    public String edit(HttpServletRequest request,CouponExp couponExp) throws Exception {
        logger.info("====couponExp===={}",couponExp);
        SysUser sysUser = getCurrentUser(request);
        couponService.edit(couponExp,sysUser);
        return Response.SUCCESS().toJson();
    }
    /**
     * 新增优惠券视图
     * @param view
     * @return
     */
    @RequestMapping(value = "addView", method = RequestMethod.GET)
    public ModelAndView addView(ModelAndView view){
        view.setViewName("coupon/addCoupon");
        return view;
    }
    /***
     * 优惠券修改
     * @param view
     * @param id
     * @return
     * @throws Exception
     */
    @RequestMapping(value="/couponEditView/{id}", method = RequestMethod.GET)
    public ModelAndView couponEditView(ModelAndView view, @PathVariable("id") String id) throws Exception {
        view.addObject("coupon",couponService.queryCouponById(id));
        view.setViewName("coupon/couponEdit");
        return view;
    }
    /***
     * 优惠券列表视图
     * @param view
     * @return
     */
    @RequestMapping(value = "couponListView", method = RequestMethod.GET)
    public ModelAndView couponListView(ModelAndView view){
        view.setViewName("coupon/couponList");
        return view;
    }
    @RequestMapping(value="/mgr/accountList")
    public String accountList(Model model) {
        return "merchant/accountList";
    }

    /***
     * 查询优惠券列表
     * @param draw
     * @param pageInfo
     * @param startTime
     * @param endTime
     * @param parkId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/ajaxList", method = RequestMethod.POST)
    public PageResult ajaxList(@RequestParam(value = "draw") int draw,
                               PageInfo pageInfo,
                               @RequestParam(value = "startTime") String startTime,
                               @RequestParam(value = "endTime") String endTime,
                               @RequestParam(value = "parkId") String parkId,
                               @RequestParam(value = "state") String state,
                               @RequestParam(value = "couponName") String couponName){
        Map map = new HashMap();
        map.put("startTime",startTime);
        map.put("endTime",endTime);
        map.put("parkId",parkId);
        map.put("state",state);
        map.put("couponName",couponName);
        PageInfo pageinfo = couponService.selectByPage(map,pageInfo);
        return new PageResult(pageinfo, draw);
    }

    /***
     * 根据停车场ID查询关联的优惠券
     * @param parkId
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "selectCouponById", method = RequestMethod.GET,produces = {"text/json;charset=UTF-8"})
    @ResponseBody
    public String selectCouponById(@RequestParam(value = "parkId", required = false) String parkId) throws Exception {
        return Response.SUCCESS().put("couponList",couponService.selectCouponByParkId(parkId)).toJson();
    }


    @RequestMapping(value = "queryCouponById", method = RequestMethod.GET,produces = {"text/json;charset=UTF-8"})
    @ResponseBody
    public String queryCouponById(@RequestParam(value = "id", required = false) String id) throws Exception {
        return Response.SUCCESS().put("coupon",couponService.queryCouponById(id)).toJson();
    }
    /***
     * 优惠券操作
     * @param ids
     * @return
     * @throws Exception
     */
    @ResponseBody
    @RequestMapping(value="/couponHandle", method = RequestMethod.GET)
    public String couponHandle( @RequestParam(value = "ids",required = true) String ids,
                              @RequestParam(value = "type",required = true) String type) throws Exception {
        couponService.handle(ids,type);
        return Response.SUCCESS().toJson();
    }

}
