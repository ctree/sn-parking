package com.suning.park.admin.esb.TR04010100.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.ArrayList;
import java.util.List;

/**
 * 功能描述：TR04010100Body响应类
 * 
 * @author 16050576
 * 
 */
public class TR04010100BodyRes {

    private String basicValue;//基本积分总余额(按参考业态汇率转)


    private String salesValue;//促销积分总余额(按参考业态汇率转)


    @XStreamAlias("pointStructList")
    private List<PointStruct> pointStructList = new ArrayList<PointStruct>();


    public String getBasicValue() {
        return basicValue;
    }

    public void setBasicValue(String basicValue) {
        this.basicValue = basicValue;
    }

    public String getSalesValue() {
        return salesValue;
    }

    public void setSalesValue(String salesValue) {
        this.salesValue = salesValue;
    }

    public List<PointStruct> getPointStructList() {
        return pointStructList;
    }

    public void setPointStructList(List<PointStruct> pointStructList) {
        this.pointStructList = pointStructList;
    }

    @XStreamAlias("pointStruct")
    public static class PointStruct{

        private String ecoType;

        private String usableStat;

        private String pointType;

        private String  totalAmt;

        private String newTotalAmt;

        private String usedAmt;

        private String newUsedAmt;

        private String  wastedAmt;

        private String newWastedAmt;

        private String balanceAmt;

        private String newBalanceAmt;


        public String getEcoType() {
            return ecoType;
        }

        public void setEcoType(String ecoType) {
            this.ecoType = ecoType;
        }

        public String getUsableStat() {
            return usableStat;
        }

        public void setUsableStat(String usableStat) {
            this.usableStat = usableStat;
        }

        public String getPointType() {
            return pointType;
        }

        public void setPointType(String pointType) {
            this.pointType = pointType;
        }

        public String getTotalAmt() {
            return totalAmt;
        }

        public void setTotalAmt(String totalAmt) {
            this.totalAmt = totalAmt;
        }

        public String getNewTotalAmt() {
            return newTotalAmt;
        }

        public void setNewTotalAmt(String newTotalAmt) {
            this.newTotalAmt = newTotalAmt;
        }

        public String getUsedAmt() {
            return usedAmt;
        }

        public void setUsedAmt(String usedAmt) {
            this.usedAmt = usedAmt;
        }

        public String getNewUsedAmt() {
            return newUsedAmt;
        }

        public void setNewUsedAmt(String newUsedAmt) {
            this.newUsedAmt = newUsedAmt;
        }

        public String getWastedAmt() {
            return wastedAmt;
        }

        public void setWastedAmt(String wastedAmt) {
            this.wastedAmt = wastedAmt;
        }

        public String getNewWastedAmt() {
            return newWastedAmt;
        }

        public void setNewWastedAmt(String newWastedAmt) {
            this.newWastedAmt = newWastedAmt;
        }

        public String getBalanceAmt() {
            return balanceAmt;
        }

        public void setBalanceAmt(String balanceAmt) {
            this.balanceAmt = balanceAmt;
        }

        public String getNewBalanceAmt() {
            return newBalanceAmt;
        }

        public void setNewBalanceAmt(String newBalanceAmt) {
            this.newBalanceAmt = newBalanceAmt;
        }
    }



    /**
     * 
     * toString
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
