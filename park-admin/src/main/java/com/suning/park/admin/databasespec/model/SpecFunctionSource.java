package com.suning.park.admin.databasespec.model;

import com.suning.park.dto.model.FunctionSource;

import java.util.List;

/**
 * 功能资源自定义实体
 * @author niecheng
 * @version 0.0.1
 */
public class SpecFunctionSource extends FunctionSource {

    /**
     * 子节点
     */
    private List<SpecFunctionSource> nodeList;

    public List<SpecFunctionSource> getNodeList() {
        return nodeList;
    }

    public void setNodeList(List<SpecFunctionSource> nodeList) {
        this.nodeList = nodeList;
    }
}
