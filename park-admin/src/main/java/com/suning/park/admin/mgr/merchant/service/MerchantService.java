package com.suning.park.admin.mgr.merchant.service;

import com.suning.park.admin.util.page.PageInfo;
import com.suning.park.dto.model.Merchant;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

/**
 * 商户信息管理
 * Created by Administrator on 2017/1/16.
 */
public interface MerchantService {


    /***
     * 查询商户账户列表
     * @param params
     * @param page
     * @return
     */
    PageInfo queryByPage(final Map<String, Object> params, final PageInfo page);

    @Transactional
    int add(Merchant merchant);

    @Transactional
    int update(Merchant merchant);

    Merchant find(String userId);
}
