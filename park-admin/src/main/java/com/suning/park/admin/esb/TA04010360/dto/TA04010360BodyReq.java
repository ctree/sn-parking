package com.suning.park.admin.esb.TA04010360.dto;

/**
 * Created by Administrator on 2017/2/15.
 */
public class TA04010360BodyReq {

    private String cardNo;
    private String custNum;
    private String ecoType;
    private String uniteOrderId;
    private String orderItemId;
    private String store;
    private String branch;
    private String operator;
    private String orderType;


    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getCustNum() {
        return custNum;
    }

    public void setCustNum(String custNum) {
        this.custNum = custNum;
    }

    public String getEcoType() {
        return ecoType;
    }

    public void setEcoType(String ecoType) {
        this.ecoType = ecoType;
    }

    public String getUniteOrderId() {
        return uniteOrderId;
    }

    public void setUniteOrderId(String uniteOrderId) {
        this.uniteOrderId = uniteOrderId;
    }

    public String getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(String orderItemId) {
        this.orderItemId = orderItemId;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }
}
