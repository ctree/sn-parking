package com.suning.park.admin.service.impl;

import com.suning.park.dto.model.Vali;
import com.suning.park.dto.model.ValiExample;
import com.suning.park.admin.service.IValiService;
import com.suning.park.admin.service.PermissionsAbstractService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 授权DAO服务接口实现
 * @author niecheng
 */
@Service(value = "iValiServiceImpl")
public class IValiServiceImpl extends PermissionsAbstractService implements IValiService {

    @Override
    public List<Vali> selectByExample(ValiExample valiExample) {
        //TODO 获取授权
        return valiMapper.selectByExample(valiExample);
    }

    @Override
    public void deleteByExample(ValiExample valiExample) {
        //TODO 删除授权
        valiMapper.deleteByExample(valiExample);
    }

    @Override
    public void insertSelective(Vali vali) {
        //TODO 插入授权
        valiMapper.insertSelective(vali);
    }
}
