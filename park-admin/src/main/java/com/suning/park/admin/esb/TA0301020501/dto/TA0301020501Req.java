package com.suning.park.admin.esb.TA0301020501.dto;

import com.suning.park.admin.esb.MbfBodyReq;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by Administrator on 2017/2/15.
 */
@XStreamAlias("MbfBody")
public class TA0301020501Req extends MbfBodyReq<TA0301020501BodyReq> {

    private TA0301020501BodyReq body = new TA0301020501BodyReq();

    public TA0301020501Req(String transCode) {
        super(transCode);
    }

    @Override
    public TA0301020501BodyReq getBody() {
        return body;
    }

    @Override
    public void setBody(TA0301020501BodyReq body) {
        this.body = body;
    }
}
