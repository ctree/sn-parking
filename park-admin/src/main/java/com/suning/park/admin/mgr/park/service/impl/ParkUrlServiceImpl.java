package com.suning.park.admin.mgr.park.service.impl;

import com.suning.park.dto.dao.ParkUrlMapper;
import com.suning.park.dto.model.ParkUrl;
import com.suning.park.dto.model.ParkUrlExample;
import com.suning.park.admin.databasespec.dao.SpecParkUrlMapper;
import com.suning.park.admin.databasespec.model.SpecParkUrl;
import com.suning.park.admin.util.IdBuilder;
import com.suning.park.admin.util.page.PageInfo;
import com.suning.park.admin.mgr.park.service.IParkUrlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/1/13.
 */
@Service
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class ParkUrlServiceImpl implements IParkUrlService {

    @Autowired
    ParkUrlMapper parkUrlMapper;
    @Autowired
    SpecParkUrlMapper specParkUrlMapper;

    @Override
    public PageInfo queryByPage(Map<String, Object> params, PageInfo page) {
        Assert.notNull(params, "params is must not null");
        Assert.notNull(page, "page is must not null");
        params.put("page", page);
        List<SpecParkUrl> parkUrls = specParkUrlMapper.selectByMap(params);
        page.setResultObject(parkUrls);
        return page;
    }

    @Override
    public void insert(ParkUrl parkUrl, String operatorId, String operatorName) {
        parkUrl.setId(IdBuilder.getID());
        parkUrl.setCreateId(operatorId);
        parkUrl.setCreateName(operatorName);
        parkUrl.setCreateTime(new Date());
        parkUrl.setOperatorId(operatorId);
        parkUrl.setOperatorName(operatorName);
        parkUrl.setLastTime(new Date());
        parkUrlMapper.insert(parkUrl);
    }

    @Override
    public void delete(String id) {
        parkUrlMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void update(ParkUrl parkUrl, String operatorId, String operatorName) {
        parkUrl.setOperatorId(operatorId);
        parkUrl.setOperatorName(operatorName);
        parkUrl.setLastTime(new Date());
        parkUrlMapper.updateByPrimaryKeySelective(parkUrl);
    }

    @Override
    public ParkUrl selectByPrimaryKey(String id) {
        return parkUrlMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<ParkUrl> selectByExample(ParkUrlExample example) {
        return parkUrlMapper.selectByExample(example);
    }
}
