package com.suning.park.admin.mgr.permissions.controller;

import com.suning.park.admin.util.page.PageInfo;
import com.suning.park.admin.util.page.PageResult;
import com.suning.park.admin.base.BasicData;
import com.suning.park.admin.mgr.permissions.service.ValiService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 授权管理模块接口
 * @author niecheng
 */
@Controller
@RequestMapping(value = "/mgr/vali")
public class ValiController implements BasicData {

    private static final Logger logger = LoggerFactory.getLogger(ValiController.class);

    @Resource(name = "valiServiceImpl")
    protected ValiService valiService;

    @RequestMapping(value = "/ajaxList")
    public
    @ResponseBody
    PageResult ajaxList(
            @RequestParam(value = "draw") int draw, PageInfo pageInfo,
            @RequestParam(value = "login", required = false) String login,
            @RequestParam(value = "name", required = false) String name,
            @RequestParam(value = "phone", required = false) String phone) {
        logger.info("用户授权管理列表查询， 姓名：{}，账号：{}，手机号：{}", new Object[]{name, login, phone});
        try {
            Map<String, Object> params = getAjaxListParmas(login, name, phone);
            pageInfo = valiService.queryByPage(params, pageInfo);
        } catch (Exception e) {
            logger.error("用户授权管理列表查询 程序异常, 失败信息：{}", new Object[]{e.getMessage()});
            return null;
        }
        return new PageResult(pageInfo, draw);
    }

    /**
     * 查询条件
     *
     * @param login 账号
     * @param name  姓名
     * @param phone 电话
     * @return
     */
    private Map<String, Object> getAjaxListParmas(String login, String name, String phone) {
        Map<String, Object> params = new HashMap<String, Object>();
        if (StringUtils.isNotBlank(login)) {
            params.put("login", login.trim());
        }
        if (StringUtils.isNotBlank(name)) {
            params.put("name", name.trim());
        }
        if (StringUtils.isNotBlank(phone)) {
            params.put("phone", phone.trim());
        }
        return params;
    }

    @RequestMapping(value="/roleUI/{userId}", method = RequestMethod.GET)
    public String roleUI(Model model, @PathVariable("userId")String userId){
        logger.info("用户授权 用户ID：{}", userId);
        try{
            valiService.roleUI(model, userId);
        }catch (Exception e){
            logger.error("用户授权 失败信息：{}", e.getMessage());
        }
        return "userRole/roleUI";
    }

    @RequestMapping(value="/role", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> role(String userId,@RequestParam(value="roleIds") String roleIds){
        logger.info("授权保存操作 用户ID：{}， 角色ID：{}", new Object[]{userId, roleIds});
        Date currentDate = new Date();
        Map<String, Object> resultMap = new HashMap<String, Object>();
        try{
            valiService.saveRoleFunctree(userId, roleIds.split(","), currentDate);
            resultMap.put(IS_SUCCESS_DESC, true);
            resultMap.put(MESSAGE_DESC, "授权成功");
        }catch (Exception e){
            resultMap.put(IS_SUCCESS_DESC, false);
            resultMap.put(MESSAGE_DESC, "授权失败");
        }
        return resultMap;
    }

}
