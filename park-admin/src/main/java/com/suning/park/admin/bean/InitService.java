package com.suning.park.admin.bean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 启动加载
 * Created by Administrator on 2017/1/11.
 */
@Component
public class InitService {

    private static final Logger logger = LoggerFactory.getLogger(InitService.class);




    @PostConstruct
    public void initData(){

        logger.info("初始化字典.....");

//        testService.test();
    }
}
