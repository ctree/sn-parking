package com.suning.park.admin.databasespec.dao;

import com.suning.database.mybatis.SuperMapper;
import com.suning.park.dto.model.BaseData;

import java.util.List;
import java.util.Map;

/**
 * 基础数据扩展mapper
 */
public interface SpecBaseDataMapper extends SuperMapper {

    List<BaseData> selectByMap(Map<String, Object> params);
}
