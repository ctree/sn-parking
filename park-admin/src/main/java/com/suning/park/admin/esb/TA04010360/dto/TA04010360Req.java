package com.suning.park.admin.esb.TA04010360.dto;

import com.suning.park.admin.esb.MbfBodyReq;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by Administrator on 2017/2/15.
 */
@XStreamAlias("MbfBody")
public class TA04010360Req extends MbfBodyReq<TA04010360BodyReq> {

    private TA04010360BodyReq body = new TA04010360BodyReq();

    public TA04010360Req(String transCode) {
        super(transCode);
    }

    @Override
    public TA04010360BodyReq getBody() {
        return body;
    }

    @Override
    public void setBody(TA04010360BodyReq body) {
        this.body = body;
    }
}
