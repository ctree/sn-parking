package com.suning.park.admin.esb.TA0301020501.dto;

/**
 * Created by Administrator on 2017/2/15.
 */
public class TA0301020501BodyRes {

    private String cardNo;

    private String custNum;

    private String store;


    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getCustNum() {
        return custNum;
    }

    public void setCustNum(String custNum) {
        this.custNum = custNum;
    }

    public String getStore() {
        return store;
    }

    public void setStore(String store) {
        this.store = store;
    }
}
