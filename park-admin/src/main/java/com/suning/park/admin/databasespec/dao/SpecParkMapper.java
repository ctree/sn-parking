package com.suning.park.admin.databasespec.dao;

import com.suning.database.mybatis.SuperMapper;
import com.suning.park.admin.databasespec.model.SpecPark;

import java.util.List;
import java.util.Map;

/**
 * 停车场扩展mapper
 */
public interface SpecParkMapper extends SuperMapper {

    List<SpecPark> selectByMap(Map<String, Object> params);
}
