package com.suning.park.admin.bean;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component
public class Properties {


	@Value("${eppsMerchantPrivateKey}")
	private String eppsMerchantPrivateKey;

	@Value("${eppsMerchantPublicKey}")
	private String eppsMerchantPublicKey;

	@Value("${eppsPublickey}")//易付宝公钥
	private String eppsPublickey;

	@Value("${eppsPublicKeyIndex}")//商户公钥索引
	private String eppsPublicKeyIndex;

	@Value("${eppsGoodsType}")//商品类型 由易付宝分配,签署完协议 后给予
	private String eppsGoodsType;

	@Value("${eppsMerchantNo}")//易付宝商户号
	private String eppsMerchantNo;

	@Value("${eppsPayUrl}")//支付地址
	private String eppsPayUrl;

	@Value("${eppsPayNotifyUrl}")//支付回调地址
	private String eppsPayNotifyUrl;

	@Value("${eppsPayReturnUrl}")//支付回调页面地址
	private String eppsPayReturnUrl;

	@Value("${eppsSelectOrderUrl}")
	private String eppsSelectOrderUrl;//易付宝订单查询地址

	@Value("${eppsPayRefundUrl}")//申请退款地址
	private String eppsPayRefundUrl;

	@Value("${eppsPayRefundNotifyUrl}")
	private String eppsPayRefundNotifyUrl;//易付宝退款通知URL

	@Value("${eppsQueryRefundUrl}")
	private String eppsQueryRefundUrl;//易付宝退款订单查询地址


	public String getEppsPayRefundNotifyUrl() {
		return eppsPayRefundNotifyUrl;
	}

	public void setEppsPayRefundNotifyUrl(String eppsPayRefundNotifyUrl) {
		this.eppsPayRefundNotifyUrl = eppsPayRefundNotifyUrl;
	}

	public String getEppsSelectOrderUrl() {
		return eppsSelectOrderUrl;
	}

	public void setEppsSelectOrderUrl(String eppsSelectOrderUrl) {
		this.eppsSelectOrderUrl = eppsSelectOrderUrl;
	}

	public String getEppsPublickey() {
		return eppsPublickey;
	}

	public void setEppsPublickey(String eppsPublickey) {
		this.eppsPublickey = eppsPublickey;
	}

	public String getEppsPublicKeyIndex() {
		return eppsPublicKeyIndex;
	}

	public void setEppsPublicKeyIndex(String eppsPublicKeyIndex) {
		this.eppsPublicKeyIndex = eppsPublicKeyIndex;
	}

	public String getEppsPayUrl() {
		return eppsPayUrl;
	}

	public void setEppsPayUrl(String eppsPayUrl) {
		this.eppsPayUrl = eppsPayUrl;
	}

	public String getEppsPayNotifyUrl() {
		return eppsPayNotifyUrl;
	}

	public void setEppsPayNotifyUrl(String eppsPayNotifyUrl) {
		this.eppsPayNotifyUrl = eppsPayNotifyUrl;
	}

	public String getEppsPayReturnUrl() {
		return eppsPayReturnUrl;
	}

	public void setEppsPayReturnUrl(String eppsPayReturnUrl) {
		this.eppsPayReturnUrl = eppsPayReturnUrl;
	}

	public String getEppsMerchantNo() {
		return eppsMerchantNo;
	}

	public void setEppsMerchantNo(String eppsMerchantNo) {
		this.eppsMerchantNo = eppsMerchantNo;
	}

	public String getEppsGoodsType() {
		return eppsGoodsType;
	}

	public void setEppsGoodsType(String eppsGoodsType) {
		this.eppsGoodsType = eppsGoodsType;
	}

	public String getEppsPayRefundUrl() {
		return eppsPayRefundUrl;
	}

	public void setEppsPayRefundUrl(String eppsPayRefundUrl) {
		this.eppsPayRefundUrl = eppsPayRefundUrl;
	}

	public String getEppsMerchantPrivateKey() {
		return eppsMerchantPrivateKey;
	}

	public void setEppsMerchantPrivateKey(String eppsMerchantPrivateKey) {
		this.eppsMerchantPrivateKey = eppsMerchantPrivateKey;
	}

	public String getEppsMerchantPublicKey() {
		return eppsMerchantPublicKey;
	}

	public void setEppsMerchantPublicKey(String eppsMerchantPublicKey) {
		this.eppsMerchantPublicKey = eppsMerchantPublicKey;
	}
}
