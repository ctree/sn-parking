package com.suning.park.admin.mgr.park.service.impl;

import com.alibaba.fastjson.JSON;
import com.suning.park.dto.dao.ParkIntervalFeeRuleMapper;
import com.suning.park.dto.dao.ParkMapper;
import com.suning.park.dto.model.Park;
import com.suning.park.dto.model.ParkExample;
import com.suning.park.dto.model.ParkIntervalFeeRule;
import com.suning.park.dto.model.ParkIntervalFeeRuleExample;
import com.suning.park.admin.databasespec.model.SpecPark;
import com.suning.park.admin.mgr.park.service.IParkService;
import com.suning.park.admin.util.IdBuilder;
import com.suning.park.admin.util.page.PageInfo;
import com.suning.park.admin.databasespec.dao.SpecParkMapper;
import com.suning.park.admin.enums.YesOrNoEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/1/13.
 */
@Service
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
public class ParkServiceImpl implements IParkService {

    @Autowired
    ParkMapper parkMapper;
    @Autowired
    ParkIntervalFeeRuleMapper parkIntervalFeeRuleMapper;
    @Autowired
    SpecParkMapper specParkMapper;

    @Override
    public PageInfo queryByPage(Map<String, Object> params, PageInfo page) {
        Assert.notNull(params, "params is must not null");
        Assert.notNull(page, "page is must not null");
        params.put("page", page);
        List<SpecPark> parks = specParkMapper.selectByMap(params);
        page.setResultObject(parks);
        return page;
    }

    @Override
    public void insert(Park park, List<ParkIntervalFeeRule> parkIntervalFeeRules, String operatorId, String operatorName) {
        park.setId(IdBuilder.getID());
        park.setCreateId(operatorId);
        park.setCreateName(operatorName);
        park.setCreateTime(new Date());
        park.setOperatorId(operatorId);
        park.setOperatorName(operatorName);
        park.setLastTime(new Date());
        park.setState(YesOrNoEnum.YES.getValue());
        parkMapper.insert(park);
        batchInsertParkIntervalFeeRule(park.getId(), parkIntervalFeeRules, operatorId, operatorName);
    }

    /**
     * 批量创建按时段计费规则
     * @param parkId
     * @param parkIntervalFeeRules
     * @param operatorId
     * @param operatorName
     */
    @Override
    public void batchInsertParkIntervalFeeRule(String parkId, List<ParkIntervalFeeRule> parkIntervalFeeRules,
                                                String operatorId, String operatorName){
        //删除所有该停车场按时段停车数据
        ParkIntervalFeeRuleExample example = new ParkIntervalFeeRuleExample();
        example.createCriteria().andParkIdEqualTo(parkId);
        parkIntervalFeeRuleMapper.deleteByExample(example);
        //批量新增
        for (ParkIntervalFeeRule parkIntervalFeeRule : parkIntervalFeeRules) {
            parkIntervalFeeRule.setId(IdBuilder.getID());
            parkIntervalFeeRule.setParkId(parkId);
            parkIntervalFeeRule.setCreateId(operatorId);
            parkIntervalFeeRule.setCreateTime(new Date());
            parkIntervalFeeRule.setOperatorId(operatorId);
            parkIntervalFeeRule.setOperatorName(operatorName);
            parkIntervalFeeRule.setLastTime(new Date());
            parkIntervalFeeRuleMapper.insert(parkIntervalFeeRule);
        }
    }

    @Override
    public List<ParkIntervalFeeRule> selectByFeeRuleExample(ParkIntervalFeeRuleExample example) {
        return parkIntervalFeeRuleMapper.selectByExample(example);
    }

    @Override
    public String getParkIntervalFeeRules(String parkId) {
        ParkIntervalFeeRuleExample example = new ParkIntervalFeeRuleExample();
        example.createCriteria().andParkIdEqualTo(parkId);
        example.setOrderByClause("START_TIME");
        List<ParkIntervalFeeRule> list = parkIntervalFeeRuleMapper.selectByExample(example);
        return JSON.toJSONString(list);
    }

    @Override
    public void delete(String id) {
        parkMapper.deleteByPrimaryKey(id);
    }

    @Override
    public void update(Park park, String operatorId, String  operatorName) {
        park.setOperatorId(operatorId);
        park.setOperatorName(operatorName);
        park.setLastTime(new Date());
        parkMapper.updateByPrimaryKeySelective(park);
    }

    @Override
    public Park selectByPrimaryKey(String id) {
        return parkMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<Park> selectByExample(ParkExample example) {
        return parkMapper.selectByExample(example);
    }

    @Override
    public List<Park> selectAllList() {
        ParkExample example = new ParkExample();
        example.createCriteria().andStateEqualTo(YesOrNoEnum.YES.getValue());
        return parkMapper.selectByExample(example);
    }

    @Override
    public void updateStateByIds(List<String> ids, String state) {
        Park park = new Park();
        park.setState(state);
        ParkExample example = new ParkExample();
        example.createCriteria().andIdIn(ids);
        parkMapper.updateByExampleSelective(park, example);
    }
}
