package com.suning.park.admin.mgr.park.service;

import com.suning.park.dto.model.ParkUrl;
import com.suning.park.dto.model.ParkUrlExample;
import com.suning.park.admin.util.page.PageInfo;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/2/10.
 */
public interface IParkUrlService {

    PageInfo queryByPage(final Map<String, Object> params, final PageInfo page);

    void insert(ParkUrl parkUrl, String operatorId, String operatorName);

    void delete(String id);

    void update(ParkUrl parkUrl, String operatorId, String operatorName);

    ParkUrl selectByPrimaryKey(String id);

    List<ParkUrl> selectByExample(ParkUrlExample example);
}
