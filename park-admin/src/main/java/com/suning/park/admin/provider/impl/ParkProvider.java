package com.suning.park.admin.provider.impl;

import com.suning.park.dto.model.Park;
import com.suning.park.dto.model.ParkExample;
import com.suning.park.dto.model.ParkIntervalFeeRule;
import com.suning.park.dto.model.ParkIntervalFeeRuleExample;
import com.suning.park.admin.provider.IParkProvider;
import com.suning.park.admin.enums.YesOrNoEnum;
import com.suning.park.admin.mgr.park.service.IParkService;
import com.suning.rsf.provider.annotation.Implement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 停车场服务实现
 * Created by Administrator on 2017/2/14.
 */
@Service
@Implement(contract = IParkProvider.class)
public class ParkProvider implements IParkProvider {

    private static final Logger logger = LoggerFactory.getLogger(ParkProvider.class);

    @Autowired
    IParkService parkService;

    @Override
    public List<Park> selectByRegion(String provinceCode, String cityCode) {
        logger.info("ParkProvider.selectByRegion, provinceCode:{}, cityCode:{}", provinceCode, cityCode);
        ParkExample example = new ParkExample();
        example.createCriteria()
                .andProvinceCodeEqualTo(provinceCode)
                .andCityCodeEqualTo(cityCode)
                .andStateEqualTo(YesOrNoEnum.YES.getValue());
        example.setOrderByClause("CREATE_TIME DESC");
        return parkService.selectByExample(example);
    }

    @Override
    public Map<String, Object> getFeeRuleByParkId(String parkId) {
        logger.info("ParkProvider.getFeeRuleByParkId, parkId:{}", parkId);
        Map<String, Object> map = new HashMap<>();
        // park
        Park park = parkService.selectByPrimaryKey(parkId);
        map.put("park", park);
        // feeRule
        ParkIntervalFeeRuleExample example = new ParkIntervalFeeRuleExample();
        example.createCriteria().andParkIdEqualTo(parkId);
        example.setOrderByClause("START_TIME");
        List<ParkIntervalFeeRule> feeRules = parkService.selectByFeeRuleExample(example);
        map.put("feeRules", feeRules);
        return map;
    }
}
