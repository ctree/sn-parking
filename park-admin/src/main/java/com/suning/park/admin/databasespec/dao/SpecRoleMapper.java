package com.suning.park.admin.databasespec.dao;

import com.suning.database.mybatis.SuperMapper;
import com.suning.park.dto.model.Role;

import java.util.List;
import java.util.Map;

/**
 * 自定义角色DAO服务接口
 *
 * @author niecheng
 */
public interface SpecRoleMapper extends SuperMapper{

    List<Role> selectByMap(Map<String, Object> params);
}
