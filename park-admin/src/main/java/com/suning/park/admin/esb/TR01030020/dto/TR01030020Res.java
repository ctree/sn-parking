package com.suning.park.admin.esb.TR01030020.dto;

import com.suning.park.admin.esb.MbfBodyRes;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 功能描述：TR01030020响应类
 * 
 * @author 16050576
 * 
 */
@XStreamAlias("MbfBody")
public class TR01030020Res extends MbfBodyRes<TR01030020BodyRes> {
    /**
     * 
     * body
     */
    private TR01030020BodyRes body;

    /**
     * 
     * 设置body
     */
    @Override
    public void setBody(TR01030020BodyRes body) {
        this.body = body;
    }

    /**
     * 
     * 获取body
     */
    @Override
    public TR01030020BodyRes getBody() {
        return body;
    }

}
