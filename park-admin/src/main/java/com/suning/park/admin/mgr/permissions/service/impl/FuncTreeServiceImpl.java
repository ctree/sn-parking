package com.suning.park.admin.mgr.permissions.service.impl;

import com.suning.park.dto.model.FunctionSource;
import com.suning.park.dto.model.FunctionSourceExample;
import com.suning.park.dto.model.Role;
import com.suning.park.admin.databasespec.model.SpecFunctionSource;
import com.suning.park.admin.mgr.permissions.service.FuncTreeService;
import com.suning.park.admin.service.PermissionsAbstractService;
import com.suning.park.admin.util.IdBuilder;
import com.suning.park.admin.util.StringUtil;
import com.suning.park.admin.util.page.PageInfo;
import com.suning.park.admin.enums.EnableStatusEnum;
import com.suning.park.admin.enums.YesOrNoEnum;
import com.suning.park.admin.util.CheckValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 功能资源业务逻辑实现
 * @author niehceng
 */
@Service(value = "funcTreeServiceImpl")
@Transactional(propagation= Propagation.REQUIRED,rollbackFor=Exception.class)
public class FuncTreeServiceImpl extends PermissionsAbstractService implements FuncTreeService {

    private static final Logger logger = LoggerFactory.getLogger(FuncTreeServiceImpl.class);

    @Override
    public PageInfo queryByPage(Map<String, Object> params, PageInfo page) {
        //TODO 查询权限资源列表信息
        Assert.notNull(params, "params is must not null");
        Assert.notNull(page, "page is must not null");
        params.put("page", page);
        List<FunctionSource> assFunctionSources = iFunctionSource.selectByMap(params);
        page.setResultObject(assFunctionSources);
        return page;
    }

    @Override
    public List<SpecFunctionSource> selectSpecByParentCodeForRoleAuth(SpecFunctionSource specFunctionSource, List<Role> roleList) {
        Assert.notNull(specFunctionSource.getCode());
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("enableStatus", EnableStatusEnum.ENABLE.getValue());
        params.put("parentCode", specFunctionSource.getCode());
        if(null != roleList){
            params.put("auRoleList", roleList);
        }
        List<SpecFunctionSource> specFunctionSourceList = iFunctionSource.selectSpecByMap(params);
        if(CheckValue.isEmpty(specFunctionSource)){
            specFunctionSource.setIsLeaf(YesOrNoEnum.YES.getValue());
            return null;
        }
        for (SpecFunctionSource safs : specFunctionSourceList) {
            if(YesOrNoEnum.YES.getValue().equals(safs.getIsLeaf())){
                continue;//叶子节点不需要设置nodeList
            }
            safs.setNodeList(this.selectSpecByParentCodeForRoleAuth(safs,roleList));
        }
        return specFunctionSourceList;
    }

    @Override
    public boolean isExistByName(String name, String functionSourceId) {
        //TODO 根据资源名称查询名称是否存在
        if(StringUtil.isBlank(name)){
            return false;
        }
        FunctionSource functionSource = this.selectByName(name);
        if(CheckValue.isEmpty(functionSource)){
            return false;
        }
        if(StringUtil.isBlank(functionSourceId)){
            return true;
        }
        if(functionSourceId.trim().equals(functionSource.getFunctionSourceId())){
            return false;
        }
        return true;
    }

    @Override
    public FunctionSource selectByName(String name) {
        //TODO 查询资源信息
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("sourceName", name);
        return this.selectFirstByMap(params);
    }

    @Override
    public List<FunctionSource> selectByMap(Map<String, Object> params) {
        //TODO 动态条件查询资源信息
        Assert.notNull(params, "params is must not null");
        return iFunctionSource.selectByMap(params);
    }

    @Override
    public List<FunctionSource> selectByPrimaryKey(String functionSourceId) {
        //TODO 查询资源信息
        return queryAssFunctionSource(functionSourceId);
    }

    @Override
    public boolean isExistByUrl(String sSourceUrl, String functionSourceId) {
        //TODO 查询该资源路径名称是否存在
        if(StringUtil.isBlank(sSourceUrl)){
            return false;
        }
        FunctionSource assFunctionSource = this.selectByUrl(sSourceUrl);
        if(null == assFunctionSource){
            return false;
        }
        if(StringUtil.isBlank(functionSourceId)){
            return true;
        }
        if(functionSourceId.trim().equals(assFunctionSource.getFunctionSourceId())){
            return false;
        }
        return true;
    }

    @Override
    public FunctionSource selectByUrl(String sSourceUrl) {
        //TODO 根据资源路径查询功能资源信息
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("sourceUrl", sSourceUrl);
        return this.selectFirstByMap(params);
    }

    @Override
    public void add(FunctionSource assFunctionSource, Date currentDate) {
        //TODO 添加资源功能
        assFunctionSource.setFunctionSourceId(IdBuilder.getID());
        assFunctionSource.setCode(assFunctionSource.getFunctionSourceId());
        assFunctionSource.setEnableStatus(EnableStatusEnum.ENABLE.getValue());
        assFunctionSource.setEnableTime(currentDate);
        assFunctionSource.setCreateTime(currentDate);
        iFunctionSource.insertSelective(assFunctionSource);
    }

    @Override
    public void modifyEnableStatus(String functionSourceId, String sEnableStatus) {
        //TODO 修改资源使用状态
        FunctionSourceExample functionSourceExample = new FunctionSourceExample();
        FunctionSourceExample.Criteria criteria = functionSourceExample.createCriteria();
        criteria.andFunctionSourceIdEqualTo(functionSourceId);
        FunctionSource assFunctionSource = new FunctionSource();
        assFunctionSource.setEnableStatus(sEnableStatus);
        iFunctionSource.updateByExampleSelective(assFunctionSource, functionSourceExample);
    }

    @Override
    public void update(FunctionSource assFunctionSource, Date currentDate) {
        //TODO 更新资源路径
        FunctionSourceExample functionSourceExample = new FunctionSourceExample();
        FunctionSourceExample.Criteria criteria = functionSourceExample.createCriteria();
        criteria.andFunctionSourceIdEqualTo(assFunctionSource.getFunctionSourceId());
        assFunctionSource.setUpdateTime(currentDate);
        iFunctionSource.updateByExampleSelective(assFunctionSource, functionSourceExample);
    }

    /**
     * 根据map条件获取一条记录
     * @param params
     * @return
     */
    private FunctionSource selectFirstByMap(Map<String, Object> params){
        List<FunctionSource> aftList = this.selectByMap(params);
        if(null == aftList || aftList.size() == 0){
            return null;
        }
        return aftList.get(0);
    }

    /**
     * 查询资源信息
     * @param functionSourceId 资源ID
     * @return
     */
    public List<FunctionSource> queryAssFunctionSource(String functionSourceId){
        FunctionSourceExample functionSourceExample = new FunctionSourceExample();
        FunctionSourceExample.Criteria criteria = functionSourceExample.createCriteria();
        criteria.andFunctionSourceIdEqualTo(functionSourceId);
        return iFunctionSource.selectByExample(functionSourceExample);
    }
}
