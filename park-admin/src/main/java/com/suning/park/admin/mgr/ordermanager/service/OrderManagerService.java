package com.suning.park.admin.mgr.ordermanager.service;

import com.suning.park.dto.model.ParkOrder;
import com.suning.park.admin.databasespec.model.SpecParkUserOrder;
import com.suning.park.admin.util.page.PageInfo;
import com.suning.park.dto.model.ParkOrderExample;

import java.util.List;

/*
 * Author:   niecheng
 * Date:     2017/1/19 11:19
 * Project:  park-pom
 * Description: //订单管理服务接口
 */
public interface OrderManagerService {

    /**
     * 获取支付订单信息
     * @param pageInfo 分页信息
     * @param parkOrder 支付订单条件信息
     * @return
     */
    PageInfo ajaxParkOrderList(PageInfo pageInfo, ParkOrder parkOrder);

    /**
     * 获取临停车辆收费日报表
     * @param pageInfo 分页信息
     * @param parkId 停车场Id
     * @param payType 支付方式
     * @param queryTime 查询时间
     * @return
     */
    PageInfo ajaxParkOrderDailyList(PageInfo pageInfo, String parkId, String payType,  String queryTime);

    /**
     * 获取支付明细
     * @param parkOrderId 停车场支付订单号
     * @param queryDate 查询日期
     * @return
     */
    SpecParkUserOrder getOrderPay(String parkOrderId, String queryDate);

    /**
     * 导出临停车辆收费订单明细Excel
     * @param parkOrder 订单查询条件
     * @return
     */
    byte[] exportPayOrderList(ParkOrder parkOrder);

    /**
     * 根据example查询停车支付订单
     * @param example
     * @return
     */
    List<ParkOrder> selectByExample(ParkOrderExample example);
}
