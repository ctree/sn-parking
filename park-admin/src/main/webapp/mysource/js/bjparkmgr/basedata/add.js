$(function () {

    $("#basedataAdd_form").validate({
        focusCleanup: true,
        focusInvalid: false,
        onkeyup: true,
        rules: {
            type: {
                required: true
            }, name: {
                required: true
            }, text: {
                required: true
            }, value: {
                required: true,
                remote:{
                    url :ctx+'/mgr/basedata/isExistByTypeAndValue',
                    type:'post',
                    data:{
                        type:function(){
                            return $("#type").val();
                        },
                        value:function(){
                            return $("#value").val();
                        }
                    }
                }
            }, orderCode: {
                required: true,
            }
        },
        messages: {
            type: {
                required: "类型不能为空"
            }, name: {
                required: "名称不能为空"
            }, text: {
                required: "内容不能为空"
            }, value: {
                required: "值不能为空",
                remote:   "类型和值已存在"
            }, orderCode: {
                required: "排序不能为空",
            }
        },
        submitHandler: submit
    });
});

/**
 * 添加数据字典信息
 */
function submit() {
    $.ajax({
        url: ctx + "/mgr/basedata/add",
        type: 'post',
        dataType: 'json',
        data: $('#basedataAdd_form').serialize(),
        timeout: ajaxTimeout,
        error: function (json) {
            toastr.error("添加数据字典失败，网络连接超时请重新登陆")
        },
        success: function (json) {
            if (json.isSuccess==0) {
                toastr.success("添加成功");
            } else {
                toastr.error("添加失败");
            }
        }
    });
}
