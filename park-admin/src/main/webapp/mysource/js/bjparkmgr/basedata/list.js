var table;
$(function() {
	$("#baseDataList_queryDiv").bind("keydown",function(e){
		// 兼容FF和IE和Opera
		var theEvent = e || window.event;
		var code = theEvent.keyCode || theEvent.which || theEvent.charCode;
		if (code == 13) {
			//回车执行查询
			$("#serach").click();
		}
	});

	table = $('#baseDataList_dataList').dataTable({
		"fnServerData" : function(sSource, aoData, fnCallback) {
			$.ajax({
				"dataType" : 'json',
				"type" : "POST",
				"url" : ctx + "/mgr/basedata/ajaxList",
				"data" : {
					"currentResult" : aoData.start,
					"showCount" : aoData.length,
					"draw":aoData.draw,
					"type" : $("#type").val(),
					"name" : $("#name").val(),
                    "text" : $("#text").val(),
                    "value" : $("#value").val()
				},
				"success" : fnCallback
			});
		},
		"aoColumns": [
			{"data": null},
			// {"data": "baseId"},
			{"data": "type"},
			{"data": "name"},
            {"data": "text"},
			{"data": "value"},
            {"data": "orderCode"},
			{"data": "createTime"},
			{"data": "updateTime"},
			{"data": null}
		],
		"fnRowCallback":function(nRow, aData, iDisplayIndex){
			$('td:eq(0)', nRow).html(viewNumber(iDisplayIndex,aData));
            $('td:eq(6)', nRow).html(formatDate(aData.createTime, 3));
            $('td:eq(7)', nRow).html(formatDate(aData.updateTime, 3));
            $('td:eq(8)', nRow).html(getOperate(aData));
		}
	});

	/**
	 * 搜索
	 */
	$('#baseDataList_serach').click(function() {
		table.fnDraw();
	});
});

function viewNumber(index, data){
	return index+1;
}

function getOperate(data) {
	var str = '<button onclick="getBaseDataInfo(\'' + data.baseId + '\')" class="btn btn-primary btn-xs">编辑</button>&nbsp;&nbsp;';
	str += '<button onclick="deleteBaseData(\'' + data.baseId + '\')" class="btn btn-primary btn-xs">删除</button>';
	return str;
}

/**
 * 删除数据字典
 * @param baseId 数据字典ID
 */
function deleteBaseData(baseId){
	if (!window.confirm("确认该操作吗?")) {
		return;
	}
	$.ajax({
		url: ctx + "/mgr/basedata/delete",
		type: 'post',
		dataType: 'json',
		"data": {
			"baseId" : baseId
		},
		timeout: ajaxTimeout,
		error: function (json) {
			toastr.error("操作失败," + json.errorMSG)
		},
		success: function (json) {
			if (json.isSuccess==0) {
				toastr.success("删除成功")
			} else {
				toastr.error("删除失败")
			}
			table.fnPageChange(getPageNum(table, false));
		}
	});
}

/**
 * 数据字典信息编辑查看
 * @param baseId 数据字典ID
 */
function getBaseDataInfo(baseId) {
	var url = ctx + "/mgr/basedata/editUI/" + baseId;
	window.location.href = url;
}

