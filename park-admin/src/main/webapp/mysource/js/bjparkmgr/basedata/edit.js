$(function() {

	$("#baseDataEdit_backButton").click(function() {
		/*history.back(-1);*/
		getBaseDataInfoList();
	});

	$("#baseDataEdit_rejectForm").validate({
        focusCleanup: true,
        focusInvalid: false,
        onkeyup: true,
        rules: {
            type: {
                required: true
            }, name: {
                required: true
            }, text: {
                required: true
            }, value: {
                required: true,
                remote:{
                    url :ctx+'/mgr/basedata/isExistByTypeAndValue',
                    type:'post',
                    data:{
                        baseId:function(){
                            return $("#baseId").val();
                        },
                        type:function(){
                            return $("#type").val();
                        },
                        value:function(){
                            return $("#value").val();
                        }
                    }
                }
            }, orderCode: {
                required: true,
            }
        },
        messages: {
            type: {
                required: "类型不能为空"
            }, name: {
                required: "名称不能为空"
            }, text: {
                required: "内容不能为空"
            }, value: {
                required: "值不能为空",
                remote:   "数据字典类型和值已存在"
            }, orderCode: {
                required: "排序不能为空",
            }
        },
        submitHandler: submit
    });

});


/**
 * 数据字典信息修改
 */
function submit(){
	$.ajax({
		url:ctx+"/mgr/basedata/edit",
		type:'post',
		dataType:'json',
		"data" : {
			"baseId":$("#baseId").val(),
			"type":$("#type").val(),
			"name":$("#name").val(),
            "text":$("#text").val(),
            "value":$("#value").val(),
            "orderCode":$("#orderCode").val()
		},
		timeout:ajaxTimeout,
		error:function(json){
			toastr.error("修改数据字典信息失败，网络连接超时请重新登陆")
		},
		success:function(json){
			if(json.isSuccess==0){
				toastr.success("修改成功");
			}else{
				toastr.error("修改失败");
			}
		}
	});
}

/**
 * 查看数据字典信息列表
 */
function getBaseDataInfoList() {
	var url = ctx + "/mgr/basedata/list";
	window.location.href = url;
}