/***
 * @type {{init: couponEdit.init, submit: couponEdit.submit}}
 */
var couponEdit = {
    /**
     * 初始化页面
     */
    init:function () {
        Common.Ajax.ajaxRequest({
            url: "/mgr/park/getParkList",
            type: "get",
            data: {}
        },function(data){
            if(!data.parkList){
                toastr.error("请先配置广场！");
                return;
            };
            var optionHtml = "";
            var parkId = $("#parkIdTem").val();
            $.each(data.parkList,function (i,item) {
                optionHtml += '<option value='+item.id+' '+(item.id== parkId?'selected':'')+'>'+item.parkName+'</option>';
            })
            $("#parkId").append(optionHtml).comboSelect();
        });
        $("#parkId").on("change",function () {
            $("#ascription").val($("#parkId").find("option:selected").text());
        });
        $("#backButton").on("click",function () {

        })
    },
    submit : function () {
        Common.Ajax.ajaxRequest({
            url: "/mgr/coupon/edit",
            type: "post",
            data: $('#editForm').serializeArray(),
        }, function (data) {
            toastr.success("修改成功");
            window.location.href = ctx + "/mgr/coupon/couponListView";
        })
    }
}
$(document).ready(function () {
    $("#editForm").validate({
        focusCleanup: true,
        focusInvalid: false,
        onkeyup: true,
        rules: {
            couponName: {
                required: true
            }, amount: {
                required: true
            }, total: {
                required: true
            }, startTime: {
                required: true
            }, endTime: {
                required: true,
            }, ruleDepict: {
                required: true
            }
        },
        messages: {
            couponName: {
                required: "优惠券名称不能为空"
            }, amount: {
                required: "优惠券面值不能为空"
            }, total: {
                required: "优惠券数量不能为空"
            }, startTime: {
                required: "优惠券生效时间不能为空"
            }, endTime: {
                required: "优惠券失效时间不能为空"
            }, ruleDepict: {
                required: "优惠券规则描述不能为空"
            }
        },
        submitHandler: couponEdit.submit
    });
    couponEdit.init();
})