/**
 * Created by Administrator on 2017/1/19.
 */
var CouponList = {
    parkId:'',//选择停车场id
    couponName:'',//券名称
    startTime:'',//开始时间
    endTime : '',//结束时间
    table : null,
    init:function () {
        var _t = this;
        Common.Ajax.ajaxRequest({
            url: "/mgr/park/getParkList",
            type: "get",
            data: {}
        },function(data){
            var optionHtml = "";
            $.each(data.parkList,function (i,item) {
                optionHtml += `<option value='${item.id}'>${item.parkName}</option>`;
            })
            $("#parkId").append(optionHtml).comboSelect();
        });
        $("#coupon_serach").on("click",function () {
            _t.table.fnDraw();
        });
        $("#couponAdd").on("click",function () {
            window.location.href = ctx + "/mgr/coupon/addView";
        });
        $("#couponStop").on("click",function () {
            _t.couponHandle(_t,2);
        });
        $("#couponDel").on("click",function () {
            _t.couponHandle(_t,1);
        });
        $("#couponStart").on("click",function () {
            _t.couponHandle(_t,0);
        });
        $("#selectAll").on("click",function () {
            if($("input[name='selectAll']:checked")[0]){
                $("input[name='select']").prop("checked",true);
            }else{
                $("input[name='select']").removeAttr("checked");
            }
        });
        $("#couponReset").on("click",this.reset);
        this.getTableData();
    },

    getTableData:function(){
        var _t = this;
        this.table = $('#couponList').dataTable({
            "fnServerData": function (sSource, aoData, fnCallback) {
                $.ajax({
                    "dataType": 'json',
                    "type": "POST",
                    "url": ctx + "/mgr/coupon/ajaxList",
                    "data": {
                        "draw": aoData.draw,
                        "currentResult": aoData.start,
                        "showCount": aoData.length,
                        "parkId" : $("#parkId").val(),
                        "startTime":$("#startTime").val(),
                        "endTime":$("#endTime").val(),
                        "couponName" : $("#couponName").val(),
                        "state":$("#state").val()
                    },
                    "success": fnCallback
                });
            },
            "bPaginate" : true,
            "aoColumns": [{
                "data": null
            },{
                "data": "id"
            }, {
                "data": "ascription"
            },{
                "data": "couponName"
            }, {
                "data": null
            }, {
                "data": null
            }, {
                "data": null
            },{
                "data": null
            },{
                "data": null
            }],
            "fnRowCallback":function(nRow, aData, iDisplayIndex){
                $('td:eq(0)', nRow).html(`<input type="checkbox" class="select" name="select" value="${aData.id}">&nbsp;&nbsp;` + (iDisplayIndex+1));
                $('td:eq(4)', nRow).html(aData.total - aData.useCount);
                $('td:eq(5)', nRow).html(formatDate(aData.startTime,4));
                $('td:eq(6)', nRow).html(formatDate(aData.endTime,4));
                $('td:eq(7)', nRow).html(aData.state==0?"启用":"禁用");
                $('td:eq(8)', nRow).html(_t.oprateHtml(aData.id,aData.state));
            },
            "fnInitComplete": function(oSettings, json) {
                $(".select").on("click",function (e) {
                    if(!$(e.currentTarget).prop("checked")){
                        $("input[name='selectAll']").prop("checked",false);
                    }
                });
                $(".coupon-show").on("click",function (e) {
                   var id = $(e.currentTarget).attr("o");
                    $.ajax({
                        "dataType": 'json',
                        "type": "get",
                        "url": ctx + "/mgr/coupon/queryCouponById",
                        "data": {id:id},
                        "success": function (data) {
                            _t.setModalData(data.coupon);
                        }
                    });
                });
            }
        });
    },
    setModalData:function (coupon) {
        $("#couponName_dialog").html(coupon.couponName);
        $("#parkName_dialog").html(coupon.ascription);
        $("#total_dialog").html(coupon.total+"张");
        $("#amount_dialog").html(coupon.amount+"元");
        $("#couponTime_dialog").html(formatDate(coupon.startTime,4)+" 至 "+formatDate(coupon.endTime,4));
        $("#ruleDepict_dialog").html(coupon.ruleDepict);
        $("#my-modal-alert").modal("toggle");
    },
    oprateHtml:function(id,state){
        if(state == 0){
            return `<a href="javascrit:void(0)"><i class="fa text-muted">编辑</i></a>
                    <a href="javascrit:void(0)"><i class="fa text-navy coupon-show" o="${id}">查看</i></a>`;
        }else if(state == 2){
            return `<a href="couponEditView/${id}"><i class="fa text-navy">编辑</i></a>
                    <a href="javascrit:void(0)"><i class="fa text-muted">查看</i></a>`;
        }
    },
    couponHandle:function (that,type) {
        Common.Ajax.ajaxRequest({
            url: "/mgr/coupon/couponHandle",
            type: "get",
            data: {ids:that.getAllSelected(),type:type}
        },function(data){
            if(data.isSuccess == 0){
                that.table.fnDraw();
                if(type == 0){
                    toastr.success("启用成功");
                }else if(type == 1){
                    toastr.success("删除成功");
                }else{
                    toastr.success("禁用成功");
                }

            }
        })
    },
    reset:function () {
        $("#parkId").val('');
        $("#startTime").val('');
        $("#endTime").val('');
        $("#couponName").val('');
        $("#state").val('');
    },
    getAllSelected :function () {
        var value = '';
        $("input[name='select']:checkbox:checked").each(function(){
            value+=$(this).val()+","
        })
        return value.slice(0,value.length-1);
    }
}
$(document).ready(function () {
    CouponList.init();
})