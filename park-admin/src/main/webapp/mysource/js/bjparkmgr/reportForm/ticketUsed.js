var table;
$(function () {
    $("#userList_queryDiv").bind("keydown", function (e) {
        // 兼容FF和IE和Opera
        var theEvent = e || window.event;
        var code = theEvent.keyCode || theEvent.which || theEvent.charCode;
        if (code == 13) {
            //回车执行查询
            $("#serach").click();
        }
    });

    getParkId();

    table = $('#ticketUsed_table').dataTable({
        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                "dataType": 'json',
                "type": "POST",
                "url": ctx +
                "/mgr/ticketUsed/ajaxTicketUsedList",
                "data": {
                    "currentResult": aoData.start,
                    "showCount": aoData.length,
                    "draw": aoData.draw,
                    "parkId": $("#parkId").val(),
                    "couponName": $("#couponName").val(),
                    "startTimeStr": $("#startTime").val(),
                    "endTimeStr": $("#endTime").val()
                },
                error: function (json) {
                    // table.fnDraw();
                    toastr.error("后台异常")
                },
                success: fnCallback
            });
        },
        "aoColumns": [
            {"data": null},
            {"data": "ascription"},
            {"data": "couponName"},
            {"data": "startTime"},
            {"data": "endTime"},
            {"data": "total"},
            {"data": "useCount"},
            {"data": "total"},
            {"data": "useCount"},
            {"data": null}
        ]
        ,
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            $('td:eq(0)', nRow).html(iDisplayIndex + 1);
            $('td:eq(3)', nRow).html(DateFormatHandle(aData.startTime));
            $('td:eq(4)', nRow).html(DateFormatHandle(aData.endTime));
            $('td:eq(9)', nRow).html(getOperate(aData));
        }
    });

    /**
     * 搜索
     */
    $('#ticketUsed_serach').click(function () {
        table.fnDraw();
    });
});


/**
 * 导出Excel
 */
$('#ticketUsed_export').click(function() {
   var parkId = $("#parkId").val();
   var couponName = $("#couponName").val();
   var startTimeStr = $("#startTime").val();
   var endTimeStr = $("#endTime").val();
    var url = ctx + "/mgr/ticketUsed/exportTicketUsedList" + "?" +
                "parkId=" + parkId + "&" +
                "couponName=" + couponName + "&" +
                "startTimeStr=" + startTimeStr + "&" +
                "endTimeStr=" + endTimeStr;
    window.location.href = url;

});



/**
 * 时间格式处理
 * @param data 时间
 * @constructor
 */
function DateFormatHandle(time) {
    return formatDate(time, 7);
}


/**
 * 操作
 * @param data
 * @returns {string}
 */
function getOperate(data) {
    var str = '<button onclick="detail(\'' + data.id + '\')" class="btn btn-primary btn-xs">查看明细</button>&nbsp;&nbsp;';
    // var str = '<button onclick="detail(\'' + data.id + '\'' + ',' + '\'' + data.parkId + '\')" class="btn btn-primary btn-xs">查看明细</button>&nbsp;&nbsp;';
    return str;
}


/**
 * 获取明细
 * @param couponId
 */
function detail(couponId) {
    var url = ctx + "/mgr/ticketUsed/getDetail" + "?" +
        "couponId=" + couponId;
    window.location.href = url;
}

/**
 * 获取停车场
 */
function getParkId() {
    $.get("/mgr/park/getParkList",function(data,status){
        if(status == "success"){
            if(data.isSuccess){
                data.parkList.forEach(function(obj, index ) {
                    // console.log( val, index, this );
                    $("#parkId").append("<option value=" + obj.id + ">" + obj.parkName + "</option>");
                });
            }
        }
    });
}
