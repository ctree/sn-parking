var table;
$(function () {
    $("#userList_queryDiv").bind("keydown", function (e) {
        // 兼容FF和IE和Opera
        var theEvent = e || window.event;
        var code = theEvent.keyCode || theEvent.which || theEvent.charCode;
        if (code == 13) {
            //回车执行查询
            $("#serach").click();
        }
    });


    var table = $('#settlement_table').dataTable({
        "bInfo" : false,// 页脚信息
        // "bAutoWidth": true,//自动宽度
        "fnInitComplete" : function() {
            getParkId();
        },
        "sScrollY": "100%", "sScrollX": "98%",
        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                "dataType": 'json',
                "type": "POST",
                "url": ctx +
                "/mgr/couponDetails/couponDetailsList",
                "data": {
                    "currentResult": aoData.start,
                    "showCount": aoData.length,
                    "draw": aoData.draw,
                    "parkId": $("#parkId").val(),
                    "startTimeStr": $("#startTime").val(),
                    "endTimeStr": $("#endTime").val()
                },
                error: function (json) {
                    // table.fnDraw();
                    toastr.error("后台异常")
                },
                success: fnCallback
            });
        },
        "aoColumns": [
            {"data": null,
                // "sClass": "center",
                // "sWidth":"10%",//定义列宽度，以百分比表示
                "sDefaultContent" : "",
            },
            {"data": null},
            {"data": null},
            {"data": null},
            {"data": null},
            {"data": null},
            {"data": null},
            {"data": null},
            {"data": null},
            {"data": null},
            {"data": null},
            {"data": null},
            {"data": null}
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            $('td:eq(0)', nRow).html(iDisplayIndex + 1);
            $('td:eq(1)', nRow).html( getParkName());
            $('td:eq(2)', nRow).html( aData.id);
            $('td:eq(3)', nRow).html( aData.createName);
            $('td:eq(4)', nRow).html( aData.carNum);
            $('td:eq(5)', nRow).html( getRecvChannel(aData.recvChannel));
            $('td:eq(6)', nRow).html( DateFormatHandle(aData.successTime));
            $('td:eq(7)', nRow).html( DateFormatHandle(aData.successTime));

            $('td:eq(8)', nRow).html( getPayType(aData));
            $('td:eq(8)', nRow).attr("style", "padding:0px!important");
            $('td:eq(9)', nRow).html( getPayAmount(aData));
            $('td:eq(9)', nRow).attr("style", "padding:0px!important");
            $('td:eq(10)', nRow).html( getCouponName(aData));
            $('td:eq(10)', nRow).attr("style", "padding:0px!important");
            $('td:eq(11)', nRow).html( getCouponId(aData));
            $('td:eq(11)', nRow).attr("style", "padding:0px!important");

            $('td:eq(12)', nRow).html( getCouponAmount(aData));
            $('td:eq(12)', nRow).attr("style", "padding:0px!important");

            merge(nRow, aData, iDisplayIndex, 1);
            // table.mergeCells()
        }
    });

    // $("#ticketUsed_table tbody td").attr("rowSpan", 10);
    // $("#ticketUsed_table ").find("tbody").find("td").attr("rowSpan", 10);

    /**
     * 搜索
     */
    $('#settlement_serach').click(function () {
        table.fnDraw();
    });

});

/**
 * 收费渠道
 * @param data
 */
function getRecvChannel(recvChannel) {
    var str = '';
    if ( recvChannel == "0") {
        str += "微信";
    } else if (recvChannel == "1") {
        str += "APP";
    } else {
        str += "未知";
    }
    return str;
}

/**
 * 获取支付类型
 * @param aData
 * @returns {string}
 */
function getPayType(aData){
    var str = "";
    if( !$.isEmptyObject(aData)) {
        var orderPayList = aData.orderPayList;
        if ( !$.isEmptyObject(orderPayList)){
            for(var i =0; i< orderPayList.length; i++){
                if(i > 0 && i !== ( orderPayList.length) ){
                    str += "<hr class='hr-define'/>";
                }
                if( i > 0 && i == ( orderPayList.length-1) ){
                    str +="<div style='text-align: center'></div>";
                }
                str  += "<br/><div style='text-align: center'>&nbsp;" + getPayTypeName(orderPayList[i].payType) + "&nbsp;</div>";
            }
        }
    }
    return str;
}

/**
 * 获取支付方式名称
 * @param state 付款方式
 * @returns {string}
 */
function getPayTypeName(payType) {
    var str = '';
    if ( payType == "0") {
        str += "<span class='label label-info'>现金</span>";
    } else if (payType == "1") {
        str += "<span class='label label-success'>优惠券</span>";
    } else {
        str += "<span class='label label-danger'>未知</span>";
    }
    return str;
}

/**
 * 获取支付金额
 * @param aData
 * @returns {string}
 */
function getPayAmount(aData){
    var str = "";
    if( !$.isEmptyObject(aData)) {
        var orderPayList = aData.orderPayList;
        if ( !$.isEmptyObject(orderPayList)){
            for(var i =0; i< orderPayList.length; i++){
                if(i > 0 && i !== ( orderPayList.length) ){
                    str += "<hr class='hr-define'/>";
                }
                if( i > 0 && i == ( orderPayList.length-1) ){
                    str +="<div style='text-align: center'></div>";
                }
                str  += "<br/><div style='text-align: center'>&nbsp;" + orderPayList[i].amount + "&nbsp;</div>";
            }
        }
    }
    return str;
}

/**
 * 获取优惠券名称
 * @param aData
 * @returns {string}
 */
function getCouponName(aData){
    var str = "";
    if( !$.isEmptyObject(aData)) {
        var orderPayList = aData.orderPayList;
        if ( !$.isEmptyObject(orderPayList)){
            for(var i =0; i< orderPayList.length; i++){
                if(i > 0 && i !== ( orderPayList.length) ){
                    str += "<hr class='hr-define'/>";
                }
                if( i > 0 && i == ( orderPayList.length-1) ){
                    str +="<div style='text-align: center'></div>";
                }
                str  += "<br/><div style='text-align: center'>&nbsp;" +
                    getCoupon( orderPayList[i] )
                     + "&nbsp;</div>";
            }
        }
    }
    return str;
}

/**
 * 获取优惠券
 * @param data 返回数据
 * @param payType 支付类型
 */
function  getCoupon(data) {
    if(data.payType==1){ //支付类型为优惠券
        return ( $.isEmptyObject(data.couponName) ? "-" : data.couponName )
    }else{
        return " - "
    }
}



/**
 * 获取优惠券ID
 * @param aData
 * @returns {string}
 */
function getCouponId(aData){
    var str = "";
    if( !$.isEmptyObject(aData)) {
        var orderPayList = aData.orderPayList;
        if ( !$.isEmptyObject(orderPayList)){
            for(var i =0; i< orderPayList.length; i++){
                if(i > 0 && i !== ( orderPayList.length) ){
                    str += "<hr class='hr-define'/>";
                }
                if( i > 0 && i == ( orderPayList.length-1) ){
                    str +="<div style='text-align: center'></div>";
                }
                str  += "<br/><div style='text-align: center'>&nbsp;" +
                    couponIdHandle( orderPayList[i] )
                    + "&nbsp;</div>";
            }
        }
    }
    return str;
}

/**
 * 券号处理
 * @param data
 * @returns {string}
 */
function  couponIdHandle(data) {
    if(data.payType==1){ //支付类型为优惠券
        return ( $.isEmptyObject(data.couponUserId) ? " - " : data.couponUserId )
    }else{
        return " - "
    }
}



/**
 * 获取券金额
 * @param aData
 * @returns {string}
 */
function getCouponAmount(aData){
    var str = "";
    if( !$.isEmptyObject(aData)) {
        var orderPayList = aData.orderPayList;
        if ( !$.isEmptyObject(orderPayList)){
            for(var i =0; i< orderPayList.length; i++){
                if(i > 0 && i !== ( orderPayList.length) ){
                    str += "<hr class='hr-define'/>";
                }
                if( i > 0 && i == ( orderPayList.length-1) ){
                    str +="<div style='text-align: center'></div>";
                }
                str  += "<br/><div style='text-align: center'>&nbsp;" +
                    couponAmountHandle( orderPayList[i] )
                    + "&nbsp;</div>";
            }
        }
    }
    return str;
}
/**
 * 获取券金额
 * @param data
 * @returns {string}
 */
function  couponAmountHandle(data) {
    if(data.payType==1){ //支付类型为优惠券
        return ( $.isEmptyObject(data.couponAmount) ? " - " : data.couponAmount )
    }else{
        return " - "
    }
}

/**
 * 合并单元格
 * @param nRow 行数据
 * @param aData 回调数据
 * @param iDataIndex 行角标（0开始）
 * @param mergeTd 合并列 （0开始）
 */
function merge( nRow, aData, iDataIndex, mergeTd ) {
    if (iDataIndex == 0) {
        $('td:eq(' + mergeTd + ')', nRow).attr("rowSpan", 10);
        $('td:eq(' + mergeTd + ')', nRow).attr("style", "text-align:center; vertical-align: middle;height: 60px;");

        // $('td:eq(' + mergeTd+1 + ')', nRow).remove();
    }
    if (iDataIndex >= 1) {
        $('td:eq(' + mergeTd + ')', nRow).remove();
    }
}



/**
 * 导出Excel
 */
$('#settlement_export').click(function() {
   var parkId = $("#parkId").val();
    var obj = document.getElementById("parkId");
    var parkName = obj.options[obj.selectedIndex].text;
    // var parkName = $("#parkId").option.text();
   var startTimeStr =  $("#startTime").val();
   var endTimeStr =  $("#endTime").val();
    var url = ctx + "/mgr/settleMent/exportSettlementList" + "?" +
                "parkId=" + parkId + "&" +
                "parkName=" + parkName + "&" +
                "startTimeStr=" + startTimeStr + "&" +
                "endTimeStr=" + endTimeStr;
    window.location.href = url;

});


/**
 * 时间格式处理
 * @param data 时间
 * @constructor
 */
function DateFormatHandle(time) {
    return formatDate(time, 7);
}
