var table;
var tables;
$(function () {
    $("#userList_queryDiv").bind("keydown", function (e) {
        // 兼容FF和IE和Opera
        var theEvent = e || window.event;
        var code = theEvent.keyCode || theEvent.which || theEvent.charCode;
        if (code == 13) {
            //回车执行查询
            $("#serach").click();
        }
    });
    /**
     * 明细列表
     * @type {*}
     */
    table = $('#ticketUsed_detailed_table').dataTable({
        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                "dataType": 'json',
                "type": "POST",
                "url": ctx +
                "/mgr/ticketUsed/ajaxTicketUsedDetailedList",
                "data": {
                    "currentResult": aoData.start,
                    "showCount": aoData.length,
                    "draw": aoData.draw,
                    "state": $("#state").val(),
                    "couponId" : $("#couponId").val()
                },
                error: function (json) {
                    // table.fnDraw();
                    toastr.error("后台异常")
                },
                success: fnCallback
            });
        },
        "aoColumns": [
            {"data": null},
            {"data": "snUserId"},
            {"data": "couponName"},
            {"data": "actionName"},
            {"data": "createTime"},
            {"data": "lastTime"},
            {"data": "state"}
        ]
        ,
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            $('td:eq(0)', nRow).html(iDisplayIndex + 1);
            $('td:eq(4)', nRow).html(DateFormatHandle(aData.createTime));
            $('td:eq(5)', nRow).html(DateFormatHandle(aData.lastTime));
            $('td:eq(6)', nRow).html(getState(aData.state));
        }
    });

    /**
     * 搜索
     */
    $('#ticketUsed_detailed_search').click(function () {
        table.fnDraw();
    });
});


/**
 * 导出明细Excel
 */
$('#ticketUsed_detailed_export').click(function() {
    var state = $("#state").val();
    var couponId = $("#couponId").val();
    var url = ctx + "/mgr/ticketUsed/exportTicketUsedDetailed" + "?" +
                "state=" + state + "&" +
                "couponId=" + couponId;
    window.location.href = url;

});

/**
 * 时间格式处理
 * @param data 时间
 * @constructor
 */
function DateFormatHandle(time) {
    return formatDate(time, 7);
}

/**
 * 获明细取券状态
 * @param state 券状态标识
 * @returns {string}
 */
function getState(state) {
    var str = '';
    if ( state == "0") {
        str += "<span class='label label-info'>未使用</span>";
    } else if (state == "1") {
        str += "<span class='label label-success'>已使用</span>";
    } else {
        str += "<span class='label label-danger'>未知</span>";
    }
    return str;
}

