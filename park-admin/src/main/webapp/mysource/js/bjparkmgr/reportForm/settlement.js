var table;
$(function () {
    $("#userList_queryDiv").bind("keydown", function (e) {
        // 兼容FF和IE和Opera
        var theEvent = e || window.event;
        var code = theEvent.keyCode || theEvent.which || theEvent.charCode;
        if (code == 13) {
            //回车执行查询
            $("#serach").click();
        }
    });


    var table = $('#settlement_table').dataTable({
        "fnInitComplete" : function() {
            getParkId();
        },
        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                "dataType": 'json',
                "type": "POST",
                "url": ctx +
                "/mgr/settleMent/ajaxSettlementList",
                "data": {
                    "currentResult": aoData.start,
                    "showCount": aoData.length,
                    "draw": aoData.draw,
                    "parkId": $("#parkId").val(),
                    "startTimeStr": $("#startTime").val(),
                    "endTimeStr": $("#endTime").val()
                },
                error: function (json) {
                    // table.fnDraw();
                    toastr.error("后台异常")
                },
                success: fnCallback
            });
        },
        "aoColumns": [
            {"data": null},
            {"data": "parkId"},
            {"data": "successTime"},
            {"data": "payType"},
            {"data": "amount"}
        ]
        ,
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            $('td:eq(0)', nRow).html(iDisplayIndex + 1);
            $('td:eq(1)', nRow).html( getParkName());
            $('td:eq(2)', nRow).html(DateFormatHandle(aData.successTime));
            $('td:eq(3)', nRow).html(getPayType( aData.payType ));
            merge(nRow, aData, iDisplayIndex);
        }
    });

    // $("#ticketUsed_table tbody td").attr("rowSpan", 10);
    // $("#ticketUsed_table ").find("tbody").find("td").attr("rowSpan", 10);

    /**
     * 搜索
     */
    $('#settlement_serach').click(function () {
        table.fnDraw();
    });

});

/**
 * 合并单元格
 * @param nRow
 * @param aData
 * @param iDataIndex
 */
function merge( nRow, aData, iDataIndex ) {
    if (iDataIndex == 0) {
        $('td:eq(1)', nRow).attr("rowSpan", 10);
        $('td:eq(1)', nRow).attr("style", "text-align:center; vertical-align: middle;height: 60px;");
    }
    if (iDataIndex >= 1) {
        $('td:eq(1)', nRow).remove();
    }
}

/**
 * 获取支付方式
 * @param state 付款方式
 * @returns {string}
 */
function getPayType(payType) {
    var str = '';
    if ( payType == "0") {
        str += "<span class='label label-info'>现金</span>";
    } else if (payType == "1") {
        str += "<span class='label label-success'>优惠券</span>";
    } else {
        str += "<span class='label label-danger'>未知</span>";
    }
    return str;
}

/**
 * 导出Excel
 */
$('#settlement_export').click(function() {
   var parkId = $("#parkId").val();
    var obj = document.getElementById("parkId");
    var parkName = obj.options[obj.selectedIndex].text;
    // var parkName = $("#parkId").option.text();
   var startTimeStr =  $("#startTime").val();
   var endTimeStr =  $("#endTime").val();
    var url = ctx + "/mgr/settleMent/exportSettlementList" + "?" +
                "parkId=" + parkId + "&" +
                "parkName=" + parkName + "&" +
                "startTimeStr=" + startTimeStr + "&" +
                "endTimeStr=" + endTimeStr;
    window.location.href = url;

});




/**
 * 时间格式处理
 * @param data 时间
 * @constructor
 */
function DateFormatHandle(time) {
    return formatDate(time, 7);
}
