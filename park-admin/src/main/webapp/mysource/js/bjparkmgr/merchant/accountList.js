var CouponList = {
    init : function () {
        this.getTableData();
        /**
         * 查询
         */
        $('#serach').click(function () {
            if (table == null) {
                toastr.error("没有数据");
            } else {
                table.fnDraw();
            }
        });
    },

    getTableData:function(){
        var _t = this;
        $('#couponList').dataTable({
            "fnServerData": function (sSource, aoData, fnCallback) {
                $.ajax({
                    "dataType": 'json',
                    "type": "POST",
                    "url": ctx + "/mgr/merchant/ajaxList",
                    "data": {
                        "draw": aoData.draw,
                        "currentResult": aoData.start,
                        "showCount": aoData.length,
                        "merchantId" : $("#merchantId").val(),
                        "parkId" : $("#parkId").val()
                    },
                    "success": fnCallback
                });
            },
            "bPaginate" : true,
            "aoColumns": [{
                "data": null
            }, {
                "data": "login"
            }, {
                "data": "createDate"
            }, {
                "data": null
            }],
            "fnRowCallback":function(nRow, aData, iDisplayIndex){
                $('td:eq(0)', nRow).html(iDisplayIndex+1);
                $('td:eq(2)', nRow).html(formatDate(aData.createDate,4));
                $('td:eq(3)', nRow).html(_t.oprateHtml(aData.userId));

            }

        });
    },

    oprateHtml:function(userId){
        return `<a href="accountIn/${userId}"><i class="fa text-navy">充值&nbsp;&nbsp;</i></a>
                <a href="accountOut/${userId}"><i class="fa text-navy">扣款&nbsp;&nbsp;</i></a>
                <a href="couponMng/${userId}"><i class="fa text-navy">优惠券&nbsp;&nbsp;</i></a>`;
    }
}

$(document).ready(function () {
    CouponList.init();
})
