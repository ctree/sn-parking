var CouponList = {
    init : function () {
        this.getTableData()
        /**
         * 查询
         */
        $('#serach').click(function () {
            if (table == null) {
                toastr.error("没有数据");
            } else {
                table.fnDraw();
            }
        });
    },

    getTableData:function(){
        $('#couponList').dataTable({
            "fnServerData": function (sSource, aoData, fnCallback) {
                $.ajax({
                    "dataType": 'json',
                    "type": "POST",
                    "url": ctx + "/mgr/coupon/ajaxList",
                    "data": {
                        "draw": aoData.draw,
                        "currentResult": aoData.start,
                        "showCount": aoData.length,
                        "startTime" : $("#startTime").val(),
                        "endTime" : $("#endTime").val()
                    },
                    "success": fnCallback
                });
            },
            "bPaginate" : true,
            "aoColumns": [{
                "data": "createTime"
            }, {
                "data": "createId"
            }, {
                "data": "createName"
            }, {
                "data": "createTime"
            }, {
                "data": "startTime"
            },{
                "data": "ascription"
            },  {
                "data": "ruleType"
            }, {
                "data": "lowestAmount"
            }, {
                "data": "lastTime"
            }, {
                "data": "createTime"
            }, {
                "data": "lastTime"
            }, {
                "data": "lastTime"
            }],

        });
    }
}

$(document).ready(function () {
    CouponList.init();
})
