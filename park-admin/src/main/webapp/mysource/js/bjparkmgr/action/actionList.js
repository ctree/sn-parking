/**
 * Created by Administrator on 2017/1/19.
 */
var ActionList = {
    parkId:'',//选择停车场id
    startTime:'',//开始时间
    endTime : '',//结束时间
    table : null,
    init:function () {
        var _t = this;
        Common.Ajax.ajaxRequest({
            url: "/mgr/park/getParkList",
            type: "get",
            data: {}
        },function(data){
            var optionHtml = "";
            $.each(data.parkList,function (i,item) {
                optionHtml += `<option value='${item.id}'>${item.parkName}</option>`;
            })
            $("#parkId").append(optionHtml).comboSelect();
        })
        this.getTableData();

        $("#action_serach").on("click",function () {
            _t.table.fnDraw();
        });
        $("#actionStop").on("click",function () {
            _t.actionHandle(_t,2);
        });
        $("#actionDel").on("click",function () {
            _t.actionHandle(_t,1);
        });
        $("#actionStart").on("click",function () {
            _t.actionHandle(_t,0);
        });
        $("#selectAll").on("click",function () {
            if($("input[name='selectAll']:checked")[0]){
                $("input[name='select']").prop("checked",true);
            }else{
                $("input[name='select']").removeAttr("checked");
            }
        });
        $("#actionReset").on("click",this.reset);
        $("#actionAdd").on("click",function () {
           window.location.href = ctx + "/mgr/action/addActionView";
        });
    },

    getTableData:function(){
        var _t = this;
       this.table = $('#actionList').dataTable({
            "fnServerData": function (sSource, aoData, fnCallback) {
                $.ajax({
                    "dataType": 'json',
                    "type": "POST",
                    "url": ctx + "/mgr/action/actionList",
                    "data": {
                        "draw": aoData.draw,
                        "currentResult": aoData.start,
                        "showCount": aoData.length,
                        "parkId" : $("#parkId").val(),
                        "startTime":$("#startTime").val(),
                        "endTime":$("#endTime").val(),
                        "actionName" : $("#actionName").val(),
                        "state":$("#state").val(),
                        "type":$("#type").val()
                    },
                    "success": fnCallback
                });
            },
            "bPaginate" : true,
            "aoColumns": [{
                "data": null
            }, {
                "data": "parkName"
            },{
                "data": "actionName"
            }, {
                "data": null
            }, {
                "data": null
            }, {
                "data": "type"
            }, {
                "data": null
            }, {
                "data": "operatorName"
            }, {
                "data": null
            }],
            "fnRowCallback":function(nRow, aData, iDisplayIndex){
                $('td:eq(0)', nRow).html(`<input type="checkbox" class="select" name="select" value="${aData.id}">&nbsp;&nbsp;` + (iDisplayIndex+1));
                $('td:eq(3)', nRow).html(formatDate(aData.startTime,4));
                $('td:eq(4)', nRow).html(formatDate(aData.endTime,4));
                $('td:eq(5)', nRow).html(aData.type==0?"注册有礼":"会员权益");
                $('td:eq(6)', nRow).html(aData.state==0?"启用":"禁用");
                $('td:eq(8)', nRow).html(_t.oprateHtml(aData.id,aData.state));
            },
            "fnInitComplete": function(oSettings, json) {
                $(".select").on("click",function (e) {
                    if(!$(e.currentTarget).prop("checked")){
                        $("input[name='selectAll']").prop("checked",false);
                    }
                });
                $(".action-show").on("click",function (e) {
                    var id = $(e.currentTarget).attr("o");
                    $.ajax({
                        "dataType": 'json',
                        "type": "get",
                        "url": ctx + "/mgr/action/queryActionById",
                        "data": {id:id},
                        "success": function (data) {
                            $.ajax({
                                "dataType": 'json',
                                "type": "get",
                                "url": ctx + "/mgr/action/queryActionCouponByActionId",
                                "data": {actionId: id},
                                "success": function (data1) {
                                    _t.setModalData(data.action,data1.actionCouponList);
                                }
                            });

                        }
                    });
                });
            }
        });
    },
    setModalData:function (action,actionCouponList) {
        var couponHtml = "";
        if(type == 0){
            $("#participant_dialog").hide();
        }else{
            $("#participant_dialog").html(this.parseParticipant(action.participant));
        }
        $("#actionName_dialog").html(action.actionName);
        $("#parkName_dialog").html(action.parkName);
        $("#type_dialog").html(action.type=="0"?"注册有礼":"会员权益");
        $("#actionTime_dialog").html(formatDate(action.startTime,4)+" 至 "+formatDate(action.endTime,4));
        $("#coverUrl_dialog").html(action.coverUrl);
        $("#logoUrl_dialog").attr("src",action.logo);
        $.each(actionCouponList,function (i,item) {
            couponHtml += `<ul>
                            <li class="col-sm-3">
                                <div>${++i}</div>
                            </li>
                            <li class="col-sm-4">
                               <div>${item.couponName}</div>
                            </li>
                            <li class="col-sm-3">${item.num}</li>
                        </ul>`;
        });
        $(".coupon-group").find("ul:gt(0)").remove();
        $(".coupon-group").append(couponHtml);
        $("#my-modal-alert").modal("toggle");
    },
    oprateHtml:function(id,state){
        if(state == 0){
            return `<a href="javascrit:void(0)"><i class="fa text-muted">编辑</i></a>
                    <a href="javascrit:void(0)"><i class="fa text-navy action-show" o="${id}">查看</i></a>`;
        }else if(state == 2){
            return `<a href="actionEditView/${id}"><i class="fa text-navy">编辑</i></a>
                    <a href="javascrit:void(0)"><i class="fa text-muted">查看</i></a>`;
        }
    },
    parseParticipant:function (part) {
        switch (part){
            case '':
                return '全部会员';
            case '0':
                return '至尊会员';
            case '1':
                return '钻石会员';
            case '2':
                return '电子会员';
        }
    },
    actionHandle:function (that,type) {
        Common.Ajax.ajaxRequest({
            url: "/mgr/action/actionHandle",
            type: "get",
            data: {ids:that.getAllSelected(),type:type}
        },function(data){
            if(data.isSuccess == 0){
                that.table.fnDraw();
                if(type == 0){
                    toastr.success("启用成功");
                }else if(type == 1){
                    toastr.success("删除成功");
                }else{
                    toastr.success("禁用成功");
                }

            }
        })
    },
    reset:function () {
        $("#parkId").val('');
        $("#startTime").val('');
        $("#endTime").val('');
        $("#type").val('');
        $("#actionName").val('');
        $("#state").val('');
    },
    getAllSelected :function () {
        var value = '';
        $("input[name='select']:checkbox:checked").each(function(){
            value+=$(this).val()+","
        })
        return value.slice(0,value.length-1);
    }
}

$(document).ready(function () {
    ActionList.init();
});