

function showBlock(){
	jQuery.blockUI({ message: "操作处理中，请耐心等待...", css: {color:'#55AAAA',border:'0px #FFFFFF',backgroundColor:'rgba(0, 0, 0, 0.0)',width:'20%',margin:'10%'},overlayCSS: { opacity:'0.0' }});
	setTimeout('hideBlock()',5000);//5000毫秒后调用hideBlock()
}
function hideBlock(){
	jQuery.unblockUI();
}



$(function() {
	$("#passwordForm").validate({
		focusCleanup:true,
		focusInvalid:false,
		onkeyup:true,
		ignore:"input:radio[name=parentCode]",
		rules : {
			oldPassword : {
				required : true,
				minlength : 1,
				maxlength:25,
				remote:{
					url :ctx+'/mgr/userManager/checkOldPassword',
					type:'post',
					data:{
						oldPassword:function(){
							return $("#oldPassword").val();
						}
					}
				}
			},

			password : {
				required : true,
				minlength : 1,
				maxlength:25
			},
			passwordConfirm : {
				equalTo:"#password",
				required : true,
				minlength : 1,
				maxlength:25
			}

		},
		messages : {
			oldPassword:{
				required : "请输入旧密码",
				minlength : "密码不能小于1",
				maxlength:"密码不能大于25",
				remote :"旧密码不正确,请修改正确！"
			},
			password:{
				required : "请输入新密码",
				minlength : "新密码不能小于1",
				maxlength:"新密码不能大于25",
			},
			passwordConfirm : {
				equalTo : "两次输入的密码不同,请重新输入",
				required : "请输入确认新密码",
				minlength : "新密码不能小于1",
				maxlength:"新密码不能大于25",
			}
		},
		submitHandler:submit
	});
});


/**
 * 表单提交
 */
function submit(){
	$.ajax({
		url : ctx + '/mgr/userManager/updatePassword',
		type : 'post',
		dataType : 'json',
		timeout : ajaxTimeout,
		data : $('#passwordForm').serialize(),
		error : function(json) {
			toastr.error("修改失败");

		},
		success : function(json) {
			if(json.isSuccess){
				toastr.success(json.msg);
			}else{
				toastr.error(json.msg);
			}
		}
	});
}
