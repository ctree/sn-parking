var table;
$(function () {
    $("#userList_queryDiv").bind("keydown", function (e) {
        // 兼容FF和IE和Opera
        var theEvent = e || window.event;
        var code = theEvent.keyCode || theEvent.which || theEvent.charCode;
        if (code == 13) {
            //回车执行查询
            $("#serach").click();
        }
    });

    getParkId();

    table = $('#orderList_dataList').dataTable({
        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                "dataType": 'json',
                "type": "POST",
                "url": ctx +
                "/mgr/orderManager/ajaxParkOrderList",
                "data": {
                    "currentResult": aoData.start,
                    "showCount": aoData.length,
                    "draw": aoData.draw,
                    "parkId": $("#parkId").val(),
                    "orderType": $("#orderType").val(),
                    "recvChannel": $("#recvChannel").val(),
                    "carNum": $("#carNum").val(),
                    "queryTime": $("#successTime").val(),
                    "createName": $("#createName").val()
                },
                error: function (json) {
                    // table.fnDraw();
                    toastr.error("后台异常")
                },
                success: fnCallback
            });
        },
        "aoColumns": [
            {"data": null},
            {"data": "id"},
            {"data": "orderType"},
            {"data": "carNum"},
            {"data": "parkTime"},
            {"data": "recvChannel"},
            {"data": "parkAmount"},
            {"data": "successTime"},
            {"data": null}
        ]
        ,
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            $('td:eq(0)', nRow).html(iDisplayIndex + 1);
            $('td:eq(2)', nRow).html(orderState(aData));
            $('td:eq(5)', nRow).html(getRecvChannel(aData));
            // $('td:eq(4)', nRow).attr("style", "padding:0px!important");
            // $('td:eq(4)', nRow).html(a(aData,2))


            $('td:eq(7)', nRow).html(DateFormatHandle(aData));
            $('td:eq(8)', nRow).html(getOrderListOperate(aData));
        }
    });



    /**
     * 搜索
     */
    $('#payOrderList_serach').click(function () {
        table.fnDraw();
    });
});

// function a(aData, max){
//     var str = "";
//     for(var i =0; i<max; i++){
//         str  += "<br/><div style='text-align: center'>sdafsaf</div>";
//         if(i>=0 && i !== max-1){
//         str += "<hr class='hr-define'/>";
//         }
//     }
//     return str;
// }


/**
 * 导出Excel
 */
$('#payOrderList_export').click(function() {
    var parkId = $("#parkId").val();
    var orderType = $("#orderType").val();
    var recvChannel = $("#recvChannel").val();
    var carNum = $("#carNum").val();
    var queryTime = $("#successTime").val();
    var createName = $("#createName").val();
    var url = ctx + "/mgr/orderManager/exportPayOrderList" + "?" +
                "parkId=" + parkId + "&" +
                "orderType=" + orderType + "&" +
                "recvChannel=" + recvChannel + "&" +
                "carNum=" + carNum + "&" +
                "queryTime=" + queryTime + "&" +
                "createName=" + createName;
    window.location.href = url;
});

/**
 * 时间格式处理
 * @param data 时间
 * @constructor
 */
function DateFormatHandle(data) {
    return formatDate(data.successTime, 7);
}


/**
 * 订单状态
 * @param data 响应JSON
 * @returns 返回订单状态
 */
function orderState(data) {
    var str = '';
    if ( data.orderType == "0") {
        str += "<span class='label label-info'>未支付</span>";
    } else if (data.orderType == "1") {
        str += "<span class='label label-info'>支付中</span>";
    } else if (data.orderType == "2") {
        str += "<span class='label label-success'>支付成功</span>";
        // str += "支付成功";
    } else if (data.orderType == "3") {
       str += "<span class='label label-warning'>支付失败</span>";
    } else if (data.orderType == "4") {
       str += "<span class='label label-info'>退款中</span>";
    } else if (data.orderType == "5") {
       str += "<span class='label label-warning'>退款失败</span>";
    } else if (data.orderType == "6") {
       str += "<span class='label label-success'>退款成功</span>";
    } else {
        str += "<span class='label label-danger'>未知</span>";
    }
    return str;
}

/**
 * 收费渠道
 * @param data
 */
function getRecvChannel(data) {
    var str = '';
    if ( data.recvChannel == "0") {
        str += "微信";
    } else if (data.recvChannel == "1") {
        str += "APP";
    } else {
        str += "未知";
    }
    return str;
}

/**
 * 操作
 * @param data
 * @returns {string}
 */
function getOrderListOperate(data) {
    var str = '<button onclick="queryOrderPay(\'' + data.id + '\')" class="btn btn-primary btn-xs">查看明细</button>&nbsp;&nbsp;';
    if (data.orderType == "2") {
        str += '<button onclick="refund(\'' + data.id + '\')" class="btn btn-primary btn-xs">退款</button>&nbsp;&nbsp;';
    }
    return str;
}

/**
 * 明细查看
 */
var queryOrderPay = function (parkOrderId) {
    $.ajax({
        url: ctx + "/mgr/orderManager/orderPay",
        type: 'post',
        dataType: 'json',
        "data": {
            "parkOrderId": parkOrderId,
            "queryDate" :$("#successTime").val()
        },
        timeout: ajaxTimeout,
        error: function (json) {
            toastr.error("操作失败," + json.errorMSG)
        },
        success: function (json) {
            if (json.isSuccess == "0") {
                detailsHandle(json.result)
                //回写部分收费明细
                /*
                    //收费渠道-易付宝
                     $("#payByYFB_dialog").html(data.id);
                     //收费渠道-云钻抵扣
                     $("#payByYZ_dialog").html(data.id);
                    //收费渠道-停车券
                     $("#parkTicket_dialog").html(data.id);
                  */
                $("#my-modal-alert").modal("toggle");
            } else {
                toastr.error(json.errorMSG)
            }
        }
    });
    // $(".modal-backdrop").remove();//删除class值为modal-backdrop的标签，可去除阴影
}

/**
 * 查看明细
 * @param data
 */
function detailsHandle(data){
    //订单编号
    $("#orderId_dialog").html(data.id);
    //停车订单号
    $("#parkOrderId_dialog").html(data.parkOsId);
    //车牌号
    $("#carNum_dialog").html(data.carNum);
    //会员号
    $("#memberId_dialog").html(data.createId);
    //进场时间
    $("#enterTime_dialog").html(formatDate(data.inTime, 7));
    //出场时间
    $("#outTime_dialog").html(formatDate(data.outTime, 7));
    // 停放时长
    $("#parkTime_dialog").html(data.parkTime + "分钟");
    //收费渠道
    $("#tollChannel_dialog").html(getRecvChannel(data));
    //支付金额
    $("#payAmount_dialog").html("¥ " + data.payAmount);
    // 收费日期
    $("#chargeDate_dialog").html(formatDate(data.successTime, 7));

}

/**
 * 退款操作
 * @param id 订单ID
 */
var refund = function (id) {
    if (!window.confirm("确认该操作吗?")) {
        return;
    }
    $.ajax({
        url: ctx + "/mgr/orderManager/refund",
        type: 'post',
        dataType: 'json',
        "data": {
            "id": id
        },
        timeout: ajaxTimeout,
        error: function (json) {
            toastr.error("操作失败," + json.errorMSG)
        },
        success: function (json) {
            if (json.isSuccess == "0") {
                toastr.success("退款成功")
            } else {
                toastr.error(json.errorMSG)
            }
            table.fnPageChange(getPageNum(table, false));
        }
    });
}
