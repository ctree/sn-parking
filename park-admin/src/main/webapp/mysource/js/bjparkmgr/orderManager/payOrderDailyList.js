var table;
$(function () {
    $("#userList_queryDiv").bind("keydown", function (e) {
        // 兼容FF和IE和Opera
        var theEvent = e || window.event;
        var code = theEvent.keyCode || theEvent.which || theEvent.charCode;
        if (code == 13) {
            //回车执行查询
            $("#serach").click();
        }
    });


    var table = $('#payOrderDaily_dataList').dataTable({
        "bInfo" : false,// 页脚信息
        "sDom": "t<'row-fluid'<'span6'i><'span6'p>>",//定义表格的显示方式
        // "sPaginationType": "bootstrap",
        "fnInitComplete" : function() {
            getParkId();
        },
        "aoColumns" : [
            {
            "mData" : "",
            "sTitle" : "序号",
                // "sClass": "center",
            // "sWidth":"10%",//定义列宽度，以百分比表示
            "sDefaultContent" : "",
        }, {
            "mData" : "",
            "sTitle" : "收费渠道",
            "sDefaultContent" : "",
        }, {
            "mData" : "",
            "sTitle" : "收费方式",
            "sDefaultContent" : "",
        },  {
            "mData" : "",
            "sTitle" : "收费金额",
            "sDefaultContent" : "",
        }],
        // "fnCreatedRow": function( nRow, aData, iDataIndex ) {
        //     $('td:eq(1)', nRow).attr("rowSpan", 2);
        //     $("#payOrderDaily_dataList").find("tr").eq(1).find("td").eq(2).remove();
        // },
        "fnServerData": function (sSource, aoData, fnCallback) {
            $.ajax({
                "dataType": 'json',
                "type": "POST",
                "url": ctx +
                "/mgr/orderManager/ajaxPayOrderDailyList",
                "data": {
                    "currentResult": aoData.start,
                    "showCount": aoData.length,
                    "draw": aoData.draw,
                    "parkId": $("#parkId").val(),
                    "payType": $("#payType").val(),
                    "queryTime": $("#queryTime").val()
                },
                error: function (json) {
                    // table.fnDraw();
                    toastr.error("后台异常")
                },
                success: fnCallback
            });
        },
        "columns": [
            {"data": null            },
            {"data": null},
            {"data": null},
            {"data": null}
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            // $('td:eq(0)', nRow).attr("rowSpan", aData.orderType);
            // $('td:eq(1)', nRow).attr("rowSpan", aData.orderType);
            $('td:eq(0)', nRow).html(iDisplayIndex + 1);
            $('td:eq(1)', nRow).html(getRecvChannel(aData, iDisplayIndex));
            $('td:eq(2)', nRow).html(getThr(aData));
        }
    });
    /**
     * 搜索
     */
    $('#payOrderList_serach').click(function () {
        table.fnDraw();
    });
});


/**
 * 收费渠道
 * @param data
 */
function getRecvChannel(data, iDisplayIndex) {
    var str = '';
    if ( data.recvChannel == "0") {
        str += "微信";
    } else if (data.recvChannel == "1") {
        str += "APP";
    } else {
        str += "未知";
    }
    return str;
}


/**
 * 收费渠道
 * @param data
 */
function getThr(data) {
    var str = data.orderPayList[0].amount;

    return str;
}
