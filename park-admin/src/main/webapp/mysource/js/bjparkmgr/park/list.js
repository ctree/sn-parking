var table;
$(function() {
	$("#parkList_queryDiv").bind("keydown",function(e){
		// 兼容FF和IE和Opera
		var theEvent = e || window.event;
		var code = theEvent.keyCode || theEvent.which || theEvent.charCode;
		if (code == 13) {
			//回车执行查询
			$("#parkList_serach").click();
		}
	});

	table = $('#parkList_dataList').dataTable({
		"fnServerData" : function(sSource, aoData, fnCallback) {
			$.ajax({
				"dataType" : 'json',
				"type" : "POST",
				"url" : ctx + "/mgr/park/ajaxList",
				"data" : {
					"currentResult" : aoData.start,
					"showCount" : aoData.length,
					"draw":aoData.draw,
					"property" : $("#property").val(),
					"parkType" : $("#parkType").val(),
					"businessEntity" : $("#businessEntity").val(),
					"isFee" : $("#isFee").val(),
                    "parkName" : $("#parkName").val(),
                    "integralBodyType" : $("#integralBodyType").val()
				},
				"success" : fnCallback
			});
		},
		"aoColumns": [
			{"data": null},
			{"data": "id"},
			{"data": "parkName"},
			{"data": "propertyText"},
			{"data": "parkTypeText"},
			{"data": "businessEntityText"},
			{"data": "companyNo"},
			{"data": "companyName"},
			{"data": "projectNo"},
			{"data": "projectName"},
			{"data": "isFee"},
			{"data": "state"},
            {"data": "operatorName"},
			{"data": null}
		],
		"fnRowCallback":function(nRow, aData, iDisplayIndex){
			$('td:eq(0)', nRow).html(viewNumber(iDisplayIndex,aData));
            $('td:eq(10)', nRow).html(aData.isFee=="0"?"是":aData.isFee=="1"?"否":"");
			$('td:eq(11)', nRow).html(viewState(aData));
			$('td:eq(13)', nRow).html(getOperate(aData));
		}
	});

	/**
	 * 查询
	 */
	$('#parkList_serach').click(function() {
		table.fnDraw();
	});

    /**
	 * 启用
     */
	$("#enable").click(function(){
		var ids = [];
		$("input[name='number']:checked ").each(function(index, row) {
            ids[index] = $(row).val();
		});
		if(ids.length == 0) {
			return;
		}
        if (!window.confirm("确认启用选定停车场吗?")) {
            return;
        }
		console.info(ids);
        $.ajax({
            url: ctx + "/mgr/park/enable",
            type: 'post',
            dataType: 'json',
            data: {
                "list" : ids
            },
            timeout: ajaxTimeout,
            error: function (json) {
                toastr.error("操作失败," + json.errorMSG)
            },
            success: function (json) {
                if (json.isSuccess==0) {
                    toastr.success("停车场启用成功")
                } else {
                    toastr.error("停车场启用失败")
                }
                table.fnPageChange(getPageNum(table, false));
            }
        });
	});

    /**
     * 禁用
     */
    $("#disable").click(function(){
        var ids = [];
        $("input[name='number']:checked ").each(function(index, row) {
            ids[index] = $(row).val();
        });
        if(ids.length == 0) {
            return;
        }
        if (!window.confirm("确认禁用选定停车场吗?")) {
            return;
        }
        $.ajax({
            url: ctx + "/mgr/park/disable",
            type: 'post',
            dataType: 'json',
            data: {
                "list" : ids
            },
            timeout: ajaxTimeout,
            error: function (json) {
                toastr.error("操作失败," + json.errorMSG)
            },
            success: function (json) {
                if (json.isSuccess==0) {
                    toastr.success("停车场禁用成功")
                } else {
                    toastr.error("停车场禁用失败")
                }
                table.fnPageChange(getPageNum(table, false));
            }
        });
    });


    initBaseDataSelect("Property", "#property");					//产权方
    initBaseDataSelect("ParkType", "#parkType");					//业态
    initBaseDataSelect("BusinessEntity", "#businessEntity");		//经营主体
    initBaseDataSelect("IntegralBodyType", "#integralBodyType");	//积分主体类型
});

/**
 * 停车场复选框
 * @param index
 * @param data
 * @returns {string}
 */
function viewNumber(index, data){
	return '<input type="checkbox" name="number" value="'+ data.id +'" />' +"&nbsp;"+ (index+1);
}

/**
 * 停车场状态
 * @param data
 * @returns {*}
 */
function viewState(data) {
	if(data.state == "0"){
		return "<small class='label label label-info'>启用</small>";
	}else if(data.state == "1") {
        return "<small class='label label-warning'>禁用</small>";
	}
	return "";
}

/**
 * 获得操作按钮
 * @param data
 * @returns {string}
 */
function getOperate(data) {
	var str = '<button onclick="getParkInfo(\'' + data.id + '\')" class="btn btn-primary btn-xs">编辑</button>';
	return str;
}

/**
 * 初始化数据字典下拉菜单
 * @param type
 * @param jqueryId
 */
function initBaseDataSelect(type, jqueryId) {
	console.info(type, jqueryId);
    //积分主体类型
    $.getJSON(ctx + "/mgr/basedata/listByType/" + type, {random: Math.random}, function(json){
        var options = "<option value=''>全部</option>";
        $.each(json,function(index, row) {
            options += "<option value='" + row.value + "'>" + row.text + "</option>";
        });
        $(jqueryId).html(options);
    });
}

/**
 * 停车场信息编辑查看
 * @param id 停车场ID
 */
function getParkInfo(id) {
	var url = ctx + "/mgr/park/editUI/"+ id;
	window.location.href = url;
}

