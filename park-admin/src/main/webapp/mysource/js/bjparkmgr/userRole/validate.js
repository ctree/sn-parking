$(function () {
    for (var key in aurList) {
        $("#auroleInfo.chosen-select").find("option[value='" + aurList[key].roleId + "']").attr("selected", true);
    }

    $("#auroleInfo.chosen-select").chosen();

    $("#editForm").validate({
        focusCleanup: true,
        focusInvalid: false,
        onkeyup: true,
        rules: {
            auRoleIds: {
                required: true
            }
        },
        messages: {
            auRoleIds: {
                required: "请选择角色"
            }
        },
        submitHandler: submit
    });
});

/**
 * 表单提交
 */
function submit() {
    var auroleInfo = '';
    $('#auroleInfo option:selected').each(function () {
        auroleInfo += $(this).val() + ",";
    });
    if (auroleInfo != '') {
        auroleInfo = auroleInfo.substring(0, auroleInfo.length - 1);
        $('#auRoleIds').val(auroleInfo);
    }

    $.ajax({
        url: ctx + "/mgr/vali/role",
        type: 'post',
        dataType: 'json',
        timeout: ajaxTimeout,
        data: {
            userId: $("#userId").val(),
            roleIds: auroleInfo
        },
        error: function (json) {
            toastr.error("操作失败");

        },
        success: function (json) {
            if (json.isSuccess) {
                toastr.success(json.errorMSG);
            } else {
                toastr.error(json.errorMSG);
            }
        }
    });
}