var table;
$(function() {
	//表格
	table = $('#dataList').dataTable({
		"fnServerData" : function(sSource, aoData, fnCallback) {
			$.ajax({
				"dataType" : 'json',
				"type" : "POST",
				"url" : ctx + "/mgr/vali/ajaxList",
				"data" : {
					"draw":aoData.draw,
					"currentResult":aoData.start,
					"showCount":aoData.length,
					"login" : $("#login").val(),
					"name" : $("#name").val(),
					"phone" : $("#phone").val()
				},
				"success" : fnCallback
			});
		},
		"aoColumns": [
                    {"data": "userId"},
                    {"data": "login"},
					{"data": "email"},
                    {"data": "phone"},
                    {"data": "type"},
                    {"data": "state"},
            		{"data": null}
                ],
        "fnRowCallback":function(nRow, aData, iDisplayIndex){
        	$('td:eq(0)', nRow).html(iDisplayIndex+1);
        	//用户类型
        	if(aData.type == 0 ){
        		$('td:eq(4)', nRow).html('系统用户');
        	}else if (aData.type == 1){
        		$('td:eq(4)', nRow).html('商户');
        	}else if (aData.type == -1){
        		$('td:eq(4)', nRow).html('系统管理员');
        	}else {
        		$('td:eq(4)', nRow).html('未知');
			}
        	//用户状态
        	var stateStr = "";
        	if(aData.state == 0 ||aData.state == null || aData.state ==""){
        		stateStr = "正常";
        	}else if(aData.state == 1){
        		stateStr = "删除";
        	}else{
				stateStr = "";
			}
        	$('td:eq(5)', nRow).html(stateStr);
        	$('td:eq(6)', nRow).html('<a href="'+ctx+'/mgr/vali/roleUI/'+aData.userId+'"><i class="fa text-navy">授权</i></a>');
        }
	});
	
	/**
	 * 搜索
	 */
	$('#serach').click(function() {
		table.fnDraw();
	});
});