$(function() {
	$("#addForm").validate({
		focusCleanup:true,
		focusInvalid:false,
		onkeyup:true,
		rules : {
			positionName : {
				required : true,
				minlength : 2,
				maxlength:50,
				remote:{
					url :ctx+'/mgr/role/isExistByName',
					type:'post',
					data:{
						sPositionName:function(){
							return $("#positionName").val();
						},
						roleId:function(){
							var auRoleId = $("#auRoleId").val();
							return auRoleId?auRoleId:"";
						}
					}
				}
			},
			remark : {
				required : true,
				minlength : 1,
				maxlength:1000
			}
		},
		messages : {
			positionName : {
				required : "请输入角色名称",
				minlength : "角色名称必须3个字符以上",
				maxlength:"角色名称必须50字符以内",
				remote:'角色名称已存在'
			},
			remark : {
				required : "请输入备注",
				minlength : "备注必须3个字符以上",
				maxlength:"备注必须1000字符以内"
			}
		},
		submitHandler:submit
	});
});

/**
 * 表单提交
 */
function submit(){
	var roleId = $("#roleId").val();
	var positionName = $("#positionName").val();
    var enableStatus = $('input:radio[name="enableStatus"]:checked').val()
	var remark = $('#remark').val();
	var url;
	if( roleId == null || roleId == undefined || roleId == ''){
		url = ctx + '/mgr/role/add';
	}else{
		url = ctx + '/mgr/role/edit';
	}
	$.ajax({
		url : url,
		type : 'post',
		dataType : 'json',
		timeout : ajaxTimeout,
		data : {
			roleId : roleId,
            positionName : positionName,
            enableStatus:enableStatus,
            remark:remark
		},
		error : function(json) {
			toastr.error("网络异常,请联系系统管理员");
			
		},
		success : function(json) {
			if(json.isSuccess == "0"){
                toastr.success(json.errorMSG)
			}
			else if (json.isSuccess == "1"){
                toastr.error(json.errorMSG)
			}else{
                toastr.error("系统错误,请联系系统管理员");
			}
		}
	});
}