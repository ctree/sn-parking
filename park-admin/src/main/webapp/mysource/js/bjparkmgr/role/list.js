var table;
$(function() {
	//表格
	table = $('#dataList').dataTable({
		"fnServerData" : function(sSource, aoData, fnCallback) {
			$.ajax({
				"dataType" : 'json',
				"type" : "POST",
				"url" : ctx + "/mgr/role/ajaxList",
				"data" : {
					"draw":aoData.draw,
					"currentResult":aoData.start,
					"showCount":aoData.length,
					"positionName" : $("#positionName").val()
				},
				"success" : fnCallback
			});
		},
		"aoColumns": [
                    {"data": "positionName"},
                    {"data": "positionName"},
                    {"data": "remark"},
                    {"data": "enableStatus"},
                    {"data": "positionName"}
                ],
        "fnRowCallback":function(nRow, aData, iDisplayIndex){
        	$('td:eq(0)', nRow).html(iDisplayIndex+1);
        	if(aData.enableStatus == 0){
        		$('td:eq(3)', nRow).html('启用');
        	}else {
        		$('td:eq(3)', nRow).html('停用');
			}
        	$('td:eq(4)', nRow).html(operateHtml(aData));
        }
	});
	
	/**
	 * 搜索
	 */
	$('#serach').click(function() {
		table.fnDraw();
	});
});

/**
 * 获取操作数据
 */
function operateHtml(aData){
	var operateHtml= '<a href="'+ctx+'/mgr/role/editUI/'+aData.roleId+'"><i class="fa text-navy">编辑</i></a>';
	//operateHtml += '&nbsp;&nbsp;<a href="#" onclick="delete_(\''+ aData.auRoleId +'\')"><i class="fa text-navy">删除</i></a>';
	return operateHtml;
}

/**
 * 删除数据
 */
function delete_(id) {
	if(!window.confirm("确定删除吗?")) {
		return;
	}
	$.ajax({
		url:ctx+"/role/delete/"+id,
		type:'post',
		dataType:'json',
		timeout:ajaxTimeout,
		error:function(json){
			toastr.error("删除失败");
		},
		success:function(json){
			if(json.isSuccess){
				toastr.success(json.msg);
			}else{
				toastr.error(json.msg);
			}
			table.fnPageChange(getPageNum(table,true));
		}
	});
};