var ztree;
$(function () {
    // 使用jquery chosen
    $(".chosen-select").chosen();

    initAurole();

    $('#savebutton').click(function () {
        var auFunctreeEle = ztree.getCheckedNodes(true);
        var auFunctreeArr = new Array();
        for (var index = 0; index < auFunctreeEle.length; index++) {
            auFunctreeArr.push(auFunctreeEle[index].functionSourceId);
        }
        $.ajax({
            url: ctx + "/mgr/role/roleAndTree",
            type: 'post',
            dataType: 'json',
            timeout: ajaxTimeout,
            data: {
                roleId: $("#roleId").val(),
                functionSourceIds: auFunctreeArr.toString()
            },
            error: function (json) {
                toastr.error("操作失败");
            },
            success: function (json) {
                if (json.isSuccess) {
                    toastr.success(json.errorMSG);
                } else {
                    toastr.error(json.errorMSG);
                }
            }
        });
    });
});


function initAurole() {
    $.ajax({
        url: ctx + '/mgr/role/initAurole',
        type: 'post',
        dataType: 'json',
        timeout: ajaxTimeout,
        error: function (json) {
            toastr.error("信息获取失败");
        },
        success: function (json) {
            if (json.isSuccess) {
                $("#tree").html("");
                $("#selectDiv").html("");
                var str = '<select id="roleId" name="roleId" class="chosen-select form-control" style="width:350px;" tabindex="2" onchange="toSelect()"><option value="0">请选择</option>';
                $.each(json.roleList, function (i, role) {
                    str = str + '<option value=' + role.roleId + '>' + role.positionName + '</option>';
                })
                str += '</select>';
                $("#selectDiv").html(str);
                $("#roleId.chosen-select").chosen();
            } else {
                toastr.error(json.msg);
                $("#tree").html("");
                $("#selectDiv").html("");
                var str = '<select id="roleId" name="roleId" class="chosen-select form-control" style="width:350px;" tabindex="2" onchange="toSelect()"><option value="0">请选择</option>';
                str += '</select>';
                $("#selectDiv").html(str);
                $("#roleId.chosen-select").chosen();
            }
        }
    })
}

// 根据角色获取树信息
function initRole(roleId) {
    $.ajax({
        url: ctx + '/mgr/role/getRoleAndTree/' + roleId,
        type: 'post',
        dataType: 'json',
        timeout: ajaxTimeout,
        error: function (json) {
            toastr.error("加载失败");
        },
        success: function (json) {
            var result = json.isSuccess;
            if (!result) {
                $("#tree").html(json.msg);
            } else {
                // 生成树
                var list = json.resourceList;
//				alert(JSON.stringify(list));
                var setting = {
                    view: {
                        showIcon: false
                    },
                    check: {
                        enable: true,
                        chkboxType: {"Y": "", "N": "s"}
                    },
                    data: {
                        key: {
                            children: "nodeList",
                            name: "sourceName",
                            title: "sourceName",
                            url: "zurl"
                        }
                    }
                };
                ztree = $.fn.zTree.init($("#tree"), setting, list);
                var node;
                // 已选资源勾选
                var roleResList = json.roleResList;
                $(roleResList).each(
                    function (i) {
                        node = ztree.getNodeByParam("functionSourceId", roleResList[i].functionSourceId, null);
                        ztree.checkNode(node, true, true);
                    });
            }
        }
    });
}

// 增加树样式
function treeInit() {
    setTimeout((function initTree() {
        $("#red").treeview({
            animated: "fast",
            collapsed: true
        });
        $("#tree").css("display", "block");
    }), 500);
}

// 异步生成关系树
function toSelect() {
    var roleId = $("#roleId option:selected").val();
    initRole(roleId);
    treeInit();
}

function simpleSelected(temp) {
    // 向下选择和取消
    $().childrenCheck($(temp));
    // 向上选择和取消
    $().parentCheck($(temp));
}
