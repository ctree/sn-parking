var Common = {
    /**
     * 错误消息定义
     * @memberof Common
     * @name errorCode
     * @type Object
     * @example "ajaxError": "网络错误，请检查网络连接！"
     */
    errorCode: {
        "ajaxError": "网络错误，请检查网络连接！"
    },
}
Common.Ajax = {
    /**
     * 发送Ajax的Post请求
     * @memberof Common.Ajax
     * @function
     * @name postRequest
     * @param (Model) model - Model实例
     * @param (bool) async - 是否同步
     */
    ajaxRequest : function (modal,callback) {
        if(!modal.url){
            toastr.error("请求地址为空")
            return;
        }
       return $.ajax({
           async: (modal.async == undefined || modal.async) ? true : false,
           url: ctx + modal.url,
           type: modal.type,
           dataType: 'json',
           data: modal.data,
           timeout: 30000,
           error: function (data) {
               toastr.error("操作失败," + data.errorMSG)
           },
           success: function (data) {
               if (data.isSuccess == "0" || data.isSuccess == true) {
                   if (callback && $.isFunction(callback)) {
                       callback(data);
                   } else if ($.isArray(callback) && $.isFunction(callback[0])) {
                       callback[0](data);
                   }
               } else {
                   toastr.error(data.errorMSG)
               }
           }
       })
    }
}

/**
 * 非空校验
 * @param param 参数
 * @returns {boolean} true 非空  false空
 */
var checkValue = function(param){
    if (param == null || param == ""){
        return false;
    }else{
        return true;
    }
}

/**
 * 获取广场
 */
function getParkId() {
    $.get("/mgr/park/getParkList",function(data,status){
        if(status == "success"){
            if(data.isSuccess){
                data.parkList.forEach(function(obj, index ) {
                    // console.log( val, index, this );
                    $("#parkId").append("<option value=" + obj.id + ">" + obj.parkName + "</option>");
                });
            }
        }
    });
}

/**
 * 获取停车场名称
 */
function getParkName() {
    var obj = document.getElementById("parkId");
    var parkName = obj.options[obj.selectedIndex].text;
    return parkName;
}