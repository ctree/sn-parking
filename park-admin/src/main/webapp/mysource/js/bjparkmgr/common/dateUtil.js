/**
 * Created by Administrator on 2017/1/17.
 */
function formatDate(time,type){
    if(!time) return
    var date = new Date(time),
    year = date.getFullYear(),
    month = date.getMonth() + 1,
    day = date.getDate(),
    hours = date.getHours(),
    minutes = date.getMinutes(),
    second = date.getSeconds();

    switch (type) {
        case 0: // 01-05
            return zerofill(month) +'-'+ zerofill(day);
        case 1: // 11:12
            return zerofill(hours) +':'+ zerofill(minutes);
        case 2: // 2016-11-05
            return year+'-'+zerofill(month) +'-'+ zerofill(day);
        case 3: //  2016-11-05 11:12
            return year+'-'+zerofill(month) +'-'+ zerofill(day)+' '+zerofill(hours) +':'+ zerofill(minutes);
        case 4: //  2016.11.05 11:12
            return year+'.'+zerofill(month) +'.'+ zerofill(day)+' '+zerofill(hours) +':'+ zerofill(minutes) +':'+ zerofill(second);
        case 5: // 2016.11.05
            return  year+'.'+zerofill(month) +'.'+ zerofill(day);
        case 6: // 2016-11-05
            return zerofill(hours) +':'+ zerofill(minutes) +':'+ zerofill(second);
        case 7: //  2016-11-05 11:12:13
            return year+'-'+zerofill(month) +'-'+ zerofill(day)+' '+zerofill(hours) +':'+ zerofill(minutes) +':'+ zerofill(second);
        default:
            return ""
    }
}

function zerofill(val) {
    return val >= 10 ? val : '0' + val;
}




