$(function() {

	$("#backButton").click(function() {
		/*history.back(-1);*/
        getParkUrlInfoList();
	});

	$("#parkUrlEdit_rejectForm").validate({
        focusCleanup: true,
        focusInvalid: false,
        onkeyup: true,
        rules: {
            url: {
                required: true
            },
            parkId: {
                min: 1
            }
        },
        messages: {
            url: {
                required: "URL不能为空"
            },
            parkId: {
                min: "请选择所属停车场"
            }
        },
        submitHandler: submit
    });

    initParkListSelect("#parkId", "所属停车场");

    initBaseDataSelect("RequestType", "#requestType", "请求类型");	//请求类型
    initBaseDataSelect("UrlType", "#urlType", "网络类型");	//网络类型

});

/**
 * 初始化数据字典下拉菜单
 * @param type
 * @param jqueryId
 */
function initBaseDataSelect(type, jqueryId, name) {
    var value = $(jqueryId).attr("value");
    //积分主体类型
    $.getJSON(ctx + "/mgr/basedata/listByType/" + type, {random: Math.random}, function(json){
        var options = "<option value=''>请选择" + name + "</option>";
        $.each(json,function(index, row) {
            console.info(row.value, value);
            options += "<option "+ (row.value==value?"selected='selected'":"") +" value='" + row.value + "'>" + row.text + "</option>";
        });
        $(jqueryId).html(options);
    });
}

/**
 * 初始化停车场下拉菜单
 * @param type
 * @param name
 */
function initParkListSelect(jqueryId, name) {
    var value = $(jqueryId).attr("value");
    $.getJSON(ctx + "/mgr/park/getParkList/", {random: Math.random}, function(json){
        var options = "<option value=''>请选择" + name + "</option>";
        $.each(json.parkList,function(index, row) {
            options += "<option "+ (row.id==value?"selected='selected'":"") +" value='" + row.id + "'>" + row.parkName + "</option>";
        });
        $(jqueryId).html(options);
        $(jqueryId).chosen();
    });
}

/**
 * 修改
 */
function submit(){
    $.ajax({
        url:ctx+"/mgr/parkUrl/edit",
        type:'post',
        dataType:'json',
        "data" : $('#parkUrlEdit_rejectForm').serialize(),
        timeout:ajaxTimeout,
        error:function(json){
            toastr.error("修改停车场本地系统访问地址失败，网络连接超时请重新登陆")
        },
        success:function(json){
            if(json.isSuccess==0){
                toastr.success("修改成功");
            }else{
                toastr.error("修改失败");
            }
        }
    });
}

/**
 * 查看
 */
function getParkUrlInfoList() {
	var url = ctx + "/mgr/parkUrl/list";
	window.location.href = url;
}