var table;
$(function() {
	$("#parkUrlList_queryDiv").bind("keydown",function(e){
		// 兼容FF和IE和Opera
		var theEvent = e || window.event;
		var code = theEvent.keyCode || theEvent.which || theEvent.charCode;
		if (code == 13) {
			//回车执行查询
			$("#parkUrlList_serach").click();
		}
	});

	table = $('#parkUrlList_dataList').dataTable({
		"fnServerData" : function(sSource, aoData, fnCallback) {
			$.ajax({
				"dataType" : 'json',
				"type" : "POST",
				"url" : ctx + "/mgr/parkUrl/ajaxList",
				"data" : {
					"currentResult" : aoData.start,
					"showCount" : aoData.length,
					"draw":aoData.draw,
					"parkId" : $("#parkId").val()
				},
				"success" : fnCallback
			});
		},
		"aoColumns": [
			{"data": null},
			{"data": "parkName"},
			{"data": "requestType"},
            {"data": "urlType"},
            {"data": "url"},
			{"data": null}
		],
		"fnRowCallback":function(nRow, aData, iDisplayIndex){
			$('td:eq(0)', nRow).html(viewNumber(iDisplayIndex,aData));
            $('td:eq(2)', nRow).html(viewStateRequestType(aData));
            $('td:eq(3)', nRow).html(viewStateUrlType(aData));
			$('td:eq(5)', nRow).html(getOperate(aData));
		}
	});

	/**
	 * 查询
	 */
	$('#parkUrlList_serach').click(function() {
		table.fnDraw();
	});

    initParkListSelect("#parkId");

});

function viewNumber(index, data){
	return index+1;
}

/**
 * 获得操作按钮
 * @param data
 * @returns {string}
 */
function getOperate(data) {
    var str = '<button onclick="getParkUrlInfo(\'' + data.id + '\')" class="btn btn-primary btn-xs">编辑</button>&nbsp;&nbsp;';
    str += '<button onclick="deleteParkUrl(\'' + data.id + '\')" class="btn btn-primary btn-xs">删除</button>';
    return str;
}

/**
 * 请求类型
 * @param data
 * @returns {*}
 */
function viewStateRequestType(data) {
	if(data.requestType==null)return "";
	return "<small class='label label-info'>" +data.requestType+"</small>";
}

/**
 * 网络类型
 * @param data
 * @returns {*}
 */
function viewStateUrlType(data) {
    if(data.urlType==null)return "";
    return "<small class='label label-primary'>" +data.urlType+"</small>";
}

/**
 * 初始化停车场下拉菜单
 * @param type
 * @param name
 */
function initParkListSelect(jqueryId) {
    $.getJSON(ctx + "/mgr/park/getParkList/", {random: Math.random}, function(json){
        var options = "<option value=''>全部</option>";
        $.each(json.parkList,function(index, row) {
            options += "<option value='" + row.id + "'>" + row.parkName + "</option>";
        });
        $(jqueryId).html(options);
        $("#parkId").chosen();
    });
}

/**
 * 编辑查看
 * @param id
 */
function getParkUrlInfo(id) {
	var url = ctx + "/mgr/parkUrl/editUI/"+ id;
	window.location.href = url;
}

/**
 * 删除
 * @param id
 */
function deleteParkUrl(id){
    if (!window.confirm("确认该操作吗?")) {
        return;
    }
    $.ajax({
        url: ctx + "/mgr/parkUrl/delete",
        type: 'post',
        dataType: 'json',
        "data": {
            "id" : id
        },
        timeout: ajaxTimeout,
        error: function (json) {
            toastr.error("操作失败," + json.errorMSG)
        },
        success: function (json) {
            if (json.isSuccess==0) {
                toastr.success("删除成功")
            } else {
                toastr.error("删除失败")
            }
            table.fnPageChange(getPageNum(table, false));
        }
    });
}

