$(function() {
	$("#addForm").validate({
		focusCleanup:true,
		focusInvalid:false,
		onkeyup:true,
		ignore:"input:radio[name=parentCode]",
		rules : {
			name : {
				required : true,
				minlength : 2,
				maxlength:50,
				remote:{
					url :ctx+'/mgr/funcTree/isExistByName',
					type:'post',
					data:{
						name:function(){
							return $("#name").val();
						}
					}
				}
			},
			url : {
				required : true,
				minlength : 1,
				maxlength:50,
				remote:{
					url :ctx+'/mgr/funcTree/isExistByUrl',
					type:'post',
					data:{
                        sourceUrl:function(){
							return $("#url").val();
						}
					}
				}
			},
			orderCode:{
				required:true,
				digits:true,
				range:[0,32767]
			}
		},
		messages : {
			name : {
				required : "请输入资源名称",
				minlength : "资源名称必须两个字符以上",
				maxlength:"资源名称必须50字符以内",
				remote:'资源名称已存在'
			},
			url : {
				required : "请输入资源链接",
				minlength : "资源链接必须5个字符以上",
				maxlength:"资源链接必须50字符以内",
				remote:'资源链接已存在'
			},
			orderCode:{
				required:"请输入排序编码",
				digits:"排序编码必须为大于0的整数",
				range:"排序编码必须在0-32767之间"
			}
		},
		submitHandler:submit
	});
});

/**
 * 表单提交
 */
function submit(){
	var treeObj = $.fn.zTree.getZTreeObj("tree");
	var nodes = treeObj.getSelectedNodes();
	$.ajax({
		url : ctx + '/mgr/funcTree/add',
		type : 'post',
		dataType : 'json',
		timeout : ajaxTimeout,
		data : {
			sourceName : $("#name").val(),
			sourceUrl:$("#url").val(),
			orderCode:$("#orderCode").val(),
			parentCode:nodes[0].functionSourceId,
			help:$("input[name=help]").val(),
			sourceType:$("#sourceType").val()
			// sourceType:001
		},
		error : function(json) {
			//showMsgDiv(false,"添加失败",$("#msgSpan"),$("#msgDiv"),3000);
			toastr.error("添加失败");
			
		},
		success : function(json) {
			//showMsgDiv(json.isSuccess,json.msg,$("#msgSpan"),$("#msgDiv"),3000);
			if(json.isSuccess){
				toastr.success(json.msg);
			}else{
				toastr.error(json.msg);
			}
		}
	});
}