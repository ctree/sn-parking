var ztree;
$(function() {
	getAufunctree();
	
});


function getAufunctree(){
	initAufunctree();
	treeInit();
}

function initAufunctree(){
	$.ajax({
		url : ctx + '/mgr/funcTree/getAuFuncTrees',
		type : 'post',
		dataType : 'json',
		timeout : ajaxTimeout,
		error : function(json) {
			toastr.error("加载失败");
		},
		success : function(json) {
			if(json.success){

				var result = json.success;
				if (!result) {
					$("#tree").html(json.msg);
				} else {
					// 生成树
					var list = json.resourceList;
//					alert(JSON.stringify(list));
					var setting = {
//							check: {
//								enable: true,
//								chkboxType:{ "Y" : "", "N" : "s" }
//							},
							view: {
								showIcon: false,
								selectedMulti: false
							},
							data: {
								key: {
									children: "nodeList",
										name: "sourceName",
										title: "sourceName",
										url:"zurl"
								}
							},
							callback:{
								onClick: zTreeOnClick
							}
					};

					ztree = $.fn.zTree.init($("#tree"), setting, list);
					
					var treeObj = $.fn.zTree.getZTreeObj("tree");
					var nodes = treeObj.getNodes();
					treeObj.selectNode(nodes[0]);
				}
				
			}
		}
		})
}

function  zTreeOnClick(event, treeId, treeNode) {
	var treeObj = $.fn.zTree.getZTreeObj("tree");
	// if(treeNode.type != '0' && $("select[name=type]").val() != '0'){
	// 	alert("按钮及区域资源只能添加到末级菜单中");
	// 	treeObj.cancelSelectedNode(treeNode);
	// }
}

// 增加树样式
function treeInit() {
	setTimeout((function initTree() {
		$("#red").treeview({
			animated : "fast",
			collapsed : true
		});
		$("#tree").css("display", "block");
	}), 500);
}
