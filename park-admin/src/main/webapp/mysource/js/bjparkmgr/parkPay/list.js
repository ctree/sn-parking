var table;
$(function() {
	$("#parkPayList_queryDiv").bind("keydown",function(e){
		// 兼容FF和IE和Opera
		var theEvent = e || window.event;
		var code = theEvent.keyCode || theEvent.which || theEvent.charCode;
		if (code == 13) {
			//回车执行查询
			$("#parkPayList_serach").click();
		}
	});

	table = $('#parkPayList_dataList').dataTable({
		"fnServerData" : function(sSource, aoData, fnCallback) {
			$.ajax({
				"dataType" : 'json',
				"type" : "POST",
				"url" : ctx + "/mgr/parkPay/ajaxList",
				"data" : {
					"currentResult" : aoData.start,
					"showCount" : aoData.length,
					"draw":aoData.draw,
					"parkId" : $("#parkId").val()
				},
				"success" : fnCallback
			});
		},
		"aoColumns": [
			{"data": null},
			{"data": "parkName"},
			{"data": "merchantNo"},
            {"data": "publicKeyIndex"},
			{"data": null}
		],
		"fnRowCallback":function(nRow, aData, iDisplayIndex){
			$('td:eq(0)', nRow).html(viewNumber(iDisplayIndex,aData));
			$('td:eq(4)', nRow).html(getOperate(aData));
		}
	});

	/**
	 * 查询
	 */
	$('#parkPayList_serach').click(function() {
		table.fnDraw();
	});

    initParkListSelect("#parkId");

});

function viewNumber(index, data){
	return index+1;
}

/**
 * 获得操作按钮
 * @param data
 * @returns {string}
 */
function getOperate(data) {
    var str = '<button onclick="getParkPayInfo(\'' + data.id + '\')" class="btn btn-primary btn-xs">编辑</button>&nbsp;&nbsp;';
    str += '<button onclick="deleteParkPay(\'' + data.id + '\')" class="btn btn-primary btn-xs">删除</button>';
    return str;
}

/**
 * 初始化停车场下拉菜单
 * @param type
 * @param name
 */
function initParkListSelect(jqueryId) {
    $.getJSON(ctx + "/mgr/park/getParkList/", {random: Math.random}, function(json){
        var options = "<option value=''>全部</option>";
        $.each(json.parkList,function(index, row) {
            options += "<option value='" + row.id + "'>" + row.parkName + "</option>";
        });
        $(jqueryId).html(options);
        $("#parkId").chosen();
    });
}

/**
 * 易付宝商户号配置编辑查看
 * @param id
 */
function getParkPayInfo(id) {
	var url = ctx + "/mgr/parkPay/editUI/"+ id;
	window.location.href = url;
}

/**
 * 删除易付宝商户号配置
 * @param id
 */
function deleteParkPay(id){
    if (!window.confirm("确认该操作吗?")) {
        return;
    }
    $.ajax({
        url: ctx + "/mgr/parkPay/delete",
        type: 'post',
        dataType: 'json',
        "data": {
            "id" : id
        },
        timeout: ajaxTimeout,
        error: function (json) {
            toastr.error("操作失败," + json.errorMSG)
        },
        success: function (json) {
            if (json.isSuccess) {
                toastr.success(json.errorMSG)
            } else {
                toastr.error(json.errorMSG)
            }
            table.fnPageChange(getPageNum(table, false));
        }
    });
}

