$(function () {

    $("#parkPayAdd_form").validate({
        focusCleanup: true,
        focusInvalid: false,
        onkeyup: true,
        rules: {
            merchantNo: {
                required: true
            },
            publicKeyIndex: {
                required: true
            },
            parkId: {
                min: 1
            }
        },
        messages: {
            merchantNo: {
                required: "易付宝商户号不能为空"
            },
            publicKeyIndex: {
                required: "商户公钥索引号不能为空"
            },
            parkId: {
                min: "请选择所属停车场"
            }
        },
        submitHandler: submit
    });

    initParkListSelect("#parkId", "所属停车场");
});

/**
 * 初始化停车场下拉菜单
 * @param type
 * @param name
 */
function initParkListSelect(jqueryId, name) {
    var value = $(jqueryId).attr("value");
    $.getJSON(ctx + "/mgr/park/getParkList/", {random: Math.random}, function(json){
        var options = "<option value=''>请选择" + name + "</option>";
        $.each(json.parkList,function(index, row) {
            options += "<option "+ (row.id==value?"selected='selected'":"") +" value='" + row.id + "'>" + row.parkName + "</option>";
        });
        $(jqueryId).html(options);
        $(jqueryId).chosen();
    });
}

/**
 * 添加停车场信息
 */
function submit() {
    $.ajax({
        url: ctx + "/mgr/parkPay/add",
        type: 'post',
        dataType: 'json',
        data: $('#parkPayAdd_form').serialize(),
        timeout: ajaxTimeout,
        error: function (json) {
            toastr.error("添加易付宝商户号配置失败，网络连接超时请重新登陆")
        },
        success: function (json) {
            if (json.isSuccess) {
                toastr.success(json.errorMSG);
            } else {
                toastr.error(json.errorMSG);
            }
        }
    });
}
