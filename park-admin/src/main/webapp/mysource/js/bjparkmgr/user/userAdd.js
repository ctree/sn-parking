$(function () {
    $("#gmCompanyId.chosen-select").chosen();



// 手机号码验证
    $.validator.addMethod("isMobile", function (value, element) {
        var length = value.length;
        var mobile = /^(((13[0-9]{1})|(14[0-9]{1})|(15[0-9]{1})|(17[0-9]{1})|(18[0-9]{1}))+\d{8})$/;
        return this.optional(element) || (length == 11 && mobile.test(value));
    }, "请正确填写您的手机号码");

// 身份证验证
    $.validator.addMethod("isCardId", function (value, element) {
        var length = value.length;
        var cardId = /^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$/;
        return this.optional(element) || cardId.test(value);
    }, "请正确填写您的身份证号码");

// 车牌号验证
    $.validator.addMethod("isCarNum", function (value, element) {
        var length = value.length;
        var carNum = /^[\u4E00-\u9FA5][\da-zA-Z]{6}$/;
        return this.optional(element) || carNum.test(value);
    }, "请正确填写您的车牌号码");


    //修改时，如果被初始化了
    $("#parkId.chosen-select").chosen();

    /*解决浏览器自动填充问题*/
    var isLTIE9 = function () {
        var bObj = document.createElement('b');
        bObj.innerHTML = '<!--[if lt IE 9]><i></i><![endif]-->';
        return bObj.getElementsByTagName('i').length === 1;
    }
    if (!isLTIE9()) {
        $("#password").focus(function (event) {
            $(this).attr('type', 'password');
        });
    } else {
        var pswInput = document.getElementById("password");
        var pswInput2 = pswInput.cloneNode(false);
        pswInput2.type = 'password';
        pswInput.parentNode.replaceChild(pswInput2, pswInput);
    }


    $("#useradd_form").validate({
        focusCleanup: true,
        focusInvalid: false,
        onkeyup: true,
        rules: {
            login: {
                required: true
            }, password: {
                required: true
            }, name: {
                required: true
            }, email: {
                required: true
            }, phone: {
                required: true,
                isMobile: "请输入正确手机号"
            }, type: {
                required: true
            }
        },
        messages: {
            login: {
                required: "账号不能为空"
            }, password: {
                required: "密码不能为空"
            }, name: {
                required: "姓名不能为空"
            }, email: {
                required: "邮箱不能为空"
            }, phone: {
                required: "手机号不能为空"
            }, type: {
                required: "用户类型不能为空"
            }
        },
        submitHandler: submit
    });
});



$("#type").change(function () {
    var typeValue = $("#type").val();
    if (typeValue == "0") {
        $("#guardian").css("display", "none");
        $("#owner").css("display", "block");
    } else if (typeValue == "1") {
        $("#owner").css("display", "none");
        $("#guardian").css("display", "block");
    } else if (typeValue == "2") {
        $("#owner").css("display", "none");
        $("#guardian").css("display", "none");
    } else {
        $("#owner").css("display", "none");
        $("#guardian").css("display", "none");
        //alert("未知类型");
    }
});

/**
 * 添加用户信息
 */
function submit() {
    $.ajax({
        url: ctx + "/mgr/sysUserManager/add",
        type: 'post',
        dataType: 'json',
        data: $('#useradd_form').serialize(),
        timeout: ajaxTimeout,
        error: function (json) {
            toastr.error("添加用户失败，网络连接超时请重新登陆")
        },
        success: function (json) {
            if (json.isSuccess == "0") {
                toastr.success("添加成功");
            } else {
                toastr.error(json.errorMSG);
            }
        }
    });
}

/**
 * 跳转用户信息列表
 */
function toUserList() {
    var url = ctx + "/mgr/userManager/list";
    window.location.href = url;
}
