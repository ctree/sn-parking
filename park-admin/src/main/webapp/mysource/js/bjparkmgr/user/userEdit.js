$(function() {

	$("#userEdit_backButton").click(function() {
		/*history.back(-1);*/
		getUserInfoList();
	});

	$("#userEdit_rejectForm").validate({
		focusCleanup:true,
		focusInvalid:false,
		onkeyup:true,
		rules : {
			userId : {
				required : true
			},
		},
		messages : {
			userId : {
				required : "请重新登陆，身份超时"
			}
		},
		submitHandler:submit
	});

});


/**
 * 个人信息修改
 */
function submit(){
	$.ajax({
		url:ctx+"/mgr/sysUserManager/updateUserInfo",
		type:'post',
		dataType:'json',
		"data" : {
			"userId":$("#userId").val(),
			"name":$("#name").val(),
			"sex":$("#sex").val(),
			"email":$("#email").val(),
			"phone":$("#phone").val(),
			"type":$('#type').val(),
			"state":$('#state').val()
		},
		timeout:ajaxTimeout,
		error:function(json){
			toastr.error("网络异常,请重新登录")
		},
		success:function(json){
			if(json.isSuccess){
				toastr.success("编辑用户信息成功");
				getUserInfo($("#userId").val());//刷新

			}else{
				toastr.error(json.errorMSG);
			}
		}
	});
}

/**
 * 查看用户信息列表
 * @param userId 用户ID
 */
function getUserInfoList() {
	var url = ctx + "/view/userManager";
	window.location.href = url;
}