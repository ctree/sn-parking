	/* TYPE YOUR JAVA SCRIPT HERE */
$(function() {
	$("#login_form").validate({
		focusCleanup:true,
		focusInvalid:false,
		onkeyup:true,
		rules : {
			userName : {
				required : true
			},passWord : {
				required : true
			}
		},
		messages : {
			userName:{
				required : "请填写账号"
			},passWord : {
				required : "请填写密码"
			},
		},
		submitHandler:login_submit
	});
});



function login_submit(){
	$.ajax({
		url: ctx + "/mgr/login/in",
		dataType : 'json',
		type : "POST",
		data : {
			login:$('#userName').val(),
			pwd:$('#passWord').val(),
		},
		timeout:ajaxTimeout,
		error:function(json){
			alert("服务器连接失败");
		},
		success : function(json) {
			if(json.isSuccess=="0"){
				location.href = ctx + "/view/main";
			}else{
				alert(json.errorMSG);
			}
		}
	});
}
