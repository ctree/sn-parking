<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="../commons/meta.jsp" %>
    <title>系统管理</title>
    <style>
        #show_img{
            width: 850px;
            height:650px;
        }
    </style>
    <link rel="stylesheet" href="${ctx}/mysource/css/action/action.css">
    <link rel="stylesheet" href="${ctx}/mysource/js/plugins/combo.select/combo.select.css">
</head>
<body class="fixed-sidebar full-height-layout">
<div id="wrapper">
    <%@ include file="../commons/left.jsp" %>
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <%@ include file="../commons/top.jsp" %>
        <!-- 业务代码 -->
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>新增活动</h2>
                <ol class="breadcrumb">
                    <li><a href="/mgr/">主页</a></li>
                    <li><a href="/mgr/merchant/list">活动管理</a></li>
                    <li><strong>新增活动</strong></li>
                </ol>
            </div>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>新增活动</h5>
                        </div>
                        <div class="ibox-content">
                            <form class="form-horizontal m-t" id="addForm" method="post"  enctype="multipart/form-data" action="#">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><i class="red">*</i>活动广场：</label>
                                    <div class="col-sm-4">
                                        <select id='parkId' name='parkId' class='form-control'>
                                            <option value=''>请选择活动广场</option>
                                        </select>
                                    </div>
                                    <input type="hidden" name="parkName" id="parkName">
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><i class="red">*</i>活动名称：</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" id="actionName" name="actionName" placeholder="请输入活动名称" type="text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><i class="red">*</i>活动类型：</label>
                                    <div class="col-sm-4">
                                        <select id='type' name='type' class='form-control'>
                                            <option value="0">注册有礼</option>
                                            <option value='1'>会员权益</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group" id="amountDiv">
                                    <label class="col-sm-3 control-label"><i class="red">*</i>活动时间：</label>
                                    <div class="col-sm-2">
                                        <input class="form-control" id="startDate" name="startDate"  placeholder="活动开始时间" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})">
                                    </div>
                                    <div class="col-sm-2">
                                        <input class="form-control" id="endDate" name="endDate"  placeholder="活动结束时间" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><i class="red">*</i>活动封面：</label>
                                    <div class="col-sm-4">
                                        <input type="file" name="cover" id="cover">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-4">
                                        <img src="" alt="" id="coverPre">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><i class="red">*</i>活动LOGO：</label>
                                    <div class="col-sm-4">
                                        <input type="file" name="logo" id="logo">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-4">
                                        <img src="" alt="" id="logoPre">
                                    </div>
                                </div>
                                <p class="full-line"></p>
                                <div class="form-group register">
                                    <label class="col-sm-3 control-label"><i class="red">*</i>参与会员：</label>
                                    <div class="col-sm-4">
                                        <select id='participant' name='participant' class='form-control'>
                                            <option value='0'>全部</option>
                                            <option value="1">至尊会员</option>
                                            <option value='2'>钻石会员</option>
                                            <option value="3">电子会员</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group register">
                                    <label class="col-sm-3 control-label"><i class="red">*</i>参与次数：</label>
                                    <div class="col-sm-2">
                                        <select id='partType' name='partType' class='form-control'>
                                            <option value='0'>不限</option>
                                            <option value="1">每日活动</option>
                                            <option value='2'>活动期间</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-1">
                                        <input id="partNum" name="partNum" class="form-control" type="text" />
                                    </div>
                                    <div class="col-sm-1 line-height">
                                        次
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">请选择关联的券：</label>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3 control-label">
                                        <button type="button" class="btn btn-primary btn-sm add-coupon">
                                            <i class="fa fa-plus">添加</i>
                                        </button>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-1 control-label"></label>
                                    <div class="col-sm-6 coupon-group">
                                        <ul>
                                            <li class="col-sm-3">
                                                <div>序号</div>
                                            </li>
                                            <li class="col-sm-4">
                                                <div>券名称</div>
                                            </li>
                                            <li class="col-sm-3">
                                                <div>发券数量</div>
                                            </li>
                                        </ul>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-3">
                                        <button class="btn btn-primary" type="submit">确定</button>
                                        <button class="btn btn-primary" type="button" id="backButton">返回</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%@ include file="../commons/footer.jsp" %>
    </div>
</div>
<script src="${ctx}/mysource/js/plugins/laydate/laydate.js"></script>
<script src="${ctx}/mysource/js/bjparkmgr/common/common.js"></script>
<script src="${ctx}/mysource/js/plugins/combo.select/jquery.combo.select.js"></script>
<script src="${ctx}/mysource/js/bjparkmgr/action/addAction.js"></script>
</body>
</html>