<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="../commons/meta.jsp" %>
    <title>系统管理</title>
    <style>
        #show_img{
            width: 850px;
            height:650px;
        }
    </style>
    <link rel="stylesheet" href="${ctx}/mysource/css/action/action.css">
    <link rel="stylesheet" href="${ctx}/mysource/js/plugins/combo.select/combo.select.css">
</head>
<body class="fixed-sidebar full-height-layout">
<div id="wrapper">
    <%@ include file="../commons/left.jsp" %>
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <%@ include file="../commons/top.jsp" %>
        <!-- 业务代码 -->
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>活动管理</h2>
                <ol class="breadcrumb">
                    <li><strong>活动列表</strong></li>
                </ol>
            </div>
            <div class="col-lg-2"></div>
        </div>

        <div class="gray-bg wrapper wrapper-content animated fadeInRight dashbard-1">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox-content">
                        <!-- 查询start -->
                        <div class="form-horizontal" id="coupon_queryDiv">
                            <div class="form-group">
                                <label class="col-lg-1 col-md-2 col-sm-4 control-label">所属广场</label>
                                <div class="col-sm-2">
                                    <select id='parkId' name='parkId' class='form-control'>
                                        <option value=''>请选择所属广场</option>
                                    </select>
                                </div>
                                <label class="col-lg-1 col-md-2 col-sm-4 control-label">活动类型</label>
                                <div class="col-sm-2">
                                    <select id='type' name='type' class='form-control'>
                                        <option value=''>全部</option>
                                        <option value='0'>注册有礼</option>
                                        <option value='1'>会员权益</option>
                                    </select>
                                </div>
                                <label class="col-lg-1 col-md-2 col-sm-4 control-label">状态</label>
                                <div class="col-sm-2">
                                    <select id='state' name='state' class='form-control'>
                                        <option value=''>全部</option>
                                        <option value='0'>启用</option>
                                        <option value='1'>禁用</option>
                                    </select>
                                </div>
                                <label class="col-lg-1 col-md-2 col-sm-4 control-label">活动名称</label>
                                <div class="col-lg-2 col-md-2 col-sm-4">
                                    <input type="text" class="form-control" name="actionName" id="actionName"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-1 col-md-2 col-sm-3 control-label">活动时间</label>
                                <div class="col-lg-2 col-md-4 col-sm-3">
                                    <input type="date" class="form-control" name="startTime" id="startTime"/>
                                </div>
                                <div class="col-lg-2 col-md-4 col-sm-3">
                                    <input type="date" class="form-control" name="endTime" id="endTime"/>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-3">
                                    <button type="button" id="action_serach" class="btn btn-primary">查询</button>
                                    <button type="button" id="actionReset" class="btn btn-primary">重置</button>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-3">
                                    <button type="button" id="actionAdd" class="btn btn-primary">新增</button>
                                    <button type="button" id="actionDel" class="btn btn-primary">删除</button>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-3">
                                    <button type="button" id="actionStart" class="btn btn-primary">启用</button>
                                    <button type="button" id="actionStop" class="btn btn-primary">禁用</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12" style="overflow: auto;">
                    <table id="actionList" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th><input type="checkbox" name="selectAll" id="selectAll">&nbsp;&nbsp;序号</th>
                            <th>所属广场</th>
                            <th>活动名称</th>
                            <th>开始时间</th>
                            <th>结束时间</th>
                            <th>活动类型</th>
                            <th>状态</th>
                            <th>最后操作人</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="modal" id="my-modal-alert">
                <div class="modal-dialog">
                    <div class=" modal-content modal-content-define">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times</span><span class="sr-only">Close</span>
                            </button>
                            <h4 class="modal-title" id="modal-title">查看停车券</h4><span id="num"></span>
                        </div>
                    <div class="modal-body">
                        <div id="modal_message">
                            <div class="row">
                                <label class="col-sm-3 control-label">所属广场：</label>
                                <div class="col-sm-8" id="parkName_dialog"></div>
                            </div>
                            <div class="row">
                                <label class="col-sm-3 control-label">活动名称：</label>
                                <div class="col-sm-5" id="actionName_dialog"></div>
                            </div>
                            <div class="row">
                                <label class="col-sm-3 control-label">活动类型：</label>
                                <div class="col-sm-8" id="type_dialog"></div>
                            </div>
                            <div class="row">
                                <label class="col-sm-3 control-label">活动时间：</label>
                                <div class="col-sm-8" id="actionTime_dialog"></div>
                            </div>
                            <div class="row">
                                <label class="col-sm-3 control-label">活动封面：</label>
                                <div class="col-sm-8" id="coverUrl_dialog"></div>
                            </div>
                            <div class="row">
                                <label class="col-sm-3 control-label">活动LOGO：</label>
                                <div class="col-sm-8">
                                    <img src="" id="logoUrl_dialog" alt="">
                                </div>
                            </div>
                            <p class="full-line"></p>
                            <div class="row">
                                <label class="col-sm-3 control-label">参与会员：</label>
                                <div class="col-sm-8" id="participant_dialog"></div>
                            </div>
                            <div class="row">
                                <label class="col-sm-3 control-label">关联的券：</label>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 coupon-group">
                                    <ul>
                                        <li class="col-sm-3">
                                            <div>序号</div>
                                        </li>
                                        <li class="col-sm-4">
                                            <div>券名称</div>
                                        </li>
                                        <li class="col-sm-3">
                                            <div>发券数量</div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/*modal-body*/-->
                    <div class="modal-footer" id="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">确定</button>
                        <!--/*modal-body*/-->
                    </div>
                    <!--/*modal-footer*/-->
                </div>
            <!--/*modal-dialog*/-->
            <!--/*modal-content*/-->
             </div>
        </div>

</div>

        <%@ include file="../commons/footer.jsp" %>
    </div>
</div>
<script src="${ctx}/mysource/js/bjparkmgr/common/common.js"></script>
<script src="${ctx}/mysource/js/bjparkmgr/common/dateUtil.js"></script>
<script src="${ctx}/mysource/js/plugins/combo.select/jquery.combo.select.js"></script>
<script src="${ctx}/mysource/js/bjparkmgr/action/actionList.js"></script>
</body>
</html>