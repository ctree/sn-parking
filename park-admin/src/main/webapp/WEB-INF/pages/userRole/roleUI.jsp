<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="../commons/meta.jsp" %>
    <link rel="stylesheet" type="text/css" href="${ctx}/hplus/css/plugins/treeview/jquery.treeview.css"/>
    <script type="text/javascript">
        var aurList = '${aurList}';
        aurList = JSON.parse(aurList);
    </script>
    <title>${commonTitle} - 用户授权</title>
</head>
<body class="fixed-sidebar full-height-layout">
<div id="wrapper">
    <%@ include file="../commons/left.jsp" %>
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <%@ include file="../commons/top.jsp" %>
        <!-- 业务代码 -->
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>用户授权</h2>
                <ol class="breadcrumb">
                    <li>
                        <a href="${ctx}/">主页</a>
                    </li>
                    <li>
                        <a>权限管理</a>
                    </li>
                    <li>
                        <a href="${ctx}/mgr/vali/roleUI">用户授权管理</a>
                    </li>
                    <li>
                        <strong>用户授权</strong>
                    </li>
                </ol>
            </div>
            <div class="col-lg-2">
            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>修改角色</h5>
                            <div class="ibox-tools">
                                <a href="${ctx}/mgr/vali/list" class="btn btn-primary btn-xs">返回列表</a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <form class="form-horizontal m-t" id="editForm" method="post" action="#" novalidate>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">用户：</label>
                                    <div class="col-sm-4">
                                        <input id="userId" value="${sysUser.userId}" type="hidden">
                                        <input id="positionName" name="positionName" value="${sysUser.login}" disabled
                                               class="form-control" type="text" placeholder="请输入角色名称">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">选择角色：</label>
                                    <div class="col-sm-4">
                                        <select id='auroleInfo' name='auroleInfo' multiple='true'
                                                data-placeholder="请输入角色名称" class='chosen-select col-sm-12'>
                                            <c:forEach items="${arList}" var="item">
                                                <option value="${item.roleId}">${item.positionName}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                </div>
                                <input type="hidden" id="auRoleIds" name="auRoleIds"/>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-3">
                                        <button class="btn btn-default" type="reset">重置</button>
                                        <button class="btn btn-primary" type="submit">授权</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%@ include file="../commons/footer.jsp" %>
    </div>
</div>
<!-- jQuery Validation plugin javascript-->
<%@ include file="../commons/validate.jsp" %>
<script src="${ctx}/mysource/js/bjparkmgr/userRole/validate.js"></script>
</body>
</html>