<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="../commons/meta.jsp" %>
    <title>${commonTitle} - 用户授权管理</title>
</head>
<body class="fixed-sidebar full-height-layout">
<div id="wrapper">
    <%@ include file="../commons/left.jsp" %>
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <%@ include file="../commons/top.jsp" %>
        <!-- 业务代码 -->
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>用户授权管理</h2>
                <ol class="breadcrumb">
                    <li><a href="${ctx}/">主页</a></li>
                    <li><a>权限管理</a></li>
                    <li><strong>用户授权管理</strong></li>
                </ol>
            </div>
            <div class="col-lg-2">
            </div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>用户列表</h5>
                            <div class="ibox-tools">
                            </div>
                        </div>
                        <div class="ibox-content">
                            <!-- 查询start -->
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-1 control-label">账号</label>
                                    <div class="col-sm-2">
                                        <div class="input-group">
                                            <input type="text" id="login" name="login" class="form-control"
                                                   placeholder="请输入账号">
                                        </div>
                                    </div>
                                    <label class="col-sm-1 control-label">手机号</label>
                                    <div class="col-sm-2">
                                        <div class="input-group">
                                            <input type="text" id="phone" name="phone" class="form-control"
                                                   placeholder="请输入手机号">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-1">
                                        <div class="input-group">
                                            <button type="button" id="serach" class="btn btn-primary">查询</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- 查询end -->

                            <table id="dataList" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>序号</th>
                                    <th>账号</th>
                                    <th>邮编</th>
                                    <th>手机号</th>
                                    <th>用户类型</th>
                                    <th>用户状态启用</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <%@ include file="../commons/footer.jsp" %>
    </div>
</div>
<script src="${ctx}/mysource/js/bjparkmgr/userRole/list.js"></script>
</body>
</html>