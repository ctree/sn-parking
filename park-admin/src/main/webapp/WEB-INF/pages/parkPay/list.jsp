<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="../commons/meta.jsp" %>
    <!--轮播图-->
    <title>易付宝商户号配置管理</title>
    <style>
        #show_img{
            width: 850px;
            height:650px;
        }
    </style>
</head>
<body class="fixed-sidebar full-height-layout">
<div id="wrapper">
    <%@ include file="../commons/left.jsp" %>
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <%@ include file="../commons/top.jsp" %>
        <!-- 业务代码 -->
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>易付宝商户号配置</h2>
                <ol class="breadcrumb">
                    <li><a href="${ctx}/mgr/parkPay/list">易付宝商户号配置管理</a></li>
                    <li><strong>易付宝商户号配置</strong></li>
                </ol>
            </div>
            <div class="col-lg-2"></div>
        </div>

        <div class="gray-bg wrapper wrapper-content animated fadeInRight dashbard-1">
            <div class="row">

                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>易付宝商户号配置列表</h5>
                            <div class="ibox-tools">
                                <a href="${ctx}/mgr/parkPay/addUI" class="btn btn-primary btn-xs">添加易付宝商户号配置</a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <!-- 查询start -->
                            <div class="form-horizontal" id="parkPayList_queryDiv">
                                <div class="form-group">
                                    <label class="col-lg-1 col-md-2 col-sm-4 control-label" style="padding-left:0;padding-right:0;">所属停车场</label>
                                    <div class="col-lg-2 col-md-4 col-sm-8">
                                        <select id="parkId" name="parkId" class='chosen-select form-control'></select>
                                    </div>
                                    <div class="col-lg-2 col-md-4 col-sm-8">
                                        <button type="button" id="parkPayList_serach" class="btn btn-primary">查询</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- 查询end -->
                    </div>
                </div>
            </div>

            <div class="col-lg-12" style="overflow: auto;">
                <table id="parkPayList_dataList" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>序号</th>
                        <th>停车场</th>
                        <th>易付宝商户号</th>
                        <th>商户公钥索引号</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

        <%@ include file="../commons/footer.jsp" %>
    </div>
</div>
</div>
<script src="${ctx}/mysource/js/bjparkmgr/parkPay/list.js"></script>
</body>
</html>