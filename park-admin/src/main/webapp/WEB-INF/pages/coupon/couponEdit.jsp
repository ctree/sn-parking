<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="../commons/meta.jsp" %>
    <title>编辑优惠券</title>
    <style>
        #show_img{
            width: 850px;
            height:650px;
        }
    </style>
    <link rel="stylesheet" href="${ctx}/mysource/css/coupon/coupon.css">
    <link rel="stylesheet" href="${ctx}/mysource/js/plugins/combo.select/combo.select.css">
</head>
<body class="fixed-sidebar full-height-layout">
<div id="wrapper">
    <%@ include file="../commons/left.jsp" %>
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <%@ include file="../commons/top.jsp" %>
        <!-- 业务代码 -->
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>编辑优惠券</h2>
                <ol class="breadcrumb">
                    <li><a href="/mgr/">主页</a></li>
                    <li><a href="/mgr/merchant/list">优惠券管理</a></li>
                    <li><strong>修改优惠券</strong></li>
                </ol>
            </div>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>修改优惠券</h5>
                        </div>
                        <div class="ibox-content">
                            <form class="form-horizontal m-t" id="editForm" method="post" action="#">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><i class="red">*</i>优惠券名称：</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" id="couponName" name="couponName" value="${coupon.couponName}" placeholder="请输入优惠名称" type="text" />
                                        <input class="form-control" name="id" value="${coupon.id}" type="hidden" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><i class="red">*</i>所属广场：</label>
                                    <div class="col-sm-4">
                                        <select id='parkId' name='parkId' class='form-control' value="${coupon.parkId}">
                                            <option value=''>请选择所属广场</option>
                                        </select>
                                    </div>
                                    <input type="hidden" id="parkIdTem" value="${coupon.parkId}">
                                    <input type="hidden" name="ascription" id="ascription" value="${coupon.ascription}">
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><i class="red">*</i>优惠券面值：</label>
                                    <div class="col-sm-2">
                                        <input class="form-control" id="amount" name="amount" value="${coupon.amount}" placeholder="" type="text" />
                                    </div>元
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><i class="red">*</i>优惠券总数：</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" id="total" name="total" value="${coupon.total}" placeholder="" type="text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><i class="red">*</i>优惠券有效时间：</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" id="startTimeStr" name="startTimeStr" value="<fmt:formatDate pattern="yyyy-MM-dd hh:mm:ss" value="${coupon.startTime}" />"  placeholder="请选择有效开始时间" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-4">
                                        <input class="form-control" id="endTimeStr" name="endTimeStr" value="<fmt:formatDate pattern="yyyy-MM-dd hh:mm:ss" value="${coupon.endTime}" />"   placeholder="请选择有效结束时间" onclick="laydate({istime: true, format: 'YYYY-MM-DD hh:mm:ss'})">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><i class="red">*</i>用券规则：</label>
                                    <div class="col-sm-8 radio">
                                        <input type="radio" name="ruleType" id="unlimit" <c:if test="${coupon.ruleType=='0'}">checked="checked"</c:if> value="0">
                                        <label for="unlimit">不限</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-8 radio">
                                        <input type="radio" name="ruleType" id="limit" value="1" <c:if test="${coupon.ruleType=='1'}">checked="checked"</c:if>>
                                        <label for="limit">
                                            单笔订单满限
                                            <input type="text" class="large-input" name="lowestAmount" value="${coupon.ruleType=='1'?coupon.lowestAmount:''}" id="lowestAmount">
                                            元使用
                                            <input type="text" class="large-input" name="useCount" value="${coupon.ruleType=='1'?coupon.useCount:''}" id="useCount"> 张
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"><i class="red">*</i>规则描述：</label>
                                    <div class="col-sm-4">
                                        <textarea name="ruleDepict" id="ruleDepict" placeholder="请填写规则描述" cols="30" rows="10">${coupon.ruleDepict}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-3">
                                        <button class="btn btn-primary" type="submit" id="submit">确定</button>
                                        <button class="btn btn-primary" type="button" id="backButton">返回</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%@ include file="../commons/footer.jsp" %>
    </div>
</div>
<script src="${ctx}/mysource/js/plugins/laydate/laydate.js"></script>
<script src="${ctx}/mysource/js/bjparkmgr/common/common.js"></script>
<script src="${ctx}/mysource/js/plugins/combo.select/jquery.combo.select.js"></script>
<script src="${ctx}/mysource/js/bjparkmgr/coupon/couponEdit.js"></script>
</body>
</html>