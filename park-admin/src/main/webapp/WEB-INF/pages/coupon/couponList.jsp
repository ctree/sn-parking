<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="../commons/meta.jsp" %>
    <title>系统管理</title>
    <style>
        #show_img{
            width: 850px;
            height:650px;
        }
        .modal-dialog{
            margin-top: 10%!important;
        }
    </style>
    <link rel="stylesheet" href="${ctx}/mysource/css/coupon/coupon.css">
    <link rel="stylesheet" href="${ctx}/mysource/js/plugins/combo.select/combo.select.css">
</head>
<body class="fixed-sidebar full-height-layout">
<div id="wrapper">
    <%@ include file="../commons/left.jsp" %>
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <%@ include file="../commons/top.jsp" %>
        <!-- 业务代码 -->
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>优惠券管理</h2>
                <ol class="breadcrumb">
                    <li><strong>优惠券记录</strong></li>
                </ol>
            </div>
            <div class="col-lg-2"></div>
        </div>

        <div class="gray-bg wrapper wrapper-content animated fadeInRight dashbard-1">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox-content">
                        <!-- 查询start -->
                        <div class="form-horizontal" id="coupon_queryDiv">
                            <div class="form-group">
                                <label class="col-lg-1 col-md-2 col-sm-4 control-label">所属广场</label>
                                <div class="col-sm-3">
                                    <select id='parkId' name='parkId' class='form-control'>
                                        <option value=''>请选择所属广场</option>
                                    </select>
                                </div>
                                <label class="col-lg-1 col-md-2 col-sm-4 control-label">券状态</label>
                                <div class="col-sm-3">
                                    <select id='state' name='state' class='form-control'>
                                        <option value=''>全部</option>
                                        <option value='0'>启用</option>
                                        <option value='1'>禁用</option>
                                    </select>
                                </div>
                                <label class="col-lg-1 col-md-2 col-sm-4 control-label">券名称</label>
                                <div class="col-lg-2 col-md-4 col-sm-8">
                                    <input type="text" class="form-control" name="couponName" id="couponName"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-1 col-md-2 col-sm-3 control-label">券有效期</label>
                                <div class="col-lg-2 col-md-4 col-sm-3">
                                    <input type="date" class="form-control" name="startTime" id="startTime"/>
                                </div>
                                <div class="col-lg-2 col-md-4 col-sm-3">
                                    <input type="date" class="form-control" name="endTime" id="endTime"/>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-3">
                                    <button type="button" id="coupon_serach" class="btn btn-primary">查询</button>
                                    <button type="button" id="couponReset" class="btn btn-primary">重置</button>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-3">
                                    <button type="button" id="couponAdd" class="btn btn-primary">新增</button>
                                    <button type="button" id="couponDel" class="btn btn-primary">删除</button>
                                </div>
                                <div class="col-lg-2 col-md-3 col-sm-3">
                                    <button type="button" id="couponStart" class="btn btn-primary">启用</button>
                                    <button type="button" id="couponStop" class="btn btn-primary">禁用</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12" style="overflow: auto;">
                    <table id="couponList" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>
                                <input type="checkbox" name="selectAll" id="selectAll">&nbsp;&nbsp;序号
                            </th>
                            <th>券编号</th>
                            <th>所属广场</th>
                            <th>券名称</th>
                            <th>剩余券</th>
                            <th>开始时间</th>
                            <th>结束时间</th>
                            <th>券状态</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="modal" id="my-modal-alert">
                <div class="modal-dialog">
                    <div class=" modal-content modal-content-define">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times</span><span class="sr-only">Close</span>
                            </button>
                            <h4 class="modal-title" id="modal-title">查看停车券</h4><span id="num"></span>
                        </div>
                        <!--/*modal-header*/-->
                        <div class="modal-body">
                            <div id="modal_message">
                                <div class="row">
                                    <label class="col-sm-3 control-label">券名称：</label>
                                    <div class="col-sm-5" id="couponName_dialog"></div>
                                </div><br/>
                                <div class="row">
                                    <label class="col-sm-3 control-label">所属广场：</label>
                                    <div class="col-sm-8" id="parkName_dialog"></div>
                                </div><br/>
                                <div class="row">
                                    <label class="col-sm-3 control-label">券面额：</label>
                                    <div class="col-sm-8" id="amount_dialog"></div>
                                </div><br/>
                                <div class="row">
                                    <label class="col-sm-3 control-label">券总数：</label>
                                    <div class="col-sm-8" id="total_dialog"></div>
                                </div><br/>
                                <div class="row">
                                    <label class="col-sm-3 control-label">券有效期：</label>
                                    <div class="col-sm-8" id="couponTime_dialog"></div>
                                </div><br/>
                                <div class="row">
                                    <label class="col-sm-3 control-label">规则描述：</label>
                                    <div class="col-sm-8" id="ruleDepict_dialog"></div>
                                </div><br/>
                            </div>
                        </div>
                        <!--/*modal-body*/-->
                        <div class="modal-footer" id="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">确定</button>
                            <!--/*modal-body*/-->
                        </div>
                        <!--/*modal-footer*/-->
                    </div>
                    <!--/*modal-dialog*/-->
                <!--/*modal-content*/-->
            </div>
        </div>

        </div>

        <%@ include file="../commons/footer.jsp" %>
    </div>
</div>
<script src="${ctx}/mysource/js/bjparkmgr/common/common.js"></script>
<script src="${ctx}/mysource/js/bjparkmgr/common/dateUtil.js"></script>
<script src="${ctx}/mysource/js/plugins/combo.select/jquery.combo.select.js"></script>
<script src="${ctx}/mysource/js/bjparkmgr/coupon/couponList.js"></script>
</body>
</html>