<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
   <%@ include file="../commons/meta.jsp"%>
   <link rel="stylesheet" type="text/css" href="${ctx}/hplus/css/plugins/treeview/jquery.treeview.css" />
     <style type="text/css">
<!--
.atten {font-size:12px;font-weight:normal;color:#F00;}
-->
</style>
<title>${commonTitle} - 添加角色</title>
</head>
<body class="fixed-sidebar full-height-layout">
    <div id="wrapper">
        <%@ include file="../commons/left.jsp"%>
        <div id="page-wrapper" class="gray-bg dashbard-1">
            <%@ include file="../commons/top.jsp"%>
            <!-- 业务代码 -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>添加角色</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="${ctx}/">主页</a>
                        </li>
                        <li>
                            <a>权限管理</a>
                        </li>
                        <li>
                            <a href="${ctx}/role/list">角色管理</a>
                        </li>
                        <li>
                            <strong>添加角色</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            
                            <div class="ibox-title">
                                <h5>添加角色</h5>
                                <div class="ibox-tools">
                                    <a href="${ctx}/mgr/role/list" class="btn btn-primary btn-xs">返回列表</a>
                                </div>
                            </div>
                            <div class="ibox-content">
                                <form class="form-horizontal m-t" id="addForm" method="post" action="#" novalidate>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">角色名称：</label>
                                        <div class="col-sm-4">
                                            <input id="positionName" name="positionName" class="form-control" type="text" placeholder="请输入角色名称" class="{required:true,minlength:5,messages:{required:'请输入角色名称'}}" >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">是否启用：</label>
                                        <div class="col-sm-4" id="radio_id">
                                           <div class="radio col-md-2">
                                                <label>
                                                        <input type="radio" checked="checked" value="0" id="enableStatus" name="enableStatus">是</label>
                                            </div>
                                            <div class="radio col-md-2">
                                                <label>
                                                    <input type="radio" checked="checked" value="1" id="enableStatus" name="enableStatus">否</label>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">备注：</label>
                                        <div class="col-sm-4">
                                            <textarea id="remark" rows="5" name="remark" class="form-control" placeholder="请输入备注"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-3">
                                            <button class="btn btn-default" type="reset">重置</button>
                                            <button class="btn btn-primary" type="submit">添加</button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <%@ include file="../commons/footer.jsp"%>
        </div>
    </div>
    <!-- jQuery Validation plugin javascript-->
    <%@ include file="../commons/validate.jsp"%>
    <script src="${ctx}/mysource/js/bjparkmgr/role/validate.js"></script>
</body>
</html>