<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
   <%@ include file="../commons/meta.jsp"%>
   <link rel="stylesheet" type="text/css" href="${ctx}/hplus/css/plugins/treeview/jquery.treeview.css" />
     <style type="text/css">
<!--
.atten {font-size:12px;font-weight:normal;color:#F00;}
.help-block{display: inline;}
-->
</style>
<title>${commonTitle} - 角色授权</title>
</head>
<body class="fixed-sidebar full-height-layout">
    <div id="wrapper">
        <%@ include file="../commons/left.jsp"%>
        <div id="page-wrapper" class="gray-bg dashbard-1">
            <%@ include file="../commons/top.jsp"%>
            <!-- 业务代码 -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>角色授权</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="${ctx}/">主页</a>
                        </li>
                        <li>
                            <a>权限管理</a>
                        </li>
                        <li>
                            <a href="${ctx}/mgr/funcTree/list">角色管理</a>
                        </li>
                        <li>
                            <strong>角色授权</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">

                            <div class="ibox-title">
                                <h5>角色授权</h5>
                                <div class="ibox-tools">
                                    <a href="${ctx}/mgr/role/list" class="btn btn-primary btn-xs">返回列表</a>
                                </div>
                            </div>
                            <div class="ibox-content">

                                <form class="form-horizontal m-t" id="editForm" method="post" action="#">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">角色名称：</label>
                                        <div class="col-sm-4" id="selectDiv">
                                        <select data-placeholder="选择角色..." id="auRoleId" name="auRoleId" class="chosen-select" onchange="toSelect()" style="width:350px;" tabindex="2">
                                            <option value="0">请选择</option>
                                            <c:choose>
								  	            <c:when test="${not empty roleList}">
									  	            <c:forEach items="${roleList}" var="item">
											            <option value="${item.auRoleId}">${item.positionName}</option>
										            </c:forEach>
								  	            </c:when>
								            </c:choose>
                                        </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">资源列表：</label>
                                        <div class="col-sm-4" id="treeDiv">
                                            <div id="tree" class="ztree" ></div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-3">
                                            <button class="btn btn-default" type="reset">重置</button>
                                            <button class="btn btn-primary" type="button" id="savebutton" >确定</button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <%@ include file="../commons/footer.jsp"%>
        </div>
    </div>
    <!-- jQuery Validation plugin javascript-->
    <%@ include file="../commons/validate.jsp"%>
    <link rel="stylesheet" href="${ctx}/hplus/js/plugins/ztree/css/zTreeStyle/zTreeStyle.css" type="text/css">
    <script src="${ctx}/hplus/js/plugins/ztree/jquery.ztree.core-3.5.min.js"></script>
	<script src="${ctx}/hplus/js/plugins/ztree/jquery.ztree.excheck-3.5.min.js"></script>
    <script src='${ctx}/hplus/js/plugins/treeview/jquery.treeview.js'></script>
    <script src="${ctx}/mysource/js/bjparkmgr/role/roleFuncTree.js"></script>

</body>
</html>