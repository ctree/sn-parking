<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="../commons/meta.jsp" %>
    <title>系统管理</title>
    <style>
        #show_img{
            width: 850px;
            height:650px;
        }
    </style>
</head>
<body class="fixed-sidebar full-height-layout">
<div id="wrapper">
    <%@ include file="../commons/left.jsp" %>
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <%@ include file="../commons/top.jsp" %>
        <!-- 业务代码 -->
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>商户充值</h2>
                <ol class="breadcrumb">
                    <li><a href="/mgr/">主页</a></li>
                    <li><a href="/mgr/merchant/list">商户管理</a></li>
                    <li><strong>商户扣款</strong></li>
                </ol>
            </div>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>商户扣款</h5>
                        </div>
                        <div class="ibox-content">
                            <form class="form-horizontal m-t" id="addForm" method="post" action="#">
                                <input type="hidden" name="mmMerchantId" id="mmMerchantId" value="420921355d944378ba43541353df128b" />
                                <input type="hidden" name="isTime" value="0" />
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">商户名称：</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" value="南京紫峰大厦发放商户" type="text" readonly />
                                    </div>
                                </div>
                                <div class="form-group" id="balanceIdDiv">
                                    <label class="col-sm-3 control-label">商户余额（元）：</label>
                                    <div class="col-sm-4">
                                        <input id="balanceId" class="form-control" value="19.00" type="text" readonly />
                                    </div>
                                </div>
                                <div class="form-group" id="amountDiv">
                                    <label class="col-sm-3 control-label">充值金额（元）：</label>
                                    <div class="col-sm-4">
                                        <input id="amount" name="amount" class="form-control" type="text" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">描述：</label>
                                    <div class="col-sm-4">
                                        <textarea rows="6" style="width: 100%;" id="content" name="content"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-3">
                                        <button class="btn btn-primary" type="submit">确定</button>
                                        <button class="btn btn-primary" type="button" id="backButton">返回</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <%@ include file="../commons/footer.jsp" %>
    </div>
</div>
<script src="${ctx}/mysource/js/bjparkmgr/common/dateUtil.js"></script>
<script src="${ctx}/mysource/js/bjparkmgr/merchant/accountOut.js"></script>
</body>
</html>