<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="../commons/meta.jsp" %>
    <title>系统管理</title>
    <style>
        #show_img{
            width: 850px;
            height:650px;
        }
    </style>
</head>
<body class="fixed-sidebar full-height-layout">
<div id="wrapper">
    <%@ include file="../commons/left.jsp" %>
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <%@ include file="../commons/top.jsp" %>
        <!-- 业务代码 -->
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>账户管理</h2>
                <ol class="breadcrumb">
                    <li><strong>账户管理</strong></li>
                </ol>
            </div>
            <div class="col-lg-2"></div>
        </div>

        <div class="gray-bg wrapper wrapper-content animated fadeInRight dashbard-1">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox-content">
                        <!-- 查询start -->
                        <div class="form-horizontal" id="coupon_queryDiv">
                            <div class="form-group">
                                <label class="col-lg-2 col-md-2 col-sm-4 control-label">停车场名称</label>
                                <div class="col-lg-2 col-md-4 col-sm-8">
                                    <input type="text" class="form-control" name="parkName" id="parkId"/>
                                </div>
                                <label class="col-lg-2 col-md-2 col-sm-4 control-label">结束时间</label>
                                <div class="col-lg-2 col-md-4 col-sm-8">
                                    <input type="text" class="form-control" name="merchant" id="merchantId"/>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3">
                                    <button type="button" id="coupon_serach" class="btn btn-primary">查询</button>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="col-lg-12" style="overflow: auto;">
                    <table id="couponList" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <th>序号</th>
                            <th>账号</th>
                            <th>创建时间</th>
                            <th>操作</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal inmodal" id="userGrantImageModel" tabindex="-1" role="dialog" aria-hidden="true" onclick="$('#userGrantImageModel').hide()">
            <div class="modal-dialog" style="width:850px; hight:650px">
                <div class="modal-content animated fadeIn">
                    <div class="modal-body" id="bodyDiv" style=" positon:absolute;padding:0">
                         <div class="carousel-inner" >
                            <div>
                                <img alt="First slide"  id="show_img" style="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%@ include file="../commons/footer.jsp" %>
    </div>
</div>
<script src="${ctx}/mysource/js/bjparkmgr/common/dateUtil.js"></script>
<script src="${ctx}/mysource/js/bjparkmgr/merchant/accountList.js"></script>
</body>
</html>