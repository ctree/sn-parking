<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="expires" content="0">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="pragma" content="no-cache">
    <%@ include file="../commons/meta.jsp" %>
    <style type="text/css">
        .payOrder_center {
            text-align: center;
        }
        .payOrder_center_bt {
            width: 100px;
        }
        .modal-content-define{
            width: 100%;
        }
        .hr-define{
            border-bottom:1px solid #dddddd!important;
            border-top: 0!important;
            /*text-align: center;*/
            /*vertical-align: middle;*/
            /*height: 60px;*/
        }
    </style>
    <title>订单管理</title>
</head>
<body class="fixed-sidebar full-height-layout">
<div id="wrapper">
    <%@ include file="../commons/left.jsp" %>
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <%@ include file="../commons/top.jsp" %>
        <!-- 业务代码 -->
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>临停车辆收费订单明细</h2>
                <ol class="breadcrumb">
                    <li><a href="${ctx}/view/main">订单管理</a></li>
                    <li><strong>临停车辆收费订单明细</strong></li>
                </ol>
            </div>
            <div class="col-lg-2"></div>
        </div>

        <div class="gray-bg wrapper wrapper-content animated fadeInRight dashbard-1">
            <div class="row">

                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                        </div>
                        <div class="ibox-content">
                            <!-- 查询start -->
                            <div class="form-horizontal" id="userList_queryDiv">
                                <form class="form-horizontal m-t" id="payOrder_list" method="post" action="#">
                                    <div class="form-group">
                                        <label class="col-lg-1 col-md-2 col-sm-4 control-label">停车场</label>
                                        <div class="col-lg-2 col-md-4 col-sm-8">
                                            <select id='parkId' name='parkId' class='chosen-select form-control'>
                                                <option value=''>全部</option>
                                            </select>
                                        </div>
                                        <label class="col-lg-1 col-md-2 col-sm-4 control-label">订单状态</label>
                                        <div class="col-lg-2 col-md-4 col-sm-8">
                                            <select id='orderType' name='orderType' class='chosen-select form-control'>
                                                <option value=''>全部</option>
                                                <option value="0">待支付</option>
                                                <option value="2">已支付</option>
                                                <option value="4">退款中</option>
                                                <option value="5">退款失败</option>
                                                <option value="6">退款成功</option>
                                                <option value="7">已取消</option>
                                            </select>
                                        </div>
                                        <label class="col-lg-1 col-md-2 col-sm-4 control-label">收费渠道</label>
                                        <div class="col-lg-2 col-md-4 col-sm-8">
                                            <select id='recvChannel' name='recvChannel' class='chosen-select form-control'>
                                                <option value=''>全部</option>
                                                <option value='0'>微信</option>
                                                <option value='1'>APP</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-lg-1 col-md-2 col-sm-4 control-label">车牌号</label>
                                        <div class="col-lg-2 col-md-4 col-sm-8">
                                            <input type="text" class="form-control" name="carNum" id="carNum"/>
                                        </div>
                                        <label class="col-lg-1 col-md-2 col-sm-4 control-label">收费日期</label>
                                        <div class="col-lg-2 col-md-4 col-sm-8">
                                            <input type="date" class="form-control" name="successTime"
                                                   id="successTime"/>
                                        </div>
                                        <label class="col-lg-1 col-md-2 col-sm-4 control-label">操作员</label>
                                        <div class="col-lg-2 col-md-4 col-sm-8">
                                            <input type="text" class="form-control" name="createName" id="createName"/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-lg-4 col-md-8 col-sm-8">
                                            <button class="btn btn-primary" type="reset">重置</button>
                                        </div>
                                        <div class="col-lg-4 col-md-8 col-sm-8">
                                            <button type="button" id="payOrderList_serach" class="btn btn-primary">查询
                                            </button>
                                        </div>
                                        <div class="col-lg-4 col-md-8 col-sm-8">
                                            <button type="button" id="payOrderList_export" class="btn btn-primary">导出
                                                Excel
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- 查询end -->
                    </div>
                </div>
            </div>

            <div class="col-lg-12" style="overflow: auto;">
                <table id="orderList_dataList" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>序&nbsp;号</th>
                        <th>订&nbsp;单&nbsp;号</th>
                        <th>订&nbsp;单&nbsp;状&nbsp;态</th>
                        <th>车&nbsp;牌&nbsp;号</th>
                        <th>停&nbsp;车&nbsp;时&nbsp;长 ( 小时 )</th>
                        <th>收&nbsp;费&nbsp;渠&nbsp;道</th>
                        <th>收&nbsp;费&nbsp;金&nbsp;额 ( 元 )</th>
                        <th>收&nbsp;费&nbsp;日&nbsp;期</th>
                        <th>操&nbsp;作&nbsp;</th>
                    </tr>
                    </thead>
                </table>
            </div>

            <%--查看明细--%>
            <div class="modal" id="my-modal-alert">
                <div class="modal-dialog">
                    <div class=" modal-content  modal-content-define">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times</span><span class="sr-only">Close</span>
                            </button>
                            <h4 class="modal-title" id="modal-title">收费明细</h4><span id="num"></span>
                        </div>
                        <!--/*modal-header*/-->
                        <div class="modal-body">
                            <div id="modal_message">
                                <div class="row">
                                    <label class="col-sm-3 control-label">订单号：</label>
                                    <div class="col-sm-5" id="orderId_dialog"></div>
                                </div><br/>
                                <div class="row">
                                    <label class="col-sm-3 control-label">停车订单号：</label>
                                    <div class="col-sm-5" id="parkOrderId_dialog"></div>
                                </div><br/>
                                <div class="row">
                                    <label class="col-sm-3 control-label">车牌号：</label>
                                    <div class="col-sm-5" id="carNum_dialog"></div>
                                </div><br/>
                                <div class="row">
                                    <label class="col-sm-3 control-label">会员号：</label>
                                    <div class="col-sm-5" id="memberId_dialog"></div>
                                </div><br/>
                                <div class="row">
                                    <label class="col-sm-3 control-label">进场时间：</label>
                                    <div class="col-sm-5" id="enterTime_dialog"></div>
                                </div><br/>
                                <div class="row">
                                    <label class="col-sm-3 control-label">出场时间：</label>
                                    <div class="col-sm-5" id="outTime_dialog"></div>
                                </div><br/>
                                <div class="row">
                                    <label class="col-sm-3 control-label">停车时长：</label>
                                    <div class="col-sm-5" id="parkTime_dialog"></div>
                                </div><br/>
                                <div class="row">
                                    <label class="col-sm-3 control-label">收费渠道：</label>
                                    <div class="col-sm-5" id="tollChannel_dialog"></div>
                                </div><br/>
                                <div class="row">
                                    <label class="col-sm-3 control-label">支付金额：</label>
                                    <div class="col-sm-5" id="payAmount_dialog"></div>
                                </div><br/>
                                <div class="row">
                                    <label class="col-sm-3 control-label">易付宝：</label>
                                    <div class="col-sm-5" id="payByYFB_dialog">5元</div>
                                </div><br/>
                                <div class="row">
                                    <label class="col-sm-3 control-label">云钻抵扣：</label>
                                    <div class="col-sm-5" id="payByYZ_dialog">2元</div>
                                </div><br/>
                                <div class="row">
                                    <label class="col-sm-3 control-label">停车券：</label>
                                    <div class="col-sm-5" id="parkTicket_dialog">注册有礼 - 2小时免费券</div>
                                </div><br/>
                                <div class="row">
                                    <label class="col-sm-3 control-label">收费日期：</label>
                                    <div class="col-sm-5" id="chargeDate_dialog"></div>
                                </div>
                            </div>
                        </div>
                        <!--/*modal-body*/-->
                        <div class="modal-footer payOrder_center" id="modal-footer">
                            <button type="button" class="btn btn-primary payOrder_center_bt" data-dismiss="modal">确定</button>
                            <!--/*modal-body*/-->
                        </div>
                        <!--/*modal-footer*/-->
                    </div>
                    <!--/*modal-dialog*/-->
                </div>
                <!--/*modal-content*/-->
            </div>


        </div>
        <%@ include file="../commons/footer.jsp" %>
    </div>
</div>
</div>
<script src="${ctx}/mysource/js/bjparkmgr/common/dateUtil.js"></script>
<script src="${ctx}/mysource/js/bjparkmgr/common/common.js"></script>
<script src="${ctx}/mysource/js/bjparkmgr/orderManager/payOrderList.js"></script>
</body>
</html>