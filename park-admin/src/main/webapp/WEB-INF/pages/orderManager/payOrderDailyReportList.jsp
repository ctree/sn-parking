<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="../commons/meta.jsp" %>
    <link href="${ctx}/mysource/plugins/bootstrap-table/bootstrap-table.min.css" rel="stylesheet">
    <script src="${ctx}/mysource/plugins/bootstrap-table/bootstrap-table.min.js"></script>
    <script src="${ctx}/mysource/plugins/bootstrap-table/bootstrap-table-zh-CN.min.js"></script>
    <style type="text/css">
        .payOrder_center {
            text-align: center;
        }

        .payOrder_center_bt {
            width: 100px;
        }

        .modal-content-define {
            width: 100%;
        }
    </style>
    <title>订单管理</title>
</head>
<body class="fixed-sidebar full-height-layout">
<div id="wrapper">
    <%@ include file="../commons/left.jsp" %>
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <%@ include file="../commons/top.jsp" %>
        <!-- 业务代码 -->
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>临停车辆收费订单明细</h2>
                <ol class="breadcrumb">
                    <li><a href="${ctx}/view/main">订单管理</a></li>
                    <li><strong>临停车辆收费订单明细</strong></li>
                </ol>
            </div>
            <div class="col-lg-2"></div>
        </div>

        <div class="gray-bg wrapper wrapper-content animated fadeInRight dashbard-1">
            <div class="row">

                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                        </div>
                        <div class="ibox-content">
                            <!-- 查询start -->
                            <div class="form-horizontal" id="userList_queryDiv">
                                <form class="form-horizontal m-t" id="payOrder_list" method="post" action="#">
                                    <div class="form-group">
                                        <label class="col-lg-1 col-md-2 col-sm-4 control-label">停车场</label>
                                        <div class="col-lg-2 col-md-4 col-sm-8">
                                            <select id='parkId' name='parkId' class='chosen-select form-control'>
                                                <option value=''>全部</option>
                                            </select>
                                        </div>
                                        <label class="col-lg-1 col-md-2 col-sm-4 control-label">收费方式</label>
                                        <div class="col-lg-2 col-md-4 col-sm-8">
                                            <select id='payType' name='payType' class='chosen-select form-control'>
                                                <option value=''>全部</option>
                                                <option value='0'>易付宝</option>
                                                <option value='1'>停车券</option>
                                                <option value='2'>云钻</option>
                                            </select>
                                        </div>
                                        <label class="col-lg-1 col-md-2 col-sm-4 control-label">查询日期</label>
                                        <div class="col-lg-2 col-md-4 col-sm-8">
                                            <input type="date" class="form-control" name="queryTime"
                                                   id="queryTime"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-4 col-md-8 col-sm-8">
                                            <button type="button" id="payOrderList_serach" class="btn btn-primary">查询
                                            </button>
                                        </div>
                                        <div class="col-lg-4 col-md-8 col-sm-8">
                                            <button class="btn btn-primary" type="reset">重置</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- 查询end -->
                    </div>
                </div>
            </div>

            <div class="col-lg-12" style="overflow: auto;">
                <table id="payOrderDaily_dataList" cellpadding="0" cellspacing="0" border="0"  class="table table-striped table-bordered table-hover">

                    <%--<thead>--%>
                    <%--<tr>--%>
                        <%--<th>序号</th>--%>
                        <%--<th>收费渠道</th>--%>
                        <%--<th>收费方式</th>--%>
                        <%--<th>收费金额</th>--%>
                    <%--</tr>--%>
                    <%--</thead>--%>
                </table>

            </div>

        </div>
        <%@ include file="../commons/footer.jsp" %>
    </div>
</div>
</div>
<script src="${ctx}/mysource/js/bjparkmgr/common/dateUtil.js"></script>
<script src="${ctx}/mysource/js/bjparkmgr/common/common.js"></script>
<script src="${ctx}/mysource/js/bjparkmgr/orderManager/payOrderDailyList.js"></script>
</body>
</html>