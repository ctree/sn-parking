<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="../commons/meta.jsp" %>
    <title>编辑停车场信息</title>
    <link rel="stylesheet" href="${ctx}/mysource/js/plugins/combo.select/combo.select.css">
    <style type="text/css">
        <!--
        .atten {
            font-size: 12px;
            font-weight: normal;
            color: #F00;
        }
        .help-block{display: inline;}
        .byTimeRow{display: inline-block;margin-bottom:10px}
        .byTimeRow .fm5{float: left;margin-right:5px}
        .byTimeRow .fm3{float: left;margin-right:30px}
        .byTimeRow .mc{margin: 10px;cursor: pointer}
        .byTimeRow .mm{float: left;margin-left:5px;margin-right:5px}
        .byTimeRow .fmw{float: left;margin-right:5px;width: 50px}
        .byTimeRow .fw8{float: left;width:120px;}
        -->
    </style>
    <script>
        var rules = ${rules};
    </script>
</head>
<body class="fixed-sidebar full-height-layout">
<div id="wrapper">
    <%@ include file="../commons/left.jsp" %>
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <%@ include file="../commons/top.jsp" %>
        <!-- 业务代码 -->
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>编辑停车场信息</h2>
                <ol class="breadcrumb">
                    <li><a href="/mgr/park/list">停车场管理</a></li>
                    <li><strong>编辑停车场信息</strong></li>
                </ol>
            </div>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>编辑停车场信息</h5>
                        </div>
                        <div class="ibox-content">

                            <form class="form-horizontal m-t" id="parkEdit_rejectForm" method="post" action="#" enctype ="multipart/form-data">

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">停车场名称：</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" type="hidden" name="id" id="id" value="${park.id}" />
                                        <input class="form-control" name="parkName" id="parkName" value="${park.parkName}" placeholder="请输入停车场名称" type="text" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">停车场封面：</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" type="file" name="file" id="parkConverPath" value="${park.parkConverPath}" onchange="preImg(this.id,'imgPre');"  />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-3">
                                        <c:set var="nopic" value="${ctx}/hplus/img/nopic.gif"></c:set>
                                        <c:set var="parkImg" value="${ctx}/hplus/parkImg/${park.parkConverPath}"></c:set>
                                        <img id="imgPre" class="form-control" style="height:150px" src="${park.parkConverPath==null?nopic:parkImg}" />
                                    </div>
                                </div>

                                <hr>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">所在省份：</label>
                                    <div class="col-sm-4">
                                        <select id="provinceCode" name="provinceCode"  class='form-control' value="${park.provinceCode}"></select>
                                        <input class="form-control" name="provinceName" id="provinceName" type="hidden" value="${park.provinceName}" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">所在城市：</label>
                                    <div class="col-sm-4">
                                        <select id="cityCode" name="cityCode"  class='form-control' value="${park.cityCode}">
                                            <option value=''>请选择所在城市</option>
                                            <input class="form-control" name="cityName" id="cityName" type="hidden" value="${park.cityCode}" />
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">积分主体类型：</label>
                                    <div class="col-sm-4">
                                        <select id="integralBodyType" name="integralBodyType" value="${park.integralBodyType}" class='form-control'></select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">积分主体编码：</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" name="integralBodyCode" id="integralBodyCode"  value="${park.integralBodyCode}" placeholder="请输入积分主体编码" type="text" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">产权方：</label>
                                    <div class="col-sm-4">
                                        <select id="property" name="property" value="${park.property}" class='form-control'></select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">业态：</label>
                                    <div class="col-sm-4">
                                        <select id="parkType" name="parkType" value="${park.parkType}" class='form-control'></select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">经营主体：</label>
                                    <div class="col-sm-4">
                                        <select id="businessEntity" name="businessEntity" value="${park.businessEntity}" class='form-control'></select>
                                    </div>
                                </div>

                                <%--<div class="form-group">--%>
                                    <%--<label class="col-sm-3 control-label">积分主体类型：</label>--%>
                                    <%--<div class="col-sm-4">--%>
                                        <%--<select id="integralBodyType" name="integralBodyType" value="${park.integralBodyType}" class='form-control'></select>--%>
                                    <%--</div>--%>
                                <%--</div>--%>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">所属公司编码：</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" name="companyNo" id="companyNo" value="${park.companyNo}" placeholder="请输入所属公司编码" type="text" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">所属公司名称：</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" name="companyName" id="companyName" value="${park.companyName}" placeholder="请输入所属公司名称" type="text" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">所属项目编码：</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" name="projectNo" id="projectNo" value="${park.projectNo}" placeholder="请输入所属项目编码" type="text" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">所属项目名称：</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" name="projectName" id="projectName" value="${park.projectName}" placeholder="请输入所属项目名称" type="text" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">是否收费：</label>
                                    <div class="col-sm-4">
                                        <select id="isFee" name="isFee" value="${park.isFee}" class='form-control'>
                                            <option value="">请选择是否收费</option>
                                            <option value="0">是</option>
                                            <option value="1">否</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">备注：</label>
                                    <div class="col-sm-4">
                                        <textarea class="form-control" name="remark" id="remark">${park.remark}</textarea>
                                    </div>
                                </div>

                                <hr>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">收费标准：</label>
                                    <div class="col-sm-4">
                                        <label class="control-label" style="float: left;margin-right: 10px">免费停车时长(分钟)：</label>
                                        <input class="form-control" style="float: left;margin-right:5px;width: 80px" name="freeParkingTime" id="freeParkingTime" type="text" value="${park.freeParkingTime}" />
                                    </div>
                                    <div class="col-sm-4">
                                        <label class="control-label" style="float: left;margin-right: 10px">每天总限额(元)：</label>
                                        <input class="form-control" style="float: left;margin-right:5px;width: 80px" name="dayQuotaAmount" id="dayQuotaAmount"  type="text" value="${park.dayQuotaAmount}" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-4">
                                        <input type="radio" id="byTime" ${park.feeType=='1'?'checked="checked"':''} name="feeType" value="1" style="margin-right:3px" /><label style="margin-right: 10px;" for="byTime">按时段</label>
                                        <button id="addByTimeRowBtn" type="button" class="btn btn-primary btn-sm">
                                            <i class="fa fa-plus">添加</i>
                                        </button>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <input type="hidden" id="rules" name="parkIntervalFeeRules" value='' />
                                    <div class="col-sm-7" id="byTimeDiv"></div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-4">
                                        <input type="radio" id="byCount" ${park.feeType=='2'?'checked="checked"':''} name="feeType" value="2" style="margin-right:3px" /><label for="byCount">按次</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-5">
                                        <label class="control-label" style="float: left;margin-right:5px;">每次收费(元)：</label>
                                        <input class="form-control" style="float: left;margin-right:5px;width: 80px" name="feeAmount" id="feeAmount"  type="text" value="${park.feeAmount}" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-3">
                                        <input type="radio" id="other" ${park.feeType=='3'?'checked="checked"':''} name="feeType" value="3" style="margin-right:3px" /><label for="other">其他</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-3">
                                        <textarea class="form-control" name="feeExplain" id="feeExplain">${park.feeExplain}</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-3">
                                        <button class="btn btn-primary" type="submit">确定</button>
                                        <button class="btn btn-primary" type="button" id="backButton">返回</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%@ include file="../commons/footer.jsp" %>
    </div>
</div>
<script src="${ctx}/mysource/js/bjparkmgr/common/common.js"></script>
<script src="${ctx}/mysource/js/bjparkmgr/park/edit.js"></script>
</body>
</html>