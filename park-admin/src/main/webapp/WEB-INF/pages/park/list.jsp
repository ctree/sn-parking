<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="../commons/meta.jsp" %>
    <!--轮播图-->
    <title>停车场管理</title>
    <style>
        #show_img{
            width: 850px;
            height:650px;
        }
    </style>
</head>
<body class="fixed-sidebar full-height-layout">
<div id="wrapper">
    <%@ include file="../commons/left.jsp" %>
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <%@ include file="../commons/top.jsp" %>
        <!-- 业务代码 -->
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>停车场信息</h2>
                <ol class="breadcrumb">
                    <li><a href="${ctx}/mgr/park/list">停车场管理</a></li>
                    <li><strong>停车场信息</strong></li>
                </ol>
            </div>
            <div class="col-lg-2"></div>
        </div>

        <div class="gray-bg wrapper wrapper-content animated fadeInRight dashbard-1">
            <div class="row">

                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>停车场列表</h5>
                            <div class="ibox-tools">
                                <a href="${ctx}/mgr/park/addUI" class="btn btn-primary btn-xs">添加停车场</a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <!-- 查询start -->
                            <div class="form-horizontal" id="parkList_queryDiv">
                                <div class="form-group">
                                    <label class="col-lg-1 col-md-2 col-sm-4 control-label">产权方</label>
                                    <div class="col-lg-2 col-md-4 col-sm-8">
                                        <select id="property" name="property" class='chosen-select form-control'></select>
                                    </div>
                                    <label class="col-lg-1 col-md-2 col-sm-4 control-label">业态</label>
                                    <div class="col-lg-2 col-md-4 col-sm-8">
                                        <select id="parkType" name="parkType" class='chosen-select form-control'></select>
                                    </div>
                                    <label class="col-lg-1 col-md-2 col-sm-4 control-label">经营主体</label>
                                    <div class="col-lg-2 col-md-4 col-sm-8">
                                        <select id="businessEntity" name="businessEntity" class='chosen-select form-control'></select>
                                    </div>
                                    <label class="col-lg-1 col-md-2 col-sm-4 control-label">是否收费</label>
                                    <div class="col-lg-2 col-md-4 col-sm-8">
                                        <select id="isFee" name="isFee" class='chosen-select form-control'>
                                            <option value=''>全部</option>
                                            <option value="0">是</option>
                                            <option value="1">否</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-1 col-md-2 col-sm-4 control-label" style="padding-left: 0">停车场名称</label>
                                    <div class="col-lg-2 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="parkName" id="parkName"/>
                                    </div>
                                    <label class="col-lg-1 col-md-2 col-sm-4 control-label" style="padding-left:0;padding-right:0;">积分主体类型</label>
                                    <div class="col-lg-2 col-md-4 col-sm-8">
                                        <select id="integralBodyType" name="integralBodyType" class='chosen-select form-control'></select>
                                    </div>
                                    <div class="col-lg-2 col-md-4 col-sm-8">
                                        <button type="button" id="parkList_serach" class="btn btn-primary">查询</button>
                                    </div>
                                    <div class="col-lg-2 col-md-4 col-sm-8">
                                        <button type="button" id="enable" class="btn btn-primary">启用</button>
                                        <button type="button" id="disable" class="btn btn-primary">禁用</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- 查询end -->
                    </div>
                </div>
            </div>

            <div class="col-lg-12" style="overflow: auto;">
                <table id="parkList_dataList" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>序号</th>
                        <th>停车场编码</th>
                        <th>停车场名称</th>
                        <th>产权方</th>
                        <th>业态</th>
                        <th>经营主体</th>
                        <th>公司编码</th>
                        <th>所属公司</th>
                        <th>项目编码</th>
                        <th>所属项目</th>
                        <th>是否收费</th>
                        <th>状态</th>
                        <th>最后操作人</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>

        <%@ include file="../commons/footer.jsp" %>
    </div>
</div>
</div>
<script src="${ctx}/mysource/js/bjparkmgr/park/list.js"></script>
</body>
</html>