<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="../commons/meta.jsp" %>
    <title>用户管理</title>
</head>
<body class="fixed-sidebar full-height-layout">
<div id="wrapper">
    <%@ include file="../commons/left.jsp" %>
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <%@ include file="../commons/top.jsp" %>
        <!-- 业务代码 -->
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>用户信息</h2>
                <ol class="breadcrumb">
                    <li><a href="${ctx}/">用户管理</a></li>
                    <li><strong>用户信息</strong></li>
                </ol>
            </div>
            <div class="col-lg-2"></div>
        </div>

        <div class="gray-bg wrapper wrapper-content animated fadeInRight dashbard-1">
            <div class="row">

                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>人员列表</h5>
                            <div class="ibox-tools">
                                <a href="${ctx}/view/userAdd" class="btn btn-primary btn-xs">添加人员</a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <!-- 查询start -->
                            <div class="form-horizontal" id="userList_queryDiv">
                                <div class="form-group">
                                    <label class="col-lg-1 col-md-2 col-sm-4 control-label">账号</label>
                                    <div class="col-lg-2 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="login" id="login"/>
                                    </div>
                                    <label class="col-lg-1 col-md-2 col-sm-4 control-label">名称</label>
                                    <div class="col-lg-2 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="name" id="name"/>
                                    </div>
                                    <label class="col-lg-1 col-md-2 col-sm-4 control-label">手机号</label>
                                    <div class="col-lg-2 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="phone" id="phone"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-lg-1 col-md-2 col-sm-4 control-label">用户类型</label>
                                    <div class="col-lg-2 col-md-4 col-sm-8">
                                        <select id='type' name='type' class='chosen-select form-control'>
                                            <option value=''>请选择用户类型</option>
                                            <option value="0">系统用户</option>
                                            <option value="1">商户</option>
                                        </select>
                                    </div>
                                    <label class="col-lg-1 col-md-2 col-sm-4 control-label">用户状态</label>
                                    <div class="col-lg-2 col-md-4 col-sm-8">
                                        <select id='state' name='state' class='chosen-select form-control'>
                                            <option value=''>请选择状态</option>
                                            <option value="0">正常</option>
                                            <option value="1">删除</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-2 col-md-4 col-sm-8">
                                        <button type="button" id="userList_serach" class="btn btn-primary">查询</button>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- 查询end -->
                    </div>
                </div>
            </div>

            <div class="col-lg-12" style="overflow: auto;">
                <table id="userList_dataList" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>序&nbsp;号</th>
                        <th>账&nbsp;号</th>
                        <th>名&nbsp;称</th>
                        <th>手&nbsp;机&nbsp;号</th>
                        <th>邮&nbsp;箱</th>
                        <th>用&nbsp;户&nbsp;类&nbsp;型</th>
                        <th>用&nbsp;户&nbsp;状&nbsp;态</th>
                        <th>更&nbsp;新&nbsp;时&nbsp;间</th>
                        <th>操&nbsp;作&nbsp;</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <%@ include file="../commons/footer.jsp" %>
    </div>
</div>
</div>
<script src="${ctx}/mysource/js/bjparkmgr/user/list.js"></script>
</body>
</html>