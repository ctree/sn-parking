<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  ModelF: niecheng
  Date: 2015/11/30
  Time: 19:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="../commons/meta.jsp" %>
    <link href="${ctx}/tcmgr/css/charisma-app.css" rel="stylesheet">
    <script src="${ctx}/tcmgr/js/common.js"></script>
    <title>编辑用户信息</title>

    <style>
        select {
            margin-top: 20px;
            width: 275px;
            height: 30px;
            padding: 3px;
            text-align: center;
            font-size: 14px;
            font-family: "微软雅黑";
            float: left;
            margin-right: 10px;
            border: 1px solid #CCC;
        }
    </style>
</head>
<body class="fixed-sidebar full-height-layout">

<div id="wrapper">
    <%@ include file="../commons/left.jsp" %>
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <%@ include file="../commons/top.jsp" %>
        <!-- 业务代码 -->
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>编辑用户信息</h2>
                <ol class="breadcrumb">
                    <li><a href="${ctx}/">用户管理</a></li>
                    <li><strong>编辑用户信息</strong></li>
                </ol>
            </div>
            <div class="col-lg-2"></div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">

                        <div class="ibox-title">
                            <h5>个人信息</h5>

                            <div class="ibox-tools">
                            </div>
                        </div>

                        <input type="hidden" name="userId" id="userId" value="${sysUser.userId}">

                        <div class="ibox-content">
                            <form class="form-horizontal m-t" id="userEdit_rejectForm" method="post" action="#">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">用户账号：</label>
                                    <div class="col-sm-4">
                                        <input id="login" name="login" class="form-control"
                                               placeholder="请输入用户账号" type="text" value="${sysUser.login}" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">用户名称：</label>
                                    <div class="col-sm-4">
                                        <input id="name" name="name" class="form-control"
                                               placeholder="请输入用户名称" type="text" value="${sysUser.name}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">邮箱：</label>
                                    <div class="col-sm-4">
                                        <input id="email" name="email" class="form-control"
                                               placeholder="请输入邮箱" type="email"  value="${sysUser.email}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">手机号码：</label>
                                    <div class="col-sm-4">
                                        <input id="phone" name="phone" class="form-control"
                                               placeholder="请输入手机号码" type="text"  value="${sysUser.phone}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">用户类型：</label>
                                    <div class="col-sm-4">
                                        <select id='type' name='type' class='chosen-select form-control'>
                                            <c:choose>
                                                <c:when test="${sysUser.type ==0}">
                                                    <option value="0">系统用户</option>
                                                    <option value="1">商户</option>
                                                </c:when>
                                                <c:when test="${sysUser.type == 1}">
                                                    <option value="1">商户</option>
                                                    <option value="0">系统用户</option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value=" ${sysUser.type}">未知</option>
                                                    <option value="1">商户</option>
                                                    <option value="0">系统用户</option>
                                                </c:otherwise>
                                            </c:choose>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">用户状态：</label>
                                    <div class="col-sm-4">
                                        <select id='state' name='state' class='chosen-select form-control'>
                                            <c:choose>
                                                <c:when test="${sysUser.state ==0}">
                                                    <option value="0">启用</option>
                                                    <option value="1">删除</option>
                                                </c:when>
                                                <c:when test="${sysUser.state == 1}">
                                                    <option value="1">删除</option>
                                                    <option value="0">启用</option>
                                                </c:when>
                                                <c:otherwise>
                                                    <option value=" ${sysUser.state}">未知</option>
                                                    <option value="0">启用</option>
                                                    <option value="1">删除</option>
                                                </c:otherwise>
                                            </c:choose>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-3">
                                        <%--<c:if test='${9 eq billInfo.stae}'--%>
                                        <button class="btn btn-primary" type="submit">修改</button>
                                        <button class="btn btn-primary" type="button" id="userEdit_backButton">返回
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <%@ include file="../commons/validate.jsp" %>
        <script src="${ctx}/mysource/js/bjparkmgr/user/userEdit.js"></script>
</body>
</html>
