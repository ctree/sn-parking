<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="../commons/meta.jsp" %>
    <link rel="stylesheet" type="text/css" href="${ctx}/hplus/css/plugins/treeview/jquery.treeview.css"/>
    <style type="text/css">
        <!--
        .atten {
            font-size: 12px;
            font-weight: normal;
            color: #F00;
        }

        -->
    </style>
    <title>添加用户</title>
</head>
<body class="fixed-sidebar full-height-layout">
<div id="wrapper">
    <%@ include file="../commons/left.jsp" %>
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <%@ include file="../commons/top.jsp" %>
        <!-- 业务代码 -->
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>添加用户</h2>
                <ol class="breadcrumb">
                    <li><a href="${ctx}/">用户管理</a></li>
                    <li><strong>添加用户</strong></li>
                </ol>
            </div>
            <div class="col-lg-2"></div>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">

                        <div class="ibox-title">
                            <h5>添加用户</h5>
                            <div class="ibox-tools">
                                <a href="${ctx}/view/userManager" class="btn btn-primary btn-xs">返回列表</a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <form class="form-horizontal m-t" id="useradd_form"
                                  method="post" action="#">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">用户账号：</label>
                                    <div class="col-sm-4">
                                        <input id="login" name="login" class="form-control"
                                               placeholder="请输入用户账号" type="text">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">登录密码：</label>
                                    <div class="col-sm-4">
                                        <input id="password" name="password" class="form-control"
                                               placeholder="请输入登录密码" type="text">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">用户名称：</label>
                                    <div class="col-sm-4">
                                        <input id="name" name="name" class="form-control"
                                               placeholder="请输入用户名称" type="text">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">邮箱：</label>
                                    <div class="col-sm-4">
                                        <input id="email" name="email" class="form-control"
                                               placeholder="请输入邮箱" type="email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">手机号码：</label>
                                    <div class="col-sm-4">
                                        <input id="phone" name="phone" class="form-control"
                                               placeholder="请输入手机号码" type="text">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">用户类型：</label>
                                    <div class="col-sm-4">
                                        <select id='type' name='type' class='chosen-select form-control'>
                                            <option value=''>请选择用户类型</option>
                                            <option value="0">系统用户</option>
                                            <option value="1">商户</option>
                                        </select>
                                    </div>
                                </div>
                                <div id="owner"  style="display: none">
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-3">
                                        <button class="btn btn-default" type="reset">重置</button>
                                        <button class="btn btn-primary" type="submit">添加</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <%@ include file="../commons/footer.jsp" %>
    </div>
</div>
<!-- jQuery Validation plugin javascript-->
<%@ include file="../commons/validate.jsp" %>
<script src="${ctx}/mysource/js/bjparkmgr/user/userAdd.js"></script>
</body>
</html>