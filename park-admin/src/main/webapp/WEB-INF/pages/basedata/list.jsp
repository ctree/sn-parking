<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="../commons/meta.jsp" %>
    <!--轮播图-->
    <title>数据字典管理</title>
    <style>
        #show_img{
            width: 850px;
            height:650px;
        }
    </style>
</head>
<body class="fixed-sidebar full-height-layout">
<div id="wrapper">
    <%@ include file="../commons/left.jsp" %>
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <%@ include file="../commons/top.jsp" %>
        <!-- 业务代码 -->
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>数据字典信息</h2>
                <ol class="breadcrumb">
                    <li><a href="${ctx}/mgr/basedata/list">数据字典管理</a></li>
                    <li><strong>数据字典信息</strong></li>
                </ol>
            </div>
            <div class="col-lg-2"></div>
        </div>

        <div class="gray-bg wrapper wrapper-content animated fadeInRight dashbard-1">
            <div class="row">

                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>数据字典列表</h5>
                            <div class="ibox-tools">
                                <a href="${ctx}/mgr/basedata/addUI" class="btn btn-primary btn-xs">添加数据字典</a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <!-- 查询start -->
                            <div class="form-horizontal" id="baseDataList_queryDiv">
                                <div class="form-group">
                                    <label class="col-lg-1 col-md-2 col-sm-4 control-label" style="padding-left: 0">类型</label>
                                    <div class="col-lg-2 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="type" id="type"/>
                                    </div>
                                    <label class="col-lg-1 col-md-2 col-sm-4 control-label" style="padding-left: 0">名称</label>
                                    <div class="col-lg-2 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="name" id="name"/>
                                    </div>

                                    <div class="col-lg-2 col-md-4 col-sm-8">
                                        <button type="button" id="baseDataList_serach" class="btn btn-primary">查询</button>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="col-lg-1 col-md-2 col-sm-4 control-label" style="padding-left: 0">内容</label>
                                    <div class="col-lg-2 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="text" id="text"/>
                                    </div>
                                    <label class="col-lg-1 col-md-2 col-sm-4 control-label" style="padding-left: 0">值</label>
                                    <div class="col-lg-2 col-md-4 col-sm-8">
                                        <input type="text" class="form-control" name="value" id="value"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- 查询end -->
                    </div>
                </div>
            </div>

            <div class="col-lg-12" style="overflow: auto;">
                <table id="baseDataList_dataList" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>序号</th>
                        <%--<th>ID</th>--%>
                        <th>类型</th>
                        <th>名称</th>
                        <th>内容</th>
                        <th>值</th>
                        <th>排序</th>
                        <th>创建时间</th>
                        <th>更新时间</th>
                        <th>操作</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
        <%@ include file="../commons/footer.jsp" %>
    </div>
</div>
</div>
<script src="${ctx}/mysource/js/bjparkmgr/common/dateUtil.js"></script>
<script src="${ctx}/mysource/js/bjparkmgr/basedata/list.js"></script>
</body>
</html>