<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="../commons/meta.jsp" %>
    <link rel="stylesheet" type="text/css" href="${ctx}/hplus/css/plugins/treeview/jquery.treeview.css"/>
    <style type="text/css">
        <!--
        .atten {
            font-size: 12px;
            font-weight: normal;
            color: #F00;
        }

        -->
    </style>
    <title>添加数据字典</title>
</head>
<body class="fixed-sidebar full-height-layout">
<div id="wrapper">
    <%@ include file="../commons/left.jsp" %>
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <%@ include file="../commons/top.jsp" %>
        <!-- 业务代码 -->
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>添加数据字典</h2>
                <ol class="breadcrumb">
                    <li><a href="${ctx}/mgr/basedata/list">数据字典管理</a></li>
                    <li><strong>添加数据字典</strong></li>
                </ol>
            </div>
            <div class="col-lg-2"></div>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">

                        <div class="ibox-title">
                            <h5>添加数据字典</h5>
                            <div class="ibox-tools">
                                <a href="${ctx}/mgr/basedata/list" class="btn btn-primary btn-xs">返回列表</a>
                            </div>
                        </div>
                        <div class="ibox-content">

                            <form class="form-horizontal m-t" id="basedataAdd_form" method="post" action="#">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">类型：</label>
                                    <div class="col-sm-4">
                                        <input id="type" name="type" class="form-control"
                                               placeholder="请输入类型" type="text">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">名称：</label>
                                    <div class="col-sm-4">
                                        <input id="name" name="name" class="form-control"
                                               placeholder="请输入名称" type="text">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">内容：</label>
                                    <div class="col-sm-4">
                                        <input id="text" name="text" class="form-control"
                                               placeholder="请输入内容" type="text">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">值：</label>
                                    <div class="col-sm-4">
                                        <input id="value" name="value" class="form-control"
                                               placeholder="请输入值" type="text">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">排序：</label>
                                    <div class="col-sm-4">
                                        <input id="orderCode" name="orderCode" class="form-control"
                                               placeholder="请输入排序" value="5" type="text">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-3">
                                        <button class="btn btn-default" type="reset">重置</button>
                                        <button class="btn btn-primary" type="submit">添加</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <%@ include file="../commons/footer.jsp" %>
    </div>
</div>
<!-- jQuery Validation plugin javascript-->
<%@ include file="../commons/validate.jsp" %>
<script src="${ctx}/mysource/js/bjparkmgr/basedata/add.js"></script>
</body>
</html>