<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  ModelF: niecheng
  Date: 2015/11/30
  Time: 19:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="../commons/meta.jsp" %>
    <link href="${ctx}/tcmgr/css/charisma-app.css" rel="stylesheet">
    <script src="${ctx}/tcmgr/js/common.js"></script>
    <title>编辑数据字典信息</title>

    <style>
        select {
            margin-top: 20px;
            width: 275px;
            height: 30px;
            padding: 3px;
            text-align: center;
            font-size: 14px;
            font-family: "微软雅黑";
            float: left;
            margin-right: 10px;
            border: 1px solid #CCC;
        }
    </style>
</head>
<body class="fixed-sidebar full-height-layout">

<div id="wrapper">
    <%@ include file="../commons/left.jsp" %>
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <%@ include file="../commons/top.jsp" %>
        <!-- 业务代码 -->
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>编辑数据字典信息</h2>
                <ol class="breadcrumb">
                    <li><a href="${ctx}/">数据字典管理</a></li>
                    <li><strong>编辑数据字典信息</strong></li>
                </ol>
            </div>
            <div class="col-lg-2"></div>
        </div>

        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">

                        <div class="ibox-title">
                            <h5>数据字典</h5>

                            <div class="ibox-tools">
                            </div>
                        </div>

                        <input type="hidden" name="baseId" id="baseId" value="${object.baseId}">

                        <div class="ibox-content">
                            <form class="form-horizontal m-t" id="baseDataEdit_rejectForm" method="post" action="#">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">类型：</label>

                                    <div class="col-sm-4">
                                        <input id="type" name="type" class="form-control" value="${object.type}"
                                               type="text"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">名称：</label>

                                    <div class="col-sm-4">
                                        <input id="name" name="name" class="form-control" value="${object.name}"
                                               type="text"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">内容：</label>

                                    <div class="col-sm-4">
                                        <input id="text" name="text" class="form-control" value="${object.text}"
                                               type="text"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">值：</label>

                                    <div class="col-sm-4">
                                        <input id="value" name="value" class="form-control" value="${object.value}"
                                               type="text"/>
                                    </div>
                                </div>



                                <div class="form-group">
                                    <label class="col-sm-3 control-label">排序：</label>

                                    <div class="col-sm-4">
                                        <input id="orderCode" name="orderCode" class="form-control" value="${object.orderCode}"
                                               type="text" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-3">
                                        <%--<c:if test='${9 eq billInfo.stae}'--%>
                                        <button class="btn btn-primary" type="submit">修改</button>
                                        <button class="btn btn-primary" type="button" id="baseDataEdit_backButton">返回
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <%@ include file="../commons/validate.jsp" %>
        <script src="${ctx}/mysource/js/bjparkmgr/basedata/edit.js"></script>
</body>
</html>
