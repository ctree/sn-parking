<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="commons/meta.jsp"%>
<title>${commonTitle}-苏宁停车场管理平台</title>
</head>
<body class="fixed-sidebar full-height-layout">
	<div id="wrapper">
		<%@ include file="commons/left.jsp"%>
		<div id="page-wrapper" class="gray-bg dashbard-1">
			<%@ include file="commons/top.jsp"%>
			<!-- 业务代码 -->
			<div class="row">
				<div class="col-lg-10">
					
				</div>
				<div class="col-lg-2"></div>
			</div>

			<%@ include file="commons/footer.jsp"%>
		</div>
	</div>
</body>
</html>