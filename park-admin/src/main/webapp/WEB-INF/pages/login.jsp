<%@ page language="java" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <title>管理系统</title>
    <link rel="icon" href="${ctx}/hplus/img/suning-favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="${ctx}/hplus/img/suning-favicon.ico" type="image/x-icon"/>
    <link href="${ctx}/hplus/css/bootstrap.min.css?v=3.4.0" rel="stylesheet">
    <link href="${ctx}/hplus/font-awesome/css/font-awesome.css?v=4.3.0" rel="stylesheet">
    <link href="${ctx}/hplus/css/animate.css" rel="stylesheet">
    <link href="${ctx}/hplus/css/style.css?v=2.2.0" rel="stylesheet">
    <script type="text/javascript">
        var ctx = '${ctx}';
        var ajaxTimeout = 10000;
    </script>
</head>
<body class="gray-bg">
<div class="container">
    <div class="row" style="padding-top:10%;">
        <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4 col-xs-8 col-xs-offset-2 ">
            <div class="text-center animated fadeInDown">
                <div class="row">
                    <div class="col-md-12">
                        <img alt="image" class="m-t-xs" style="width:70px" src="${ctx}/hplus/img/suning-logo.png">
                    </div>
                    <div class="col-md-12">
                        <form id="login_form" class="m-t" role="form" method="post" action="">
                            <div class="form-group">
                                <input type="login" name="userName" id="userName" class="form-control" placeholder="用户名"
                                       required="">
                            </div>
                            <div class="form-group">
                                <input type="password" name="passWord" id="passWord" class="form-control"
                                       placeholder="密码"
                                       required="">
                            </div>
                            <button type="submit" class="btn btn-primary block full-width m-b">登录</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="${ctx}/hplus/js/jquery-2.1.1.min.js"></script>
<script src="${ctx}/hplus/js/bootstrap.min.js?v=3.4.0"></script>
<script src="${ctx}/mysource/js/plugins/validate/jquery.validate.min.js"></script>
<script src="${ctx}/mysource/js/bjparkmgr/login/login.js"></script>
</body>
</html>
