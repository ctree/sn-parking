<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="../commons/meta.jsp" %>
    <title>停车券使用数据报表</title>
</head>
<body class="fixed-sidebar full-height-layout">
<div id="wrapper">
    <%@ include file="../commons/left.jsp" %>
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <%@ include file="../commons/top.jsp" %>
        <!-- 业务代码 -->
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>停车券使用数据报表</h2>
                <ol class="breadcrumb">
                    <li><a href="${ctx}/view/main">报表</a></li>
                    <li><strong>停车券使用数据报表</strong></li>
                </ol>
            </div>
            <div class="col-lg-2"></div>
        </div>

        <div class="gray-bg wrapper wrapper-content animated fadeInRight dashbard-1">
            <div class="row">

                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                        </div>
                        <div class="ibox-content">
                            <!-- 查询start -->
                            <div class="form-horizontal" id="userList_queryDiv">
                                <form class="form-horizontal m-t" id="payOrder_list" method="post" action="#">
                                    <div class="form-group">
                                        <label class="col-lg-1 col-md-2 col-sm-4 control-label">券名称</label>
                                        <div class="col-lg-2 col-md-4 col-sm-8">
                                            <input id="couponName" type="text" class="form-control">
                                        </div>
                                        <label class="col-lg-2 col-md-2 col-sm-4 control-label">券有效开始时间</label>
                                        <div class="col-lg-2 col-md-4 col-sm-8">
                                            <input type="date" class="form-control" name="startTime" id="startTime"/>
                                        </div>
                                        <label class="col-lg-2 col-md-2 col-sm-4 control-label">券有效结束时间</label>
                                        <div class="col-lg-2 col-md-4 col-sm-8">
                                            <input type="date" class="form-control" name="endTime" id="endTime"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-1 col-md-2 col-sm-4 control-label">停车场</label>
                                        <div class="col-lg-2 col-md-4 col-sm-8" id="selectDiv">
                                            <select data-placeholder="选择停车场..." id="parkId" name="parkId" class=" form-control" tabindex="2">
                                                <option value="">请选择</option>
                                            </select>
                                        </div>
                                        <label class="col-lg-1 col-md-2 col-sm-4 control-label"></label>
                                        <div class="col-lg-2 col-md-4 col-sm-8">
                                            <button type="button" id="ticketUsed_serach" class="btn btn-primary">查询
                                            </button>
                                        </div>
                                        <label class="col-lg-1 col-md-2 col-sm-4 control-label"></label>
                                        <div class="col-lg-2 col-md-4 col-sm-8">
                                            <button type="button" id="ticketUsed_export" class="btn btn-primary">导出
                                                Excel
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- 查询end -->
                    </div>
                </div>
            </div>

            <div class="col-lg-12" style="overflow: auto;">
                <table id="ticketUsed_table" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>序&nbsp;号</th>
                        <th>广&nbsp;场</th>
                        <th>券&nbsp;名&nbsp;称</th>
                        <th>券&nbsp;有&nbsp;效&nbsp;开&nbsp;始&nbsp;时&nbsp;间</th>
                        <th>券&nbsp;有&nbsp;效&nbsp;结&nbsp;束&nbsp;时&nbsp;间</th>
                        <th>券&nbsp;点&nbsp;数</th>
                        <th>已&nbsp;领&nbsp;券</th>
                        <th>已&nbsp;使&nbsp;用</th>
                        <th>未&nbsp;使&nbsp;用</th>
                        <th>操&nbsp;作</th>
                    </tr>
                    </thead>
                </table>
            </div>


        </div>
        <%@ include file="../commons/footer.jsp" %>
    </div>
</div>
</div>
<script src="${ctx}/mysource/js/bjparkmgr/common/dateUtil.js"></script>
<script src="${ctx}/mysource/js/bjparkmgr/reportForm/ticketUsed.js"></script>
</body>
</html>