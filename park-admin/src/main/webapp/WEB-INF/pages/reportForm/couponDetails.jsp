<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
<head>

    <%@ include file="../commons/meta.jsp" %>
    <title>停车券结算明细报表</title>
</head>
<body class="fixed-sidebar full-height-layout">
<div id="wrapper">
    <%@ include file="../commons/left.jsp" %>

    <%--<script type="text/javascript">--%>
        <%--$(document).ready(function () {--%>
            <%--var arrPos = new Array(0, 1, 12, 13, 20, 21, 28, 29);  //合并单元格的位置（首为0，先行后列的顺序）--%>
            <%--var arrNum = new Array(3, 3, 2, 2, 2, 2, 2, 2);     //不同位置合并单元格对应的个数--%>
            <%--var col = 4;                            //列的个数--%>

            <%--var mergerCol = new Array--%>
            <%--{--%>
                <%--0, 1--%>
            <%--}--%>
            <%--;--%>
            <%--$("#table1 tbody tr").each(function (i) {--%>

            <%--}--%>
            <%--$("#table1 tbody tr td").each(function (i) {--%>
                <%--var n = arrPos.length;--%>
                <%--for (var j = 0; j < n; j++) {--%>
                    <%--if (i == arrPos[j]) {--%>
                        <%--alert("j:" + j);--%>
                        <%--$(this).attr({rowspan: arrNum[j]}); //合并单元格--%>
                    <%--}--%>

                    <%--var index = arrPos[j] + col;--%>
                    <%--var maxIndex = index + col * (arrNum[j] - 2);--%>
                    <%--for (var k = index; k <= maxIndex; k += col) {--%>
                        <%--if (i == k) {--%>
                            <%--alert("i:" + i);--%>
                            <%--$(this).remove(); //删除对应的单元格--%>
                        <%--}--%>
                    <%--}--%>
                <%--}--%>
                <%--$(this).remove(); //删除对应的单元格--%>
            <%--});--%>
        <%--});--%>
    <%--</script>--%>

    <div id="page-wrapper" class="gray-bg dashbard-1">
        <%@ include file="../commons/top.jsp" %>
        <!-- 业务代码 -->
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>停车券结算明细报表</h2>
                <ol class="breadcrumb">
                    <li><a href="${ctx}/view/main">报表</a></li>
                    <li><strong>停车券结算明细报表</strong></li>
                </ol>
            </div>
            <div class="col-lg-2"></div>
        </div>

        <div class="gray-bg wrapper wrapper-content animated fadeInRight dashbard-1">
            <div class="row">

                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                        </div>
                        <div class="ibox-content">
                            <!-- 查询start -->
                            <div class="form-horizontal" id="userList_queryDiv">
                                <form class="form-horizontal m-t" id="payOrder_list" method="post" action="#">
                                    <div class="form-group">
                                        <input type="hidden" id="parkName">
                                        <label class="col-lg-1 col-md-2 col-sm-4 control-label">停车场</label>
                                        <div class="col-lg-2 col-md-4 col-sm-8" id="selectDiv">
                                            <select data-placeholder="选择停车场..." id="parkId" name="parkId"
                                                    class=" form-control" tabindex="2">
                                                <option>请选择停车场</option>
                                            </select>
                                        </div>
                                        <label class="col-lg-2 col-md-2 col-sm-4 control-label">开始时间</label>
                                        <div class="col-lg-2 col-md-4 col-sm-8">
                                            <input type="date" class="form-control" name="startTime" id="startTime"/>
                                        </div>
                                        <label class="col-lg-2 col-md-2 col-sm-4 control-label">结束时间</label>
                                        <div class="col-lg-2 col-md-4 col-sm-8">
                                            <input type="date" class="form-control" name="endTime" id="endTime"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-1 col-md-2 col-sm-4 control-label">收费渠道</label>
                                        <div class="col-lg-2 col-md-4 col-sm-8">
                                            <select id='recvChannel' name='recvChannel'
                                                    class='chosen-select form-control'>
                                                <option value=''>全部</option>
                                                <option value='0'>微信</option>
                                                <option value='1'>APP</option>
                                            </select>
                                        </div>
                                        <label class="col-lg-2 col-md-2 col-sm-4 control-label">收费方式</label>
                                        <div class="col-lg-2 col-md-4 col-sm-8">
                                            <select id='payType' name='payType' class='chosen-select form-control'>
                                                <option value=''>全部</option>
                                                <option value='0'>易付宝</option>
                                                <option value='1'>停车券</option>
                                                <option value='2'>积分</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-lg-1 col-md-2 col-sm-4 control-label"></label>
                                        <div class="col-lg-2 col-md-4 col-sm-8">
                                            <button class="btn btn-primary" type="reset">重置</button>
                                        </div>
                                        <label class="col-lg-2 col-md-2 col-sm-4 control-label"></label>
                                        <div class="col-lg-2 col-md-4 col-sm-8">
                                            <button type="button" id="settlement_serach" class="btn btn-primary">查询
                                            </button>
                                        </div>
                                        <label class="col-lg-2 col-md-2 col-sm-4 control-label"></label>
                                        <div class="col-lg-2 col-md-4 col-sm-8">
                                            <button type="button" id="settlement_export" class="btn btn-primary">导出
                                                Excel
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- 查询end -->
                    </div>
                </div>
            </div>

            <div class="col-lg-12" style="overflow: auto;">
                <table id="settlement_table" class="table table-striped table-bordered table-hover"
                       style="white-space: nowrap; text-overflow: ellipsis;">
                    <thead>
                    <tr>
                        <th>序号</th>
                        <th>广场</th>
                        <th>订单号</th>
                        <th>会员卡号</th>
                        <th>车牌号</th>
                        <th>缴费渠道</th>
                        <th>缴费日期</th>
                        <th>缴费时间</th>
                        <th>付款方式</th>
                        <th>付款金额</th>
                        <th>券名称</th>
                        <th>券号</th>
                        <th>券金额</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>总计</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>12</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <%@ include file="../commons/footer.jsp" %>
    </div>
</div>
</div>
<script src="${ctx}/mysource/js/bjparkmgr/common/dateUtil.js"></script>
<script src="${ctx}/mysource/js/bjparkmgr/common/common.js"></script>
<script src="${ctx}/mysource/js/bjparkmgr/reportForm/couponDetails.js"></script>
</body>
</html>