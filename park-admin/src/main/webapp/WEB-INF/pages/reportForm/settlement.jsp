<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="../commons/meta.jsp" %>
    <title>停车券结算报表</title>
</head>
<body class="fixed-sidebar full-height-layout">
<div id="wrapper">
    <%@ include file="../commons/left.jsp" %>
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <%@ include file="../commons/top.jsp" %>
        <!-- 业务代码 -->
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>停车券结算报表</h2>
                <ol class="breadcrumb">
                    <li><a href="${ctx}/view/main">报表</a></li>
                    <li><strong>停车券结算报表</strong></li>
                </ol>
            </div>
            <div class="col-lg-2"></div>
        </div>

        <div class="gray-bg wrapper wrapper-content animated fadeInRight dashbard-1">
            <div class="row">

                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                        </div>
                        <div class="ibox-content">
                            <!-- 查询start -->
                            <div class="form-horizontal" id="userList_queryDiv">
                                <form class="form-horizontal m-t" id="payOrder_list" method="post" action="#">
                                    <div class="form-group">
                                        <input type="hidden" id="parkName">
                                        <label class="col-lg-1 col-md-2 col-sm-4 control-label">停车场</label>
                                        <div class="col-lg-2 col-md-4 col-sm-8" id="selectDiv">
                                            <select data-placeholder="选择停车场..." id="parkId" name="parkId" class=" form-control" tabindex="2">
                                                <option>请选择停车场</option>
                                            </select>
                                        </div>
                                        <label class="col-lg-2 col-md-2 col-sm-4 control-label">开始时间</label>
                                        <div class="col-lg-2 col-md-4 col-sm-8">
                                            <input type="date" class="form-control" name="startTime" id="startTime"/>
                                        </div>
                                        <label class="col-lg-2 col-md-2 col-sm-4 control-label">结束时间</label>
                                        <div class="col-lg-2 col-md-4 col-sm-8">
                                            <input type="date" class="form-control" name="endTime" id="endTime"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-lg-4 col-md-8 col-sm-8">
                                            <button class="btn btn-primary" type="reset">重置</button>
                                        </div>
                                        <label class="col-lg-1 col-md-2 col-sm-4 control-label"></label>
                                        <div class="col-lg-2 col-md-4 col-sm-8">
                                            <button type="button" id="settlement_serach" class="btn btn-primary">查询
                                            </button>
                                        </div>
                                        <label class="col-lg-1 col-md-2 col-sm-4 control-label"></label>
                                        <div class="col-lg-2 col-md-4 col-sm-8">
                                            <button type="button" id="settlement_export" class="btn btn-primary">导出
                                                Excel
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- 查询end -->
                    </div>
                </div>
            </div>

            <div class="col-lg-12" style="overflow: auto;">
                <table id="settlement_table" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>序&nbsp;号</th>
                        <th>广&nbsp;场</th>
                        <th>日&nbsp;期</th>
                        <th>付&nbsp;款&nbsp;方&nbsp;式</th>
                        <th>付&nbsp;款&nbsp;金&nbsp;额</th>
                    </tr>
                    </thead>
                </table>
            </div>


        </div>
        <%@ include file="../commons/footer.jsp" %>
    </div>
</div>
</div>
<script src="${ctx}/mysource/js/bjparkmgr/common/dateUtil.js"></script>
<script src="${ctx}/mysource/js/bjparkmgr/common/common.js"></script>
<script src="${ctx}/mysource/js/bjparkmgr/reportForm/settlement.js"></script>
</body>
</html>