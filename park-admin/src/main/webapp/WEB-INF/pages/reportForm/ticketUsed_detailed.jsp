<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="../commons/meta.jsp" %>
    <style type="text/css">
        .payOrder_center {
            text-align: center;
        }
        .payOrder_center_bt {
            width: 100px;
        }
        .modal-content-define{
            width: 100%;
        }
    </style>
    <title>查看明细</title>
</head>
<body class="fixed-sidebar full-height-layout">
<div id="wrapper">
    <%@ include file="../commons/left.jsp" %>
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <%@ include file="../commons/top.jsp" %>
        <!-- 业务代码 -->
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>查看明细</h2>
                <ol class="breadcrumb">
                    <li><a href="${ctx}/view/main">报表</a></li>
                    <li><a href="${ctx}/view/mgr/ticketUsed">停车券使用数据报表</a></li>
                    <li><strong>查看明细</strong></li>
                </ol>
            </div>
            <div class="col-lg-2"></div>
        </div>

        <div class="gray-bg wrapper wrapper-content animated fadeInRight dashbard-1">
            <div class="row">

                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                        </div>
                        <div class="ibox-content">
                            <!-- 查询start -->
                            <div class="form-horizontal" id="userList_queryDiv">
                                <form class="form-horizontal m-t" id="payOrder_list" method="post" action="#">
                                    <input type="hidden" id="couponId" value="${couponId}">
                                    <div class="form-group">
                                        <label class="col-lg-1 col-md-2 col-sm-4 control-label">券状态</label>
                                        <div class="col-lg-2 col-md-4 col-sm-8">
                                            <select id='state' name='state' class='chosen-select form-control'>
                                                <option value=''>全部</option>
                                                <option value="0">未使用</option>
                                                <option value="1">已使用</option>
                                            </select>
                                        </div>
                                        <label class="col-lg-1 col-md-2 col-sm-4 control-label"></label>
                                        <div class="col-lg-4 col-md-4 col-sm-8">
                                            <button type="button" id="ticketUsed_detailed_search" class="btn btn-primary">查询
                                            </button>
                                        </div>
                                        <div class="col-lg-2 col-md-4 col-sm-8">
                                            <button type="button" id="ticketUsed_detailed_export" class="btn btn-primary">导出
                                                Excel
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- 查询end -->
                    </div>
                </div>
            </div>

            <div class="col-lg-12" style="overflow: auto;">
                <table id="ticketUsed_detailed_table" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>序&nbsp;号</th>
                        <th>会&nbsp;员</th>
                        <th>券&nbsp;名&nbsp;称</th>
                        <th>活&nbsp;动&nbsp;名&nbsp;称</th>
                        <th>领&nbsp;券&nbsp;时&nbsp;间</th>
                        <th>用&nbsp;券&nbsp;时&nbsp;间</th>
                        <th>券&nbsp;状&nbsp;态</th>
                    </tr>
                    </thead>
                </table>
            </div>


        </div>
        <%@ include file="../commons/footer.jsp" %>
    </div>
</div>
</div>
<script src="${ctx}/mysource/js/bjparkmgr/common/dateUtil.js"></script>
<script src="${ctx}/mysource/js/bjparkmgr/reportForm/ticketUsed_detailed.js"></script>
</body>
</html>