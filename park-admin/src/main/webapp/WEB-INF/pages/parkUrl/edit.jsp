<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <%@ include file="../commons/meta.jsp" %>
    <title>编辑停车场本地系统访问地址</title>
    <link rel="stylesheet" href="${ctx}/mysource/js/plugins/combo.select/combo.select.css">
</head>
<body class="fixed-sidebar full-height-layout">
<div id="wrapper">
    <%@ include file="../commons/left.jsp" %>
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <%@ include file="../commons/top.jsp" %>
        <!-- 业务代码 -->
        <div class="row wrapper border-bottom white-bg page-heading">
            <div class="col-lg-10">
                <h2>编辑停车场本地系统访问地址</h2>
                <ol class="breadcrumb">
                    <li><a href="/mgr/parkUrl/list">停车场本地系统访问地址管理</a></li>
                    <li><strong>编辑停车场本地系统访问地址</strong></li>
                </ol>
            </div>
        </div>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>编辑停车场本地系统访问地址</h5>
                        </div>
                        <div class="ibox-content">

                            <form class="form-horizontal m-t" id="parkUrlEdit_rejectForm" method="post" action="#">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">停车场：</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" type="hidden" name="id" id="id" value="${object.id}" />
                                        <select id="parkId" name="parkId" value="${object.parkId}" class='form-control'></select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">请求类型：</label>
                                    <div class="col-sm-4">
                                        <select id="requestType" name="requestType" value="${object.requestType}" class='form-control'></select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">网络类型：</label>
                                    <div class="col-sm-4">
                                        <select id="urlType" name="urlType" value="${object.urlType}"  class='form-control'></select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">URL：</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" name="url" value="${object.url}" id="url" placeholder="请输入URL" type="text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">备注：</label>
                                    <div class="col-sm-4">
                                        <textarea class="form-control" name="remark" id="remark">${object.remark}</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-3">
                                        <button class="btn btn-primary" type="submit">确定</button>
                                        <button class="btn btn-primary" type="button" id="backButton">返回</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%@ include file="../commons/footer.jsp" %>
    </div>
</div>
<script src="${ctx}/mysource/js/bjparkmgr/common/common.js"></script>
<script src="${ctx}/mysource/js/bjparkmgr/parkUrl/edit.js"></script>
</body>
</html>