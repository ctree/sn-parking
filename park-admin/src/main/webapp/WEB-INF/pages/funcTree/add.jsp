<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
   <%@ include file="../commons/meta.jsp"%>
   <link rel="stylesheet" type="text/css" href="${ctx}/hplus/css/plugins/treeview/jquery.treeview.css" />
     <style type="text/css">
<!--
.atten {font-size:12px;font-weight:normal;color:#F00;}
-->
</style>

<title>${commonTitle} - 添加资源</title>
</head>
<body class="fixed-sidebar full-height-layout">
    <div id="wrapper">
        <%@ include file="../commons/left.jsp"%>
        <div id="page-wrapper" class="gray-bg dashbard-1">
            <%@ include file="../commons/top.jsp"%>
            <!-- 业务代码 -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>添加资源</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="${ctx}/">主页</a>
                        </li>
                        <li>
                            <a>权限管理</a>
                        </li>
                        <li>
                            <a href="${ctx}/mgr/funcTree/list">资源管理</a>
                        </li>
                        <li>
                            <strong>添加资源</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            
                            <div class="ibox-title">
                                <h5>添加资源</h5>
                                <div class="ibox-tools">
                                <a href="${ctx}/view/mgr/funcTreeList" class="btn btn-primary btn-xs">返回列表</a>
                                </div>
                            </div>
                            <div class="ibox-content">
                            
                                <form class="form-horizontal m-t" id="addForm" method="post" action="#">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">资源名称：</label>
                                        <div class="col-sm-4">
                                            <input id="name" name="name" class="form-control" placeholder="请输入资源名称" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">资源描述：</label>
                                        <div class="col-sm-4">
                                            <input id="help" name="help" class="form-control" placeholder="请输入资源描述" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">资源链接：</label>
                                        <div class="col-sm-4">
                                            <input id="url" name="url" class="form-control" placeholder="请输入资源链接" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">排序编码：</label>
                                        <div class="col-sm-4">
                                            <p>数值越大越靠前展示</p>
                                            <input id="orderCode" name="orderCode" class="form-control" value="5" placeholder="请输入排序编码" type="number">
                                        </div>
                                    </div>
                                    
                                     <div class="form-group">
                                        <label class="col-sm-3 control-label">资源类型：</label>
                                        <div class="col-sm-4">
                                            <select name="type" class="form-control" id="sourceType">
                                            	<option value="0">菜单资源</option>
                                                <option value="1">区域资源</option>
                                            	<option value="2">按钮资源</option>
                                            </select>
                                        </div>
                                    </div> 
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">父节点：</label>
                                        <!-- <div class="col-sm-4">
                                            <div id="tree"></div>
                                        </div> -->
                                        <div class="col-sm-4" id="treeDiv">
                                            <div id="tree" class="ztree" ></div>
                                        </div>
                                    </div>
                                     
                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-3">
                                            <button class="btn btn-default" type="reset">重置</button>
                                            <button class="btn btn-primary" type="submit">添加</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <%@ include file="../commons/footer.jsp"%>
        </div>
    </div>
    <!-- jQuery Validation plugin javascript-->
    <%@ include file="../commons/validate.jsp"%>
    <link rel="stylesheet" href="${ctx}/hplus/js/plugins/ztree/css/zTreeStyle/zTreeStyle.css" type="text/css">
    <script src="${ctx}/hplus/js/plugins/ztree/jquery.ztree.core-3.5.min.js"></script>
	<script src="${ctx}/hplus/js/plugins/ztree/jquery.ztree.excheck-3.5.min.js"></script>
    <script src='${ctx}/hplus/js/plugins/treeview/jquery.treeview.js'></script>
    <script src="${ctx}/mysource/js/bjparkmgr/funcTree/add.validate.js"></script>
    <script src="${ctx}/mysource/js/bjparkmgr/funcTree/add.js"></script>
</body>
</html>