<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
   <%@ include file="../commons/meta.jsp"%>
   <link rel="stylesheet" type="text/css" href="${ctx}/hplus/css/plugins/treeview/jquery.treeview.css" />
     <style type="text/css">
<!--
.atten {font-size:12px;font-weight:normal;color:#F00;}
-->
</style>
<title>${commonTitle} - 修改资源</title>
</head>
<body class="fixed-sidebar full-height-layout">
    <div id="wrapper">
        <%@ include file="../commons/left.jsp"%>
        <div id="page-wrapper" class="gray-bg dashbard-1">
            <%@ include file="../commons/top.jsp"%>
            <!-- 业务代码 -->
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>修改资源</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="${ctx}/">主页</a>
                        </li>
                        <li>
                            <a>权限管理</a>
                        </li>
                        <li>
                            <a href="${ctx}/mgr/funcTree/list">资源管理</a>
                        </li>
                        <li>
                            <strong>修改资源</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            
                            <div class="ibox-title">
                                <h5>修改资源</h5>
                                <div class="ibox-tools">
                                    <%-- <a href="${ctx}/funcTree/list" class="btn btn-primary btn-xs">返回列表</a> --%>
                                    <a href="javascript:void(0)" onclick="history.go(-1);" class="btn btn-primary btn-xs">返回列表</a>
                                </div>
                            </div>
                            <div class="ibox-content">
                                
                                <form class="form-horizontal m-t" id="editForm" method="post" action="#">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">资源名称：</label>
                                        <div class="col-sm-4">
                                            <input type="hidden" id="functionSourceId" name="functionSourceId" value="${object.functionSourceId}">
                                            <input id="name" name="name" class="form-control" value="${object.sourceName}" type="text">
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-sm-3 control-label">资源描述：</label>
                                        <div class="col-sm-4">
                                            <input id="help" name="help" class="form-control" value="${object.help}" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">资源链接：</label>
                                        <div class="col-sm-4">
                                            <input id="url" name="url" value="${object.sourceUrl}" class="form-control" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">排序编码：</label>
                                        <div class="col-sm-4">
                                            <input id="orderCode" name="orderCode" value="${object.orderCode}" class="form-control" type="number">
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-sm-3 control-label">资源类型：</label>
                                        <div class="col-sm-4">
                                            <select name="type" class="form-control" disabled>
                                            	<option <c:if test='${object.sourceType eq "0" }'>selected="selected" </c:if> value="0">菜单资源</option>
                                            	<option <c:if test='${object.sourceType eq "1" }'>selected="selected" </c:if> value="1">区域资源</option>
                                            	<option <c:if test='${object.sourceType eq "2" }'>selected="selected" </c:if> value="2">按钮资源</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">是否启用：</label>
                                        <div class="col-sm-4">
                                           <div class="radio col-md-2">
                                                <label>
                                                    <input type="radio" <c:if test="${object.enableStatus eq '0'}">checked="checked"</c:if> value="0" id="enableStatus" name="enableStatus">是</label>
                                            </div>
                                            <div class="radio col-md-2">
                                                <label>
                                                    <input type="radio" <c:if test="${object.enableStatus eq '1'}">checked="checked"</c:if> value="1" id="enableStatus" name="enableStatus">否</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">父节点：</label>
                                        <!-- <div class="col-sm-4">
                                            <div id="tree"></div>
                                        </div> -->
                                        <div class="col-sm-4" id="treeDiv">
                                            <div id="tree" class="ztree" ></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-3">
                                            <button class="btn btn-default" type="reset">重置</button>
                                            <button class="btn btn-primary" type="submit">确定</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <%@ include file="../commons/footer.jsp"%>
        </div>
    </div>
    <!-- jQuery Validation plugin javascript-->
    <%@ include file="../commons/validate.jsp"%>
    <link rel="stylesheet" href="${ctx}/hplus/js/plugins/ztree/css/zTreeStyle/zTreeStyle.css" type="text/css">
    <script src="${ctx}/hplus/js/plugins/ztree/jquery.ztree.core-3.5.min.js"></script>
	<script src="${ctx}/hplus/js/plugins/ztree/jquery.ztree.excheck-3.5.min.js"></script>
    <script src="${ctx}/mysource/js/bjparkmgr/funcTree/edit.validate.js"></script>
    <script src='${ctx}/hplus/js/plugins/treeview/jquery.treeview.js'></script>
    <script type="text/javascript">
    var ztree;
    $(function() {
    	
    	getAufunctree();
    	
    });
    
    
    function getAufunctree(){
    	initAufunctree();
    	treeInit();
    }

    function initAufunctree(){
    	
    	$.ajax({
    		url : ctx+'/mgr/funcTree/getAuFuncTrees',
    		type : 'post',
    		dataType : 'json',
    		timeout : ajaxTimeout,
    		error : function(json) {
    			toastr.error("加载失败");
    		},
    		success : function(json) {
    			if(json.success){

    				var result = json.success;
    				if (!result) {
    					$("#tree").html(json.msg);
    				} else {
    					// 生成树
    					var list = json.resourceList;

    					var setting = {
    							view: {
    								showIcon: false,
    								selectedMulti: false
    							},
    							data: {
    								key: {
    									children: "nodeList",
    										name: "sourceName",
    										title: "sourceName",
    										url:"zurl"
    								}
    							},
    							callback:{
    								onClick: zTreeOnClick
    							}
    					};

    					ztree = $.fn.zTree.init($("#tree"), setting, list);
    					
    					//移除需要编辑的节点
    					var treeObj = $.fn.zTree.getZTreeObj("tree"); 
        	            var node = treeObj.getNodeByParam("functionSourceId", "${object.functionSourceId}", null);
        	            treeObj.removeNode(node);
        	            
        	            //勾选需要编辑节点的父节点
        	            var parentNode = treeObj.getNodeByParam("functionSourceId", "${object.parentCode}", null);
        	            treeObj.selectNode(parentNode);
    				}
    				
    			}
    		}
    		})
    }

    function  zTreeOnClick(event, treeId, treeNode) {
    	var treeObj = $.fn.zTree.getZTreeObj("tree");
    	if(treeNode.sourceType != '0' && $("select[sourceName=sourceType]").val() != '0'){
    		alert("按钮及区域资源只能添加到末级菜单中");
    		treeObj.cancelSelectedNode(treeNode);
    	}
    }

    // 增加树样式
    function treeInit() {
    	setTimeout((function initTree() {
    		$("#red").treeview({
    			animated : "fast",
    			collapsed : true
    		});
    		$("#tree").css("display", "block");
    	}), 500);
    }
    </script>
</body>
</html>