<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html>
<head>
<%@ include file="../commons/meta.jsp"%>
<title>${commonTitle}-资源管理</title>
</head>
<body class="fixed-sidebar full-height-layout">
	<div id="wrapper">
		<%@ include file="../commons/left.jsp"%>
		<div id="page-wrapper" class="gray-bg dashbard-1">
			<%@ include file="../commons/top.jsp"%>
			<!-- 业务代码 -->
			<div class="row wrapper border-bottom white-bg page-heading">
				<div class="col-lg-10">
					<h2>资源管理</h2>
					<ol class="breadcrumb">
						<li><a href="${ctx}/">主页</a></li>
						<li><a>权限管理</a></li>
						<li><strong>资源管理</strong></li>
					</ol>
				</div>
			</div>

			<div class="wrapper wrapper-content animated fadeInRight">
				<div class="row">
					<div class="col-lg-12">
						<div class="ibox float-e-margins">
							<div class="ibox-title">
								<h5>资源列表</h5>
								<div class="ibox-tools">
									<a href="${ctx}/view/mgr/addFuncTreeUI" class="btn btn-primary btn-xs">添加资源</a>
								</div>
							</div>
							<div class="ibox-content">
								<!-- 查询start -->
								<div class="form-horizontal" id="queryDiv">
									<div class="form-group">
										<label class="col-lg-1 col-md-2 col-sm-4 control-label">资源名称</label>
										<div class="col-lg-3 col-md-4 col-sm-8">
											<input type="text" id="name" name="name" class="form-control" placeholder="请输入资源名称">
										</div>
										<div class="col-lg-3 col-md-3 col-sm-3">
											<button type="button" id="serach" class="btn btn-primary">查询</button>
										</div>
									</div>
								</div>
								<!-- 查询end -->
							</div>
						</div>
					</div>

					<div class="col-lg-12" style="overflow: auto;">
						<table id="dataList" class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>序&nbsp;号</th>
									<th>名&nbsp;称</th>
									<th>链&nbsp;接</th>
									<th>节&nbsp;点&nbsp;权&nbsp;重</th>
									<th>是&nbsp;否&nbsp;启&nbsp;用</th>
									<th>操&nbsp;作</th>
								</tr>
							</thead>
						</table>
					</div>
				</div>

			</div>
			<%@ include file="../commons/footer.jsp"%>
		</div>
	</div>
	<script src="${ctx}/mysource/js/bjparkmgr/funcTree/list.js"></script>
</body>
</html>