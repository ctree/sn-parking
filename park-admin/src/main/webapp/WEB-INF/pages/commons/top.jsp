<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<div class="row border-bottom">
	<nav class="navbar navbar-static-top" role="navigation"
		style="margin-bottom: 0">
		<div class="navbar-header">
			<a class="navbar-minimalize minimalize-styl-2 btn btn-primary "
				href="#"><i class="fa fa-bars"></i> </a>
		</div>
		<ul class="nav navbar-top-links navbar-right">
			<li><span class="m-r-sm text-muted welcome-message"><a
					href="${ctx}/view/main" title="返回首页"><i class="fa fa-home"></i></a>欢迎使用苏宁停车场管理平台</span>
			</li>
			<%--<li data-toggle="modal" data-target="#passwordModel"><a href="javascritp:void(0);" > <i class="fa fa-lock"></i>修改密码</a></li>--%>
			<li><a href="${ctx}/mgr/login/out/${sessionScope.user_in_session.userId}"> <i
					class="fa fa-sign-out"></i> 退出
			</a></li>
		</ul>

	</nav>
</div>

<div class="modal inmodal" id="passwordModel" tabindex="-1" role="dialog"  aria-hidden="true">
	<div class="modal-dialog">
		<form class="form-horizontal m-t" id="passwordForm" method="post" action="#">
			<div class="modal-content animated fadeIn">
				<div class="modal-header">
					<i class="fa fa-lock modal-icon"></i>
				</div>
				<div class="modal-body">

					<div class="form-group">
						<label class="col-sm-3 control-label">旧密码：</label>
						<div class="col-sm-7">
							<input id="oldPassword" name="oldPassword"  class="form-control" placeholder="请输入旧密码" type="password">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">新密码：</label>
						<div class="col-sm-7">
							<input id="password" name="password"  class="form-control" placeholder="请输入新密码" type="password">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">确认新密码：</label>
						<div class="col-sm-7">
							<input id="passwordConfirm" name="passwordConfirm" class="form-control"  placeholder="请确认新密码" type="password">
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">保存</button>
					<button type="button" class="btn btn-white" data-dismiss="modal">关闭</button>
				</div>
			</div>
		</form>
	</div>
</div>
<%@ include file="../commons/validate.jsp" %>
<script src="${ctx}/mysource/js/bjparkmgr/top/top.js"></script>