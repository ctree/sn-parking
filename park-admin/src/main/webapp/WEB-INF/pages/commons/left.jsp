<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<link rel="stylesheet" href="${ctx}/hplus/font-awesome/ifont/iconfont.css">
<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="side-menu">
            <li class="nav-header text-center">

                <div class="dropdown profile-element">
					<span> <img alt="image" width="64px" height="64px"
                                src="${ctx}/hplus/img/suning-logo.png"/>
					</span> <a data-toggle="dropdown" class="dropdown-toggle"
                               href="index.html#"> <span class="clear">
					<span class="block m-t-xs"> <strong class="font-bold">当前用户</strong>
						</span>
					<span class="text-muted text-xs block">${sessionScope.user_in_session.login}<b
                            class="caret"></b></span>
					</span>
                </a>
                    <ul class="dropdown-menu animated flipInX m-t-xs">
                        <!--
                            <li><a href="javascript:void(0)">修改密码</a></li>
                            <li class="divider"></li>
                             -->
                        <li><a href= "${ctx}/mgr/login/out/${sessionScope.user_in_session.userId}">安全退出</a></li>
                    </ul>

                </div>
                <div class="logo-element text-center">
						<span> <img alt="image" width="24px" height="24px"
                                    src="${ctx}/hplus/img/logo.png"/>
					</span>
                </div>
            </li>

            <%--<li class="active">--%>
                <%--<a href="${ctx}">--%>
                    <%--<i class="icon iconfont">&#xe602;</i>--%>
                    <%--<span class="nav-label">系统管理</span>--%>
                    <%--<span class="fa arrow"></span>--%>
                <%--</a>--%>
                <%--<ul class="nav nav-second-level">--%>
                    <%--<li>--%>
                        <%--<a class="active" href="${ctx}/view/userGrantList" data-index="0">预约记录</a>--%>
                    <%--</li>--%>
                <%--</ul>--%>
                <%--<ul class="nav nav-second-level">--%>
                    <%--<li>--%>
                        <%--<a class="active" href="${ctx}/view/userRecordList" data-index="0">进出场记录</a>--%>
                    <%--</li>--%>
                <%--</ul>--%>
            <%--</li>--%>

            <%--<li class="active">--%>
                <%--<a href="${ctx}">--%>
                    <%--<i class="icon iconfont">&#xe602;</i>--%>
                    <%--<span class="nav-label">系统维护</span>--%>
                    <%--<span class="fa arrow"></span>--%>
                <%--</a>--%>
                <%--<ul class="nav nav-second-level">--%>
                    <%--<li>--%>
                        <%--<a class="active" href="${ctx}/view/userGrantList" data-index="0">数据字典</a>--%>
                    <%--</li>--%>
                <%--</ul>--%>
            <%--</li>--%>
            <%--<li class="active">--%>
                <%--<a href="${ctx}">--%>
                    <%--<i class="icon iconfont">&#xe602;</i>--%>
                    <%--<span class="nav-label">数据管理</span>--%>
                    <%--<span class="fa arrow"></span>--%>
                <%--</a>--%>
                <%--<ul class="nav nav-second-level">--%>
                    <%--<li>--%>
                        <%--<a class="active" href="${ctx}/view/userGrantList" data-index="0">停车场管理</a>--%>
                    <%--</li>--%>
                <%--</ul>--%>
            <%--</li>--%>
            <%--<li class="active">--%>
                <%--<a href="${ctx}">--%>
                    <%--<i class="icon iconfont">&#xe602;</i>--%>
                    <%--<span class="nav-label">交易配置</span>--%>
                    <%--<span class="fa arrow"></span>--%>
                <%--</a>--%>
                <%--<ul class="nav nav-second-level">--%>
                    <%--<li>--%>
                        <%--<a class="active" href="${ctx}/view/userGrantList" data-index="0">交易宝商户号配置</a>--%>
                    <%--</li>--%>
                <%--</ul>--%>
            <%--</li>--%>
            <%--<li class="active">--%>
                <%--<a href="${ctx}">--%>
                    <%--<i class="icon iconfont">&#xe602;</i>--%>
                    <%--<span class="nav-label">停车券管理</span>--%>
                    <%--<span class="fa arrow"></span>--%>
                <%--</a>--%>
                <%--<ul class="nav nav-second-level">--%>
                    <%--<li>--%>
                        <%--<a class="active" href="${ctx}/view/userGrantList" data-index="0">新增停车券</a>--%>
                    <%--</li>--%>
                <%--</ul>--%>
            <%--</li>--%>
            <%--<li class="active">--%>
                <%--<a href="${ctx}">--%>
                    <%--<i class="icon iconfont">&#xe602;</i>--%>
                    <%--<span class="nav-label">活动管理</span>--%>
                    <%--<span class="fa arrow"></span>--%>
                <%--</a>--%>
                <%--<ul class="nav nav-second-level">--%>
                    <%--<li>--%>
                        <%--<a class="active" href="${ctx}/view/userGrantList" data-index="0">新增活动</a>--%>
                    <%--</li>--%>
                <%--</ul>--%>
            <%--</li>--%>
            <%--<li class="active">--%>
                <%--<a href="${ctx}">--%>
                    <%--<i class="icon iconfont">&#xe602;</i>--%>
                    <%--<span class="nav-label">订单管理</span>--%>
                    <%--<span class="fa arrow"></span>--%>
                <%--</a>--%>
                <%--<ul class="nav nav-second-level">--%>
                    <%--<li>--%>
                        <%--<a class="active" href="${ctx}/view/userGrantList" data-index="0">临停车辆收费订单明细</a>--%>
                    <%--</li>--%>
                <%--</ul>--%>
                <%--<ul class="nav nav-second-level">--%>
                    <%--<li>--%>
                        <%--<a class="active" href="${ctx}/view/userRecordList" data-index="0">临停车辆收费日报表</a>--%>
                    <%--</li>--%>
                <%--</ul>--%>
                <%--<ul class="nav nav-second-level">--%>
                    <%--<li>--%>
                        <%--<a class="active" href="${ctx}/view/userRecordList" data-index="0">停车券结算明细报表</a>--%>
                    <%--</li>--%>
                <%--</ul>--%>
            <%--</li>--%>

            <%--<li class="active">--%>
                <%--<a href="${ctx}">--%>
                    <%--<i class="icon iconfont">&#xe601;</i>--%>
                    <%--<span class="nav-label">运营监控</span>--%>
                    <%--<span class="fa arrow"></span>--%>
                <%--</a>--%>
                <%--<ul class="nav nav-second-level">--%>
                    <%--<li>--%>
                        <%--<a class="active" href="${ctx}/view/realParking" data-index="0">实时车位信息</a>--%>
                    <%--</li>--%>
                <%--</ul>--%>
            <%--</li>--%>
            <%--<li class="active">--%>
                <%--<a href="${ctx}">--%>
                    <%--<i class="icon iconfont">&#xe600;</i>--%>
                    <%--<span class="nav-label">用户管理</span>--%>
                    <%--<span class="fa arrow"></span>--%>
                <%--</a>--%>
                <%--<ul class="nav nav-second-level">--%>
                    <%--<li>--%>
                        <%--<a class="active" href="${ctx}/view/userManager" data-index="0">用户信息</a>--%>
                    <%--</li>--%>
                <%--</ul>--%>
            <%--</li>--%>
            <%--<li class="active">--%>
                <%--<a href="${ctx}">--%>
                    <%--<i class="icon iconfont">&#xe600;</i>--%>
                    <%--<span class="nav-label">权限管理</span>--%>
                    <%--<span class="fa arrow"></span>--%>
                <%--</a>--%>
                <%--<ul class="nav nav-second-level">--%>
                    <%--<li>--%>
                        <%--<a class="active" href="${ctx}/view/mgr/roleList" data-index="0">角色管理</a>--%>
                    <%--</li>--%>
                    <%--<li>--%>
                        <%--<a class="active" href="${ctx}/view/mgr/funcTreeList" data-index="0">资源管理</a>--%>
                    <%--</li>--%>
                    <%--<li>--%>
                        <%--<a class="active" href="${ctx}/view/mgr/userRoleList" data-index="0">用户授权管理</a>--%>
                    <%--</li>--%>
                <%--</ul>--%>
            <%--</li>--%>
        <%--</ul>--%>


        <c:forEach items="${aftList}" var="aft" varStatus="num">
            <c:choose>
                <c:when test="${aft.isLeaf == '0'}">
                    <li <c:if test="${aft.sourceUrl == requestUrl}">class="active"</c:if>>
                        <a href="${ctx}${aft.sourceUrl}">
                            <i class="icon iconfont">${aft.keyword}</i>
                            <span class="nav-label">${aft.sourceName}</span>
                        </a>
                    </li>
                </c:when>
                <c:otherwise>
                    <li>
                        <a href="#">
                            <i class="icon iconfont">${aft.keyword}</i>
                            <span class="nav-label">${aft.sourceName}</span>
                            <span class="fa arrow"></span>
                        </a>
                        <c:if test="${null != aft.nodeList}">
                        <ul class="nav nav-second-level">
                            <c:forEach items="${aft.nodeList}" var="aftNode" varStatus="num">
                                <li <c:if test="${aftNode.sourceUrl == requestUrl}">class="active"</c:if>>
                                    <c:choose>
                                    <c:when test="${aftNode.isPublic eq '0'}">
                                    <a href="${ctx}/common/commons?url=${aftNode.url}">
                                        </c:when>
                                        <c:otherwise>
                                        <a href="${ctx}${aftNode.sourceUrl}">
                                            </c:otherwise>
                                            </c:choose>
                                                ${aftNode.sourceName}
                                        </a>
                                </li>
                            </c:forEach>
                        </ul>
                        </c:if>
                    </li>
                </c:otherwise>
            </c:choose>
        </c:forEach>
        </div>

</nav>
<script type="text/javascript">
    $(function () {
        $(".sidebar-collapse").slimScroll({height: "100%", railOpacity: .9, alwaysVisible: !1});
        //判断浏览器是否为移动端的浏览器
        if (navigator.userAgent.match(/(iPhone|iPod|Android|ios)/i)) {
            $("body").css("height", "5000px");
            $("#page-wrapper").css("height", "5000px");
        } else {
            $("#page-wrapper").css("overflow-y", "false");
            $("#page-wrapper").slimScroll({height: "100%", railOpacity: .9, alwaysVisible: !1});
        }
    })
</script>
<script src="${ctx}/mysource/js/bjparkmgr/common/left.js"></script>