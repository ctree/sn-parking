package com.suning.esb;

import com.suning.base.TestEnvironment;
import com.suning.park.admin.esb.TA0301020501.MemberCardAccountMgmtService;
import com.suning.park.admin.esb.TA0301020501.dto.TA0301020501BodyReq;
import com.suning.park.admin.esb.TA0301020501.dto.TA0301020501Req;
import com.suning.park.admin.esb.TA0301020501.dto.TA0301020501Res;
import com.suning.park.admin.esb.TA04010350.MemberAccountMgmtService;
import com.suning.park.admin.esb.TA04010350.dto.TA04010350BodyReq;
import com.suning.park.admin.esb.TA04010350.dto.TA04010350Req;
import com.suning.park.admin.esb.TA04010350.dto.TA04010350Res;
import com.suning.park.admin.esb.TA04010360.MemberRollbackAccountService;
import com.suning.park.admin.esb.TA04010360.dto.TA04010360BodyReq;
import com.suning.park.admin.esb.TA04010360.dto.TA04010360Req;
import com.suning.park.admin.esb.TR01030020.CMFMemberInfoMgmtService;
import com.suning.park.admin.esb.TR01030020.dto.TR01030020Req;
import com.suning.park.admin.esb.TR01030020.dto.TR01030020Res;
import com.suning.park.admin.esb.TR03010952.OrderPointUseService;
import com.suning.park.admin.esb.TR03010952.dto.TR03010952BodyReq;
import com.suning.park.admin.esb.TR03010952.dto.TR03010952BodyRes;
import com.suning.park.admin.esb.TR03010952.dto.TR03010952Req;
import com.suning.park.admin.esb.TR03010952.dto.TR03010952Res;
import com.suning.park.admin.esb.TR04010100.PointAccountPointsMgmtService;
import com.suning.park.admin.esb.TR04010100.dto.TR04010100BodyReq;
import com.suning.park.admin.esb.TR04010100.dto.TR04010100Req;
import com.suning.park.admin.esb.TR04010100.dto.TR04010100Res;
import com.suning.park.admin.util.IdBuilder;
import com.suning.park.admin.util.JsonUtil;
import com.suning.rsc.RscException;
import com.suning.rsc.dto.responsedto.MbfResponse;
import com.suning.rsc.dto.responsedto.MbfServiceResponse;
import com.suning.rsc.httpservice.annotation.EsbEIHttpWired;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

/**
 * Created by Administrator on 2017/1/17.
 */

public class TestEsbService extends TestEnvironment {

    private final static Logger logger = LoggerFactory.getLogger(TestEsbService.class);

    private CMFMemberInfoMgmtService cmfMemberInfoMgmtService;

    private MemberAccountMgmtService memberAccountMgmtService;

    private PointAccountPointsMgmtService pointAccountPointsMgmtService;

    private MemberCardAccountMgmtService memberCardAccountMgmtService;

    private MemberRollbackAccountService memberRollbackAccountService;

    private OrderPointUseService orderPointUseService;

    //6054605209    5006分公司，8371门店   余额990.0    970 1487246038011830089
    //查询会员积分
    @Test
    public void testAccountBalance() {
        TR04010100Req req = new TR04010100Req("TR04010100");
        TR04010100BodyReq body = new TR04010100BodyReq();
        body.setCustNum("6054605209");
        body.setEcoType("140000000040");
        body.setStore("8371");
        body.setBranch("5006");
        req.setBody(body);
        try {
            MbfResponse mbfResponse =  pointAccountPointsMgmtService.queryPointsBalance(req);
            TR04010100Res res = (TR04010100Res) mbfResponse.getOutput().getMbfBody(TR04010100Res.class);
            logger.info("余额：{}",Double.parseDouble(res.getBody().getBasicValue())+Double.parseDouble(res.getBody().getSalesValue()));
        } catch (RscException e) {
            e.printStackTrace();
        }
    }

    //购物使用测试
    @Test
    public void testPointUseService(){
        String transId = IdBuilder.getID();
        TR03010952Req req = new TR03010952Req("TR03010952");
        TR03010952BodyReq body = new TR03010952BodyReq();
        body.setTransId(transId);
        body.setInvokerCode("PARK");
        body.setSceneCode("PE1120000560");
        body.setCustNum("6054605209");
        body.setEcoType("140000000040");

        TR03010952BodyReq.Order order = new TR03010952BodyReq.Order();
        order.setBranch("5006");
        order.setStore("8371");
        String orderItemId = System.currentTimeMillis() + ""+(int)((Math.random()*9+1)*100000);
        order.setOrderItemId(orderItemId);
        order.setOrderType("PARK001");//订单类型
        order.setOrderTypeDesc("苏宁广场停车费用支付");
        order.setSupplierType("209000000010");//209000000010苏宁自营 209000000020C店商户
        order.setSupplierCode("");
        TR03010952BodyReq.Subtract subtract = new TR03010952BodyReq.Subtract();
        subtract.setAccountType("8012");
        subtract.setSubtractAmt(new BigDecimal("10"));
        order.getSubtractItem().add(subtract);
        body.getOrderItem().add(order);
        req.setBody(body);
        try {
            MbfResponse mbfResponse =  orderPointUseService.pointUse(req);
            if(!mbfResponse.isSucceed()){
                MbfServiceResponse serviceResponse =  mbfResponse.getOutput().getMbfHeader().getServiceResponse();
                logger.info("购物使用结果:{}",serviceResponse.getDesc());
            }
            TR03010952Res res = (TR03010952Res) mbfResponse.getOutput().getMbfBody(TR03010952Res.class);
            TR03010952BodyRes.Order order1 = res.getBody().getOrderItem().get(0);
            logger.info("购物使用结果:{},{},{}",new Object[]{order1.getIsSucc(),order1.getOrderItemId(),order1.getErrorDesc()});
        } catch (RscException e) {
            e.printStackTrace();
        }

    }



    //退货账号交易
    @Test
    public void testMemberAccountMgmtService(){
//        String backId = IdBuilder.getID();
        // String orderItemId = System.currentTimeMillis() + ""+(int)((Math.random()*9+1)*100000);
        TA04010350Req req = new TA04010350Req("TA04010350");
        TA04010350BodyReq body = new TA04010350BodyReq();
        body.setCustNum("6054605209");
        body.setEcoType("140000000040");
        body.setOrderItemId("1487246038011830089");
        body.setBackOrderItemId("1432892617559");
        body.setStore("8371");
        body.setBranch("5006");
        body.setOperator("PARK");
        TA04010350BodyReq.AddStruct addStruct = new TA04010350BodyReq.AddStruct();
        addStruct.setAccountAddAmt(new BigDecimal("10"));
        addStruct.setAccountType("8012");
        body.getAddStructList().add(addStruct);
        req.setBody(body);
        try {
            MbfResponse mbfResponse =  memberAccountMgmtService.updateAccountResourceWhenReturnGoods(req);
            if(!mbfResponse.isSucceed()){
                MbfServiceResponse serviceResponse =  mbfResponse.getOutput().getMbfHeader().getServiceResponse();
                logger.info("退货账号交易结果:{}",serviceResponse.getDesc());
            }
            TA04010350Res res = (TA04010350Res) mbfResponse.getOutput().getMbfBody(TA04010350Res.class);
            logger.info("退货账号交易:{}",res.toString());
        } catch (RscException e) {
            e.printStackTrace();
        }

    }



    //账户回滚操作
    @Test
    public void testMemberAccountRollbackService(){
        TA04010360Req req = new TA04010360Req("TA04010360");
        TA04010360BodyReq body = new TA04010360BodyReq();
        body.setCustNum("6054605209");
        body.setEcoType("140000000040");
        body.setStore("8371");
        body.setBranch("5006");
        body.setOperator("PARK");
        body.setOrderType("PARK001");//订单类型
        body.setOrderItemId("1487228112521676052");//购物回滚就是购物的订单号  退货回滚，就是退货的订单号
        req.setBody(body);
        try {
            MbfResponse mbfResponse =   memberRollbackAccountService.rollbackAccount(req);
            if(!mbfResponse.isSucceed()){
                MbfServiceResponse serviceResponse =  mbfResponse.getOutput().getMbfHeader().getServiceResponse();
                logger.info("账户回滚结果:{}",serviceResponse.getDesc());
            }
        } catch (RscException e) {
            e.printStackTrace();
        }

    }

    //根据手机号查询会员编号
    @Test
    public void testQueryCustnumByPhone() {
        TR01030020Req req = new TR01030020Req("TR01030020");
        req.getBody().setAliasType("138000000010");//手机号类型
        req.getBody().setAliasName("");//手机号
        try {
            logger.info("查询会员类型请求报文:{}", JsonUtil.objectToJson(req));
            MbfResponse mbfResponse = cmfMemberInfoMgmtService.queryMemberType(req);
            TR01030020Res res = (TR01030020Res) mbfResponse.getOutput().getMbfBody(TR01030020Res.class);

            logger.info("查询会员类型请求结果:{}",res.toString());
            logger.info("会员编号：{}",res.getBody().getCustNum());
        } catch (RscException e) {
            e.printStackTrace();
        }
    }


    //查询会员类型
    @Test
    public void testCmfMemberInfoMgmtService() {
        TR01030020Req req = new TR01030020Req("TR01030020");
        req.getBody().setCustNum("6054604565");
        try {
            logger.info("查询会员类型请求报文:{}",JsonUtil.objectToJson(req));
            MbfResponse mbfResponse = cmfMemberInfoMgmtService.queryMemberType(req);
            TR01030020Res res = (TR01030020Res) mbfResponse.getOutput().getMbfBody(TR01030020Res.class);

            logger.info("查询会员类型请求结果:{}",res.toString());
            logger.info("会员类型：{}",res.getBody().getCustType());
        } catch (RscException e) {
            e.printStackTrace();
        }
    }


    //创建线下会员    6054612382(新的，没有线下账号，已经测试成功，会员卡号：100200130151)
    @Test
    public void testCreateMember(){
        String backId = IdBuilder.getID();
        TA0301020501Req req = new TA0301020501Req("TA0301020501");
        TA0301020501BodyReq body = new TA0301020501BodyReq();
        body.setCustNum("6054612382");
        body.setEcoType("140000000040");
        body.setBranch("5006");
        body.setStore("8371");
        req.setBody(body);
        try {
            MbfResponse mbfResponse =  memberCardAccountMgmtService.createMember(req);
            if(!mbfResponse.isSucceed()){
                MbfServiceResponse serviceResponse =  mbfResponse.getOutput().getMbfHeader().getServiceResponse();
                logger.info(serviceResponse.getDesc());
            }
            TA0301020501Res res = (TA0301020501Res) mbfResponse.getOutput().getMbfBody(TA0301020501Res.class);
            logger.info("创建线下会员:{}",res.toString());
        } catch (RscException e) {
            e.printStackTrace();
        }

    }


    public static Logger getLogger() {
        return logger;
    }

    public MemberAccountMgmtService getMemberAccountMgmtService() {
        return memberAccountMgmtService;
    }

    @EsbEIHttpWired
    public void setMemberAccountMgmtService(MemberAccountMgmtService memberAccountMgmtService) {
        this.memberAccountMgmtService = memberAccountMgmtService;
    }

    public CMFMemberInfoMgmtService getCmfMemberInfoMgmtService() {
        return cmfMemberInfoMgmtService;
    }

    @EsbEIHttpWired
    public void setCmfMemberInfoMgmtService(CMFMemberInfoMgmtService cmfMemberInfoMgmtService) {
        this.cmfMemberInfoMgmtService = cmfMemberInfoMgmtService;
    }

    public PointAccountPointsMgmtService getPointAccountPointsMgmtService() {
        return pointAccountPointsMgmtService;
    }

    @EsbEIHttpWired
    public void setPointAccountPointsMgmtService(PointAccountPointsMgmtService pointAccountPointsMgmtService) {
        this.pointAccountPointsMgmtService = pointAccountPointsMgmtService;
    }

    public MemberCardAccountMgmtService getMemberCardAccountMgmtService() {
        return memberCardAccountMgmtService;
    }

    @EsbEIHttpWired
    public void setMemberCardAccountMgmtService(MemberCardAccountMgmtService memberCardAccountMgmtService) {
        this.memberCardAccountMgmtService = memberCardAccountMgmtService;
    }

    public MemberRollbackAccountService getMemberRollbackAccountService() {
        return memberRollbackAccountService;
    }

    @EsbEIHttpWired
    public void setMemberRollbackAccountService(MemberRollbackAccountService memberRollbackAccountService) {
        this.memberRollbackAccountService = memberRollbackAccountService;
    }


    public OrderPointUseService getOrderPointUseService() {
        return orderPointUseService;
    }

    @EsbEIHttpWired
    public void setOrderPointUseService(OrderPointUseService orderPointUseService) {
        this.orderPointUseService = orderPointUseService;
    }
}
