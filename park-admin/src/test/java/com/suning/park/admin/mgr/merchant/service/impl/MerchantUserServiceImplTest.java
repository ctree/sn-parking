package com.suning.park.admin.mgr.merchant.service.impl;

import com.suning.base.TestEnvironment;
import com.suning.park.admin.mgr.merchant.service.IMerchantUserService;
import com.suning.park.admin.util.DateUtil;
import com.suning.park.admin.util.IdBuilder;
import com.suning.park.dto.dao.MerchantUserMapper;
import com.suning.park.dto.model.MerchantUser;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by YL-Zhang on 2017/2/17.
 */
public class MerchantUserServiceImplTest extends TestEnvironment {

    private final static Logger logger = LoggerFactory.getLogger(MerchantUserServiceImplTest.class);

    MerchantUser merchantUser;

    static final String id=IdBuilder.getID();

    @Autowired
    IMerchantUserService merchantUserService;

    @Autowired
    MerchantUserMapper merchantUserMapper;

    @Before
    public void before() {
        merchantUser = new MerchantUser();
        merchantUser.setId(id);
        merchantUser.setLogin("test");
        merchantUser.setPassword("123");
        merchantUser.setUserName("单元测试用户");
        merchantUser.setState("0");
        merchantUser.setCreateId("1233131");
        merchantUser.setCreateName("单元测试");
        merchantUser.setCreateTime(DateUtil.now());
        merchantUser.setLastTime(DateUtil.now());
        merchantUser.setMerchantId("123131");
        merchantUser.setOperatorId("ceshi1211");
        merchantUser.setOperatorName("单元测试1");
    }

    @Test
    public void add() {
        logger.debug(merchantUser.toString());
        MerchantUser merchantUserTest= new MerchantUser();
        BeanUtils.copyProperties(merchantUser,merchantUserTest);

        int result = merchantUserService.add(merchantUserTest);
        Assert.assertEquals("添加用户错误",result,1);
    }

    @Test
    public void update(){
        MerchantUser merchantUserTest= new MerchantUser();
        merchantUserTest.setId(merchantUser.getId());
        String state="1";
        merchantUserTest.setState(state);
        int result= merchantUserService.update(merchantUserTest);
        Assert.assertEquals("修改用户错误",result,1);
        MerchantUser merchantUserNew=merchantUserMapper.selectByPrimaryKey(merchantUserTest.getId());
        Assert.assertEquals("修改用户错误-1",state,merchantUserNew.getState());
    }
    @Test
    public void  modifyPassword(){
        MerchantUser merchantUserTest= new MerchantUser();
        BeanUtils.copyProperties(merchantUser,merchantUserTest);
        String pwd="321";
        String  result = merchantUserService.modifyPassword(merchantUserTest.getId(),merchantUserTest.getPassword(),pwd);
        Assert.assertEquals("修改密码失败",result,"success");

    }


}
