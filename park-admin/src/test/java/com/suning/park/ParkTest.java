package com.suning.park;

import com.suning.base.TestEnvironment;
import com.suning.park.dto.model.Park;
import com.suning.park.dto.model.ParkIntervalFeeRule;
import com.suning.park.admin.mgr.park.service.IParkService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * 停车场测试
 * Created by Administrator on 2017/2/10.
 */
public class ParkTest extends TestEnvironment {

    @Autowired
    private IParkService parkService;

    @Test
    public void insert(){
        Park park = new Park();
        park.setParkName("测试停车场");
        List<ParkIntervalFeeRule> parkIntervalFeeRules = new ArrayList<>();
        parkService.insert(park, parkIntervalFeeRules, "test", "test");
    }
}
