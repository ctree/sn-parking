package com.suning.epps;

import com.suning.base.TestEnvironment;
import com.suning.park.admin.bean.Properties;
import com.suning.park.admin.epps.service.IEppsService;
import com.suning.park.admin.util.DateUtil;
import com.suning.park.admin.util.IdBuilder;
import com.suning.park.admin.util.JsonUtil;
import org.apache.commons.collections.MapUtils;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Map;

/**
 * Created by Administrator on 2017/1/13.
 */
public class EppsServiceTest extends TestEnvironment {

    private final static Logger logger = LoggerFactory.getLogger(EppsServiceTest.class);

    @Autowired
    IEppsService eppsService;
    @Autowired
    Properties properties;


    /**
     * 支付请求
     */
    @Test
    public void payment(){
        String orderId = String.valueOf(System.currentTimeMillis());
        String goodsName = "停车费支付";
        long amount = (long) (0.1 * 100);
        String orderAmount = String.valueOf(amount);
        String orderTime = DateUtil.formatDatetimeForYmdhms(new Date());
        logger.info("订单ID:{},goodsName:{},orderAmount:{},orderTime:{}",new Object[]{orderId,goodsName,orderAmount,orderTime});
        String result = eppsService.testpayment(properties.getEppsMerchantNo(),orderId,goodsName,orderAmount,orderTime);
        logger.info(result);
    }
    //订单ID:1484878202783,goodsName:停车费支付,orderAmount:10,orderTime:20170120101002

    @Test
    public void testSelectOrder() throws UnsupportedEncodingException {
        String orderId = "1487142197099";
        String orderTime = "20170215150317";
        String result   = eppsService.selectOrder(properties.getEppsMerchantNo(),orderId,orderTime);

        //{"responseMsg":"成功","orderStatus":"S","remark":null,"outOrderNo":"1484878202783","payResult":null,"orderAmount":"10","merchantOrderNo":null,"orderId":"1701200000000067200","responseCode":"0000","payDescription":"易付宝","payTime":"20170120101259","payDetails":"[{\"payAmount\":\"10\",\"payTypeCode\":\"EPP_BALANCE\",\"rcsCode\":\"EPP_EPP_BALANCE_EPP\"}]","orderTime":"20170120101002","totalPayAmount":null,"buyerUserNo":"0000000001114458736"}
        logger.info(result);
        Map map = JsonUtil.json2Map(result);
        logger.info("响应状态:{}", MapUtils.getString(map,"responseCode"));
        logger.info("易付宝订单ID:{}", MapUtils.getString(map,"orderId"));
        logger.info("易付宝订单状态:{}", MapUtils.getString(map,"orderStatus"));//订单状态：支付成功"S"，订单关闭"C"，新建"N"
        logger.info("订单支付成功时间:{}", MapUtils.getString(map,"payTime"));
        logger.info("订单创建时间:{}", MapUtils.getString(map,"orderTime"));
        logger.info("买家易付宝会员编号:{}", MapUtils.getString(map,"buyerUserNo"));
    }


    @Test
    public void testRefundOrder() throws UnsupportedEncodingException {
        String orderId = "1487142197099";
        String orderTime = "20170215150317";
        String refundId = IdBuilder.getID();
        String refundOrderTime = DateUtil.formatDatetimeForYmdhms(new Date());
        eppsService.refundOrder(properties.getEppsMerchantNo(),refundId,refundOrderTime,"10",orderId,orderTime);
        //请求返回结果：{"responseMsg":"受理成功","responseCode":"0000"}
    }

}
