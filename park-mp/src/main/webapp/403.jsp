<%@ page contentType="text/html;charset=UTF-8" %>
<html><!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"><meta name="renderer" content="webkit">

    <title>403错误</title>
<link rel="icon" href="${pageContext.request.contextPath}/hplus/img/tjd.ico" type="image/x-icon" />
<link rel="shortcut icon" href="${pageContext.request.contextPath}/hplus/img/tjd.ico" type="image/x-icon" />
    <link  href="${pageContext.request.contextPath}/hplus/css/bootstrap.min.css?v=3.4.0" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/hplus/font-awesome/css/font-awesome.css?v=4.3.0" rel="stylesheet">

    <link href="${pageContext.request.contextPath}/hplus/css/animate.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/hplus/css/style.css?v=2.2.0" rel="stylesheet">

</head>

<body class="gray-bg">


    <div class="middle-box text-center animated fadeInDown">
        <h1>403</h1>
        <h3 class="font-bold"></h3>

        <div class="error-desc">
     		抱歉！您没有权限访问该功能
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="${pageContext.request.contextPath}/hplus/js/jquery-2.1.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/hplus/js/bootstrap.min.js?v=3.4.0"></script>


</body>

</html>
