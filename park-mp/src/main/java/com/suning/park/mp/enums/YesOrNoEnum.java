package com.suning.park.mp.enums;

import com.suning.park.mp.util.StringUtil;

public enum YesOrNoEnum {

	YES("0", "是"), NO("1", "否");
	private String value;

	private String text;

	/**
	 * 
	 * @param value
	 * @param text
	 */
	YesOrNoEnum(final String value, final String text) {
		this.value = value;
		this.text = text;
	}

	public String getValue() {
		return value;
	}

	public String getText() {
		return text;
	}

	/**
	 * 
	 * @param oridal
	 * @return
	 */
	public static YesOrNoEnum get(int oridal) {
		for (YesOrNoEnum dot : YesOrNoEnum.values()) {
			if (oridal == dot.ordinal()) {
				return dot;
			}
		}
		throw new IllegalArgumentException("Can't get enum with this oridal.");
	}

	/**
	 * 根据value获取text
	 * 
	 * @param value
	 * @return
	 */
	public static String getText(String value) {
		for (YesOrNoEnum dot : YesOrNoEnum.values()) {
			if (value.equals(dot.getValue())) {
				return dot.getText();
			}
		}
		return "";
	}

	/**
	 * 检测value是否是当前枚举的类型之一,区分大小写
	 * 
	 * @param value
	 * @return
	 */
	public static boolean isValue(String value) {
		if (StringUtil.isBlank(value)) {
			return false;
		}
		for (YesOrNoEnum dot : YesOrNoEnum.values()) {
			if (value.equals(dot.getValue())) {
				return true;
			}
		}
		return false;
	}

}
