package com.suning.park.mp.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 商户账户管理
 * Created by YL-Zhang on 2017/2/15.
 */
@Controller
@RequestMapping(value = "/user", produces = {"application/json;charset=UTF-8"})
public class AccountController {

    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);


    @ResponseBody
    @RequestMapping(value = "/add/{userId}", method = RequestMethod.POST)
    public Object balance(HttpServletRequest request, @PathVariable("userId") String userId) {

        return null;
    }



}
