package com.suning.park.mp.controller;

import com.suning.park.dto.model.MerchantUser;
import com.suning.park.mp.bean.Response;
import com.suning.park.mp.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * 操作用户
 * Created by YL-Zhang on 2017/2/15.
 */

@Controller
@RequestMapping(value = "/user", produces = {"application/json;charset=UTF-8"})
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    UserService userService;

    /**
     * 用户登录验证接口
     * @param request
     * @param userName
     * @param password
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/login/{userName}/{password}", method = RequestMethod.POST)
    public Object login(HttpServletRequest request, @PathVariable("userName") String userName,
                        @PathVariable("password") String password) {
        MerchantUser merchantUser=userService.find(userName, password);
        if(merchantUser!=null){
            request.getSession().setAttribute("MerchantUser",merchantUser);
            return Response.SUCCESS();
        }else{
            return Response.FAIL("用户名或密码不正确").toJson();
        }
    }

    /**
     * 修改密码接口
     * @param request
     * @param userId 用户ID
     * @param oldPassword 旧密码
     * @param newPassword 新密码
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/modify/{userId}/{oldPassword}/{newPassword}", method = RequestMethod.POST)
    public Object modifyPassword(HttpServletRequest request, @PathVariable("userId") String userId,
                                 @PathVariable("oldPassword") String oldPassword,
                                 @PathVariable("newPassword") String newPassword) {
        String message=userService.modifyPassword(userId, oldPassword, newPassword);
        if("success".equals(message)){
            return Response.SUCCESS();
        }else {
            return Response.FAIL(message).toJson();
        }
    }


}
