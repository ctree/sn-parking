package com.suning.park.mp.controller;

import com.suning.park.dto.model.Coupon;
import com.suning.park.dto.model.CouponUser;
import com.suning.park.dto.model.MerchantUser;
import com.suning.park.mp.bean.Response;
import com.suning.park.mp.services.CouponService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 优惠券操作
 * Created by YL-Zhang on 2017/2/15.
 */
@Controller
@RequestMapping(value = "/coupon", produces = {"application/json;charset=UTF-8"})
public class CouponController {

    private static final Logger logger = LoggerFactory.getLogger(CouponController.class);

    @Autowired
    CouponService couponService;

    @ResponseBody
    @RequestMapping(value = "/getList", method = RequestMethod.POST)
    public Object getList(HttpServletRequest request) {
        MerchantUser merchantUser = (MerchantUser) request.getSession().getAttribute("MerchantUser");
        if (merchantUser == null) {
            return Response.SUCCESS().FAIL("请先登录");
        } else {
            List<Coupon> list = couponService.getList(merchantUser.getId());
            return Response.SUCCESS().put("couponList", list);
        }
    }

    @ResponseBody
    @RequestMapping(value = "/add/{amount}/{day}", method = RequestMethod.POST)
    public Object add(HttpServletRequest request, @PathVariable("amount") BigDecimal amount, @PathVariable("day") int day) {
        MerchantUser merchantUser = (MerchantUser) request.getSession().getAttribute("MerchantUser");
        if (merchantUser == null) {
            return Response.SUCCESS().FAIL("请先登录");
        }
        String result = couponService.add(merchantUser.getId(), amount, day);
        if ("success".equals(result)) {
            return Response.SUCCESS();
        } else {
            return Response.FAIL("添加优惠券失败");
        }
    }

    @ResponseBody
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    public Object delete(HttpServletRequest request, @PathVariable("id") String id) {
        String result = couponService.delete(id);
        if ("success".equals(result)) {
            return Response.SUCCESS();
        } else {
            return Response.FAIL("删除优惠券失败");
        }
    }

    @ResponseBody
    @RequestMapping(value = "/grantList", method = RequestMethod.POST)
    public Object grantList(HttpServletRequest request, @RequestParam BigDecimal amount,
                            @RequestParam String userInfo, @RequestParam Date grantDateStart, @RequestParam Date grantDateEnd,
                            @RequestParam int limitStart, @RequestParam int limitRows) {
        if (limitStart < 0) {
            limitStart = 0;
        }
        if (limitRows < 0) {
            limitStart = 20;
        }
        List<CouponUser> couponUserList = couponService.findGrantCouponList(amount, userInfo, grantDateStart, grantDateEnd, limitStart, limitRows);
        return Response.SUCCESS().put("couponUserList", couponUserList);
    }


}
