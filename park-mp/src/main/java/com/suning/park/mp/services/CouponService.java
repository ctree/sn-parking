package com.suning.park.mp.services;

import com.suning.park.admin.provider.ICouponProvider;
import com.suning.park.admin.provider.IParkProvider;
import com.suning.park.dto.model.Coupon;
import com.suning.park.dto.model.CouponUser;
import com.suning.rsf.spring.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by YL-Zhang on 2017/2/17.
 */
@Service
public class CouponService {

    private static final Logger logger = LoggerFactory.getLogger(CouponService.class);

    @Reference(contract = ICouponProvider.class)
    ICouponProvider couponProvider;

    /**
     * 商户用户添加优惠券
     *
     * @param userId 商户用户ID
     * @param amount 优惠券金额
     * @param day    优惠券有效期天数
     * @return 成功返回“success”,错误返回原因
     */
    public String add(String userId, BigDecimal amount, int day) {
        return couponProvider.add(userId, amount, day);
    }

    /**
     * 删除优惠券，把优惠券置为删除状态
     *
     * @param id
     * @return
     */
    public String delete(String id) {
        return couponProvider.delete(id);
    }

    /**
     * 查找商户可以发放的优惠券
     *
     * @param userId 用户ID
     * @return 查询成功返回可以用优惠券列表，否则返回NULL或list.size=0
     */
    public List<Coupon> getList(String userId) {
        return couponProvider.getList(userId);
    }

    /**
     * 商户发放优惠券给易购用户
     *
     * @param couponId
     * @param userId
     * @return
     */
    public String grantCoupon(String couponId, String userId) {
        return couponProvider.grantCoupon(couponId, userId);
    }

    /**
     * 查找商户一级发放的优惠券
     *
     * @param amount         优惠券金额
     * @param userInfo       发放用户信息
     * @param grantDateStart 发放时间起点
     * @param grantDateEnd   发放时间终点
     * @param limitStart     分页起点
     * @param limitRows      每页行数
     * @return List
     */
    public List<CouponUser> findGrantCouponList(BigDecimal amount, String userInfo,
                                                Date grantDateStart, Date grantDateEnd, int limitStart, int limitRows) {
        return couponProvider.findGrantCouponList(amount, userInfo, grantDateStart, grantDateEnd, limitStart, limitRows);
    }


}
