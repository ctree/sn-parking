package com.suning.park.mp.exception;

public class BusinessFailException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BusinessFailException(String message) {
		super(message);
	}
}
