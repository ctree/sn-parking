package com.suning.park.mp.enums;


import com.suning.park.mp.util.StringUtil;

/**
 * 功能启用/禁用
 */
public enum SysUserTypeEnum {

	SYS_USER("0", "系统用户"),
	MERCHANT("1", "商户");
	private String value;

	private String text;

	/**
	 *
	 * @param value
	 * @param text
	 */
	SysUserTypeEnum(final String value, final String text) {
		this.value = value;
		this.text = text;
	}

	public String getValue() {
		return value;
	}

	public String getText() {
		return text;
	}

	/**
	 * 
	 * @param oridal
	 * @return
	 */
	public static SysUserTypeEnum get(int oridal) {
		for (SysUserTypeEnum dot : SysUserTypeEnum.values()) {
			if (oridal == dot.ordinal()) {
				return dot;
			}
		}
		throw new IllegalArgumentException("Can't get enum with this oridal.");
	}

	/**
	 * 根据value获取text
	 * 
	 * @param value
	 * @return
	 */
	public static String getText(String value) {
		for (SysUserTypeEnum dot : SysUserTypeEnum.values()) {
			if (value.equals(dot.getValue())) {
				return dot.getText();
			}
		}
		return "";
	}

	/**
	 * 检测value是否是当前枚举的类型之一,区分大小写
	 * 
	 * @param value
	 * @return
	 */
	public static boolean isValue(String value) {
		if (StringUtil.hasEmpty(value))
			return false;
		for (SysUserTypeEnum dot : SysUserTypeEnum.values()) {
			if (value.equals(dot.getValue())) {
				return true;
			}
		}
		return false;
	}

}
