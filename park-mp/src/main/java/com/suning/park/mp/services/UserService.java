package com.suning.park.mp.services;

import com.suning.park.admin.provider.IMerchantUserProvider;
import com.suning.park.dto.model.MerchantUser;
import com.suning.rsf.spring.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Created by YL-Zhang on 2017/2/16.
 */
@Service
public class UserService {
    private static final Logger logger = LoggerFactory.getLogger(UserService.class);


    @Reference(contract= IMerchantUserProvider.class)
    IMerchantUserProvider merchantUserProvider;

    /**
     * 根据用户名和密码查找
     *
     * @param login    登录用户名
     * @param password 密码
     * @return
     */
    public MerchantUser find(String login, String password) {
        return merchantUserProvider.find(login, password);
    }

    /**
     * 修改密码
     *
     * @param userId 用户ID
     * @param oldPwd 旧密码
     * @param newPwd 新密码
     * @return
     */
    public String modifyPassword(String userId, String oldPwd, String newPwd) {
        logger.info("远程调用接收参数 userId:{} oldPwd:{} newPwd:{}", new Object[]{userId, oldPwd, newPwd});
        return merchantUserProvider.modifyPassword(userId, oldPwd, newPwd);
    }

}
