package com.suning.park.mp.enums;


import com.suning.park.mp.util.StringUtil;

/**
 * 收费订单状态
 */
public enum ParkOrderTypeEnum {

	UNPAY("0", "未支付"),
	PAYING("1", "支付中"),
	PAYMENTS_SUCCESS("2", "支付成功"),
	PAYMENTS_FAILURES("3", "支付失败"),
	REFUNDING("4", "退款中"),
	REFUND_FAILED("5", "退款失败"),
	REFUND_SUCCESS("6", "退款成功");

	private String value;

	private String text;

	/**
	 *
	 * @param value
	 * @param text
	 */
	ParkOrderTypeEnum(final String value, final String text) {
		this.value = value;
		this.text = text;
	}

	public String getValue() {
		return value;
	}

	public String getText() {
		return text;
	}

	/**
	 * 
	 * @param oridal
	 * @return
	 */
	public static ParkOrderTypeEnum get(int oridal) {
		for (ParkOrderTypeEnum dot : ParkOrderTypeEnum.values()) {
			if (oridal == dot.ordinal()) {
				return dot;
			}
		}
		throw new IllegalArgumentException("Can't get enum with this oridal.");
	}

	/**
	 * 根据value获取text
	 * 
	 * @param value
	 * @return
	 */
	public static String getText(String value) {
		for (ParkOrderTypeEnum dot : ParkOrderTypeEnum.values()) {
			if (value.equals(dot.getValue())) {
				return dot.getText();
			}
		}
		return "";
	}

	/**
	 * 检测value是否是当前枚举的类型之一,区分大小写
	 * 
	 * @param value
	 * @return
	 */
	public static boolean isValue(String value) {
		if (StringUtil.hasEmpty(value))
			return false;
		for (ParkOrderTypeEnum dot : ParkOrderTypeEnum.values()) {
			if (value.equals(dot.getValue())) {
				return true;
			}
		}
		return false;
	}

}
