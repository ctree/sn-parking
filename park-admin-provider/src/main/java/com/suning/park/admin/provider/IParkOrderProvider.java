package com.suning.park.admin.provider;

import com.suning.park.dto.model.ParkOrder;
import com.suning.rsf.provider.annotation.Contract;
import com.suning.rsf.provider.annotation.Method;

import java.util.List;

/**
 * 停车支付订单服务接口
 * Created by Administrator on 2017/2/14.
 */
@Contract
public interface IParkOrderProvider {

    /**
     * 查询停车支付订单信息
     *
     * @param snUserId  苏宁用户ID
     * @param limitType 限制类型(0=7天，1=一个月，2=三个月)
     * @param pageNum   当前页码
     * @param pageSize  记录数量
     * @return 停车支付订单信息列表
     */
    @Method
    List<ParkOrder> selectByUserId(String snUserId, String limitType, Integer pageNum, Integer pageSize);

    /**
     * 根据车牌号获得未支付的停车支付订单信息
     * @param snUserId  苏宁用户ID
     * @param carNum 车牌号
     * @return 停车支付订单信息
     */
    @Method
    ParkOrder selectByNoPayAndCarNum(String snUserId, String carNum);
}
