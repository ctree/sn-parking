package com.suning.park.admin.provider;

import com.suning.park.dto.model.MerchantUser;
import com.suning.rsf.provider.annotation.Contract;
import com.suning.rsf.provider.annotation.Method;

/**
 * 商户账户操作
 * Created by YL-Zhang on 2017/2/16.
 */
@Contract
public interface IMerchantUserProvider {
    /**
     * 根据用户名和密码查找
     *
     * @param login    登录用户名
     * @param password 密码
     * @return
     */
    @Method
    MerchantUser find(String login, String password);

    /**
     * 修改用户密码
     *
     * @param userId 用户ID
     * @param oldPwd 旧密码
     * @param newPwd 新密码
     * @return 成功返回“success” ，失败返回错误提示
     */
    @Method
    String modifyPassword(String userId, String oldPwd, String newPwd);
}
