package com.suning.park.admin.provider;

import com.suning.park.dto.model.Park;
import com.suning.rsf.provider.annotation.Contract;
import com.suning.rsf.provider.annotation.Method;

import java.util.List;
import java.util.Map;

/**
 * 停车场服务接口
 * Created by Administrator on 2017/2/14.
 */
@Contract
public interface IParkProvider {

    /**
     * 根据地区查询停车场信息列表
     *
     * @param provinceCode 省份编码
     * @param cityCode     城市编码
     * @return 停车场信息列表
     */
    @Method
    List<Park> selectByRegion(String provinceCode, String cityCode);

    /**
     * 根据停车场ID获得收费规则信息
     *
     * @param parkId 停车场ID
     * @return park:Park 停车场信息, feeRules:List<ParkIntervalFeeRule> 停车场按时段计费规则信息
     */
    @Method
    Map<String, Object> getFeeRuleByParkId(String parkId);
}
