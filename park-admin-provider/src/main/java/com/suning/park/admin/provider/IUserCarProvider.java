package com.suning.park.admin.provider;


import com.suning.park.dto.model.UserCar;
import com.suning.rsf.provider.annotation.Contract;
import com.suning.rsf.provider.annotation.Method;

import java.util.List;

/**
 * 用户车辆关系服务接口
 * Created by Administrator on 2017/2/14.
 */
@Contract
public interface IUserCarProvider {

    /**
     * 根据苏宁用户ID获得用户车辆关系信息列表
     *
     * @param snUserId 苏宁用户ID
     * @return 用户车辆关系信息列表
     */
    @Method
    List<UserCar> selectBySnUserId(String snUserId);

    /**
     * 新增用户车辆关系信息
     *
     * @param userCar 用户车辆关系信息
     */
    @Method
    void insertUserCar(UserCar userCar);

    /**
     * 删除用户车辆关系信息
     *
     * @param id 用户车辆关系信息ID
     */
    @Method
    void delete(String id);
}
