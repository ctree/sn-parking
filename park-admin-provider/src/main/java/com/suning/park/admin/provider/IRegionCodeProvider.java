package com.suning.park.admin.provider;


import com.suning.park.dto.model.DDRegionCode;
import com.suning.park.dto.model.UserCar;
import com.suning.rsf.provider.annotation.Contract;
import com.suning.rsf.provider.annotation.Method;

import java.util.List;

/**
 * 地区编码服务接口
 * Created by Administrator on 2017/2/16.
 */
@Contract
public interface IRegionCodeProvider {

    /**
     * 获得省份列表
     * @return
     */
    @Method
    List<DDRegionCode> getProvinceList();

    /**
     * 根据级别编码获得城市列表
     * @param levelCode
     * @return
     */
    @Method
    List<DDRegionCode> getCityListByLevelCode(String levelCode);
}
