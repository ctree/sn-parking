package com.suning.park.admin.provider;

import com.suning.park.dto.model.Coupon;
import com.suning.park.dto.model.CouponUser;
import com.suning.park.dto.model.UserCar;
import com.suning.rsf.provider.annotation.Contract;
import com.suning.rsf.provider.annotation.Method;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 优惠券服务接口
 * Created by Administrator on 2017/2/14.
 */
@Contract
public interface ICouponProvider {

    /**
     * 根据停车场ID获得优惠券信息列表
     *
     * @param parkId 停车场ID
     * @param snUserId 苏宁用户ID
     * @param getType 领取类型(0=已领取, 1=未领取)
     * @return 优惠券信息列表
     */
    @Method
    List<Coupon> getCouponList(String parkId, String snUserId, String getType);

    /**
     * 关联优惠券
     * @param snUserId 苏宁用户ID
     * @param couponId 优惠券ID
     */
    @Method
    void provideCoupon(String snUserId,String couponId) throws Exception;

    /**
     * 商户用户添加优惠券
     *
     * @param userId 商户用户ID
     * @param amount 优惠券金额
     * @param day    优惠券有效期天数
     * @return 成功返回“success”,错误返回原因
     */
    @Method
    String add(String userId, BigDecimal amount, int day);

    /**
     * 删除优惠券，把优惠券置为删除状态
     *
     * @param id
     * @return
     */
    @Method
    String delete(String id);

    /**
     * 查找商户可以发放的优惠券
     *
     * @param userId 用户ID
     * @return 查询成功返回可以用优惠券列表，否则返回NULL或list.size=0
     */
    @Method
    List<Coupon> getList(String userId);

    /**
     * 商户发放优惠券给易购用户
     *
     * @param couponId
     * @param userId
     * @return
     */
    @Method
    String grantCoupon(String couponId, String userId);

    /**
     * 查找商户一级发放的优惠券
     * @param amount 优惠券金额
     * @param userInfo 发放用户信息
     * @param grantDateStart 发放时间起点
     * @param grantDateEnd 发放时间终点
     * @param limitStart 分页起点
     * @param limitRows 每页行数
     * @return List
     */
    @Method
    List<CouponUser> findGrantCouponList(BigDecimal amount, String userInfo, Date grantDateStart, Date grantDateEnd, int limitStart, int limitRows);
}
